﻿
namespace Serene2.Assessment.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Assessment.WellBeing")]
    [BasedOnRow(typeof(Entities.WellBeingRow))]
    public class WellBeingForm
    {

        [DisplayName("Weight (kg)"), Required(true)]
        public Double WeightKg { get; set; }

        [DisplayName("Athlete"), Visible(false)]
        public Int32 AthleteId { get; set; }

        [DisplayName("Date"), Required(true)]
        public DateTime dateof { get; set; }

        [DisplayName("Injury Status"), Width(40), Required(true)]
        public Int32 InjStatus { get; set; }
        
        [DisplayName("Disturbed Sleep"), Required(true)]
        public Int32 Sleep { get; set; }
        [DisplayName("Change To Your Daily Routine In The Past 24 Hours"), Width(40), Required(true)]
        public Int32 ChangeDailyRoutine { get; set; }
        [DisplayName("Have you sought any additional medical attention since your last S&C Session?"), Required(true)]
        public Int32 SoughtMedicalAttention { get; set; }

        [DisplayName("Tightness or Pain"), Required(true)]
        public Int32 TightnessPain { get; set; }

        [DisplayName("Academic Stress"), Required(true)]
        public Int32 AcademicStress { get; set; }
        
       
    }
}