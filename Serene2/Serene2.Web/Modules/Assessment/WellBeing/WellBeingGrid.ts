﻿

namespace Serene2.Assessment {

    @Serenity.Decorators.registerClass()
    export class WellBeingGrid extends Serenity.EntityGrid<WellBeingRow, any> {
        protected getColumnsKey() { return 'Assessment.WellBeing'; }
        protected getDialogType() { return WellBeingDialog; }
        protected getIdProperty() { return WellBeingRow.idProperty; }
        protected getLocalTextPrefix() { return WellBeingRow.localTextPrefix; }
        protected getService() { return WellBeingService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }


        protected createQuickSearchInput(): void {
        }
        
        protected getButtons() {
            var buttons = super.getButtons();

            buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Assessment/WellBeing/ListExcel',
                separator: true
            }));

            buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            return buttons;
        }


        private _AthleteId: number;

        get AthleteId() {
            return this._AthleteId;
        }

        set AthleteId(value: number) {
            if (this._AthleteId !== value) {
                this._AthleteId = value;
                this.setEquality('AthleteId', value);
                this.refresh();
            }
        }


        protected addButtonClick() {

            var user = Serene2.Authorization.userDefinition;
            
            this.editItem(<Assessment.WellBeingRow>{
                dateof: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                
                AthleteId: Athlete.AthleteRow.getLookup().items
                    .filter(x => x.Username === user.Username)[0].Id,
                InjStatus: 1,
                Sleep: 2, // NO
                ChangeDailyRoutine: 2,
                SoughtMedicalAttention: 2,
                TightnessPain: 2,
                AcademicStress: 2
            });

          


        }

        



    }
}