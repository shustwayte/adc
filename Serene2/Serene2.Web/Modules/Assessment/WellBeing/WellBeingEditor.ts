﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Assessment {
    
    @Serenity.Decorators.registerClass()
    export class WellBeingEditor extends Common.GridEditorBase<WellBeingRow> {
        protected getColumnsKey() { return 'Assessment.WellBeing'; }
        protected getDialogType() { return WellBeingEditorDialog; }
                protected getLocalTextPrefix() { return WellBeingRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}