﻿namespace Serene2.Assessment {

    @Serenity.Decorators.registerClass()
    export class WellBeingGridAthlete extends Serenity.EntityGrid<WellBeingRow, any>
    {
        protected getColumnsKey() { return 'Assessment.WellBeing'; }
        protected getDialogType() { return WellBeingDialog; }
        protected getIdProperty() { return WellBeingRow.idProperty; }
        protected getLocalTextPrefix() { return WellBeingRow.localTextPrefix; }
        protected getService() { return WellBeingService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }




        protected getButtons() {
            var buttons = super.getButtons();

            buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Assessment/WellBeing/ListExcel',
                separator: true
            }));

            buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
        }


        private _AthleteId: number;

        get AthleteId() {
            return this._AthleteId;
        }

        set AthleteId(value: number) {
            if (this._AthleteId !== value) {
                this._AthleteId = value;
                this.setEquality('AthleteId', value);
                this.refresh();
            }
        }


        protected addButtonClick() {

            var user = Serene2.Authorization.userDefinition.Username;


            this.editItem(<Assessment.WellBeingRow>
                {
                    dateof: Q.formatDate(new Date(), 'yyyy-MM-dd'),

                    AthleteId: Athlete.AthleteRow.getLookup().items
                        .filter(x => x.Username === user)[0].Id,

                    InjStatus: 1,
                    Sleep: 2, // NO
                    ChangeDailyRoutine: 2,
                    SoughtMedicalAttention: 2,
                    TightnessPain: 2,
                    AcademicStress: 2



                });
        }

        protected getQuickFilters(): Serenity.QuickFilter<Serenity.Widget<any>, any>[] {

            // get quick filter list from base class
            let filters = super.getQuickFilters();
            
            // get a reference to order row field names
            let fld = Assessment.WellBeingRow.Fields;
            //  remove filters
            var myStringArray = filters;
            
            var arrayLength = myStringArray.length ;
           
            myStringArray.splice(2, arrayLength);
            

            return myStringArray;
        }

        protected createQuickFilters(): void {

            super.createQuickFilters();
        }


    }
}


