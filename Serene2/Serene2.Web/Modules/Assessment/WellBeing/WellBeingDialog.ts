﻿
namespace Serene2.Assessment {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class WellBeingDialog extends Serenity.EntityDialog<WellBeingRow, any> {
        protected getFormKey() { return WellBeingForm.formKey; }
        protected getIdProperty() { return WellBeingRow.idProperty; }
        protected getLocalTextPrefix() { return WellBeingRow.localTextPrefix; }
        protected getService() { return WellBeingService.baseUrl; }

        protected form = new WellBeingForm(this.idPrefix);

        private assessmentGridForm: Serenity.PropertyGrid;

        private audit: Audit.AuditLogGrid;

        constructor() {
            super();
            

            this.audit = new Audit.AuditLogGrid(this.byId('ActionAuditGrid'));
            this.audit.element.flexHeightOnly(1);

            this.tabs.bind('tabsactivate', () => this.arrange());

            this.tabs.on('tabsactivate', (e, i) => {
                this.arrange();
            });

        }


        loadEntity(entity: Assessment.WellBeingRow) {

            super.loadEntity(entity);

            var user = Serene2.Authorization.userDefinition;

            if (user.isAthlete == true) {

                this.form.dateof.element.toggleClass('disabled', true);
                this.form.dateof.element.toggleClass('readOnly', true);
                this.form.dateof.element.toggleClass('isDisabled', true);

                Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);

                //this.deleteButton.hide();
                this.deleteButton.toggleClass('disabled', true);

            }
            var athid = this.entity.Id;

            this.audit.rowid = athid;

            this.audit.TableName = "[dbo].[wellBeing]";
/*
            if (!athid) {
                // just load an empty entity
                this.assessmentGridForm.load({});
                return;
            }
*/

            // load  record
            if (athid) {
                Assessment.WellBeingService.Retrieve({
                    EntityId: athid
                }, response => {
                    this.assessmentGridForm.load(response.Entity);
                });
            }
            

        }
        
        onSaveSuccess(response) {

            super.onSaveSuccess(response);

            var currenturl = window.location.href;

            if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                var user = Serene2.Authorization.userDefinition;
                if (user.isAthlete == true) {
                    location.href = "/";
                   // location.reload();
                }
            }
        }
        


        protected updateInterface(): void {

            super.updateInterface();

            var user = Serene2.Authorization.userDefinition;

            var currenturl = window.location.href;

            if (currenturl.indexOf("Assessment/WellBeing") > 0) {

                if (user.isAthlete == true) {

                    this.form.dateof.element.toggleClass('disabled', true);
                    this.form.dateof.element.toggleClass('readOnly', true);
                    this.form.dateof.element.toggleClass('isDisabled', true);

                    Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);

                    //this.deleteButton.hide();
                    this.deleteButton.toggleClass('disabled', true);
                }

            }

            if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                
                if (user.isAthlete == true) {

                    if (user.AthleteAlreadyLoggedAss == "0" || user.AthleteAlreadyLoggedAss == "") {

                        this.form.dateof.element.toggleClass('disabled', true);
                        this.form.dateof.element.toggleClass('readOnly', true);
                        this.form.dateof.element.toggleClass('isDisabled', true);

                        Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);

                        //this.deleteButton.hide();
                        this.deleteButton.toggleClass('disabled', true);


                    }

                    if (user.AthleteAlreadyLoggedAss != "0" && user.AthleteAlreadyLoggedAss != "") {

                        Q.information("Today's assessment already logged. Enjoy your workout!!", () => { });

                        this.onDialogClose();

                        

                    }
                }

            }

            // finding all editor elements and setting their readonly attribute
            // note that this helper method only works with basic inputs, 
            // some editors require widget based set readonly overload (setReadOnly)
          
        }

    }
}