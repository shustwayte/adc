﻿
namespace Serene2.Assessment.Endpoints
{
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;


    using Serenity.Reporting;
    using Serenity.Web;
    using System;


    using System.Data;
    using System.Web.Mvc;
    using MyRepository = Repositories.WellBeingRepository;
    using MyRow = Entities.WellBeingRow;
    using System.Collections.Generic;

    [RoutePrefix("Services/Assessment/WellBeing"), Route("{action}")]
    [ConnectionKey("Default"), ServiceAuthorize("Assessment")]
    public class WellBeingController : ServiceEndpoint
    {
        [HttpPost]
        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Create(uow, request);
        }

        [HttpPost]
        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Update(uow, request);
        }
 
        [HttpPost]
        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyRepository().Delete(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRepository().Retrieve(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {

            var user = (UserDefinition)Serenity.Authorization.UserDefinition;
            if (user.isAthlete != 1)
            {
                if (request.EqualityFilter.ContainsKey("dateof") == true)
                {
                    var dat = request.EqualityFilter.Get("dateof").ToString();
                    if (dat != "")
                    {
                        var datetouse = new DateTime(Convert.ToInt32(dat.Substring(6, 4)), Convert.ToInt32(dat.Substring(3, 2)), Convert.ToInt32(dat.Substring(0, 2)));
                        request.EqualityFilter.Remove("dateof");
                        request.EqualityFilter.Add("dateof", datetouse);
                    }
                }
            }


            return new MyRepository().List(connection, request);
        }

        private object TryGetValue(Dictionary<string, object> equalityFilter, string v)
        {
            throw new NotImplementedException();
        }

        public FileContentResult ListExcel(IDbConnection connection, ListRequest request)
        {
            var data = List(connection, request).Entities;
            var report = new DynamicDataReport(data, request.IncludeColumns, typeof(Columns.WellBeingColumns));
            var bytes = new ReportRepository().Render(report);
            return ExcelContentResult.Create(bytes, "AthleteList_" +
                DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx");
        }
    }
}
