﻿
namespace Serene2.Assessment.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Assessment.WellBeing")]
    [BasedOnRow(typeof(Entities.WellBeingRow))]
    public class WellBeingColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 Id { get; set; }

        [EditLink, DisplayName("Athlete"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.AthleteLookup)), QuickFilter]
        public string DisplayName { get; set; }

        [DisplayName("Username"), Visible(false)]
        public String AthleteUsername { get; set; }

        [TestFormatter, DisplayName("Status"), Width(90)]
        public String Status { get; set; }

        [EditLink, Width(100), DisplayName("Date Logged"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.DateLoggedLookup)), QuickFilter, SortOrder(1,true)]
        //[DisplayName("Date Logged")]
        public DateTime dateof { get; set; }

        //public Int32 AthleteId { get; set; }
        //public Int32 InjStatus { get; set; }
        [AnswerFormatter]
        public String InjStatusAnswer { get; set; }
        public Double WeightKg { get; set; }


        [Width(130), DisplayName("Sport"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.SportsLookup)), QuickFilter]
        public string Sport { get; set; }

        //[Width(130), DisplayName("Year"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.YearsLookup)), QuickFilter]
        //public string Year { get; set; }

        //[Width(130), DisplayName("Trainer"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.TrainersLookup)), QuickFilter]
        //public string Trainer { get; set; }

        [DisplayName("Sleep"), Width(100), AnswerFormatter]
        public String SleepAnswer { get; set; }
        [DisplayName("Routine"), Width(100), AnswerFormatter]
        public String ChangeDailyRoutineAnswer { get; set; }
        [DisplayName("Medical"), Width(100), AnswerFormatter]
        public String SoughtMedicalAttentionAnswer { get; set; }
        [DisplayName("Pain"), Width(100), AnswerFormatter]
        public String TightnessPainAnswer { get; set; }
        [DisplayName("Stress"), Width(100), AnswerFormatter]
        public String AcademicStressAnswer { get; set; }

        


    }
}