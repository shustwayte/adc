﻿

namespace Serene2.Assessment.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Assessments"), InstanceName("Assessment"), TwoLevelCached]
    [ReadPermission("Assessment")]
    [ModifyPermission("Assessment")]

    [LookupScript("WebApp.Assessment")]

    public sealed class WellBeingRow : Row, IIdRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athlete"), Column("athleteID"), ForeignKey("[dbo].[Athlete]", "id"), LeftJoin("jAthlete")]
        [LookupEditor(typeof(Serene2.Athlete.Entities.AthleteRow)), TextualField("AthleteUsername")]
        public Int32? AthleteId
        {
            get { return Fields.AthleteId[this]; }
            set { Fields.AthleteId[this] = value; }
        }



        [DisplayName("Weight Kg"), Column("weightKG")]
        public Double? WeightKg
        {
            get { return Fields.WeightKg[this]; }
            set { Fields.WeightKg[this] = value; }
        }

        

        [DisplayName("Athlete Live"), Expression("jAthlete.[live]")]
        public Int32? AthleteLive
        {
            get { return Fields.AthleteLive[this]; }
            set { Fields.AthleteLive[this] = value; }
        }

        [DisplayName("Athlete Username"), Expression("jAthlete.[username]")]
        public String AthleteUsername
        {
            get { return Fields.AthleteUsername[this]; }
            set { Fields.AthleteUsername[this] = value; }
        }

        [DisplayName("Athlete Stu Id"), Expression("jAthlete.[stuID]")]
        public String AthleteStuId
        {
            get { return Fields.AthleteStuId[this]; }
            set { Fields.AthleteStuId[this] = value; }
        }

        [DisplayName("Athlete First Name"), Expression("jAthlete.[FirstName]")]
        public String AthleteFirstName
        {
            get { return Fields.AthleteFirstName[this]; }
            set { Fields.AthleteFirstName[this] = value; }
        }

        [DisplayName("Athlete Last Name"), Expression("jAthlete.[LastName]")]
        public String AthleteLastName
        {
            get { return Fields.AthleteLastName[this]; }
            set { Fields.AthleteLastName[this] = value; }
        }


        [DisplayName("DisplayName"), Expression("jAthlete.[LastName] + ', ' + jAthlete.[FirstName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }


        [DisplayName("Status"), Expression("case when isnull(dateof,'') = '' then '' when [injStatus] = 1 and[sleep] = 2 and[changeDailyRoutine] = 2 and[soughtMedicalAttention] = 2 and[tightnessPain] = 2 and[academicStress] = 2 then 'Pass' else 'Attention' end")]
        public String Status
        {
            get { return Fields.Status[this]; }
            set { Fields.Status[this] = value; }
        }

        /**************************************/
        [DisplayName("Athlete Sport Id"), Expression("jAthlete.[SportID]"), ForeignKey("Sports", "SportID"), LeftJoin("jSport")]
        [LookupEditor(typeof(Serene2.Setup.Entities.SportsRow)), TextualField("SportName")]
        public Int32? AthleteSportId
        {
            get { return Fields.AthleteSportId[this]; }
            set { Fields.AthleteSportId[this] = value; }
        }

        [DisplayName("Sport"), Expression("jSport.[Sport]")]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }

        /**************************************/
        [DisplayName("Athlete Trainer Id"), Expression("jAthlete.[TrainerID]"), ForeignKey("Trainers", "TrainerID"), LeftJoin("jTrainer")]
        [LookupEditor(typeof(Serene2.Setup.Entities.TrainersRow)), TextualField("TrainerNameWB")]
        public Int32? AthleteTrainerId
        {
            get { return Fields.AthleteTrainerId[this]; }
            set { Fields.AthleteTrainerId[this] = value; }
        }

        [DisplayName("Trainer"), Expression("jTrainer.[Trainer]"), LookupInclude]
        public String Trainer
        {
            get { return Fields.Trainer[this]; }
            set { Fields.Trainer[this] = value; }
        }

        /**************************************/
        

        [DisplayName("Athlete Year Id"), Expression("jAthlete.[YearID]"), ForeignKey("AthleteYear", "YearID"), LeftJoin("jAthYear")]
        [LookupEditor(typeof(Serene2.Setup.Entities.AthleteYearRow)), TextualField("YearName")]
        public Int32? AthleteYearId
        {
            get { return Fields.AthleteYearId[this]; }
            set { Fields.AthleteYearId[this] = value; }
        }

        [DisplayName("Year"), Expression("jAthYear.[Year]"), LookupInclude]
        public String Year
        {
            get { return Fields.Year[this]; }
            set { Fields.Year[this] = value; }
        }

        /******************************************/


        [DisplayName("Athlete Gender Id"), Expression("jAthlete.[GenderID]")]
        public Int32? AthleteGenderId
        {
            get { return Fields.AthleteGenderId[this]; }
            set { Fields.AthleteGenderId[this] = value; }
        }

        /**************************************/

        [DisplayName("Inj Status"), Column("injStatus"), ForeignKey("[dbo].[injStatusAnswerLKUP]", "id"), LeftJoin("jInjStatus")]
        [LookupEditor(typeof(Serene2.Setup.Entities.InjStatusAnswerLkupRow)), TextualField("InjStatusAnswer")]
        public Int32? InjStatus
        {
            get { return Fields.InjStatus[this]; }
            set { Fields.InjStatus[this] = value; }
        }


        [DisplayName("Inj Status Answer"), Expression("jInjStatus.[answer]")]
        public String InjStatusAnswer
        {
            get { return Fields.InjStatusAnswer[this]; }
            set { Fields.InjStatusAnswer[this] = value; }
        }

        /******************************************/

        [DisplayName("Sleep"), Column("sleep"), ForeignKey("[dbo].[yesNoAnswerLKUP]", "id"), LeftJoin("jSleep")]
        [LookupEditor(typeof(Serene2.Setup.Entities.YesNoAnswerLkupRow)), TextualField("SleepAnswer")]
        public Int32? Sleep
        {
            get { return Fields.Sleep[this]; }
            set { Fields.Sleep[this] = value; }
        }

        [DisplayName("Sleep Answer"), Expression("jSleep.[answer]")]
        public String SleepAnswer
        {
            get { return Fields.SleepAnswer[this]; }
            set { Fields.SleepAnswer[this] = value; }
        }

        [DisplayName("Change Daily Routine"), Column("changeDailyRoutine"), ForeignKey("[dbo].[yesNoAnswerLKUP]", "id"), LeftJoin("jChangeDailyRoutine")]
        [LookupEditor(typeof(Serene2.Setup.Entities.YesNoAnswerLkupRow)), TextualField("ChangeDailyRoutineAnswer")]
        public Int32? ChangeDailyRoutine
        {
            get { return Fields.ChangeDailyRoutine[this]; }
            set { Fields.ChangeDailyRoutine[this] = value; }
        }

        [DisplayName("Change Daily Routine Answer"), Expression("jChangeDailyRoutine.[answer]")]
        public String ChangeDailyRoutineAnswer
        {
            get { return Fields.ChangeDailyRoutineAnswer[this]; }
            set { Fields.ChangeDailyRoutineAnswer[this] = value; }
        }



        [DisplayName("Sought Medical Attention"), Column("soughtMedicalAttention"), ForeignKey("[dbo].[yesNoAnswerLKUP]", "id"), LeftJoin("jSoughtMedicalAttention")]
        [LookupEditor(typeof(Serene2.Setup.Entities.YesNoAnswerLkupRow)), TextualField("SoughtMedicalAttentionAnswer")]
        public Int32? SoughtMedicalAttention
        {
            get { return Fields.SoughtMedicalAttention[this]; }
            set { Fields.SoughtMedicalAttention[this] = value; }
        }

        [DisplayName("Sought Medical Attention Answer"), Expression("jSoughtMedicalAttention.[answer]")]
        public String SoughtMedicalAttentionAnswer
        {
            get { return Fields.SoughtMedicalAttentionAnswer[this]; }
            set { Fields.SoughtMedicalAttentionAnswer[this] = value; }
        }


        [DisplayName("Tightness Pain"), Column("tightnessPain"), ForeignKey("[dbo].[yesNoAnswerLKUP]", "id"), LeftJoin("jTightnessPain")]
        [LookupEditor(typeof(Serene2.Setup.Entities.YesNoAnswerLkupRow)), TextualField("TightnessPainAnswer")]
        public Int32? TightnessPain
        {
            get { return Fields.TightnessPain[this]; }
            set { Fields.TightnessPain[this] = value; }
        }

        [DisplayName("Tightness Pain Answer"), Expression("jTightnessPain.[answer]")]
        public String TightnessPainAnswer
        {
            get { return Fields.TightnessPainAnswer[this]; }
            set { Fields.TightnessPainAnswer[this] = value; }
        }

        [DisplayName("Academic Stress"), Column("academicStress"), ForeignKey("[dbo].[yesNoAnswerLKUP]", "id"), LeftJoin("jAcademicStress")]
        [LookupEditor(typeof(Serene2.Setup.Entities.YesNoAnswerLkupRow)), TextualField("AcademicStressAnswer")]
        public Int32? AcademicStress
        {
            get { return Fields.AcademicStress[this]; }
            set { Fields.AcademicStress[this] = value; }
        }

        [DisplayName("Academic Stress Answer"), Expression("jAcademicStress.[answer]")]
        public String AcademicStressAnswer
        {
            get { return Fields.AcademicStressAnswer[this]; }
            set { Fields.AcademicStressAnswer[this] = value; }
        }


        public DateTime? dateof
        {
            get { return Fields.dateof[this]; }
            set { Fields.dateof[this] = value; }
        }


        [Expression(" convert(varchar,[dateof], 103) ")]
        public String dateofdisplay
        {
            get { return Fields.dateofdisplay[this]; }
            set { Fields.dateofdisplay[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public WellBeingRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthleteId;
            public Int32Field InjStatus;
            public DoubleField WeightKg;
            public Int32Field Sleep;
            public Int32Field ChangeDailyRoutine;
            public Int32Field SoughtMedicalAttention;
            public Int32Field TightnessPain;
            public Int32Field AcademicStress;

            public Int32Field AthleteLive;
            public StringField AthleteUsername;
            public StringField AthleteStuId;
            public StringField AthleteFirstName;
            public StringField AthleteLastName;
            public Int32Field AthleteSportId;
            public Int32Field AthleteTrainerId;
            public Int32Field AthleteGenderId;
            public Int32Field AthleteYearId;

            public StringField InjStatusAnswer;

            public StringField SleepAnswer;

            public StringField ChangeDailyRoutineAnswer;

            public StringField SoughtMedicalAttentionAnswer;

            public StringField TightnessPainAnswer;

            public StringField AcademicStressAnswer;

            public StringField DisplayName;

            public DateTimeField dateof;

            public StringField dateofdisplay;

            public StringField Status;


            public StringField Sport;
            public StringField Trainer;
            public StringField Year;


            public RowFields()
                : base("[dbo].[wellBeing]")
            {
                LocalTextPrefix = "Assessment.WellBeing";
            }
        }
    }
}