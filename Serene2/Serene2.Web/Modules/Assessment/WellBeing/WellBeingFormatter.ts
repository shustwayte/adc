﻿

namespace Serene2 {

    @Serenity.Decorators.registerFormatter()
    export class TestFormatter implements Slick.Formatter {

        format(ctx: Slick.FormatterContext) {

            if ((ctx.value == null || String(ctx.value).length == 0)) {
                return ctx.value;
            }


            var testNumber: String = ctx.value;

            if (testNumber == "Pass") {
                return "<div style='height:100%; background-color:green;text-align:center;color: white;'>" + testNumber + '</div>';
            }

            if (testNumber == "Attention") {
                return "<div style='height:100%; background-color:red;text-align:center;color: white;'>" + testNumber + '</div>';
            }

            //return "<div style='height:100%;  background-color:white'>" + testNumber + '</div>';

        }



    }


    export class AnswerFormatter implements Slick.Formatter {

        format(ctx: Slick.FormatterContext) {

            if ((ctx.value == null || String(ctx.value).length == 0)) {
                return ctx.value;
            }


            var testNumber: String = ctx.value;

            if (testNumber == "Fit to Train and Play") {
                return "<div style='height:100%; background-color:green;'>" + testNumber + '</div>';
            }

            if (testNumber == "Restricted Training and Playing") {
                return "<div style='height:100%; background-color:orange;'>" + testNumber + '</div>';
            }

            if (testNumber == "No Training or Playing") {
                return "<div style='height:100%; background-color:red;'>" + testNumber + '</div>';
            }

            if (testNumber == "Yes") {
                return "<div style='height:100%; background-color:red;'>" + testNumber + '</div>';
            }

            return "<div style='height:100%;  background-color:white'>" + testNumber + '</div>';

        }



    }
}
