﻿

namespace Serene2.Assessment.Repositories
{
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System;
    using System.Data;
    using MyRow = Entities.WellBeingRow;

    public class WellBeingRepository
    {
        private static MyRow.RowFields fld { get { return MyRow.Fields; } }

        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Create);
        }

        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Update);
        }

        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyDeleteHandler().Process(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRetrieveHandler().Process(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {
            
            //      determine if role of ATHLETE, then restrict to users own records  //
            if (Serene2.Global.MyGlobals.IsAthlete == "1")
            {
                if (request.EqualityFilter.ContainsKey("dateof") == true)
                {
                    var dat = request.EqualityFilter.Get("dateof").ToString();
                    if (dat != "")
                    {
                        var datetouse = new DateTime(Convert.ToInt32(dat.Substring(6, 4)), Convert.ToInt32(dat.Substring(3, 2)), Convert.ToInt32(dat.Substring(0, 2)));
                        request.EqualityFilter.Remove("dateof");
                        request.EqualityFilter.Add("dateof", datetouse);
                    }
                }
                var user = (UserDefinition)Serenity.Authorization.UserDefinition;
                // no filters in new grid
                request.EqualityFilter.Add("AthleteUsername", user.Username);
            }
            //  end   //
            
   

            return new MyListHandler().Process(connection, request);
        }

        private class MySaveHandler : SaveRequestHandler<MyRow> { }
        private class MyDeleteHandler : DeleteRequestHandler<MyRow> { }
        private class MyRetrieveHandler : RetrieveRequestHandler<MyRow> { }
        private class MyListHandler : ListRequestHandler<MyRow> { }
    }
}