﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Assessment {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class WellBeingEditorDialog extends Common.GridEditorDialog<WellBeingRow> {
        protected getFormKey() { return WellBeingForm.formKey; }
                protected getLocalTextPrefix() { return WellBeingRow.localTextPrefix; }
        protected form = new WellBeingForm(this.idPrefix);
    }
}