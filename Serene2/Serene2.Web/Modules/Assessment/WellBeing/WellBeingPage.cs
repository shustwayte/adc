﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Assessment/WellBeing", typeof(Serene2.Assessment.Pages.WellBeingController))]

namespace Serene2.Assessment.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Assessment/WellBeing"), Route("{action=index}")]
    public class WellBeingController : Controller
    {
        [PageAuthorize("Assessment")]
        public ActionResult Index()
        {
            return View("~/Modules/Assessment/WellBeing/WellBeingIndex.cshtml");
        }
    }
}