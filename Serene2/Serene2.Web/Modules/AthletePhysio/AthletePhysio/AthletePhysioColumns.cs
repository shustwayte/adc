﻿
namespace Serene2.AthletePhysio.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("AthletePhysio.AthletePhysio")]
    [BasedOnRow(typeof(Entities.AthletePhysioRow))]
    public class AthletePhysioColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 Id { get; set; }
        [EditLink, DisplayName("Athlete")]
        public String DisplayName { get; set; }
        [Width(130), DisplayName("Sport"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.SportsLookup)), QuickFilter, QuickFilterOption("multiple", true)]
        public string Sport { get; set; }

        [Width(130), DisplayName("Entry Date"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.EntryDateLookup)), QuickFilter]
        public string EntryDateLkup { get; set; }

        public DateTime EntryDate { get; set; }
        //public Int32 PhysioId { get; set; }
        [QuickFilter]
        public String Physio { get; set; }
        //public Int32 InjuryId { get; set; }
        [QuickFilter]
        public String Injury { get; set; }
        //public Int32 InjuryStatusId { get; set; }
        [QuickFilter]
        public String InjuryStatus { get; set; }
        [EditLink]
        public String Notes { get; set; }
        [DisplayName("Physio and S&C Plan")]
        public String PhysioPlan { get; set; }
        

        
    }
}