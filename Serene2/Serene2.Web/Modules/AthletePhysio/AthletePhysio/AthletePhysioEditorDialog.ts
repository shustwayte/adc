﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.AthletePhysio {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthletePhysioEditorDialog extends Common.GridEditorDialog<AthletePhysioRow> {
        protected getFormKey() { return AthletePhysioForm.formKey; }
                protected getLocalTextPrefix() { return AthletePhysioRow.localTextPrefix; }
        protected getNameProperty() { return AthletePhysioRow.nameProperty; }
        protected form = new AthletePhysioForm(this.idPrefix);
    }
}