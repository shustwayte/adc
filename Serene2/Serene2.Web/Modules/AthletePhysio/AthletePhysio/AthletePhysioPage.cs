﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "AthletePhysio/AthletePhysio", typeof(Serene2.AthletePhysio.Pages.AthletePhysioController))]

namespace Serene2.AthletePhysio.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("AthletePhysio/AthletePhysio"), Route("{action=index}")]
    public class AthletePhysioController : Controller
    {
        [PageAuthorize("AthletePhysio")]
        public ActionResult Index()
        {
            return View("~/Modules/AthletePhysio/AthletePhysio/AthletePhysioIndex.cshtml");
        }
    }
}