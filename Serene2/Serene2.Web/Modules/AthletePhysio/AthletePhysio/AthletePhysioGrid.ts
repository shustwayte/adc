﻿
namespace Serene2.AthletePhysio {
    
    @Serenity.Decorators.registerClass()
    export class AthletePhysioGrid extends Serenity.EntityGrid<AthletePhysioRow, any> {
        protected getColumnsKey() { return 'AthletePhysio.AthletePhysio'; }
        protected getDialogType() { return AthletePhysioDialog; }
        protected getIdProperty() { return AthletePhysioRow.idProperty; }
        protected getLocalTextPrefix() { return AthletePhysioRow.localTextPrefix; }
        protected getService() { return AthletePhysioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }


        protected getButtons() {
            var buttons = super.getButtons();

            buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'AthletePhysio/AthletePhysio/ListExcel',
                separator: true
            }));

            buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            return buttons;
        }

        private _AthleteId: number;

        get AthleteId() {
            return this._AthleteId;
        }

         

        set AthleteId(value: number) {
            if (this._AthleteId !== value) {
                this._AthleteId = value;
                this.setEquality('AthleteId', value);
                this.refresh();
            }
        }

    }
}