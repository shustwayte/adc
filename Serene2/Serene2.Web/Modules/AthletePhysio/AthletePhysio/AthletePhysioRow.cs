﻿

namespace Serene2.AthletePhysio.Entities
{
    using Modules.Common.BaseClass;
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Physio"), InstanceName("Physio"), TwoLevelCached]
    [ReadPermission("AthletePhysio")]
    [ModifyPermission("AthletePhysio")]
    public sealed class AthletePhysioRow : Row, IIdRow, INameRow, IAuditLog
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athlete"), Column("athleteID"), ForeignKey("[dbo].[Athlete]", "Id"), LeftJoin("jAthlete"), Visible(false)]
        [LookupEditor(typeof(Serene2.Athlete.Entities.AthleteRow)), TextualField("AthleteUsername")]
        public Int32? AthleteId
        {
            get { return Fields.AthleteId[this]; }
            set { Fields.AthleteId[this] = value; }
        }

        [DisplayName("Entry Date"), Column("entryDate"), DisplayFormat("dd/MM/yyyy")]
        public DateTime? EntryDate
        {
            get { return Fields.EntryDate[this]; }
            set { Fields.EntryDate[this] = value; }
        }


        [DisplayName("Entry Date"), Expression(" case when isnull(entryDate,'') <> '' then convert(varchar,[entryDate],103) else '' end ")]
        public String EntryDateLkup
        {
            get { return Fields.EntryDateLkup[this]; }
            set { Fields.EntryDateLkup[this] = value; }
        }

        [DisplayName("Physio"), Column("physioID"), ForeignKey("[dbo].[Physio]", "id"), LeftJoin("jPhysio")]
        [LookupEditor(typeof(Serene2.Setup.Entities.PhysioRow)), TextualField("Physio")]
        public Int32? PhysioId
        {
            get { return Fields.PhysioId[this]; }
            set { Fields.PhysioId[this] = value; }
        }

        [DisplayName("Physio"), Expression("jPhysio.[Physio]")]
        public String Physio
        {
            get { return Fields.Physio[this]; }
            set { Fields.Physio[this] = value; }
        }

        [DisplayName("Injury"), Column("injuryId"), ForeignKey("[dbo].[Injury]", "id"), LeftJoin("jInjury")]
        [LookupEditor(typeof(Serene2.Setup.Entities.InjuryRow)), TextualField("Injury")]
        public Int32? InjuryId
        {
            get { return Fields.InjuryId[this]; }
            set { Fields.InjuryId[this] = value; }
        }

        [DisplayName("Injury Status"), Column("injuryStatusId"), ForeignKey("[dbo].[InjuryStatus]", "id"), LeftJoin("jInjuryStatus")]
        [LookupEditor(typeof(Serene2.Setup.Entities.InjuryStatusRow)), TextualField("InjuryStatus")]
        public Int32? InjuryStatusId
        {
            get { return Fields.InjuryStatusId[this]; }
            set { Fields.InjuryStatusId[this] = value; }
        }

        [DisplayName("Notes"), Column("notes"), Size(2000), QuickSearch]
        public String Notes
        {
            get { return Fields.Notes[this]; }
            set { Fields.Notes[this] = value; }
        }

        [DisplayName("Physio Plan"), Column("physioPlan"), Size(2000)]
        public String PhysioPlan
        {
            get { return Fields.PhysioPlan[this]; }
            set { Fields.PhysioPlan[this] = value; }
        }

        [DisplayName("Athlete Live"), Expression("jAthlete.[live]")]
        public Int32? AthleteLive
        {
            get { return Fields.AthleteLive[this]; }
            set { Fields.AthleteLive[this] = value; }
        }

        [DisplayName("Athlete Username"), Expression("jAthlete.[Username]")]
        public String AthleteUsername
        {
            get { return Fields.AthleteUsername[this]; }
            set { Fields.AthleteUsername[this] = value; }
        }

        [DisplayName("Athlete Stu Id"), Expression("jAthlete.[stuID]")]
        public String AthleteStuId
        {
            get { return Fields.AthleteStuId[this]; }
            set { Fields.AthleteStuId[this] = value; }
        }

        [DisplayName("Athlete First Name"), Expression("jAthlete.[FirstName]"), Updatable(false)]
        public String AthleteFirstName
        {
            get { return Fields.AthleteFirstName[this]; }
            set { Fields.AthleteFirstName[this] = value; }
        }

        [DisplayName("Athlete Last Name"), Expression("jAthlete.[LastName]"), Updatable(false)]
        public String AthleteLastName
        {
            get { return Fields.AthleteLastName[this]; }
            set { Fields.AthleteLastName[this] = value; }
        }


        [DisplayName("DisplayName"), Expression("jAthlete.[LastName] + ', ' + jAthlete.[FirstName]"), QuickSearch]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }




        [DisplayName("Athlete Sport Id"), Expression("jAthlete.[SportID]"), ForeignKey("Sports", "SportID"), LeftJoin("jSport")]
        [LookupEditor(typeof(Serene2.Setup.Entities.SportsRow)), TextualField("SportName")]
        public Int32? AthleteSportId
        {
            get { return Fields.AthleteSportId[this]; }
            set { Fields.AthleteSportId[this] = value; }
        }

        
        [DisplayName("Sport"), Expression("jSport.[Sport]"), LookupInclude]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }
        





        [DisplayName("Athlete Trainer Id"), Expression("jAthlete.[TrainerID]")]
        public Int32? AthleteTrainerId
        {
            get { return Fields.AthleteTrainerId[this]; }
            set { Fields.AthleteTrainerId[this] = value; }
        }

        [DisplayName("Athlete Gender Id"), Expression("jAthlete.[GenderID]")]
        public Int32? AthleteGenderId
        {
            get { return Fields.AthleteGenderId[this]; }
            set { Fields.AthleteGenderId[this] = value; }
        }

        [DisplayName("Athlete Year Id"), Expression("jAthlete.[YearID]")]
        public Int32? AthleteYearId
        {
            get { return Fields.AthleteYearId[this]; }
            set { Fields.AthleteYearId[this] = value; }
        }

      

        [DisplayName("Physio Username"), Expression("jPhysio.[username]")]
        public String PhysioUsername
        {
            get { return Fields.PhysioUsername[this]; }
            set { Fields.PhysioUsername[this] = value; }
        }

        [DisplayName("Injury"), Expression("jInjury.[Injury]")]
        public String Injury
        {
            get { return Fields.Injury[this]; }
            set { Fields.Injury[this] = value; }
        }

        [DisplayName("Injury Status"), Expression("jInjuryStatus.[InjuryStatus]")]
        public String InjuryStatus
        {
            get { return Fields.InjuryStatus[this]; }
            set { Fields.InjuryStatus[this] = value; }
        }


        /// <summary>
        [DisplayName("History"), Column("history"), Size(2000), QuickSearch]
        public String history
        {
            get { return Fields.history[this]; }
            set { Fields.history[this] = value; }
        }

        [DisplayName("Previous History"), Column("previousHistory"), Size(2000), QuickSearch]
        public String previousHistory
        {
            get { return Fields.previousHistory[this]; }
            set { Fields.previousHistory[this] = value; }
        }

        [DisplayName("Objective Assessment"), Column("objectiveAssessment"), Size(2000), QuickSearch]
        public String objectiveAssessment
        {
            get { return Fields.objectiveAssessment[this]; }
            set { Fields.objectiveAssessment[this] = value; }
        }

        [DisplayName("managementPlan"), Column("managementPlan"), Size(2000), QuickSearch]
        public String managementPlan
        {
            get { return Fields.managementPlan[this]; }
            set { Fields.managementPlan[this] = value; }
        }

        [DisplayName("Concussion Clinic Review"), Column("conClinicRvw"), Size(2000), QuickSearch]
        public String conClinicRvw
        {
            get { return Fields.conClinicRvw[this]; }
            set { Fields.conClinicRvw[this] = value; }
        }
        


        /// </summary>

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Notes; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthletePhysioRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthleteId;
            public DateTimeField EntryDate;
            public Int32Field PhysioId;
            public Int32Field InjuryId;
            public Int32Field InjuryStatusId;
            public StringField Notes;
            public StringField PhysioPlan;

            public Int32Field AthleteLive;
            public StringField AthleteUsername;
            public StringField AthleteStuId;
            public StringField AthleteFirstName;
            public StringField AthleteLastName;
            public Int32Field AthleteSportId;
            public Int32Field AthleteTrainerId;
            public Int32Field AthleteGenderId;
            public Int32Field AthleteYearId;

            public StringField Physio;
            public StringField PhysioUsername;

            public StringField Injury;

            public StringField InjuryStatus;


            public StringField DisplayName;


            public StringField history;
            public StringField previousHistory;
            public StringField objectiveAssessment;
            public StringField managementPlan;

            public StringField Sport;

            public StringField EntryDateLkup;

            public StringField conClinicRvw;

            public RowFields()
                : base("[dbo].[AthletePhysio]")
            {
                LocalTextPrefix = "AthletePhysio.AthletePhysio";
            }
        }
    }
}