﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.AthletePhysio {
    
    @Serenity.Decorators.registerClass()
    export class AthletePhysioEditor extends Common.GridEditorBase<AthletePhysioRow> {
        protected getColumnsKey() { return 'AthletePhysio.AthletePhysio'; }
        protected getDialogType() { return AthletePhysioEditorDialog; }
                protected getLocalTextPrefix() { return AthletePhysioRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}