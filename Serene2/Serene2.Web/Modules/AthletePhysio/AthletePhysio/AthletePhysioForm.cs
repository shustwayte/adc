﻿
namespace Serene2.AthletePhysio.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("AthletePhysio.AthletePhysio")]
    [BasedOnRow(typeof(Entities.AthletePhysioRow))]
    public class AthletePhysioForm
    {
      //  [Visible(false)]
        public Int32 AthleteId { get; set; }
        [DisplayName("First Name")]
        public String AthleteFirstName { get; set; }
        [DisplayName("Last Name")]
        public String AthleteLastName { get; set; }
        public DateTime EntryDate { get; set; }
        public Int32 PhysioId { get; set; }
        public Int32 InjuryId { get; set; }
        public Int32 InjuryStatusId { get; set; }

        [Category("Plans")]
        [TextAreaEditor(Rows = 3)]
        public String Notes { get; set; }
        [TextAreaEditor(Rows = 3) ,DisplayName("Physio and S&C Plan")]
        public String PhysioPlan { get; set; }

        [TextAreaEditor(Rows = 3)]
        public String conClinicRvw { get; set; }

        [Category("History")]
        [TextAreaEditor(Rows = 3)]
        public String history { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String previousHistory { get; set; }
        [TextAreaEditor(Rows = 3)]

        [Category("Assessment")]
        public String objectiveAssessment { get; set; }
        [TextAreaEditor(Rows = 3), DisplayName("Management Plan")]
        public String managementPlan { get; set; }
    }
}