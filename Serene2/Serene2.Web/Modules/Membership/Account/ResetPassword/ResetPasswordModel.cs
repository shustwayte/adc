﻿
namespace Serene2.Membership
{
    using Serenity.Services;
    using System;

    public class ResetPasswordModel
    {
        public string Token { get; set; }
    }
}