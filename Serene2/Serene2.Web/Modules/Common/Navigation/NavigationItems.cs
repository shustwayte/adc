﻿using Serenity.Navigation;
using Administration = Serene2.Administration.Pages;

using Athlete = Serene2.Athlete.Pages;
using Setup = Serene2.Setup.Pages;

using Assessment = Serene2.Assessment.Pages;

using Physio = Serene2.AthletePhysio.Pages;

using AthleteWeight = Serene2.AthleteWeight.Pages;

using RPE = Serene2.RPE.Pages;


[assembly: NavigationLink(1000, "Dashboard", url: "~/", permission: "", icon: "icon-speedometer")]

[assembly: NavigationMenu(9000, "Athlete", icon: "icon-people")]
[assembly: NavigationLink(1000, "Athlete/Athlete", typeof(Athlete.AthleteController), icon: "icon-user")]
[assembly: NavigationLink(1000, "Athlete/Assessment", typeof(Assessment.WellBeingController), icon: "icon-trophy")]

[assembly: NavigationLink(1000, "Athlete/RPE", typeof(RPE.RpeController), icon: "icon-trophy")]

[assembly: NavigationLink(1000, "Athlete/Athlete Weight", typeof(AthleteWeight.AthleteWeightController), icon: "icon-trophy")]

[assembly: NavigationMenu(9000, "Physio", icon: "icon-people")]
[assembly: NavigationLink(1000, "Physio/Tracking", typeof(Physio.AthletePhysioController), icon: "icon-user")]

[assembly: NavigationMenu(9000, "Testing", icon: "icon-people")]
[assembly: NavigationLink(9000, "Testing/Testings", typeof(Setup.AthleteTestingController), icon: "icon-support")]


[assembly: NavigationMenu(9000, "Setup", icon: "icon-magic-wand")]

[assembly: NavigationLink(9000, "Setup/Ass Injury Status", typeof(Setup.InjStatusAnswerLkupController), icon: "icon-support")]

[assembly: NavigationLink(9000, "Setup/Physio", typeof(Setup.PhysioController), icon: "icon-compass")]
[assembly: NavigationLink(9000, "Setup/Phys Injury", typeof(Setup.InjuryController), icon: "icon-compass")]
[assembly: NavigationLink(9000, "Setup/Phys Injury Status", typeof(Setup.InjuryStatusController), icon: "icon-compass")]

[assembly: NavigationLink(9000, "Setup/Sports", typeof(Setup.SportsController), icon: "icon-support")]

[assembly: NavigationLink(9000, "Setup/Sport Position", typeof(Setup.AthletePositionController), icon: "icon-support")]

[assembly: NavigationLink(9000, "Setup/Testing", typeof(Setup.AthleteTestingLkUpController), icon: "icon-support")]

[assembly: NavigationLink(9000, "Setup/Coaches", typeof(Setup.TrainersController), icon: "icon-anchor")]
[assembly: NavigationLink(9000, "Setup/Year", typeof(Setup.AthleteYearController), icon: "icon-compass")]
[assembly: NavigationLink(9000, "Setup/Yes No", typeof(Setup.YesNoAnswerLkupController), icon: "icon-compass")]





[assembly: NavigationMenu(9000, "Administration", icon: "icon-screen-desktop")]
[assembly: NavigationLink(9000, "Administration/Exceptions Log", url: "~/errorlog.axd", permission: Serene2.Administration.PermissionKeys.Security, icon: "icon-ban", Target = "_blank")]
//[assembly: NavigationLink(9000, "Administration/Languages", typeof(Administration.LanguageController), icon: "icon-bubbles")]
[assembly: NavigationLink(9000, "Administration/Translations", typeof(Administration.TranslationController), icon: "icon-speech")]
[assembly: NavigationLink(9000, "Administration/Roles", typeof(Administration.RoleController), icon: "icon-lock")]
[assembly: NavigationLink(9000, "Administration/User Management", typeof(Administration.UserController), icon: "icon-people")]