﻿
namespace Serene2.Common.Pages
{

    using AthleteWeight;
    using AthleteWeight.Entities;
    
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System;
    using System.Web.Mvc;
    using System.Web.Security;

    using System.Data.SqlClient;

    [RoutePrefix("Dashboard"), Route("{action=index}")]
    public class DashboardController : Controller
    {
        [Authorize, HttpGet, Route("~/")]
        public ActionResult Index()
        {


            var user = (UserDefinition)Serenity.Authorization.UserDefinition;

            TwoLevelCache.ExpireGroupItems("DashboardPageModel");

            var constring = SqlConnections.NewByKey("Default");
            SqlConnection con = new SqlConnection(constring.ConnectionString);

            con.Open();

            var AthleteGraphLabels = "";
            var AthleteGraphRes = "";
            var AthleteGraphResBASE = "";

            var AthleteNumAttend = 0;
            var AthleteNumSessions = 0;
            var AthleteAttendPerc = new decimal(0.00);
            var AthleteAbsentPerc = new decimal(0.00);

            var SporttGraphLabel = "";
            var SporttGraphexpected = "";
            var SporttGraphattend = "";
            var SporttGraphmissing = "";


            using (SqlCommand command = new SqlCommand(" exec [dbo].[p_Athletes_AVG_Weight] '" + user.Username + "', 'graph'  ", con))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    //Console.WriteLine("{0} {1} {2}",
                    //  reader.GetInt32(0), reader.GetString(1), reader.GetString(2));

                    AthleteGraphLabels = reader.GetString(0);
                    AthleteGraphRes = reader.GetString(1);
                    AthleteGraphResBASE = reader.GetString(2);
                }

            }

            using (SqlCommand command2 = new SqlCommand(" exec [dbo].[p_Athletes_Attend_Perc] '" + user.Username + "'  ", con))
            using (SqlDataReader reader2 = command2.ExecuteReader())
            {
                while (reader2.Read())
                {
                    //Console.WriteLine("{0} {1} {2}",
                    //  reader.GetInt32(0), reader.GetString(1), reader.GetString(2));

                    AthleteNumAttend = reader2.GetInt32(0);
                    AthleteNumSessions = reader2.GetInt32(1);
                    AthleteAttendPerc = reader2.GetDecimal(2);
                    AthleteAbsentPerc = reader2.GetDecimal(3);
                }

            }


            using (SqlCommand command3 = new SqlCommand(" exec [dbo].[p_Attend_Graph]  ", con))
            using (SqlDataReader reader3 = command3.ExecuteReader())
            {
                while (reader3.Read())
                {
                    //Console.WriteLine("{0} {1} {2}",
                    //  reader.GetInt32(0), reader.GetString(1), reader.GetString(2));
                    SporttGraphLabel = reader3.GetString(0);
                    SporttGraphexpected = reader3.GetString(1);
                    SporttGraphattend = reader3.GetString(2);
                    SporttGraphmissing = reader3.GetString(3);
                }

            }


            SqlConnection.ClearAllPools();

            var cachedModel = TwoLevelCache.Get("DashboardPageModel", TimeSpan.FromMilliseconds(0),
                AthleteWeightRow.Fields.GenerationKey, () =>
                {
                    var model = new DashboardPageModel();
                    var aw = AthleteWeightRow.Fields;

                    var a = Serene2.Athlete.Entities.AthleteRow.Fields;

                    using (var connection = SqlConnections.NewFor<AthleteWeightRow>())
                    {

                        model.AthleteWeightGraphLabel = AthleteGraphLabels;
                        model.AthleteWeightGraphValue = AthleteGraphRes;

                        model.AthleteWeightGraphBASEValue = AthleteGraphResBASE;

                        model.AthleteNumAttend = AthleteNumAttend;
                        model.AthleteNumSessions = AthleteNumSessions;
                        model.AthleteAttendPerc = AthleteAttendPerc;
                        model.AthleteAbsentPerc = AthleteAbsentPerc;

                        model.last2assessmentFail = connection.Count<Athlete.Entities.AthleteRow>(a.lastTwoAssessmentId == 1 && a.Live == 1);

                        model.last2assessmentPass = connection.Count<Athlete.Entities.AthleteRow>(a.lastTwoAssessmentId == 0);



                        model.missingtoday = connection.Count<Athlete.Entities.AthleteRow>(a.AthleteLoggedAssessmentToday == 0 && a.Live == 1);
                        model.attendingtoday = connection.Count<Athlete.Entities.AthleteRow>(a.AthleteLoggedAssessmentToday == 1 && a.Live == 1);

                        model.SporttGraphLabel = SporttGraphLabel;
                        model.SporttGraphexpected = SporttGraphexpected;
                        model.SporttGraphattend = SporttGraphattend;
                        model.SporttGraphmissing = SporttGraphmissing;


                        return model;
                    }
                });
               
           // var cachedModel = new DashboardPageModel()
            {
            };


            return View(MVC.Views.Common.Dashboard.DashboardIndex, cachedModel);
        }


       


    }
}