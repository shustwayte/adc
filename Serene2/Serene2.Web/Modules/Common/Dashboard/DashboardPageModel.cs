﻿
namespace Serene2.Common
{
    public class DashboardPageModel
    {
        public int OpenOrders { get; set; }
        public int ClosedOrderPercent { get; set; }
        public int CustomerCount { get; set; }
        public int ProductCount { get; set; }


        public int last2assessmentPass { get; set; }

        public int last2assessmentFail { get; set; }


        public string AthleteWeightGraphLabel { get; set; }
        public string AthleteWeightGraphValue { get; set; }

        public string AthleteWeightGraphBASEValue { get; set; }


        public int AthleteNumAttend { get; set; }
        public int AthleteNumSessions { get; set; }
        public decimal AthleteAttendPerc { get; set; }
        public decimal AthleteAbsentPerc { get; set; }



        public string SporttGraphLabel { get; set; }

        public string SporttGraphexpected { get; set; }
        public string SporttGraphattend { get; set; }
        public string SporttGraphmissing { get; set; }


        public int missingtoday { get; set; }

        public int attendingtoday { get; set; }

    }
    
}