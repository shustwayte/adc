﻿namespace Serene2.ScriptInitialization {
    Q.Config.responsiveDialogs = true;
    Q.Config.rootNamespaces.push('Serene2');

    Serenity.DataGrid.defaultPersistanceStorage = window.sessionStorage;
    Serenity.DataGrid.defaultPersistanceStorage = window.localStorage;
}