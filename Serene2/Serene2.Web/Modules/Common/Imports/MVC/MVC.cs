﻿using System;

namespace MVC
{
    public static class Views
    {
        public static class Administration
        {
            public static class Language
            {
                public const string LanguageIndex = "~/Modules/Administration/Language/LanguageIndex.cshtml";
            }

            public static class Role
            {
                public const string RoleIndex = "~/Modules/Administration/Role/RoleIndex.cshtml";
            }

            public static class Translation
            {
                public const string TranslationIndex = "~/Modules/Administration/Translation/TranslationIndex.cshtml";
            }

            public static class User
            {
                public const string UserIndex = "~/Modules/Administration/User/UserIndex.cshtml";
            }

        }

        public static class Assessment
        {
            public static class WellBeing
            {
                public const string WellBeingIndex = "~/Modules/Assessment/WellBeing/WellBeingIndex.cshtml";
            }

        }

        public static class Athlete
        {
            public static class Athlete_
            {
                public const string AthleteIndex = "~/Modules/Athlete/Athlete/AthleteIndex.cshtml";
                public const string StatementPrint = "~/Modules/Athlete/Athlete/StatementPrint.cshtml";
            }

        }

        public static class AthleteLoggedToday
        {
            public static class AthleteLoggedToday_
            {
                public const string AthleteLoggedTodayIndex = "~/Modules/AthleteLoggedToday/AthleteLoggedToday/AthleteLoggedTodayIndex.cshtml";
            }

        }

        public static class AthleteNote
        {
            public static class AthleteNote_
            {
                public const string AthleteNoteIndex = "~/Modules/AthleteNote/AthleteNote/AthleteNoteIndex.cshtml";
            }

        }

        public static class AthletePhysio
        {
            public static class AthletePhysio_
            {
                public const string AthletePhysioIndex = "~/Modules/AthletePhysio/AthletePhysio/AthletePhysioIndex.cshtml";
            }

        }

        public static class AthleteWeight
        {
            public static class AthleteWeight_
            {
                public const string AthleteWeightIndex = "~/Modules/AthleteWeight/AthleteWeight/AthleteWeightIndex.cshtml";
            }

        }

        public static class Audit
        {
            public static class AuditLog
            {
                public const string AuditLogIndex = "~/Modules/Audit/AuditLog/AuditLogIndex.cshtml";
            }

        }

        public static class Common
        {
            public static class Dashboard
            {
                public const string DashboardIndex = "~/Modules/Common/Dashboard/DashboardIndex.cshtml";
            }

            public static class Reporting
            {
                public const string ReportPage = "~/Modules/Common/Reporting/ReportPage.cshtml";
            }
        }

        public static class Errors
        {
            public const string ValidationError = "~/Views/Errors/ValidationError.cshtml";
        }

        public static class Membership
        {
            public static class Account
            {
                public const string AccountLogin = "~/Modules/Membership/Account/AccountLogin.cshtml";
                public static class ChangePassword
                {
                    public const string AccountChangePassword = "~/Modules/Membership/Account/ChangePassword/AccountChangePassword.cshtml";
                }

                public static class ForgotPassword
                {
                    public const string AccountForgotPassword = "~/Modules/Membership/Account/ForgotPassword/AccountForgotPassword.cshtml";
                }

                public static class ResetPassword
                {
                    public const string AccountResetPassword = "~/Modules/Membership/Account/ResetPassword/AccountResetPassword.cshtml";
                    public const string AccountResetPasswordEmail = "~/Modules/Membership/Account/ResetPassword/AccountResetPasswordEmail.cshtml";
                }

                public static class SignUp
                {
                    public const string AccountActivateEmail = "~/Modules/Membership/Account/SignUp/AccountActivateEmail.cshtml";
                    public const string AccountSignUp = "~/Modules/Membership/Account/SignUp/AccountSignUp.cshtml";
                }
            }

        }

        public static class RPE
        {
            public static class Rpe
            {
                public const string RpeIndex = "~/Modules/RPE/Rpe/RpeIndex.cshtml";
            }

        }

        public static class SandCTrainerList
        {
            public static class SandCoachList
            {
                public const string SandCoachListIndex = "~/Modules/SandCTrainerList/SandCoachList/SandCoachListIndex.cshtml";
            }

        }

        public static class Setup
        {
            public static class AthletePosition
            {
                public const string AthletePositionIndex = "~/Modules/Setup/AthletePosition/AthletePositionIndex.cshtml";
            }

            public static class AthleteTesting
            {
                public const string AthleteTestingIndex = "~/Modules/Setup/AthleteTesting/AthleteTestingIndex.cshtml";
            }

            public static class AthleteTestingLkUp
            {
                public const string AthleteTestingLkUpIndex = "~/Modules/Setup/AthleteTestingLkUp/AthleteTestingLkUpIndex.cshtml";
            }

            public static class AthleteYear
            {
                public const string AthleteYearIndex = "~/Modules/Setup/AthleteYear/AthleteYearIndex.cshtml";
            }

            public static class Gender
            {
                public const string GenderIndex = "~/Modules/Setup/Gender/GenderIndex.cshtml";
            }

            public static class InjStatusAnswerLkup
            {
                public const string InjStatusAnswerLkupIndex = "~/Modules/Setup/InjStatusAnswerLkup/InjStatusAnswerLkupIndex.cshtml";
            }

            public static class Injury
            {
                public const string InjuryIndex = "~/Modules/Setup/Injury/InjuryIndex.cshtml";
            }

            public static class InjuryStatus
            {
                public const string InjuryStatusIndex = "~/Modules/Setup/InjuryStatus/InjuryStatusIndex.cshtml";
            }

            public static class Physio
            {
                public const string PhysioIndex = "~/Modules/Setup/Physio/PhysioIndex.cshtml";
            }

            public static class Sports
            {
                public const string SportsIndex = "~/Modules/Setup/Sports/SportsIndex.cshtml";
            }

            public static class Trainers
            {
                public const string TrainersIndex = "~/Modules/Setup/Trainers/TrainersIndex.cshtml";
            }

            public static class TrainersList
            {
                public const string TrainersListIndex = "~/Modules/Setup/TrainersList/TrainersListIndex.cshtml";
            }

            public static class YesNoAnswerLkup
            {
                public const string YesNoAnswerLkupIndex = "~/Modules/Setup/YesNoAnswerLkup/YesNoAnswerLkupIndex.cshtml";
            }
        }

        public static class Shared
        {
            public const string _Layout = "~/Views/Shared/_Layout.cshtml";
            public const string _LayoutHead = "~/Views/Shared/_LayoutHead.cshtml";
            public const string _LayoutNoNavigation = "~/Views/Shared/_LayoutNoNavigation.cshtml";
            public const string Error = "~/Views/Shared/Error.cshtml";
            public const string LeftNavigation = "~/Views/Shared/LeftNavigation.cshtml";
        }

        public static class SportsTrainerList
        {
            public static class SportsCoachList
            {
                public const string SportsCoachListIndex = "~/Modules/SportsTrainerList/SportsCoachList/SportsCoachListIndex.cshtml";
            }
        }
    }
}

