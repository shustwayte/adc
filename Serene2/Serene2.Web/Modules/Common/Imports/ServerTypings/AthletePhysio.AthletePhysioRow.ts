﻿namespace Serene2.AthletePhysio {
    export interface AthletePhysioRow {
        Id?: number;
        AthleteId?: number;
        EntryDate?: string;
        PhysioId?: number;
        InjuryId?: number;
        InjuryStatusId?: number;
        Notes?: string;
        PhysioPlan?: string;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        Physio?: string;
        PhysioUsername?: string;
        Injury?: string;
        InjuryStatus?: string;
        DisplayName?: string;
        history?: string;
        previousHistory?: string;
        objectiveAssessment?: string;
        managementPlan?: string;
        Sport?: string;
        EntryDateLkup?: string;
        conClinicRvw?: string;
    }

    export namespace AthletePhysioRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Notes';
        export const localTextPrefix = 'AthletePhysio.AthletePhysio';
        export const lookupKey = 'EntryDate';

        export function getLookup(): Q.Lookup<AthletePhysioRow> {
            return Q.getLookup<AthletePhysioRow>('EntryDate');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const AthleteId: string;
            export declare const EntryDate: string;
            export declare const PhysioId: string;
            export declare const InjuryId: string;
            export declare const InjuryStatusId: string;
            export declare const Notes: string;
            export declare const PhysioPlan: string;
            export declare const AthleteLive: string;
            export declare const AthleteUsername: string;
            export declare const AthleteStuId: string;
            export declare const AthleteFirstName: string;
            export declare const AthleteLastName: string;
            export declare const AthleteSportId: string;
            export declare const AthleteTrainerId: string;
            export declare const AthleteGenderId: string;
            export declare const AthleteYearId: string;
            export declare const Physio: string;
            export declare const PhysioUsername: string;
            export declare const Injury: string;
            export declare const InjuryStatus: string;
            export declare const DisplayName: string;
            export declare const history: string;
            export declare const previousHistory: string;
            export declare const objectiveAssessment: string;
            export declare const managementPlan: string;
            export declare const Sport: string;
            export declare const EntryDateLkup: string;
            export declare const conClinicRvw: string;
        }

        ['Id', 'AthleteId', 'EntryDate', 'PhysioId', 'InjuryId', 'InjuryStatusId', 'Notes', 'PhysioPlan', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'Physio', 'PhysioUsername', 'Injury', 'InjuryStatus', 'DisplayName', 'history', 'previousHistory', 'objectiveAssessment', 'managementPlan', 'Sport', 'EntryDateLkup', 'conClinicRvw'].forEach(x => (<any>Fields)[x] = x);
    }
}

