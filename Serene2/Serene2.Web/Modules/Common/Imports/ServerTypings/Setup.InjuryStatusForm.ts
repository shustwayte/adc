﻿namespace Serene2.Setup {
    export class InjuryStatusForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.InjuryStatus';

    }

    export interface InjuryStatusForm {
        InjuryStatus: Serenity.StringEditor;
    }

    [['InjuryStatus', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(InjuryStatusForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

