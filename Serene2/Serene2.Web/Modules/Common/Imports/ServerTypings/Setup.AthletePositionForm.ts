﻿namespace Serene2.Setup {
    export class AthletePositionForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.AthletePosition';

    }

    export interface AthletePositionForm {
        Position: Serenity.StringEditor;
    }

    [['Position', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(AthletePositionForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

