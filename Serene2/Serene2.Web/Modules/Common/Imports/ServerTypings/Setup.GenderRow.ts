﻿namespace Serene2.Setup {
    export interface GenderRow {
        GenderId?: number;
        Gender?: string;
    }

    export namespace GenderRow {
        export const idProperty = 'GenderId';
        export const nameProperty = 'Gender';
        export const localTextPrefix = 'Setup.Gender';
        export const lookupKey = 'WebApp.Gender';

        export function getLookup(): Q.Lookup<GenderRow> {
            return Q.getLookup<GenderRow>('WebApp.Gender');
        }

        export namespace Fields {
            export declare const GenderId: string;
            export declare const Gender: string;
        }

        ['GenderId', 'Gender'].forEach(x => (<any>Fields)[x] = x);
    }
}

