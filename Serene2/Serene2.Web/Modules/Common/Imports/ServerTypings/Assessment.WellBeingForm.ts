﻿namespace Serene2.Assessment {
    export class WellBeingForm extends Serenity.PrefixedContext {
        static formKey = 'Assessment.WellBeing';

    }

    export interface WellBeingForm {
        WeightKg: Serenity.DecimalEditor;
        AthleteId: Serenity.IntegerEditor;
        dateof: Serenity.DateEditor;
        InjStatus: Serenity.LookupEditor;
        Sleep: Serenity.LookupEditor;
        ChangeDailyRoutine: Serenity.LookupEditor;
        SoughtMedicalAttention: Serenity.LookupEditor;
        TightnessPain: Serenity.LookupEditor;
        AcademicStress: Serenity.LookupEditor;
    }

    [['WeightKg', () => Serenity.DecimalEditor], ['AthleteId', () => Serenity.IntegerEditor], ['dateof', () => Serenity.DateEditor], ['InjStatus', () => Serenity.LookupEditor], ['Sleep', () => Serenity.LookupEditor], ['ChangeDailyRoutine', () => Serenity.LookupEditor], ['SoughtMedicalAttention', () => Serenity.LookupEditor], ['TightnessPain', () => Serenity.LookupEditor], ['AcademicStress', () => Serenity.LookupEditor]].forEach(x => Object.defineProperty(WellBeingForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

