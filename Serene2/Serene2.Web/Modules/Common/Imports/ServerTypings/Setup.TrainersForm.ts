﻿namespace Serene2.Setup {
    export class TrainersForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.Trainers';

    }

    export interface TrainersForm {
        Trainer: Serenity.StringEditor;
        username: Serenity.StringEditor;
    }

    [['Trainer', () => Serenity.StringEditor], ['username', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(TrainersForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

