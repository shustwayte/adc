﻿namespace Serene2.Setup {
    export interface TrainersRow {
        TrainerId?: number;
        Trainer?: string;
        username?: string;
    }

    export namespace TrainersRow {
        export const idProperty = 'TrainerId';
        export const nameProperty = 'Trainer';
        export const localTextPrefix = 'Setup.Trainers';
        export const lookupKey = 'WebApp.Trainers';

        export function getLookup(): Q.Lookup<TrainersRow> {
            return Q.getLookup<TrainersRow>('WebApp.Trainers');
        }

        export namespace Fields {
            export declare const TrainerId: string;
            export declare const Trainer: string;
            export declare const username: string;
        }

        ['TrainerId', 'Trainer', 'username'].forEach(x => (<any>Fields)[x] = x);
    }
}

