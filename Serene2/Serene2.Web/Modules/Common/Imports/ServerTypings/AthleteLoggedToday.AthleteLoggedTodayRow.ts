﻿namespace Serene2.AthleteLoggedToday {
    export interface AthleteLoggedTodayRow {
        Id?: number;
        Athleteid?: number;
        Username?: string;
        AthleteLoggedAssessmentToday?: number;
        AthleteidLive?: number;
        AthleteidUsername?: string;
        AthleteidStuId?: string;
        AthleteidFirstName?: string;
        AthleteidLastName?: string;
        AthleteidSportId?: number;
        AthleteidTrainerId?: number;
        AthleteidGenderId?: number;
        AthleteidYearId?: number;
        recentAssessmentPass?: number;
        attendence?: number;
        AthleteLoggedRPEToday?: number;
    }

    export namespace AthleteLoggedTodayRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Username';
        export const localTextPrefix = 'AthleteLoggedToday.AthleteLoggedToday';

        export namespace Fields {
            export declare const Id: string;
            export declare const Athleteid: string;
            export declare const Username: string;
            export declare const AthleteLoggedAssessmentToday: string;
            export declare const AthleteidLive: string;
            export declare const AthleteidUsername: string;
            export declare const AthleteidStuId: string;
            export declare const AthleteidFirstName: string;
            export declare const AthleteidLastName: string;
            export declare const AthleteidSportId: string;
            export declare const AthleteidTrainerId: string;
            export declare const AthleteidGenderId: string;
            export declare const AthleteidYearId: string;
            export declare const recentAssessmentPass: string;
            export declare const attendence: string;
            export declare const AthleteLoggedRPEToday: string;
        }

        ['Id', 'Athleteid', 'Username', 'AthleteLoggedAssessmentToday', 'AthleteidLive', 'AthleteidUsername', 'AthleteidStuId', 'AthleteidFirstName', 'AthleteidLastName', 'AthleteidSportId', 'AthleteidTrainerId', 'AthleteidGenderId', 'AthleteidYearId', 'recentAssessmentPass', 'attendence', 'AthleteLoggedRPEToday'].forEach(x => (<any>Fields)[x] = x);
    }
}

