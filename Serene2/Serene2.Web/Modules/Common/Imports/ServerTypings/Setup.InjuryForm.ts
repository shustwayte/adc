﻿namespace Serene2.Setup {
    export class InjuryForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.Injury';

    }

    export interface InjuryForm {
        Injury: Serenity.StringEditor;
    }

    [['Injury', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(InjuryForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

