﻿namespace Serene2.Athlete {
    export class AthleteForm extends Serenity.PrefixedContext {
        static formKey = 'Athlete.Athlete';

    }

    export interface AthleteForm {
        FirstName: Serenity.StringEditor;
        LastName: Serenity.StringEditor;
        SportId: Serenity.LookupEditor;
        positionId: Serenity.LookupEditor;
        YearId: Serenity.LookupEditor;
        Live: Serenity.BooleanEditor;
        Trainers: Serenity.LookupEditor;
        Attendence: Serenity.IntegerEditor;
        AttendToday: Serenity.StringEditor;
        Username: Serenity.StringEditor;
        StuId: Serenity.StringEditor;
        GenderId: Serenity.LookupEditor;
        UserImage: Serenity.ImageUploadEditor;
        ManualImage: Serenity.ImageUploadEditor;
    }

    [['FirstName', () => Serenity.StringEditor], ['LastName', () => Serenity.StringEditor], ['SportId', () => Serenity.LookupEditor], ['positionId', () => Serenity.LookupEditor], ['YearId', () => Serenity.LookupEditor], ['Live', () => Serenity.BooleanEditor], ['Trainers', () => Serenity.LookupEditor], ['Attendence', () => Serenity.IntegerEditor], ['AttendToday', () => Serenity.StringEditor], ['Username', () => Serenity.StringEditor], ['StuId', () => Serenity.StringEditor], ['GenderId', () => Serenity.LookupEditor], ['UserImage', () => Serenity.ImageUploadEditor], ['ManualImage', () => Serenity.ImageUploadEditor]].forEach(x => Object.defineProperty(AthleteForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

