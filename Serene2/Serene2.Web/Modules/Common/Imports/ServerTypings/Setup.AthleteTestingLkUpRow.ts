﻿namespace Serene2.Setup {
    export interface AthleteTestingLkUpRow {
        Id?: number;
        Test1Label?: string;
        Test2Label?: string;
        Test3Label?: string;
        Test4Label?: string;
        Test5Label?: string;
        SportId?: number;
        Sport?: string;
    }

    export namespace AthleteTestingLkUpRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Test1Label';
        export const localTextPrefix = 'Setup.AthleteTestingLkUp';

        export namespace Fields {
            export declare const Id: string;
            export declare const Test1Label: string;
            export declare const Test2Label: string;
            export declare const Test3Label: string;
            export declare const Test4Label: string;
            export declare const Test5Label: string;
            export declare const SportId: string;
            export declare const Sport: string;
        }

        ['Id', 'Test1Label', 'Test2Label', 'Test3Label', 'Test4Label', 'Test5Label', 'SportId', 'Sport'].forEach(x => (<any>Fields)[x] = x);
    }
}

