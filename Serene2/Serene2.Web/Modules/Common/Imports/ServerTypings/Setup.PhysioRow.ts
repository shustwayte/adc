﻿namespace Serene2.Setup {
    export interface PhysioRow {
        Id?: number;
        Physio?: string;
        Username?: string;
    }

    export namespace PhysioRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Physio';
        export const localTextPrefix = 'Setup.Physio';
        export const lookupKey = 'WebApp.Physio';

        export function getLookup(): Q.Lookup<PhysioRow> {
            return Q.getLookup<PhysioRow>('WebApp.Physio');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Physio: string;
            export declare const Username: string;
        }

        ['Id', 'Physio', 'Username'].forEach(x => (<any>Fields)[x] = x);
    }
}

