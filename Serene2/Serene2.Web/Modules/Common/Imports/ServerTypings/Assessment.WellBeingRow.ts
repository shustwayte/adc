﻿namespace Serene2.Assessment {
    export interface WellBeingRow {
        Id?: number;
        AthleteId?: number;
        InjStatus?: number;
        WeightKg?: number;
        Sleep?: number;
        ChangeDailyRoutine?: number;
        SoughtMedicalAttention?: number;
        TightnessPain?: number;
        AcademicStress?: number;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        InjStatusAnswer?: string;
        SleepAnswer?: string;
        ChangeDailyRoutineAnswer?: string;
        SoughtMedicalAttentionAnswer?: string;
        TightnessPainAnswer?: string;
        AcademicStressAnswer?: string;
        DisplayName?: string;
        dateof?: string;
        dateofdisplay?: string;
        Status?: string;
        Sport?: string;
        Trainer?: string;
        Year?: string;
    }

    export namespace WellBeingRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'Assessment.WellBeing';
        export const lookupKey = 'WebApp.Assessment';

        export function getLookup(): Q.Lookup<WellBeingRow> {
            return Q.getLookup<WellBeingRow>('WebApp.Assessment');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const AthleteId: string;
            export declare const InjStatus: string;
            export declare const WeightKg: string;
            export declare const Sleep: string;
            export declare const ChangeDailyRoutine: string;
            export declare const SoughtMedicalAttention: string;
            export declare const TightnessPain: string;
            export declare const AcademicStress: string;
            export declare const AthleteLive: string;
            export declare const AthleteUsername: string;
            export declare const AthleteStuId: string;
            export declare const AthleteFirstName: string;
            export declare const AthleteLastName: string;
            export declare const AthleteSportId: string;
            export declare const AthleteTrainerId: string;
            export declare const AthleteGenderId: string;
            export declare const AthleteYearId: string;
            export declare const InjStatusAnswer: string;
            export declare const SleepAnswer: string;
            export declare const ChangeDailyRoutineAnswer: string;
            export declare const SoughtMedicalAttentionAnswer: string;
            export declare const TightnessPainAnswer: string;
            export declare const AcademicStressAnswer: string;
            export declare const DisplayName: string;
            export declare const dateof: string;
            export declare const dateofdisplay: string;
            export declare const Status: string;
            export declare const Sport: string;
            export declare const Trainer: string;
            export declare const Year: string;
        }

        ['Id', 'AthleteId', 'InjStatus', 'WeightKg', 'Sleep', 'ChangeDailyRoutine', 'SoughtMedicalAttention', 'TightnessPain', 'AcademicStress', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'InjStatusAnswer', 'SleepAnswer', 'ChangeDailyRoutineAnswer', 'SoughtMedicalAttentionAnswer', 'TightnessPainAnswer', 'AcademicStressAnswer', 'DisplayName', 'dateof', 'dateofdisplay', 'Status', 'Sport', 'Trainer', 'Year'].forEach(x => (<any>Fields)[x] = x);
    }
}

