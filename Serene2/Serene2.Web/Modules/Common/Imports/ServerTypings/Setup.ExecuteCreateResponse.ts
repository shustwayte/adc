﻿namespace Serene2.Setup {
    export interface ExecuteCreateResponse extends Serenity.ServiceResponse {
        outcome?: string;
        ErrorList?: string[];
    }
}

