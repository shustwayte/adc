﻿namespace Serene2.AthleteLoggedToday {
    export class AthleteLoggedTodayForm extends Serenity.PrefixedContext {
        static formKey = 'AthleteLoggedToday.AthleteLoggedToday';

    }

    export interface AthleteLoggedTodayForm {
        Athleteid: Serenity.IntegerEditor;
        Username: Serenity.StringEditor;
        AthleteLoggedAssessmentToday: Serenity.IntegerEditor;
    }

    [['Athleteid', () => Serenity.IntegerEditor], ['Username', () => Serenity.StringEditor], ['AthleteLoggedAssessmentToday', () => Serenity.IntegerEditor]].forEach(x => Object.defineProperty(AthleteLoggedTodayForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

