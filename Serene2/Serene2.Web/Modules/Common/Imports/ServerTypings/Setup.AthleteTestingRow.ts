﻿namespace Serene2.Setup {
    export interface AthleteTestingRow {
        Id?: number;
        AthleteId?: number;
        Test1Label?: string;
        Test1Result?: number;
        Test2Label?: string;
        Test2Result?: number;
        Test3Label?: string;
        Test3Result?: number;
        Test4Label?: string;
        Test4Result?: number;
        Test5Label?: string;
        Test5Result?: number;
        OnDate?: string;
        SportId?: number;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        AthleteUserImage?: string;
        AthleteManualImage?: string;
        AthletePositionId?: number;
        Sport?: string;
        SportSessionsPerWeek?: number;
        DisplayName?: string;
        TermDisplay?: string;
        testID?: number;
    }

    export namespace AthleteTestingRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Test1Label';
        export const localTextPrefix = 'Setup.AthleteTesting';
        export const lookupKey = 'WebApp.AthleteTesting';

        export function getLookup(): Q.Lookup<AthleteTestingRow> {
            return Q.getLookup<AthleteTestingRow>('WebApp.AthleteTesting');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const AthleteId: string;
            export declare const Test1Label: string;
            export declare const Test1Result: string;
            export declare const Test2Label: string;
            export declare const Test2Result: string;
            export declare const Test3Label: string;
            export declare const Test3Result: string;
            export declare const Test4Label: string;
            export declare const Test4Result: string;
            export declare const Test5Label: string;
            export declare const Test5Result: string;
            export declare const OnDate: string;
            export declare const SportId: string;
            export declare const AthleteLive: string;
            export declare const AthleteUsername: string;
            export declare const AthleteStuId: string;
            export declare const AthleteFirstName: string;
            export declare const AthleteLastName: string;
            export declare const AthleteSportId: string;
            export declare const AthleteTrainerId: string;
            export declare const AthleteGenderId: string;
            export declare const AthleteYearId: string;
            export declare const AthleteUserImage: string;
            export declare const AthleteManualImage: string;
            export declare const AthletePositionId: string;
            export declare const Sport: string;
            export declare const SportSessionsPerWeek: string;
            export declare const DisplayName: string;
            export declare const TermDisplay: string;
            export declare const testID: string;
        }

        ['Id', 'AthleteId', 'Test1Label', 'Test1Result', 'Test2Label', 'Test2Result', 'Test3Label', 'Test3Result', 'Test4Label', 'Test4Result', 'Test5Label', 'Test5Result', 'OnDate', 'SportId', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'AthleteUserImage', 'AthleteManualImage', 'AthletePositionId', 'Sport', 'SportSessionsPerWeek', 'DisplayName', 'TermDisplay', 'testID'].forEach(x => (<any>Fields)[x] = x);
    }
}

