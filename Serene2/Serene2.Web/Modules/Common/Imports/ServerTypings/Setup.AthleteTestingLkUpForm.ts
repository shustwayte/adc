﻿namespace Serene2.Setup {
    export class AthleteTestingLkUpForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.AthleteTestingLkUp';

    }

    export interface AthleteTestingLkUpForm {
        Test1Label: Serenity.StringEditor;
        Test2Label: Serenity.StringEditor;
        Test3Label: Serenity.StringEditor;
        Test4Label: Serenity.StringEditor;
        Test5Label: Serenity.StringEditor;
        SportId: Serenity.LookupEditor;
    }

    [['Test1Label', () => Serenity.StringEditor], ['Test2Label', () => Serenity.StringEditor], ['Test3Label', () => Serenity.StringEditor], ['Test4Label', () => Serenity.StringEditor], ['Test5Label', () => Serenity.StringEditor], ['SportId', () => Serenity.LookupEditor]].forEach(x => Object.defineProperty(AthleteTestingLkUpForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

