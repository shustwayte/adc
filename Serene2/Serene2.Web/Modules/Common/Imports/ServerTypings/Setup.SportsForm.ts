﻿namespace Serene2.Setup {
    export class SportsForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.Sports';

    }

    export interface SportsForm {
        Sport: Serenity.StringEditor;
        SessionsPerWeek: Serenity.IntegerEditor;
        Trainersport: Serenity.LookupEditor;
        TrainersportCoach: Serenity.LookupEditor;
    }

    [['Sport', () => Serenity.StringEditor], ['SessionsPerWeek', () => Serenity.IntegerEditor], ['Trainersport', () => Serenity.LookupEditor], ['TrainersportCoach', () => Serenity.LookupEditor]].forEach(x => Object.defineProperty(SportsForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

