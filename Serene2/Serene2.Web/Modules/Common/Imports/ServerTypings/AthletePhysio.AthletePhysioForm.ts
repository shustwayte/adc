﻿namespace Serene2.AthletePhysio {
    export class AthletePhysioForm extends Serenity.PrefixedContext {
        static formKey = 'AthletePhysio.AthletePhysio';

    }

    export interface AthletePhysioForm {
        AthleteId: Serenity.LookupEditor;
        AthleteFirstName: Serenity.StringEditor;
        AthleteLastName: Serenity.StringEditor;
        EntryDate: Serenity.DateEditor;
        PhysioId: Serenity.LookupEditor;
        InjuryId: Serenity.LookupEditor;
        InjuryStatusId: Serenity.LookupEditor;
        Notes: Serenity.TextAreaEditor;
        PhysioPlan: Serenity.TextAreaEditor;
        conClinicRvw: Serenity.TextAreaEditor;
        history: Serenity.TextAreaEditor;
        previousHistory: Serenity.TextAreaEditor;
        objectiveAssessment: Serenity.TextAreaEditor;
        managementPlan: Serenity.TextAreaEditor;
    }

    [['AthleteId', () => Serenity.LookupEditor], ['AthleteFirstName', () => Serenity.StringEditor], ['AthleteLastName', () => Serenity.StringEditor], ['EntryDate', () => Serenity.DateEditor], ['PhysioId', () => Serenity.LookupEditor], ['InjuryId', () => Serenity.LookupEditor], ['InjuryStatusId', () => Serenity.LookupEditor], ['Notes', () => Serenity.TextAreaEditor], ['PhysioPlan', () => Serenity.TextAreaEditor], ['conClinicRvw', () => Serenity.TextAreaEditor], ['history', () => Serenity.TextAreaEditor], ['previousHistory', () => Serenity.TextAreaEditor], ['objectiveAssessment', () => Serenity.TextAreaEditor], ['managementPlan', () => Serenity.TextAreaEditor]].forEach(x => Object.defineProperty(AthletePhysioForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

