﻿namespace Serene2.RPE {
    export class RpeForm extends Serenity.PrefixedContext {
        static formKey = 'RPE.Rpe';

    }

    export interface RpeForm {
        AthleteId: Serenity.IntegerEditor;
        RpEvAlue: Serenity.IntegerEditor;
        Daterecorded: Serenity.DateEditor;
        AthleteUsername: Serenity.StringEditor;
    }

    [['AthleteId', () => Serenity.IntegerEditor], ['RpEvAlue', () => Serenity.IntegerEditor], ['Daterecorded', () => Serenity.DateEditor], ['AthleteUsername', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(RpeForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

