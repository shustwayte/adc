﻿namespace Serene2.Athlete {
    export interface AthleteRow {
        Id?: number;
        Live?: boolean;
        Username?: string;
        StuId?: string;
        FirstName?: string;
        LastName?: string;
        SportId?: number;
        TrainerId?: number;
        Trainers?: number[];
        GenderId?: number;
        Sport?: string;
        Trainer?: string;
        Gender?: string;
        YearId?: number;
        Year?: string;
        DisplayName?: string;
        UserImage?: string;
        Attendence?: number;
        lastTwoAssessment?: string;
        lastTwoAssessmentId?: AthleteFlagStatus;
        AthleteLoggedAssessmentToday?: AthleteAttendStatus;
        lastassessment?: string;
        AttendToday?: string;
        ManualImage?: string;
        positionId?: number;
        Position?: string;
        Paid?: number;
    }

    export namespace AthleteRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Username';
        export const localTextPrefix = 'Athlete.Athlete';
        export const lookupKey = 'WebApp.Athlete';

        export function getLookup(): Q.Lookup<AthleteRow> {
            return Q.getLookup<AthleteRow>('WebApp.Athlete');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Live: string;
            export declare const Username: string;
            export declare const StuId: string;
            export declare const FirstName: string;
            export declare const LastName: string;
            export declare const SportId: string;
            export declare const TrainerId: string;
            export declare const Trainers: string;
            export declare const GenderId: string;
            export declare const Sport: string;
            export declare const Trainer: string;
            export declare const Gender: string;
            export declare const YearId: string;
            export declare const Year: string;
            export declare const DisplayName: string;
            export declare const UserImage: string;
            export declare const Attendence: string;
            export declare const lastTwoAssessment: string;
            export declare const lastTwoAssessmentId: string;
            export declare const AthleteLoggedAssessmentToday: string;
            export declare const lastassessment: string;
            export declare const AttendToday: string;
            export declare const ManualImage: string;
            export declare const positionId: string;
            export declare const Position: string;
            export declare const Paid: string;
        }

        ['Id', 'Live', 'Username', 'StuId', 'FirstName', 'LastName', 'SportId', 'TrainerId', 'Trainers', 'GenderId', 'Sport', 'Trainer', 'Gender', 'YearId', 'Year', 'DisplayName', 'UserImage', 'Attendence', 'lastTwoAssessment', 'lastTwoAssessmentId', 'AthleteLoggedAssessmentToday', 'lastassessment', 'AttendToday', 'ManualImage', 'positionId', 'Position', 'Paid'].forEach(x => (<any>Fields)[x] = x);
    }
}

