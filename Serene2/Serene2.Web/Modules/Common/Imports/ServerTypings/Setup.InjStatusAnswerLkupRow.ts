﻿namespace Serene2.Setup {
    export interface InjStatusAnswerLkupRow {
        Id?: number;
        Answer?: string;
    }

    export namespace InjStatusAnswerLkupRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Answer';
        export const localTextPrefix = 'Setup.InjStatusAnswerLkup';
        export const lookupKey = 'WebApp.injStatus';

        export function getLookup(): Q.Lookup<InjStatusAnswerLkupRow> {
            return Q.getLookup<InjStatusAnswerLkupRow>('WebApp.injStatus');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Answer: string;
        }

        ['Id', 'Answer'].forEach(x => (<any>Fields)[x] = x);
    }
}

