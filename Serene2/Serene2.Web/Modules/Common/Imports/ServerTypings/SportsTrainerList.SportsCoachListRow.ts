﻿namespace Serene2.SportsTrainerList {
    export interface SportsCoachListRow {
        Id?: number;
        SportId?: number;
        TrainerId?: number;
        Sport?: string;
        SportSessionsPerWeek?: number;
        SportSandccoachId?: number;
        SportScoachId?: number;
    }

    export namespace SportsCoachListRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'Setup.SportsCoachList';
        export const lookupKey = 'WebApp.SportsTrainerList';

        export function getLookup(): Q.Lookup<SportsCoachListRow> {
            return Q.getLookup<SportsCoachListRow>('WebApp.SportsTrainerList');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const SportId: string;
            export declare const TrainerId: string;
            export declare const Sport: string;
            export declare const SportSessionsPerWeek: string;
            export declare const SportSandccoachId: string;
            export declare const SportScoachId: string;
        }

        ['Id', 'SportId', 'TrainerId', 'Sport', 'SportSessionsPerWeek', 'SportSandccoachId', 'SportScoachId'].forEach(x => (<any>Fields)[x] = x);
    }
}

