﻿namespace Serene2.Setup {
    export class YesNoAnswerLkupForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.YesNoAnswerLkup';

    }

    export interface YesNoAnswerLkupForm {
        Answer: Serenity.StringEditor;
    }

    [['Answer', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(YesNoAnswerLkupForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

