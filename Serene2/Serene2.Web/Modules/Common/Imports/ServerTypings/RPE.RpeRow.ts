﻿namespace Serene2.RPE {
    export interface RpeRow {
        Id?: number;
        AthleteId?: number;
        RpEvAlue?: number;
        Daterecorded?: string;
        AthleteUsername?: string;
    }

    export namespace RpeRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'RPE.Rpe';

        export namespace Fields {
            export declare const Id: string;
            export declare const AthleteId: string;
            export declare const RpEvAlue: string;
            export declare const Daterecorded: string;
            export declare const AthleteUsername: string;
        }

        ['Id', 'AthleteId', 'RpEvAlue', 'Daterecorded', 'AthleteUsername'].forEach(x => (<any>Fields)[x] = x);
    }
}

