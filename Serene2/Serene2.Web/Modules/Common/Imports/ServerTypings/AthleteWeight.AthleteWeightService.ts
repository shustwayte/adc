﻿namespace Serene2.AthleteWeight {
    export namespace AthleteWeightService {
        export const baseUrl = 'AthleteWeight/AthleteWeight';

        export declare function List(request: AthleteWeightListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteWeightRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export namespace Methods {
            export declare const List: string;
        }

        ['List'].forEach(x => {
            (<any>AthleteWeightService)[x] = function (r, s, o) { return Q.serviceRequest(baseUrl + '/' + x, r, s, o); };
            (<any>Methods)[x] = baseUrl + '/' + x;
        });
    }
}

