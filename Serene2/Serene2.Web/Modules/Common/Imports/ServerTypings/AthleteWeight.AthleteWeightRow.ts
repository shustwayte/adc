﻿namespace Serene2.AthleteWeight {
    export interface AthleteWeightRow {
        id?: number;
        avgWeight?: number;
        datefor?: string;
    }

    export namespace AthleteWeightRow {
        export const idProperty = 'id';
        export const nameProperty = 'datefor';
        export const localTextPrefix = 'AthleteWeight.AthleteWeight';

        export namespace Fields {
            export declare const id: string;
            export declare const avgWeight: string;
            export declare const datefor: string;
        }

        ['id', 'avgWeight', 'datefor'].forEach(x => (<any>Fields)[x] = x);
    }
}

