﻿namespace Serene2.Audit {
    export class AuditLogForm extends Serenity.PrefixedContext {
        static formKey = 'Audit.AuditLog';

    }

    export interface AuditLogForm {
        Time: Serenity.StringEditor;
        DisplayName: Serenity.StringEditor;
        Action: Serenity.StringEditor;
        ChangesDisplayForm: Serenity.TextAreaEditor;
    }

    [['Time', () => Serenity.StringEditor], ['DisplayName', () => Serenity.StringEditor], ['Action', () => Serenity.StringEditor], ['ChangesDisplayForm', () => Serenity.TextAreaEditor]].forEach(x => Object.defineProperty(AuditLogForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

