﻿namespace Serene2.Setup {
    export interface InjuryRow {
        Id?: number;
        Injury?: string;
    }

    export namespace InjuryRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Injury';
        export const localTextPrefix = 'Setup.Injury';
        export const lookupKey = 'WebApp.Injury';

        export function getLookup(): Q.Lookup<InjuryRow> {
            return Q.getLookup<InjuryRow>('WebApp.Injury');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Injury: string;
        }

        ['Id', 'Injury'].forEach(x => (<any>Fields)[x] = x);
    }
}

