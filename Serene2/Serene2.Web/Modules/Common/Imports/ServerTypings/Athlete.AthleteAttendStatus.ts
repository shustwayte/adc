﻿namespace Serene2.Athlete {
    export enum AthleteAttendStatus {
        Missing = 0,
        Attend = 1
    }
    Serenity.Decorators.registerEnum(AthleteAttendStatus, 'Athlete.AthleteAttendStatus');
}

