﻿namespace Serene2.Setup {
    export class TrainersListForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.TrainersList';

    }

    export interface TrainersListForm {
        AthId: Serenity.IntegerEditor;
        TrainerId: Serenity.IntegerEditor;
    }

    [['AthId', () => Serenity.IntegerEditor], ['TrainerId', () => Serenity.IntegerEditor]].forEach(x => Object.defineProperty(TrainersListForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

