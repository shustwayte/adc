﻿namespace Serene2.Audit {
    export interface AuditLogRow {
        Id?: number;
        UserId?: number;
        UserName?: string;
        Action?: string;
        ChangedOn?: string;
        TableName?: string;
        RowId?: number;
        Module?: string;
        Page?: string;
        Changes?: string;
        ChangesDisplayForm?: string;
        DisplayName?: string;
        Time?: string;
    }

    export namespace AuditLogRow {
        export const idProperty = 'Id';
        export const nameProperty = 'UserName';
        export const localTextPrefix = 'Audit.AuditLog';

        export namespace Fields {
            export declare const Id: string;
            export declare const UserId: string;
            export declare const UserName: string;
            export declare const Action: string;
            export declare const ChangedOn: string;
            export declare const TableName: string;
            export declare const RowId: string;
            export declare const Module: string;
            export declare const Page: string;
            export declare const Changes: string;
            export declare const ChangesDisplayForm: string;
            export declare const DisplayName: string;
            export declare const Time: string;
        }

        ['Id', 'UserId', 'UserName', 'Action', 'ChangedOn', 'TableName', 'RowId', 'Module', 'Page', 'Changes', 'ChangesDisplayForm', 'DisplayName', 'Time'].forEach(x => (<any>Fields)[x] = x);
    }
}

