﻿namespace Serene2.Setup {
    export interface AthletePositionRow {
        Id?: number;
        Position?: string;
    }

    export namespace AthletePositionRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Position';
        export const localTextPrefix = 'Setup.AthletePosition';
        export const lookupKey = 'WebApp.Position';

        export function getLookup(): Q.Lookup<AthletePositionRow> {
            return Q.getLookup<AthletePositionRow>('WebApp.Position');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Position: string;
        }

        ['Id', 'Position'].forEach(x => (<any>Fields)[x] = x);
    }
}

