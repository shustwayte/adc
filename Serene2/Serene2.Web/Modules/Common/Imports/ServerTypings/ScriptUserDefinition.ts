﻿namespace Serene2 {
    export interface ScriptUserDefinition {
        Username?: string;
        DisplayName?: string;
        IsAdmin?: boolean;
        isAthlete?: boolean;
        AthleteID?: string;
        ID?: string;
        Permissions?: { [key: string]: boolean };
        Roles?: string[];
        AthleteAlreadyLoggedAss?: string;
        AthleteAlreadyLoggedRPE?: string;
        recentAssessmentPass?: string;
    }
}

