﻿namespace Serene2.AthleteTesting {
    export class CreateTestForm extends Serenity.PrefixedContext {
        static formKey = 'AthleteTesting.CreateTest';

    }

    export interface CreateTestForm {
        Sport: Serenity.StringEditor;
    }

    [['Sport', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(CreateTestForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

