﻿namespace Serene2.Setup {
    export interface SportsRow {
        SportId?: number;
        Sport?: string;
        SessionsPerWeek?: number;
        sandccoachId?: number;
        scoachId?: number;
        Trainer?: string;
        TrainerScoach?: string;
        Trainersport?: number[];
        TrainersportCoach?: number[];
    }

    export namespace SportsRow {
        export const idProperty = 'SportId';
        export const nameProperty = 'Sport';
        export const localTextPrefix = 'Setup.Sports';
        export const lookupKey = 'WebApp.Sports';

        export function getLookup(): Q.Lookup<SportsRow> {
            return Q.getLookup<SportsRow>('WebApp.Sports');
        }

        export namespace Fields {
            export declare const SportId: string;
            export declare const Sport: string;
            export declare const SessionsPerWeek: string;
            export declare const sandccoachId: string;
            export declare const scoachId: string;
            export declare const Trainer: string;
            export declare const TrainerScoach: string;
            export declare const Trainersport: string;
            export declare const TrainersportCoach: string;
        }

        ['SportId', 'Sport', 'SessionsPerWeek', 'sandccoachId', 'scoachId', 'Trainer', 'TrainerScoach', 'Trainersport', 'TrainersportCoach'].forEach(x => (<any>Fields)[x] = x);
    }
}

