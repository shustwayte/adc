﻿namespace Serene2.SandCTrainerList {
    export class SandCoachListForm extends Serenity.PrefixedContext {
        static formKey = 'SandCTrainerList.SandCoachList';

    }

    export interface SandCoachListForm {
        SportId: Serenity.IntegerEditor;
        TrainerId: Serenity.IntegerEditor;
    }

    [['SportId', () => Serenity.IntegerEditor], ['TrainerId', () => Serenity.IntegerEditor]].forEach(x => Object.defineProperty(SandCoachListForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

