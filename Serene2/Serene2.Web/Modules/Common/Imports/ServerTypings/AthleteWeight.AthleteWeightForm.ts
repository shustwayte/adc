﻿namespace Serene2.AthleteWeight {
    export class AthleteWeightForm extends Serenity.PrefixedContext {
        static formKey = 'AthleteWeight.AthleteWeight';

    }

    export interface AthleteWeightForm {
        avgWeight: Serenity.DecimalEditor;
        datefor: Serenity.StringEditor;
    }

    [['avgWeight', () => Serenity.DecimalEditor], ['datefor', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(AthleteWeightForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

