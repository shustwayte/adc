﻿namespace Serene2.Setup {
    export class AthleteTestingForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.AthleteTesting';

    }

    export interface AthleteTestingForm {
        DisplayName: Serenity.StringEditor;
        Test1Label: Serenity.StringEditor;
        Test1Result: Serenity.DecimalEditor;
        Test2Label: Serenity.StringEditor;
        Test2Result: Serenity.DecimalEditor;
        Test3Label: Serenity.StringEditor;
        Test3Result: Serenity.DecimalEditor;
        Test4Label: Serenity.StringEditor;
        Test4Result: Serenity.DecimalEditor;
        Test5Label: Serenity.StringEditor;
        Test5Result: Serenity.DecimalEditor;
        Sport: Serenity.StringEditor;
        TermDisplay: Serenity.StringEditor;
    }

    [['DisplayName', () => Serenity.StringEditor], ['Test1Label', () => Serenity.StringEditor], ['Test1Result', () => Serenity.DecimalEditor], ['Test2Label', () => Serenity.StringEditor], ['Test2Result', () => Serenity.DecimalEditor], ['Test3Label', () => Serenity.StringEditor], ['Test3Result', () => Serenity.DecimalEditor], ['Test4Label', () => Serenity.StringEditor], ['Test4Result', () => Serenity.DecimalEditor], ['Test5Label', () => Serenity.StringEditor], ['Test5Result', () => Serenity.DecimalEditor], ['Sport', () => Serenity.StringEditor], ['TermDisplay', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(AthleteTestingForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

