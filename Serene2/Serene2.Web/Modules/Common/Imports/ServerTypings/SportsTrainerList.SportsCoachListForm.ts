﻿namespace Serene2.SportsTrainerList {
    export class SportsCoachListForm extends Serenity.PrefixedContext {
        static formKey = 'SportsTrainerList.SportsCoachList';

    }

    export interface SportsCoachListForm {
        SportId: Serenity.IntegerEditor;
        TrainerId: Serenity.IntegerEditor;
    }

    [['SportId', () => Serenity.IntegerEditor], ['TrainerId', () => Serenity.IntegerEditor]].forEach(x => Object.defineProperty(SportsCoachListForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

