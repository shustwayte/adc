﻿namespace Serene2.Setup {
    export interface YesNoAnswerLkupRow {
        Id?: number;
        Answer?: string;
    }

    export namespace YesNoAnswerLkupRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Answer';
        export const localTextPrefix = 'Setup.YesNoAnswerLkup';
        export const lookupKey = 'WebApp.YesNoAnswer';

        export function getLookup(): Q.Lookup<YesNoAnswerLkupRow> {
            return Q.getLookup<YesNoAnswerLkupRow>('WebApp.YesNoAnswer');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const Answer: string;
        }

        ['Id', 'Answer'].forEach(x => (<any>Fields)[x] = x);
    }
}

