﻿namespace Serene2.AthleteNote {
    export interface AthleteNoteRow {
        Id?: number;
        AthleteId?: number;
        EntryDate?: string;
        Notes?: string;
        Username?: string;
    }

    export namespace AthleteNoteRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Notes';
        export const localTextPrefix = 'AthleteNote.AthleteNote';

        export namespace Fields {
            export declare const Id: string;
            export declare const AthleteId: string;
            export declare const EntryDate: string;
            export declare const Notes: string;
            export declare const Username: string;
        }

        ['Id', 'AthleteId', 'EntryDate', 'Notes', 'Username'].forEach(x => (<any>Fields)[x] = x);
    }
}

