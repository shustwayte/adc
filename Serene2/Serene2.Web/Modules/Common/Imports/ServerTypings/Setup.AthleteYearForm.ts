﻿namespace Serene2.Setup {
    export class AthleteYearForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.AthleteYear';

    }

    export interface AthleteYearForm {
        Year: Serenity.StringEditor;
    }

    [['Year', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(AthleteYearForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

