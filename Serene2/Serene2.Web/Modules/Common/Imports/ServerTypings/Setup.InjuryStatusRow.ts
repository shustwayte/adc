﻿namespace Serene2.Setup {
    export interface InjuryStatusRow {
        Id?: number;
        InjuryStatus?: string;
        rankOrd?: number;
    }

    export namespace InjuryStatusRow {
        export const idProperty = 'Id';
        export const nameProperty = 'InjuryStatus';
        export const localTextPrefix = 'Setup.InjuryStatus';
        export const lookupKey = 'WebApp.InjuryStatus';

        export function getLookup(): Q.Lookup<InjuryStatusRow> {
            return Q.getLookup<InjuryStatusRow>('WebApp.InjuryStatus');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const InjuryStatus: string;
            export declare const rankOrd: string;
        }

        ['Id', 'InjuryStatus', 'rankOrd'].forEach(x => (<any>Fields)[x] = x);
    }
}

