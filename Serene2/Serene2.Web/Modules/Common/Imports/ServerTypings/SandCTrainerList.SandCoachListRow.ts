﻿namespace Serene2.SandCTrainerList {
    export interface SandCoachListRow {
        Id?: number;
        SportId?: number;
        TrainerId?: number;
        Sport?: string;
        SportSessionsPerWeek?: number;
        SportSandccoachId?: number;
        SportScoachId?: number;
    }

    export namespace SandCoachListRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'SandCTrainerList.SandCoachList';

        export namespace Fields {
            export declare const Id: string;
            export declare const SportId: string;
            export declare const TrainerId: string;
            export declare const Sport: string;
            export declare const SportSessionsPerWeek: string;
            export declare const SportSandccoachId: string;
            export declare const SportScoachId: string;
        }

        ['Id', 'SportId', 'TrainerId', 'Sport', 'SportSessionsPerWeek', 'SportSandccoachId', 'SportScoachId'].forEach(x => (<any>Fields)[x] = x);
    }
}

