﻿namespace Serene2.Setup {
    export interface TrainersListRow {
        Id?: number;
        AthId?: number;
        TrainerId?: number;
        AthLive?: number;
        AthUsername?: string;
        AthStuId?: string;
        AthFirstName?: string;
        AthLastName?: string;
        AthSportId?: number;
        AthTrainerId?: number;
        AthGenderId?: number;
        AthYearId?: number;
        AthUserImage?: string;
    }

    export namespace TrainersListRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'Setup.TrainersList';
        export const lookupKey = 'WebApp.TrainersList';

        export function getLookup(): Q.Lookup<TrainersListRow> {
            return Q.getLookup<TrainersListRow>('WebApp.TrainersList');
        }

        export namespace Fields {
            export declare const Id: string;
            export declare const AthId: string;
            export declare const TrainerId: string;
            export declare const AthLive: string;
            export declare const AthUsername: string;
            export declare const AthStuId: string;
            export declare const AthFirstName: string;
            export declare const AthLastName: string;
            export declare const AthSportId: string;
            export declare const AthTrainerId: string;
            export declare const AthGenderId: string;
            export declare const AthYearId: string;
            export declare const AthUserImage: string;
        }

        ['Id', 'AthId', 'TrainerId', 'AthLive', 'AthUsername', 'AthStuId', 'AthFirstName', 'AthLastName', 'AthSportId', 'AthTrainerId', 'AthGenderId', 'AthYearId', 'AthUserImage'].forEach(x => (<any>Fields)[x] = x);
    }
}

