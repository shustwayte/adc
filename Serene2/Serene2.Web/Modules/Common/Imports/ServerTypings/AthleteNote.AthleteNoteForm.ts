﻿namespace Serene2.AthleteNote {
    export class AthleteNoteForm extends Serenity.PrefixedContext {
        static formKey = 'AthleteNote.AthleteNote';

    }

    export interface AthleteNoteForm {
        AthleteId: Serenity.IntegerEditor;
        EntryDate: Serenity.DateEditor;
        Notes: Serenity.TextAreaEditor;
        Username: Serenity.StringEditor;
    }

    [['AthleteId', () => Serenity.IntegerEditor], ['EntryDate', () => Serenity.DateEditor], ['Notes', () => Serenity.TextAreaEditor], ['Username', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(AthleteNoteForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

