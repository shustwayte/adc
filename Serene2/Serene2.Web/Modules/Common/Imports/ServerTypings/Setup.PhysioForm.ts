﻿namespace Serene2.Setup {
    export class PhysioForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.Physio';

    }

    export interface PhysioForm {
        Physio: Serenity.StringEditor;
        Username: Serenity.StringEditor;
    }

    [['Physio', () => Serenity.StringEditor], ['Username', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(PhysioForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

