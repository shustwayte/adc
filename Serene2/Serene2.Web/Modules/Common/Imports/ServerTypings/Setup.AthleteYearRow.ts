﻿namespace Serene2.Setup {
    export interface AthleteYearRow {
        YearId?: number;
        Year?: string;
    }

    export namespace AthleteYearRow {
        export const idProperty = 'YearId';
        export const nameProperty = 'Year';
        export const localTextPrefix = 'Setup.AthleteYear';
        export const lookupKey = 'WebApp.Year';

        export function getLookup(): Q.Lookup<AthleteYearRow> {
            return Q.getLookup<AthleteYearRow>('WebApp.Year');
        }

        export namespace Fields {
            export declare const YearId: string;
            export declare const Year: string;
        }

        ['YearId', 'Year'].forEach(x => (<any>Fields)[x] = x);
    }
}

