﻿namespace Serene2.Athlete {
    export enum AthleteFlagStatus {
        Pass = 0,
        Attention = 1
    }
    Serenity.Decorators.registerEnum(AthleteFlagStatus, 'Athlete.AthleteFlagStatus');
}

