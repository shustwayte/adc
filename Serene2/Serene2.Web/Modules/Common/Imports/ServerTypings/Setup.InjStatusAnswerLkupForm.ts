﻿namespace Serene2.Setup {
    export class InjStatusAnswerLkupForm extends Serenity.PrefixedContext {
        static formKey = 'Setup.InjStatusAnswerLkup';

    }

    export interface InjStatusAnswerLkupForm {
        Answer: Serenity.StringEditor;
    }

    [['Answer', () => Serenity.StringEditor]].forEach(x => Object.defineProperty(InjStatusAnswerLkupForm.prototype, <string>x[0], { get: function () { return this.w(x[0], (x[1] as any)()); }, enumerable: true, configurable: true }));
}

