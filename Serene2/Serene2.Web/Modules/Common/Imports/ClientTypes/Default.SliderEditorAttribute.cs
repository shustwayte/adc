﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Default
{
    public partial class SliderEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.Default.SliderEditor";

        public SliderEditorAttribute()
            : base(Key)
        {
        }

        public Double Max
        {
            get { return GetOption<Double>("max"); }
            set { SetOption("max", value); }
        }

        public Double Min
        {
            get { return GetOption<Double>("min"); }
            set { SetOption("min", value); }
        }

        public Double Step
        {
            get { return GetOption<Double>("step"); }
            set { SetOption("step", value); }
        }

        public object Ticks
        {
            get { return GetOption<object>("ticks"); }
            set { SetOption("ticks", value); }
        }

        public object Tickslabels
        {
            get { return GetOption<object>("tickslabels"); }
            set { SetOption("tickslabels", value); }
        }
    }
}

