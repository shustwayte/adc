﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Sports
{
    public partial class TrainerSportsListFormatterAttribute : CustomFormatterAttribute
    {
        public const string Key = "Serene2.Sports.TrainerSportsListFormatter";

        public TrainerSportsListFormatterAttribute()
            : base(Key)
        {
        }
    }
}

