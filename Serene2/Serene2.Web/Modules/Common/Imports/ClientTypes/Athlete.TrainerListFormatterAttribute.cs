﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Athlete
{
    public partial class TrainerListFormatterAttribute : CustomFormatterAttribute
    {
        public const string Key = "Serene2.Athlete.TrainerListFormatter";

        public TrainerListFormatterAttribute()
            : base(Key)
        {
        }
    }
}

