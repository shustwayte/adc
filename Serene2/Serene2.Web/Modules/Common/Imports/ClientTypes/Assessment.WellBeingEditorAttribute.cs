﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Assessment
{
    public partial class WellBeingEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.Assessment.WellBeingEditor";

        public WellBeingEditorAttribute()
            : base(Key)
        {
        }
    }
}

