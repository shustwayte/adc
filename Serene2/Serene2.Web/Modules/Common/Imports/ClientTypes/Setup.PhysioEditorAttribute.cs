﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Setup
{
    public partial class PhysioEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.Setup.PhysioEditor";

        public PhysioEditorAttribute()
            : base(Key)
        {
        }
    }
}

