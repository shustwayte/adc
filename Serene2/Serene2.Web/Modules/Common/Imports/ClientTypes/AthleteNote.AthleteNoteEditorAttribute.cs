﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.AthleteNote
{
    public partial class AthleteNoteEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.AthleteNote.AthleteNoteEditor";

        public AthleteNoteEditorAttribute()
            : base(Key)
        {
        }
    }
}

