﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Athlete
{
    public partial class AthleteEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.Athlete.AthleteEditor";

        public AthleteEditorAttribute()
            : base(Key)
        {
        }
    }
}

