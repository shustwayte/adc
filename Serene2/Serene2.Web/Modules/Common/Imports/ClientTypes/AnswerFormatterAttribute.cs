﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2
{
    public partial class AnswerFormatterAttribute : CustomFormatterAttribute
    {
        public const string Key = "Serene2.AnswerFormatter";

        public AnswerFormatterAttribute()
            : base(Key)
        {
        }
    }
}

