﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.SportsTrainerList
{
    public partial class SportsCoachListEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.SportsTrainerList.SportsCoachListEditor";

        public SportsCoachListEditorAttribute()
            : base(Key)
        {
        }
    }
}

