﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Audit
{
    public partial class AuditLogEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.Audit.AuditLogEditor";

        public AuditLogEditorAttribute()
            : base(Key)
        {
        }
    }
}

