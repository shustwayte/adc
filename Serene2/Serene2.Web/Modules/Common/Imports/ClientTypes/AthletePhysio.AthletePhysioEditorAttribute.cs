﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.AthletePhysio
{
    public partial class AthletePhysioEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.AthletePhysio.AthletePhysioEditor";

        public AthletePhysioEditorAttribute()
            : base(Key)
        {
        }
    }
}

