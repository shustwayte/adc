﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.AthleteWeight
{
    public partial class AthleteWeightEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.AthleteWeight.AthleteWeightEditor";

        public AthleteWeightEditorAttribute()
            : base(Key)
        {
        }
    }
}

