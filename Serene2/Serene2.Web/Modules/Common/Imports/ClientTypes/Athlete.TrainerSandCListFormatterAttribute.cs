﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.Athlete
{
    public partial class TrainerSandCListFormatterAttribute : CustomFormatterAttribute
    {
        public const string Key = "Serene2.Athlete.TrainerSandCListFormatter";

        public TrainerSandCListFormatterAttribute()
            : base(Key)
        {
        }
    }
}

