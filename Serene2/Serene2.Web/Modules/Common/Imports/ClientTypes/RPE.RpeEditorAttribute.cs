﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Serene2.RPE
{
    public partial class RpeEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "Serene2.RPE.RpeEditor";

        public RpeEditorAttribute()
            : base(Key)
        {
        }
    }
}

