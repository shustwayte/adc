﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class PhysioDialog extends Serenity.EntityDialog<PhysioRow, any> {
        protected getFormKey() { return PhysioForm.formKey; }
        protected getIdProperty() { return PhysioRow.idProperty; }
        protected getLocalTextPrefix() { return PhysioRow.localTextPrefix; }
        protected getNameProperty() { return PhysioRow.nameProperty; }
        protected getService() { return PhysioService.baseUrl; }

        protected form = new PhysioForm(this.idPrefix);

    }
}