﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/Physio", typeof(Serene2.Setup.Pages.PhysioController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/Physio"), Route("{action=index}")]
    public class PhysioController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/Physio/PhysioIndex.cshtml");
        }
    }
}