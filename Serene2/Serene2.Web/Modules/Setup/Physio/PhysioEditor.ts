﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class PhysioEditor extends Common.GridEditorBase<PhysioRow> {
        protected getColumnsKey() { return 'Setup.Physio'; }
        protected getDialogType() { return PhysioEditorDialog; }
                protected getLocalTextPrefix() { return PhysioRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}