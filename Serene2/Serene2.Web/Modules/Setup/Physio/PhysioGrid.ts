﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class PhysioGrid extends Serenity.EntityGrid<PhysioRow, any> {
        protected getColumnsKey() { return 'Setup.Physio'; }
        protected getDialogType() { return PhysioDialog; }
        protected getIdProperty() { return PhysioRow.idProperty; }
        protected getLocalTextPrefix() { return PhysioRow.localTextPrefix; }
        protected getService() { return PhysioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}