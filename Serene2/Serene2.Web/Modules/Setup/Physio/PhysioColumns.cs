﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.Physio")]
    [BasedOnRow(typeof(Entities.PhysioRow))]
    public class PhysioColumns
    {
      //  [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
      // public Int32 Id { get; set; }
        [EditLink]
        public String Physio { get; set; }
        public String Username { get; set; }
    }
}