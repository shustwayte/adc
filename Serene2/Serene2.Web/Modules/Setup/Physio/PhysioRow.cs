﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Physio"), InstanceName("Physio"), TwoLevelCached]
    [ReadPermission("Setup:Physio:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Physio")]
    public sealed class PhysioRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Physio"), Size(250), QuickSearch]
        public String Physio
        {
            get { return Fields.Physio[this]; }
            set { Fields.Physio[this] = value; }
        }

        [DisplayName("Username"), Column("username"), Size(50)]
        public String Username
        {
            get { return Fields.Username[this]; }
            set { Fields.Username[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Physio; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public PhysioRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Physio;
            public StringField Username;

            public RowFields()
                : base("[dbo].[Physio]")
            {
                LocalTextPrefix = "Setup.Physio";
            }
        }
    }
}