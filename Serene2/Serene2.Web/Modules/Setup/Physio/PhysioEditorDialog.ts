﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class PhysioEditorDialog extends Common.GridEditorDialog<PhysioRow> {
        protected getFormKey() { return PhysioForm.formKey; }
                protected getLocalTextPrefix() { return PhysioRow.localTextPrefix; }
        protected getNameProperty() { return PhysioRow.nameProperty; }
        protected form = new PhysioForm(this.idPrefix);
    }
}