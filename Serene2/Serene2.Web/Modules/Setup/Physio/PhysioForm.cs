﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.Physio")]
    [BasedOnRow(typeof(Entities.PhysioRow))]
    public class PhysioForm
    {
        public String Physio { get; set; }
        public String Username { get; set; }
    }
}