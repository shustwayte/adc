﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/Gender", typeof(Serene2.Setup.Pages.GenderController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/Gender"), Route("{action=index}")]
    public class GenderController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/Gender/GenderIndex.cshtml");
        }
    }
}