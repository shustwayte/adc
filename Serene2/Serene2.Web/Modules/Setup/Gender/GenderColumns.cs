﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.Gender")]
    [BasedOnRow(typeof(Entities.GenderRow))]
    public class GenderColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 GenderId { get; set; }
        //[EditLink]
        [EditLink, DisplayName("Gender"), AlignRight]
        public String Gender { get; set; }
    }
}