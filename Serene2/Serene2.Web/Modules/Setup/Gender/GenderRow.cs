﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Gender"), InstanceName("Gender"), TwoLevelCached]
    [ReadPermission("Setup:Gender:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Gender")]
    public sealed class GenderRow : Row, IIdRow, INameRow
    {
        [DisplayName("Gender Id"), Column("GenderID"), Identity]
        public Int32? GenderId
        {
            get { return Fields.GenderId[this]; }
            set { Fields.GenderId[this] = value; }
        }

        [DisplayName("Gender"), Size(10), QuickSearch]
        public String Gender
        {
            get { return Fields.Gender[this]; }
            set { Fields.Gender[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.GenderId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Gender; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public GenderRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field GenderId;
            public StringField Gender;

            public RowFields()
                : base("[dbo].[Gender]")
            {
                LocalTextPrefix = "Setup.Gender";
            }
        }
    }
}