﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class GenderEditor extends Common.GridEditorBase<GenderRow> {
        protected getColumnsKey() { return 'Setup.Gender'; }
        protected getDialogType() { return GenderEditorDialog; }
                protected getLocalTextPrefix() { return GenderRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}