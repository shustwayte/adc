﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjStatusAnswerLkupEditorDialog extends Common.GridEditorDialog<InjStatusAnswerLkupRow> {
        protected getFormKey() { return InjStatusAnswerLkupForm.formKey; }
                protected getLocalTextPrefix() { return InjStatusAnswerLkupRow.localTextPrefix; }
        protected getNameProperty() { return InjStatusAnswerLkupRow.nameProperty; }
        protected form = new InjStatusAnswerLkupForm(this.idPrefix);
    }
}