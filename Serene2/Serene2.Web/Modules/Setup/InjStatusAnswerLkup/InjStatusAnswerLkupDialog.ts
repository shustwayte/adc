﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjStatusAnswerLkupDialog extends Serenity.EntityDialog<InjStatusAnswerLkupRow, any> {
        protected getFormKey() { return InjStatusAnswerLkupForm.formKey; }
        protected getIdProperty() { return InjStatusAnswerLkupRow.idProperty; }
        protected getLocalTextPrefix() { return InjStatusAnswerLkupRow.localTextPrefix; }
        protected getNameProperty() { return InjStatusAnswerLkupRow.nameProperty; }
        protected getService() { return InjStatusAnswerLkupService.baseUrl; }

        protected form = new InjStatusAnswerLkupForm(this.idPrefix);

    }
}