﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.InjStatusAnswerLkup")]
    [BasedOnRow(typeof(Entities.InjStatusAnswerLkupRow))]
    public class InjStatusAnswerLkupColumns
    {
       // [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
       // public Int32 Id { get; set; }
        [EditLink]
        public String Answer { get; set; }
    }
}