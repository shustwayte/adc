﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/InjStatusAnswerLkup", typeof(Serene2.Setup.Pages.InjStatusAnswerLkupController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/InjStatusAnswerLkup"), Route("{action=index}")]
    public class InjStatusAnswerLkupController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/InjStatusAnswerLkup/InjStatusAnswerLkupIndex.cshtml");
        }
    }
}