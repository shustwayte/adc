﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjStatusAnswerLkupGrid extends Serenity.EntityGrid<InjStatusAnswerLkupRow, any> {
        protected getColumnsKey() { return 'Setup.InjStatusAnswerLkup'; }
        protected getDialogType() { return InjStatusAnswerLkupDialog; }
        protected getIdProperty() { return InjStatusAnswerLkupRow.idProperty; }
        protected getLocalTextPrefix() { return InjStatusAnswerLkupRow.localTextPrefix; }
        protected getService() { return InjStatusAnswerLkupService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}