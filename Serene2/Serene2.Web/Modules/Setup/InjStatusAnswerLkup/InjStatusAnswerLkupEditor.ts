﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjStatusAnswerLkupEditor extends Common.GridEditorBase<InjStatusAnswerLkupRow> {
        protected getColumnsKey() { return 'Setup.InjStatusAnswerLkup'; }
        protected getDialogType() { return InjStatusAnswerLkupEditorDialog; }
                protected getLocalTextPrefix() { return InjStatusAnswerLkupRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}