﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SportsEditorDialog extends Common.GridEditorDialog<SportsRow> {
        protected getFormKey() { return SportsForm.formKey; }
                protected getLocalTextPrefix() { return SportsRow.localTextPrefix; }
        protected getNameProperty() { return SportsRow.nameProperty; }
        protected form = new SportsForm(this.idPrefix);
    }
}