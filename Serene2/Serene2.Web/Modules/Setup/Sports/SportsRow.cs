﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Sports"), InstanceName("Sports"), TwoLevelCached]
    [ReadPermission("Setup:Sports:View")]
    [ModifyPermission("Setup")]

    
    [LookupScript("WebApp.Sports")]
    public sealed class SportsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Sport Id"), Column("SportID"), Identity]
        public Int32? SportId
        {
            get { return Fields.SportId[this]; }
            set { Fields.SportId[this] = value; }
        }

        [DisplayName("Sport"), Size(250), QuickSearch]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }

        [DisplayName("Sessions Per Week"), Column("SessionsPerWeek")]
        public Int32? SessionsPerWeek
        {
            get { return Fields.SessionsPerWeek[this]; }
            set { Fields.SessionsPerWeek[this] = value; }
        }

        [DisplayName("S&C Coach"), ForeignKey("Trainers", "TrainerID"), LeftJoin("jTrainer")]
        [LookupEditor(typeof(Serene2.Setup.Entities.TrainersRow)), TextualField("TrainerNameSport")]
        public Int32? sandccoachId
        {
            get { return Fields.sandccoachId[this]; }
            set { Fields.sandccoachId[this] = value; }
        }

        [DisplayName("S&C Coach"), Expression("jTrainer.[Trainer]"), LookupInclude]
        public String Trainer
        {
            get { return Fields.Trainer[this]; }
            set { Fields.Trainer[this] = value; }
        }


        [DisplayName("Sports Coach"), ForeignKey("Trainers", "TrainerID"), LeftJoin("jTrainerSC")]
        [LookupEditor(typeof(Serene2.Setup.Entities.TrainersRow)), TextualField("TrainerNameSportScoach")]
        public Int32? scoachId
        {
            get { return Fields.scoachId[this]; }
            set { Fields.scoachId[this] = value; }
        }

        [DisplayName("Sports Coach"), Expression("jTrainerSC.[Trainer]"), LookupInclude]
        public String TrainerScoach
        {
            get { return Fields.TrainerScoach[this]; }
            set { Fields.TrainerScoach[this] = value; }
        }





        [LookupEditor(typeof(Setup.Entities.TrainersRow), Multiple = true), NotMapped]
        [LinkingSetRelation(typeof(SportsTrainerList.Entities.SportsCoachListRow), "sportID", "TrainerId")]
        [MinSelectLevel(SelectLevel.Details)]
        public System.Collections.Generic.List<Int32> Trainersport
        {
            get { return Fields.Trainersport[this]; }
            set { Fields.Trainersport[this] = value; }
        }


        [LookupEditor(typeof(Setup.Entities.TrainersRow), Multiple = true), NotMapped]
        [LinkingSetRelation(typeof(SandCTrainerList.Entities.SandCoachListRow), "sportID", "TrainerId")]
        [MinSelectLevel(SelectLevel.Details)]
        public System.Collections.Generic.List<Int32> TrainersportCoach
        {
            get { return Fields.TrainersportCoach[this]; }
            set { Fields.TrainersportCoach[this] = value; }
        }


        IIdField IIdRow.IdField
        {
            get { return Fields.SportId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Sport; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SportsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field SportId;
            public StringField Sport;

            public Int32Field SessionsPerWeek;

            public Int32Field sandccoachId;

            public Int32Field scoachId;
            public StringField Trainer;

            public StringField TrainerScoach;

            public ListField<Int32> Trainersport;

            public ListField<Int32> TrainersportCoach;

            public RowFields()
                : base("[dbo].[Sports]")
            {
                LocalTextPrefix = "Setup.Sports";
            }
        }
    }
}