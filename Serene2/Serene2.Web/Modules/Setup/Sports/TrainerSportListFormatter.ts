﻿namespace Serene2.Sports {

    @Serenity.Decorators.registerFormatter()
    export class TrainerSportsListFormatter implements Slick.Formatter {
        format(ctx: Slick.FormatterContext) {
            var idList = ctx.value as string[];
            if (!idList || !idList.length)
                return "";

            var byId = Setup.TrainersRow.getLookup().itemById;
            let z: Setup.TrainersRow;
            return idList.map(x => ((z = byId[x]) ? z.Trainer : x)).join(", ");
        }
    }
}