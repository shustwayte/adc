﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class SportsGrid extends Serenity.EntityGrid<SportsRow, any> {
        protected getColumnsKey() { return 'Setup.Sports'; }
        protected getDialogType() { return SportsDialog; }
        protected getIdProperty() { return SportsRow.idProperty; }
        protected getLocalTextPrefix() { return SportsRow.localTextPrefix; }
        protected getService() { return SportsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}