﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.Sports")]
    [BasedOnRow(typeof(Entities.SportsRow))]
    public class SportsForm
    {
        public String Sport { get; set; }

        [DisplayName("Sessions Per Week")]
        public Int32 SessionsPerWeek { get; set; }
        /*
        [DisplayName("S&C Coach")]
        public Int32 sandccoachId { get; set; }

        [DisplayName("Sports Coach")]
        public Int32 scoachId { get; set; }
        */
        [DisplayName("S&C Coach")]
        public List<Int32> Trainersport { get; set; }

        [DisplayName("Sports Coach")]
        public List<Int32> TrainersportCoach { get; set; }
    }
}