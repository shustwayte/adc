﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;
    using Sports;
    using Athlete;

    [ColumnsScript("Setup.Sports")]
    [BasedOnRow(typeof(Entities.SportsRow))]
    public class SportsColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 SportId { get; set; }
        //[EditLink]

        [EditLink, DisplayName("Sports"), AlignRight]
        public String Sport { get; set; }

        [DisplayName("Sessions Per Week"), Width(150)]
        public Int32 SessionsPerWeek { get; set; }
        /*
        [DisplayName("S&C Coach")] //, TrainerSportsListFormatter, QuickFilter]
        public string Trainer { get; set; }
        [DisplayName("Sports Coach")] //, TrainerSportsListFormatter, QuickFilter]
        public string TrainerScoach { get; set; }
        */

        [Width(200), DisplayName("S&C Coach") , TrainerSportsListFormatter]
        public string Trainersport { get; set; }

        [Width(200), DisplayName("Sports Coach"), TrainerSandCListFormatter]
        public string TrainersportCoach { get; set; }

        
    }
}