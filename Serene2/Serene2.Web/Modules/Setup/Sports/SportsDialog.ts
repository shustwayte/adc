﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SportsDialog extends Serenity.EntityDialog<SportsRow, any> {
        protected getFormKey() { return SportsForm.formKey; }
        protected getIdProperty() { return SportsRow.idProperty; }
        protected getLocalTextPrefix() { return SportsRow.localTextPrefix; }
        protected getNameProperty() { return SportsRow.nameProperty; }
        protected getService() { return SportsService.baseUrl; }

        protected form = new SportsForm(this.idPrefix);

    }
}