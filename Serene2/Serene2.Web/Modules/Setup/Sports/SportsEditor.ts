﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class SportsEditor extends Common.GridEditorBase<SportsRow> {
        protected getColumnsKey() { return 'Setup.Sports'; }
        protected getDialogType() { return SportsEditorDialog; }
                protected getLocalTextPrefix() { return SportsRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}