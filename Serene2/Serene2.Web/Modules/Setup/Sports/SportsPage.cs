﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/Sports", typeof(Serene2.Setup.Pages.SportsController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/Sports"), Route("{action=index}")]
    public class SportsController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/Sports/SportsIndex.cshtml");
        }
    }
}