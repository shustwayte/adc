﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthleteYearGrid extends Serenity.EntityGrid<AthleteYearRow, any> {
        protected getColumnsKey() { return 'Setup.AthleteYear'; }
        protected getDialogType() { return AthleteYearDialog; }
        protected getIdProperty() { return AthleteYearRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteYearRow.localTextPrefix; }
        protected getService() { return AthleteYearService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}