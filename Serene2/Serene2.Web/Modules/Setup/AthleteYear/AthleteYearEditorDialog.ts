﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteYearEditorDialog extends Common.GridEditorDialog<AthleteYearRow> {
        protected getFormKey() { return AthleteYearForm.formKey; }
                protected getLocalTextPrefix() { return AthleteYearRow.localTextPrefix; }
        protected getNameProperty() { return AthleteYearRow.nameProperty; }
        protected form = new AthleteYearForm(this.idPrefix);
    }
}