﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.AthleteYear")]
    [BasedOnRow(typeof(Entities.AthleteYearRow))]
    public class AthleteYearColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 YearId { get; set; }
        //[EditLink]
        [EditLink, DisplayName("Year"), AlignRight]
        public String Year { get; set; }
    }
}