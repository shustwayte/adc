﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthleteYearEditor extends Common.GridEditorBase<AthleteYearRow> {
        protected getColumnsKey() { return 'Setup.AthleteYear'; }
        protected getDialogType() { return AthleteYearEditorDialog; }
                protected getLocalTextPrefix() { return AthleteYearRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}