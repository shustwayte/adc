﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteYearDialog extends Serenity.EntityDialog<AthleteYearRow, any> {
        protected getFormKey() { return AthleteYearForm.formKey; }
        protected getIdProperty() { return AthleteYearRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteYearRow.localTextPrefix; }
        protected getNameProperty() { return AthleteYearRow.nameProperty; }
        protected getService() { return AthleteYearService.baseUrl; }

        protected form = new AthleteYearForm(this.idPrefix);

    }
}