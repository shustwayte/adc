﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.AthleteYear")]
    [BasedOnRow(typeof(Entities.AthleteYearRow))]
    public class AthleteYearForm
    {
        public String Year { get; set; }
    }
}