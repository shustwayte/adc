﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/AthleteYear", typeof(Serene2.Setup.Pages.AthleteYearController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/AthleteYear"), Route("{action=index}")]
    public class AthleteYearController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/AthleteYear/AthleteYearIndex.cshtml");
        }
    }
}