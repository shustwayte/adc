﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Year"), InstanceName("Year"), TwoLevelCached]
    [ReadPermission("Setup:Year:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Year")]
    public sealed class AthleteYearRow : Row, IIdRow, INameRow
    {
        [DisplayName("Year Id"), Column("YearID"), PrimaryKey]
        public Int32? YearId
        {
            get { return Fields.YearId[this]; }
            set { Fields.YearId[this] = value; }
        }

        [DisplayName("Year"), Size(250), QuickSearch]
        public String Year
        {
            get { return Fields.Year[this]; }
            set { Fields.Year[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.YearId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Year; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteYearRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field YearId;
            public StringField Year;

            public RowFields()
                : base("[dbo].[AthleteYear]")
            {
                LocalTextPrefix = "Setup.AthleteYear";
            }
        }
    }
}