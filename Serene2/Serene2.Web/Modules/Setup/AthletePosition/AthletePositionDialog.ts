﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthletePositionDialog extends Serenity.EntityDialog<AthletePositionRow, any> {
        protected getFormKey() { return AthletePositionForm.formKey; }
        protected getIdProperty() { return AthletePositionRow.idProperty; }
        protected getLocalTextPrefix() { return AthletePositionRow.localTextPrefix; }
        protected getNameProperty() { return AthletePositionRow.nameProperty; }
        protected getService() { return AthletePositionService.baseUrl; }

        protected form = new AthletePositionForm(this.idPrefix);

    }
}