﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthletePositionEditor extends Common.GridEditorBase<AthletePositionRow> {
        protected getColumnsKey() { return 'Setup.AthletePosition'; }
        protected getDialogType() { return AthletePositionEditorDialog; }
                protected getLocalTextPrefix() { return AthletePositionRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}