﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthletePositionEditorDialog extends Common.GridEditorDialog<AthletePositionRow> {
        protected getFormKey() { return AthletePositionForm.formKey; }
                protected getLocalTextPrefix() { return AthletePositionRow.localTextPrefix; }
        protected getNameProperty() { return AthletePositionRow.nameProperty; }
        protected form = new AthletePositionForm(this.idPrefix);
    }
}