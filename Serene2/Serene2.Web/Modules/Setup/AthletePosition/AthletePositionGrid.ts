﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthletePositionGrid extends Serenity.EntityGrid<AthletePositionRow, any> {
        protected getColumnsKey() { return 'Setup.AthletePosition'; }
        protected getDialogType() { return AthletePositionDialog; }
        protected getIdProperty() { return AthletePositionRow.idProperty; }
        protected getLocalTextPrefix() { return AthletePositionRow.localTextPrefix; }
        protected getService() { return AthletePositionService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}