﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Athlete Position"), InstanceName("AthletePosition"), TwoLevelCached]
    [ReadPermission("Setup:Position:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Position")]

    public sealed class AthletePositionRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Position"), Column("position"), Size(500), QuickSearch]
        public String Position
        {
            get { return Fields.Position[this]; }
            set { Fields.Position[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Position; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthletePositionRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Position;

            public RowFields()
                : base("[dbo].[AthletePosition]")
            {
                LocalTextPrefix = "Setup.AthletePosition";
            }
        }
    }
}