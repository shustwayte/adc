﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/AthletePosition", typeof(Serene2.Setup.Pages.AthletePositionController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/AthletePosition"), Route("{action=index}")]
    public class AthletePositionController : Controller
    {
        [PageAuthorize("")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/AthletePosition/AthletePositionIndex.cshtml");
        }
    }
}