﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.AthletePosition")]
    [BasedOnRow(typeof(Entities.AthletePositionRow))]
    public class AthletePositionForm
    {
        public String Position { get; set; }
    }
}