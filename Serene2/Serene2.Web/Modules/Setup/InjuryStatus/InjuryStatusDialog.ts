﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjuryStatusDialog extends Serenity.EntityDialog<InjuryStatusRow, any> {
        protected getFormKey() { return InjuryStatusForm.formKey; }
        protected getIdProperty() { return InjuryStatusRow.idProperty; }
        protected getLocalTextPrefix() { return InjuryStatusRow.localTextPrefix; }
        protected getNameProperty() { return InjuryStatusRow.nameProperty; }
        protected getService() { return InjuryStatusService.baseUrl; }

        protected form = new InjuryStatusForm(this.idPrefix);

    }
}