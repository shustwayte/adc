﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.InjuryStatus")]
    [BasedOnRow(typeof(Entities.InjuryStatusRow))]
    public class InjuryStatusColumns
    {
       // [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
       // public Int32 Id { get; set; }
        [EditLink]
        public String InjuryStatus { get; set; }

        [DisplayName("Rank Order")]

        public Int32? rankOrd { get; set; }
    }
}