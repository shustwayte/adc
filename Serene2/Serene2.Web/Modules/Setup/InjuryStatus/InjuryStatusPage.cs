﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/InjuryStatus", typeof(Serene2.Setup.Pages.InjuryStatusController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/InjuryStatus"), Route("{action=index}")]
    public class InjuryStatusController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/InjuryStatus/InjuryStatusIndex.cshtml");
        }
    }
}