﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjuryStatusEditorDialog extends Common.GridEditorDialog<InjuryStatusRow> {
        protected getFormKey() { return InjuryStatusForm.formKey; }
                protected getLocalTextPrefix() { return InjuryStatusRow.localTextPrefix; }
        protected getNameProperty() { return InjuryStatusRow.nameProperty; }
        protected form = new InjuryStatusForm(this.idPrefix);
    }
}