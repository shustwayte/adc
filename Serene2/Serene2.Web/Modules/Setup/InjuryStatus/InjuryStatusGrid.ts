﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjuryStatusGrid extends Serenity.EntityGrid<InjuryStatusRow, any> {
        protected getColumnsKey() { return 'Setup.InjuryStatus'; }
        protected getDialogType() { return InjuryStatusDialog; }
        protected getIdProperty() { return InjuryStatusRow.idProperty; }
        protected getLocalTextPrefix() { return InjuryStatusRow.localTextPrefix; }
        protected getService() { return InjuryStatusService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getDefaultSortBy() {
            return [InjuryStatusRow.Fields.rankOrd];
        }
    }
}