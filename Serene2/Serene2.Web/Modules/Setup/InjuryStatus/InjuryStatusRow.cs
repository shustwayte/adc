﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("InjuryStatus"), InstanceName("InjuryStatus"), TwoLevelCached]
    [ReadPermission("Setup:InjStatus:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.InjuryStatus")]
    public sealed class InjuryStatusRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Injury Status"), Size(250), QuickSearch]
        public String InjuryStatus
        {
            get { return Fields.InjuryStatus[this]; }
            set { Fields.InjuryStatus[this] = value; }
        }

        [DisplayName("Rank Order"), Size(250), QuickSearch]
        public Int32? rankOrd
        {
            get { return Fields.rankOrd[this]; }
            set { Fields.rankOrd[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.InjuryStatus; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public InjuryStatusRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField InjuryStatus;

            public Int32Field rankOrd;

            public RowFields()
                : base("[dbo].[InjuryStatus]")
            {
                LocalTextPrefix = "Setup.InjuryStatus";
            }
        }
    }
}