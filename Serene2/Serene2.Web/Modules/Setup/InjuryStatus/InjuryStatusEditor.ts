﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjuryStatusEditor extends Common.GridEditorBase<InjuryStatusRow> {
        protected getColumnsKey() { return 'Setup.InjuryStatus'; }
        protected getDialogType() { return InjuryStatusEditorDialog; }
                protected getLocalTextPrefix() { return InjuryStatusRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}