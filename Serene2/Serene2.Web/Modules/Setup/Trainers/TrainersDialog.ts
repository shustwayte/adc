﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class TrainersDialog extends Serenity.EntityDialog<TrainersRow, any> {
        protected getFormKey() { return TrainersForm.formKey; }
        protected getIdProperty() { return TrainersRow.idProperty; }
        protected getLocalTextPrefix() { return TrainersRow.localTextPrefix; }
        protected getNameProperty() { return TrainersRow.nameProperty; }
        protected getService() { return TrainersService.baseUrl; }

        protected form = new TrainersForm(this.idPrefix);

    }
}