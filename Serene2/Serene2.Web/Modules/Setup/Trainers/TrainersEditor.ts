﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class TrainersEditor extends Common.GridEditorBase<TrainersRow> {
        protected getColumnsKey() { return 'Setup.Trainers'; }
        protected getDialogType() { return TrainersEditorDialog; }
                protected getLocalTextPrefix() { return TrainersRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}