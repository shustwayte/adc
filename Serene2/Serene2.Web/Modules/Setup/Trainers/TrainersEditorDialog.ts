﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class TrainersEditorDialog extends Common.GridEditorDialog<TrainersRow> {
        protected getFormKey() { return TrainersForm.formKey; }
                protected getLocalTextPrefix() { return TrainersRow.localTextPrefix; }
        protected getNameProperty() { return TrainersRow.nameProperty; }
        protected form = new TrainersForm(this.idPrefix);
    }
}