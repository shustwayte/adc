﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.Trainers")]
    [BasedOnRow(typeof(Entities.TrainersRow))]
    public class TrainersColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 TrainerId { get; set; }
        //[EditLink]
        [EditLink, DisplayName("Coach"), AlignRight]
        public String Trainer { get; set; }

        public String username { get; set; }
    }
}