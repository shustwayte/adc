﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/Trainers", typeof(Serene2.Setup.Pages.TrainersController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/Trainers"), Route("{action=index}")]
    public class TrainersController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/Trainers/TrainersIndex.cshtml");
        }
    }
}