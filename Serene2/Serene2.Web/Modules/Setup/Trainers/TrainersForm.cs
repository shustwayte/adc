﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.Trainers")]
    [BasedOnRow(typeof(Entities.TrainersRow))]
    public class TrainersForm
    {
        [DisplayName("Coach")]
        public String Trainer { get; set; }

        [DisplayName("Username / Delete to stop email alerts"), Width(250)]
        public String username { get; set; }
    }
}