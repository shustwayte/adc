﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Coaches"), InstanceName("Coach"), TwoLevelCached]
    [ReadPermission("Setup:Trainer:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Trainers")]
    public sealed class TrainersRow : Row, IIdRow, INameRow
    {
        [DisplayName("Trainer Id"), Column("TrainerID"), Identity]
        public Int32? TrainerId
        {
            get { return Fields.TrainerId[this]; }
            set { Fields.TrainerId[this] = value; }
        }

        [DisplayName("Trainer"), Size(250), QuickSearch]
        public String Trainer
        {
            get { return Fields.Trainer[this]; }
            set { Fields.Trainer[this] = value; }
        }

        [DisplayName("Username / Delete to stop email alerts"), Size(250), Width(250) QuickSearch]
        public String username
        {
            get { return Fields.username[this]; }
            set { Fields.username[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.TrainerId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Trainer; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TrainersRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TrainerId;
            public StringField Trainer;

            public StringField username;

            public RowFields()
                : base("[dbo].[Trainers]")
            {
                LocalTextPrefix = "Setup.Trainers";
            }
        }
    }
}