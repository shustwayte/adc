﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class TrainersGrid extends Serenity.EntityGrid<TrainersRow, any> {
        protected getColumnsKey() { return 'Setup.Trainers'; }
        protected getDialogType() { return TrainersDialog; }
        protected getIdProperty() { return TrainersRow.idProperty; }
        protected getLocalTextPrefix() { return TrainersRow.localTextPrefix; }
        protected getService() { return TrainersService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}