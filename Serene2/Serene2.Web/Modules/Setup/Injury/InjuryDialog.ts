﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjuryDialog extends Serenity.EntityDialog<InjuryRow, any> {
        protected getFormKey() { return InjuryForm.formKey; }
        protected getIdProperty() { return InjuryRow.idProperty; }
        protected getLocalTextPrefix() { return InjuryRow.localTextPrefix; }
        protected getNameProperty() { return InjuryRow.nameProperty; }
        protected getService() { return InjuryService.baseUrl; }

        protected form = new InjuryForm(this.idPrefix);

    }
}