﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.Injury")]
    [BasedOnRow(typeof(Entities.InjuryRow))]
    public class InjuryForm
    {
        public String Injury { get; set; }
    }
}