﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Injury"), InstanceName("Injury"), TwoLevelCached]
    [ReadPermission("Setup:Injury:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.Injury")]
    public sealed class InjuryRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Injury"), Size(250), QuickSearch]
        public String Injury
        {
            get { return Fields.Injury[this]; }
            set { Fields.Injury[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Injury; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public InjuryRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Injury;

            public RowFields()
                : base("[dbo].[Injury]")
            {
                LocalTextPrefix = "Setup.Injury";
            }
        }
    }
}