﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/Injury", typeof(Serene2.Setup.Pages.InjuryController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/Injury"), Route("{action=index}")]
    public class InjuryController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/Injury/InjuryIndex.cshtml");
        }
    }
}