﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjuryEditor extends Common.GridEditorBase<InjuryRow> {
        protected getColumnsKey() { return 'Setup.Injury'; }
        protected getDialogType() { return InjuryEditorDialog; }
                protected getLocalTextPrefix() { return InjuryRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}