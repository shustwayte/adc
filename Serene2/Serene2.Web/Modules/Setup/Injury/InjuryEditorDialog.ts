﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class InjuryEditorDialog extends Common.GridEditorDialog<InjuryRow> {
        protected getFormKey() { return InjuryForm.formKey; }
                protected getLocalTextPrefix() { return InjuryRow.localTextPrefix; }
        protected getNameProperty() { return InjuryRow.nameProperty; }
        protected form = new InjuryForm(this.idPrefix);
    }
}