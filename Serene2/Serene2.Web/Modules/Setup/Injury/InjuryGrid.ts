﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class InjuryGrid extends Serenity.EntityGrid<InjuryRow, any> {
        protected getColumnsKey() { return 'Setup.Injury'; }
        protected getDialogType() { return InjuryDialog; }
        protected getIdProperty() { return InjuryRow.idProperty; }
        protected getLocalTextPrefix() { return InjuryRow.localTextPrefix; }
        protected getService() { return InjuryService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}