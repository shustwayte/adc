﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthleteTestingLkUpEditor extends Common.GridEditorBase<AthleteTestingLkUpRow> {
        protected getColumnsKey() { return 'Setup.AthleteTestingLkUp'; }
        protected getDialogType() { return AthleteTestingLkUpEditorDialog; }
                protected getLocalTextPrefix() { return AthleteTestingLkUpRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}