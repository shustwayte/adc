﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Athlete Testing Setup"), InstanceName("AthleteTestingLkUp"), TwoLevelCached]
    [ReadPermission("Testing")]
    [ModifyPermission("Testing")]
    public sealed class AthleteTestingLkUpRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Test1 Label"), Size(500), QuickSearch]
        public String Test1Label
        {
            get { return Fields.Test1Label[this]; }
            set { Fields.Test1Label[this] = value; }
        }

        [DisplayName("Test2 Label"), Size(500)]
        public String Test2Label
        {
            get { return Fields.Test2Label[this]; }
            set { Fields.Test2Label[this] = value; }
        }

        [DisplayName("Test3 Label"), Size(500)]
        public String Test3Label
        {
            get { return Fields.Test3Label[this]; }
            set { Fields.Test3Label[this] = value; }
        }

        [DisplayName("Test4 Label"), Size(500)]
        public String Test4Label
        {
            get { return Fields.Test4Label[this]; }
            set { Fields.Test4Label[this] = value; }
        }

        [DisplayName("Test5 Label"), Size(500)]
        public String Test5Label
        {
            get { return Fields.Test5Label[this]; }
            set { Fields.Test5Label[this] = value; }
        }

        [DisplayName("Sport Id"), Column("sportID"), ForeignKey("Sports", "SportID"), LeftJoin("jSport")]
        [LookupEditor(typeof(Serene2.Setup.Entities.SportsRow)), TextualField("SportName")]
        public Int32? SportId
        {
            get { return Fields.SportId[this]; }
            set { Fields.SportId[this] = value; }
        }

        [DisplayName("Sport"), Expression("jSport.[Sport]"), LookupInclude]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Test1Label; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteTestingLkUpRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Test1Label;
            public StringField Test2Label;
            public StringField Test3Label;
            public StringField Test4Label;
            public StringField Test5Label;
            public Int32Field SportId;

            public StringField Sport;

            public RowFields()
                : base("[dbo].[AthleteTestingLkUp]")
            {
                LocalTextPrefix = "Setup.AthleteTestingLkUp";
            }
        }
    }
}