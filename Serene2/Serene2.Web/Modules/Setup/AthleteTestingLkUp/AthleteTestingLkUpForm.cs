﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.AthleteTestingLkUp")]
    [BasedOnRow(typeof(Entities.AthleteTestingLkUpRow))]
    public class AthleteTestingLkUpForm
    {
        public String Test1Label { get; set; }
        public String Test2Label { get; set; }
        public String Test3Label { get; set; }
        public String Test4Label { get; set; }
        public String Test5Label { get; set; }
        public Int32 SportId { get; set; }
    }
}