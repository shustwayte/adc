﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteTestingLkUpEditorDialog extends Common.GridEditorDialog<AthleteTestingLkUpRow> {
        protected getFormKey() { return AthleteTestingLkUpForm.formKey; }
                protected getLocalTextPrefix() { return AthleteTestingLkUpRow.localTextPrefix; }
        protected getNameProperty() { return AthleteTestingLkUpRow.nameProperty; }
        protected form = new AthleteTestingLkUpForm(this.idPrefix);
    }
}