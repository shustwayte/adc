﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthleteTestingLkUpGrid extends Serenity.EntityGrid<AthleteTestingLkUpRow, any> {
        protected getColumnsKey() { return 'Setup.AthleteTestingLkUp'; }
        protected getDialogType() { return AthleteTestingLkUpDialog; }
        protected getIdProperty() { return AthleteTestingLkUpRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteTestingLkUpRow.localTextPrefix; }
        protected getService() { return AthleteTestingLkUpService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}