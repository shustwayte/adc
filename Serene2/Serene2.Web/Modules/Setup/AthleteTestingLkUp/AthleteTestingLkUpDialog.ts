﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteTestingLkUpDialog extends Serenity.EntityDialog<AthleteTestingLkUpRow, any> {
        protected getFormKey() { return AthleteTestingLkUpForm.formKey; }
        protected getIdProperty() { return AthleteTestingLkUpRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteTestingLkUpRow.localTextPrefix; }
        protected getNameProperty() { return AthleteTestingLkUpRow.nameProperty; }
        protected getService() { return AthleteTestingLkUpService.baseUrl; }

        protected form = new AthleteTestingLkUpForm(this.idPrefix);

    }
}