﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/AthleteTestingLkUp", typeof(Serene2.Setup.Pages.AthleteTestingLkUpController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/AthleteTestingLkUp"), Route("{action=index}")]
    public class AthleteTestingLkUpController : Controller
    {
        [PageAuthorize("")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/AthleteTestingLkUp/AthleteTestingLkUpIndex.cshtml");
        }
    }
}