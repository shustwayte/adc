﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.AthleteTestingLkUp")]
    [BasedOnRow(typeof(Entities.AthleteTestingLkUpRow))]
    public class AthleteTestingLkUpColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 Id { get; set; }
        [EditLink]
        public String Sport { get; set; }
        public String Test1Label { get; set; }
        public String Test2Label { get; set; }
        public String Test3Label { get; set; }
        public String Test4Label { get; set; }
        public String Test5Label { get; set; }
        
    }
}