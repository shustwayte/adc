﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class YesNoAnswerLkupDialog extends Serenity.EntityDialog<YesNoAnswerLkupRow, any> {
        protected getFormKey() { return YesNoAnswerLkupForm.formKey; }
        protected getIdProperty() { return YesNoAnswerLkupRow.idProperty; }
        protected getLocalTextPrefix() { return YesNoAnswerLkupRow.localTextPrefix; }
        protected getNameProperty() { return YesNoAnswerLkupRow.nameProperty; }
        protected getService() { return YesNoAnswerLkupService.baseUrl; }

        protected form = new YesNoAnswerLkupForm(this.idPrefix);

    }
}