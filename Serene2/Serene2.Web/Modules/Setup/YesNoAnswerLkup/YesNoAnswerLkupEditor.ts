﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class YesNoAnswerLkupEditor extends Common.GridEditorBase<YesNoAnswerLkupRow> {
        protected getColumnsKey() { return 'Setup.YesNoAnswerLkup'; }
        protected getDialogType() { return YesNoAnswerLkupEditorDialog; }
                protected getLocalTextPrefix() { return YesNoAnswerLkupRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}