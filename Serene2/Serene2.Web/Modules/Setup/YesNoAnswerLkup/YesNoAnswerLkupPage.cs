﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/YesNoAnswerLkup", typeof(Serene2.Setup.Pages.YesNoAnswerLkupController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/YesNoAnswerLkup"), Route("{action=index}")]
    public class YesNoAnswerLkupController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/YesNoAnswerLkup/YesNoAnswerLkupIndex.cshtml");
        }
    }
}