﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.YesNoAnswerLkup")]
    [BasedOnRow(typeof(Entities.YesNoAnswerLkupRow))]
    public class YesNoAnswerLkupColumns
    {
      //  [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
      //  public Int32 Id { get; set; }
        [EditLink]
        public String Answer { get; set; }
    }
}