﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.YesNoAnswerLkup")]
    [BasedOnRow(typeof(Entities.YesNoAnswerLkupRow))]
    public class YesNoAnswerLkupForm
    {
        public String Answer { get; set; }
    }
}