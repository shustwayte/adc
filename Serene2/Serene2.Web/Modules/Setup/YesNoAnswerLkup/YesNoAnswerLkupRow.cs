﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("yesNoAnswerLKUP"), InstanceName("yesNoAnswerLKUP"), TwoLevelCached]
    [ReadPermission("Setup:YesNo:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.YesNoAnswer")]
    public sealed class YesNoAnswerLkupRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Answer"), Column("answer"), Size(10), QuickSearch]
        public String Answer
        {
            get { return Fields.Answer[this]; }
            set { Fields.Answer[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Answer; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public YesNoAnswerLkupRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Answer;

            public RowFields()
                : base("[dbo].[yesNoAnswerLKUP]")
            {
                LocalTextPrefix = "Setup.YesNoAnswerLkup";
            }
        }
    }
}