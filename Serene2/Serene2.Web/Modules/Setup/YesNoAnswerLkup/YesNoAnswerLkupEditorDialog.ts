﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class YesNoAnswerLkupEditorDialog extends Common.GridEditorDialog<YesNoAnswerLkupRow> {
        protected getFormKey() { return YesNoAnswerLkupForm.formKey; }
                protected getLocalTextPrefix() { return YesNoAnswerLkupRow.localTextPrefix; }
        protected getNameProperty() { return YesNoAnswerLkupRow.nameProperty; }
        protected form = new YesNoAnswerLkupForm(this.idPrefix);
    }
}