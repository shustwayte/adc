﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class YesNoAnswerLkupGrid extends Serenity.EntityGrid<YesNoAnswerLkupRow, any> {
        protected getColumnsKey() { return 'Setup.YesNoAnswerLkup'; }
        protected getDialogType() { return YesNoAnswerLkupDialog; }
        protected getIdProperty() { return YesNoAnswerLkupRow.idProperty; }
        protected getLocalTextPrefix() { return YesNoAnswerLkupRow.localTextPrefix; }
        protected getService() { return YesNoAnswerLkupService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}