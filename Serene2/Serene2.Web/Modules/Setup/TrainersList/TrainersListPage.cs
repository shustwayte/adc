﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/TrainersList", typeof(Serene2.Setup.Pages.TrainersListController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/TrainersList"), Route("{action=index}")]
    public class TrainersListController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/TrainersList/TrainersListIndex.cshtml");
        }
    }
}