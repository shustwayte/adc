﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.TrainersList")]
    [BasedOnRow(typeof(Entities.TrainersListRow))]
    public class TrainersListColumns
    {
      //  [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
      //  public Int32 Id { get; set; }
        public Int32 AthId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}