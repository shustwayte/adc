﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class TrainersListDialog extends Serenity.EntityDialog<TrainersListRow, any> {
        protected getFormKey() { return TrainersListForm.formKey; }
        protected getIdProperty() { return TrainersListRow.idProperty; }
        protected getLocalTextPrefix() { return TrainersListRow.localTextPrefix; }
        protected getService() { return TrainersListService.baseUrl; }

        protected form = new TrainersListForm(this.idPrefix);

    }
}