﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class TrainersListEditor extends Common.GridEditorBase<TrainersListRow> {
        protected getColumnsKey() { return 'Setup.TrainersList'; }
        protected getDialogType() { return TrainersListEditorDialog; }
                protected getLocalTextPrefix() { return TrainersListRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}