﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class TrainersListEditorDialog extends Common.GridEditorDialog<TrainersListRow> {
        protected getFormKey() { return TrainersListForm.formKey; }
                protected getLocalTextPrefix() { return TrainersListRow.localTextPrefix; }
        protected form = new TrainersListForm(this.idPrefix);
    }
}