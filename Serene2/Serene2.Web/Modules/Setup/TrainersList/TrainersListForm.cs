﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.TrainersList")]
    [BasedOnRow(typeof(Entities.TrainersListRow))]
    public class TrainersListForm
    {
        public Int32 AthId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}