﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("TrainersList"), InstanceName("TrainersList"), TwoLevelCached]
    [ReadPermission("Setup:TrainerList:View")]
    [ModifyPermission("Setup")]

    [LookupScript("WebApp.TrainersList")]

    public sealed class TrainersListRow : Row, IIdRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Ath"), Column("athID"), ForeignKey("[dbo].[Athlete]", "Id"), LeftJoin("jAth"), TextualField("AthUsername")]
        public Int32? AthId
        {
            get { return Fields.AthId[this]; }
            set { Fields.AthId[this] = value; }
        }

        [DisplayName("Trainer Id")]
        public Int32? TrainerId
        {
            get { return Fields.TrainerId[this]; }
            set { Fields.TrainerId[this] = value; }
        }

        [DisplayName("Ath Live"), Expression("jAth.[live]")]
        public Int32? AthLive
        {
            get { return Fields.AthLive[this]; }
            set { Fields.AthLive[this] = value; }
        }

        [DisplayName("Ath Username"), Expression("jAth.[Username]")]
        public String AthUsername
        {
            get { return Fields.AthUsername[this]; }
            set { Fields.AthUsername[this] = value; }
        }

        [DisplayName("Ath Stu Id"), Expression("jAth.[stuID]")]
        public String AthStuId
        {
            get { return Fields.AthStuId[this]; }
            set { Fields.AthStuId[this] = value; }
        }

        [DisplayName("Ath First Name"), Expression("jAth.[FirstName]")]
        public String AthFirstName
        {
            get { return Fields.AthFirstName[this]; }
            set { Fields.AthFirstName[this] = value; }
        }

        [DisplayName("Ath Last Name"), Expression("jAth.[LastName]")]
        public String AthLastName
        {
            get { return Fields.AthLastName[this]; }
            set { Fields.AthLastName[this] = value; }
        }

        [DisplayName("Ath Sport Id"), Expression("jAth.[SportID]")]
        public Int32? AthSportId
        {
            get { return Fields.AthSportId[this]; }
            set { Fields.AthSportId[this] = value; }
        }

        [DisplayName("Ath Trainer Id"), Expression("jAth.[TrainerID]")]
        public Int32? AthTrainerId
        {
            get { return Fields.AthTrainerId[this]; }
            set { Fields.AthTrainerId[this] = value; }
        }

        [DisplayName("Ath Gender Id"), Expression("jAth.[GenderID]")]
        public Int32? AthGenderId
        {
            get { return Fields.AthGenderId[this]; }
            set { Fields.AthGenderId[this] = value; }
        }

        [DisplayName("Ath Year Id"), Expression("jAth.[YearID]")]
        public Int32? AthYearId
        {
            get { return Fields.AthYearId[this]; }
            set { Fields.AthYearId[this] = value; }
        }

        [DisplayName("Ath User Image"), Expression("jAth.[UserImage]")]
        public String AthUserImage
        {
            get { return Fields.AthUserImage[this]; }
            set { Fields.AthUserImage[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TrainersListRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthId;
            public Int32Field TrainerId;

            public Int32Field AthLive;
            public StringField AthUsername;
            public StringField AthStuId;
            public StringField AthFirstName;
            public StringField AthLastName;
            public Int32Field AthSportId;
            public Int32Field AthTrainerId;
            public Int32Field AthGenderId;
            public Int32Field AthYearId;
            public StringField AthUserImage;

            public RowFields()
                : base("[dbo].[TrainersList]")
            {
                LocalTextPrefix = "Setup.TrainersList";
            }
        }
    }
}