﻿
namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class TrainersListGrid extends Serenity.EntityGrid<TrainersListRow, any> {
        protected getColumnsKey() { return 'Setup.TrainersList'; }
        protected getDialogType() { return TrainersListDialog; }
        protected getIdProperty() { return TrainersListRow.idProperty; }
        protected getLocalTextPrefix() { return TrainersListRow.localTextPrefix; }
        protected getService() { return TrainersListService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}