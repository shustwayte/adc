﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteTestingEditorDialog extends Common.GridEditorDialog<AthleteTestingRow> {
        protected getFormKey() { return AthleteTestingForm.formKey; }
                protected getLocalTextPrefix() { return AthleteTestingRow.localTextPrefix; }
        protected getNameProperty() { return AthleteTestingRow.nameProperty; }
        protected form = new AthleteTestingForm(this.idPrefix);
    }
}