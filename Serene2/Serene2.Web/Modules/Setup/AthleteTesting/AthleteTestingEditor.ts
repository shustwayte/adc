﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Setup {
    
    @Serenity.Decorators.registerClass()
    export class AthleteTestingEditor extends Common.GridEditorBase<AthleteTestingRow> {
        protected getColumnsKey() { return 'Setup.AthleteTesting'; }
        protected getDialogType() { return AthleteTestingEditorDialog; }
                protected getLocalTextPrefix() { return AthleteTestingRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}