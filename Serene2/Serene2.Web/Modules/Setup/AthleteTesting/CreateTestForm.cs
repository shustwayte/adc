﻿
namespace Serene2.AthleteTesting.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("AthleteTesting.CreateTest")]
    [BasedOnRow(typeof(Setup.Entities.AthleteTestingRow))]

    public class CreateTestForm
    {
        [Required]
        //[HardcodedValuesEditor]
        public String Sport { get; set; }
    }
}