﻿
namespace Serene2.Setup {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteTestingDialog extends Serenity.EntityDialog<AthleteTestingRow, any> {
        protected getFormKey() { return AthleteTestingForm.formKey; }
        protected getIdProperty() { return AthleteTestingRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteTestingRow.localTextPrefix; }
        protected getNameProperty() { return AthleteTestingRow.nameProperty; }
        protected getService() { return AthleteTestingService.baseUrl; }

        protected form = new AthleteTestingForm(this.idPrefix);

       
    }
}