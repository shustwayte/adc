﻿
namespace Serene2.Setup.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Setup.AthleteTesting")]
    [BasedOnRow(typeof(Entities.AthleteTestingRow))]
    public class AthleteTestingForm
    {
        public String DisplayName { get; set; }
        //public Int32 AthleteId { get; set; }
        public String Test1Label { get; set; }
        public Double Test1Result { get; set; }
        public String Test2Label { get; set; }
        public Double Test2Result { get; set; }
        public String Test3Label { get; set; }
        public Double Test3Result { get; set; }
        public String Test4Label { get; set; }
        public Double Test4Result { get; set; }
        public String Test5Label { get; set; }
        public Double Test5Result { get; set; }
        //public DateTime OnDate { get; set; }
        //public Int32 SportId { get; set; }
        [ReadOnly(true)]
        public String Sport { get; set; }
        public String TermDisplay { get; set; }
    }
}