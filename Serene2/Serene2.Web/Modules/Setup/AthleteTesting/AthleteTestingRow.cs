﻿

namespace Serene2.Setup.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Athlete Testing"), InstanceName("AthleteTesting"), TwoLevelCached]
    [ReadPermission("Testing")]
    [ModifyPermission("Testing")]

    [LookupScript("WebApp.AthleteTesting")]
    public sealed class AthleteTestingRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athlete"), Column("athleteID"), ForeignKey("[dbo].[Athlete]", "Id"), LeftJoin("jAthlete"), TextualField("AthleteUsername"), ReadOnly(true)]
        public Int32? AthleteId
        {
            get { return Fields.AthleteId[this]; }
            set { Fields.AthleteId[this] = value; }
        }

        [DisplayName("Test1 Label"), Size(500), QuickSearch, ReadOnly(true)]
        public String Test1Label
        {
            get { return Fields.Test1Label[this]; }
            set { Fields.Test1Label[this] = value; }
        }

        [DisplayName("Test1 Result")]
        public Double? Test1Result
        {
            get { return Fields.Test1Result[this]; }
            set { Fields.Test1Result[this] = value; }
        }

        [DisplayName("Test2 Label"), Size(500), ReadOnly(true)]
        public String Test2Label
        {
            get { return Fields.Test2Label[this]; }
            set { Fields.Test2Label[this] = value; }
        }

        [DisplayName("Test2 Result")]
        public Double? Test2Result
        {
            get { return Fields.Test2Result[this]; }
            set { Fields.Test2Result[this] = value; }
        }

        [DisplayName("Test3 Label"), Size(500), ReadOnly(true)]
        public String Test3Label
        {
            get { return Fields.Test3Label[this]; }
            set { Fields.Test3Label[this] = value; }
        }

        [DisplayName("Test3 Result")]
        public Double? Test3Result
        {
            get { return Fields.Test3Result[this]; }
            set { Fields.Test3Result[this] = value; }
        }

        [DisplayName("Test4 Label"), Size(500), ReadOnly(true)]
        public String Test4Label
        {
            get { return Fields.Test4Label[this]; }
            set { Fields.Test4Label[this] = value; }
        }

        [DisplayName("Test4 Result")]
        public Double? Test4Result
        {
            get { return Fields.Test4Result[this]; }
            set { Fields.Test4Result[this] = value; }
        }

        [DisplayName("Test5 Label"), Size(500), ReadOnly(true)]
        public String Test5Label
        {
            get { return Fields.Test5Label[this]; }
            set { Fields.Test5Label[this] = value; }
        }

        [DisplayName("Test5 Result")]
        public Double? Test5Result
        {
            get { return Fields.Test5Result[this]; }
            set { Fields.Test5Result[this] = value; }
        }

        [DisplayName("On Date"), Column("onDate"), ReadOnly(true)]
        public DateTime? OnDate
        {
            get { return Fields.OnDate[this]; }
            set { Fields.OnDate[this] = value; }
        }

        [DisplayName("Term"), Expression(" SUBSTRING(CONVERT(varchar, [onDate],106	), 4, 10)  "), ReadOnly(true)]
        public String TermDisplay
        {
            get { return Fields.TermDisplay[this]; }
            set { Fields.TermDisplay[this] = value; }
        }



        [DisplayName("Sport"), Column("sportID"), ForeignKey("[dbo].[Sports]", "SportID"), LeftJoin("jSport") ] //, TextualField("Sport")]
        [LookupEditor(typeof(Serene2.Setup.Entities.SportsRow)), TextualField("SportName")]
        public Int32? SportId
        {
            get { return Fields.SportId[this]; }
            set { Fields.SportId[this] = value; }
        }

        [DisplayName("Sport"), Expression("jSport.[Sport]"), LookupInclude]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }

        [DisplayName("Sport Sessions Per Week"), Expression("jSport.[SessionsPerWeek]"), ReadOnly(true)]
        public Int32? SportSessionsPerWeek
        {
            get { return Fields.SportSessionsPerWeek[this]; }
            set { Fields.SportSessionsPerWeek[this] = value; }
        }

        [DisplayName("Athlete Live"), Expression("jAthlete.[live]"), ReadOnly(true)]
        public Int32? AthleteLive
        {
            get { return Fields.AthleteLive[this]; }
            set { Fields.AthleteLive[this] = value; }
        }

        [DisplayName("Athlete Username"), Expression("jAthlete.[Username]"), ReadOnly(true)]
        public String AthleteUsername
        {
            get { return Fields.AthleteUsername[this]; }
            set { Fields.AthleteUsername[this] = value; }
        }

        [DisplayName("Athlete Stu Id"), Expression("jAthlete.[stuID]"), ReadOnly(true)]
        public String AthleteStuId
        {
            get { return Fields.AthleteStuId[this]; }
            set { Fields.AthleteStuId[this] = value; }
        }

        [DisplayName("Athlete First Name"), Expression("jAthlete.[FirstName]"), ReadOnly(true)]
        public String AthleteFirstName
        {
            get { return Fields.AthleteFirstName[this]; }
            set { Fields.AthleteFirstName[this] = value; }
        }

        [DisplayName("Athlete Last Name"), Expression("jAthlete.[LastName]"), ReadOnly(true)]
        public String AthleteLastName
        {
            get { return Fields.AthleteLastName[this]; }
            set { Fields.AthleteLastName[this] = value; }
        }

        [DisplayName("DisplayName"), Expression("jAthlete.[LastName] + ', ' + jAthlete.[FirstName]"), ReadOnly(true)]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Athlete Sport Id"), Expression("jAthlete.[SportID]"), ReadOnly(true)]
        public Int32? AthleteSportId
        {
            get { return Fields.AthleteSportId[this]; }
            set { Fields.AthleteSportId[this] = value; }
        }

        [DisplayName("Athlete Trainer Id"), Expression("jAthlete.[TrainerID]"), ReadOnly(true)]
        public Int32? AthleteTrainerId
        {
            get { return Fields.AthleteTrainerId[this]; }
            set { Fields.AthleteTrainerId[this] = value; }
        }

        [DisplayName("Athlete Gender Id"), Expression("jAthlete.[GenderID]"), ReadOnly(true)]
        public Int32? AthleteGenderId
        {
            get { return Fields.AthleteGenderId[this]; }
            set { Fields.AthleteGenderId[this] = value; }
        }

        [DisplayName("Athlete Year Id"), Expression("jAthlete.[YearID]"), ReadOnly(true)]
        public Int32? AthleteYearId
        {
            get { return Fields.AthleteYearId[this]; }
            set { Fields.AthleteYearId[this] = value; }
        }

        [DisplayName("Athlete User Image"), Expression("jAthlete.[UserImage]"), ReadOnly(true)]
        public String AthleteUserImage
        {
            get { return Fields.AthleteUserImage[this]; }
            set { Fields.AthleteUserImage[this] = value; }
        }

        [DisplayName("Athlete Manual Image"), Expression("jAthlete.[ManualImage]"), ReadOnly(true)]
        public String AthleteManualImage
        {
            get { return Fields.AthleteManualImage[this]; }
            set { Fields.AthleteManualImage[this] = value; }
        }

        [DisplayName("Athlete Position Id"), Expression("jAthlete.[positionId]"), ReadOnly(true)]
        public Int32? AthletePositionId
        {
            get { return Fields.AthletePositionId[this]; }
            set { Fields.AthletePositionId[this] = value; }
        }

        

        [DisplayName("Test ID"), ReadOnly(true), LookupInclude]
        public Int32? testID
        {
            get { return Fields.testID[this]; }
            set { Fields.testID[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Test1Label; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteTestingRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthleteId;
            public StringField Test1Label;
            public DoubleField Test1Result;
            public StringField Test2Label;
            public DoubleField Test2Result;
            public StringField Test3Label;
            public DoubleField Test3Result;
            public StringField Test4Label;
            public DoubleField Test4Result;
            public StringField Test5Label;
            public DoubleField Test5Result;
            public DateTimeField OnDate;
            public Int32Field SportId;

            public Int32Field AthleteLive;
            public StringField AthleteUsername;
            public StringField AthleteStuId;
            public StringField AthleteFirstName;
            public StringField AthleteLastName;
            public Int32Field AthleteSportId;
            public Int32Field AthleteTrainerId;
            public Int32Field AthleteGenderId;
            public Int32Field AthleteYearId;
            public StringField AthleteUserImage;
            public StringField AthleteManualImage;
            public Int32Field AthletePositionId;

            public StringField Sport;
            public Int32Field SportSessionsPerWeek;

            public StringField DisplayName;

            public StringField TermDisplay;

            public Int32Field testID;
            public RowFields()
                : base("[dbo].[AthleteTesting]")
            {
                LocalTextPrefix = "Setup.AthleteTesting";
            }
        }
    }
}