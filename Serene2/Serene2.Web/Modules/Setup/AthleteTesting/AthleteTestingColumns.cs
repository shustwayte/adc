﻿
namespace Serene2.Setup.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Setup.AthleteTesting")]
    [BasedOnRow(typeof(Entities.AthleteTestingRow))]
    public class AthleteTestingColumns
    {
        // [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        // public Int32 Id { get; set; }
        //public Int32 AthleteId { get; set; }
        [Width(80), DisplayName("Test No."), QuickFilter ]
        public Int32 testID { get; set; }

        [EditLink, Width(120)]
        public String DisplayName { get; set; }

        
        [Width(100), LookupEditor(typeof(Serene2.Modules.Setup.AthleteTesting.SportDisplay)), QuickFilter]
        public String Sport { get; set; }
        [Width(80), LookupEditor(typeof(Serene2.Modules.Setup.AthleteTesting.TermDisplay)), QuickFilter]
        public String TermDisplay { get; set; }
        [Width(150)]
        public String Test1Label { get; set; }
        public Double Test1Result { get; set; }
        [Width(150)]
        public String Test2Label { get; set; }
        public Double Test2Result { get; set; }
        [Width(150)]
        public String Test3Label { get; set; }
        public Double Test3Result { get; set; }
        [Width(150)]
        public String Test4Label { get; set; }
        public Double Test4Result { get; set; }
        [Width(150)]
        public String Test5Label { get; set; }
        public Double Test5Result { get; set; }
    }
}