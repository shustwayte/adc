﻿
namespace Serene2.Setup.Endpoints
{
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System.Data;
    using System.Web.Mvc;
    using MyRepository = Repositories.AthleteTestingRepository;
    using MyRow = Entities.AthleteTestingRow;


    using Entities;
    using System;
    
    using OfficeOpenXml;
    using System.IO;
    using System.Collections.Generic;
    using Repositories;


    using System.Data.SqlClient;


    [RoutePrefix("Services/Setup/AthleteTesting"), Route("{action}")]
    [ConnectionKey("Default"), ServiceAuthorize("Testing")]
    public class AthleteTestingController : ServiceEndpoint
    {

        public class SPData
        {
            public string Sport { get; set; }
            public IEnumerable<object> OrderIDs { get; internal set; }
        }

        public class ExecuteCreateResponse : ServiceResponse
        {
            public string outcome { get; set; }
            public List<string> ErrorList { get; set; }

        }

        [HttpPost]
        public ExecuteCreateResponse ExecuteCreateTest(IDbConnection connection, SPData request)
        {
            var response = new ExecuteCreateResponse();

            try
            {
                // your C# Code for calling a SQL Stored procedure here...
                var constring = SqlConnections.NewByKey("Default");
                SqlConnection con = new SqlConnection(constring.ConnectionString);


                con.Open();

                var res = "";

                using (SqlCommand command = new SqlCommand(" exec [dbo].p_CreateTest '" + request.Sport + "', ''  ", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //Console.WriteLine("{0} {1} {2}",
                        //  reader.GetInt32(0), reader.GetString(1), reader.GetString(2));

                        res = reader.GetString(0);
                        response.outcome = res;
                    }

                }
            }
            catch (Exception ex)
            {
                response.ErrorList.Add("Error: " + ex.Message);
            }

            //return "ok";
            return response;
        }


        [HttpPost]
        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Create(uow, request);
        }

        [HttpPost]
        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Update(uow, request);
        }
 
        [HttpPost]
        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyRepository().Delete(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRepository().Retrieve(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {
            return new MyRepository().List(connection, request);
        }
    }
}
