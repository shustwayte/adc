﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Setup/AthleteTesting", typeof(Serene2.Setup.Pages.AthleteTestingController))]

namespace Serene2.Setup.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Setup/AthleteTesting"), Route("{action=index}")]
    public class AthleteTestingController : Controller
    {
        [PageAuthorize("Setup")]
        public ActionResult Index()
        {
            return View("~/Modules/Setup/AthleteTesting/AthleteTestingIndex.cshtml");
        }
    }
}