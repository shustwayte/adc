﻿namespace Serene2.AthleteTesting {

    @Serenity.Decorators.registerClass()
    export class CreateTestDialog extends Serenity.PropertyDialog<any, any> {

        private formImp: CreateTestForm;

        constructor() {
            super();

            this.formImp = new CreateTestForm(this.idPrefix);
        }

        protected getDialogTitle(): string {
            return "Create Test";
        }

       
        protected getDialogButtons(): Serenity.DialogButton[] {
            return [
                {
                    
                    text: 'Create Test',
                    click: () => {
                        if (!this.validateBeforeSave())
                            return;

                        if (this.formImp.Sport.value == null ) {
                            Q.notifyError("Please select a sport!");
                            return;
                        }

                        Setup.AthleteTestingService.ExecuteCreateTest({
                            Sport: this.formImp.Sport.value
                        }, response => {

                            Q.information(Q.text(response.outcome ), () => {
                            });


                            /*
                             Q.notifySuccess(
                                 'Inserted: ' + (response.Inserted || 0) +
                                 ', Updated: ' + (response.Updated || 0));
                            */

                            if (response.ErrorList.length > 0) {
                                Q.warning(response.ErrorList.join(',\r\n '));
                            }

                            this.dialogClose();
                        });
                    },
                },
                {
                    text: 'Cancel',
                    click: () => this.dialogClose()
                }
            ];
        }
    }
}