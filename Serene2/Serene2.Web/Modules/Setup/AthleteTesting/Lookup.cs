﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serene2.Modules.Setup.AthleteTesting
{

    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Web;

    [LookupScript("TestDisplay")]
    public class TestDisplay : RowLookupScript<Serene2.Setup.Entities.AthleteTestingRow>
    {
        public TestDisplay()
        {
            IdField = TextField = "testID";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.AthleteTestingRow.Fields;
            query.Distinct(true)
                .Select(fld.testID)
                .Where(
                    new Criteria(fld.testID) != "" &
                    new Criteria(fld.testID).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

    [LookupScript("TermDisplay")]
    public class TermDisplay : RowLookupScript<Serene2.Setup.Entities.AthleteTestingRow>
    {
        public TermDisplay()
        {
            IdField = TextField = "TermDisplay";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.AthleteTestingRow.Fields;
            query.Distinct(true)
                .Select(fld.TermDisplay)
                .Where(
                    new Criteria(fld.TermDisplay) != "" &
                    new Criteria(fld.TermDisplay).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

    [LookupScript("SportDisplay")]
    public class SportDisplay : RowLookupScript<Serene2.Setup.Entities.AthleteTestingRow>
    {
        public SportDisplay()
        {
            IdField = TextField = "Sport";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.AthleteTestingRow.Fields;
            query.Distinct(true)
                .Select(fld.Sport)
                .Where(
                    new Criteria(fld.Sport) != "" &
                    new Criteria(fld.Sport).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

}