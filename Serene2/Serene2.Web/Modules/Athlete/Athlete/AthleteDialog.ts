﻿
namespace Serene2.Athlete {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteDialog extends Serenity.EntityDialog<AthleteRow, any> {
        protected getFormKey() { return AthleteForm.formKey; }
        protected getIdProperty() { return AthleteRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteRow.localTextPrefix; }
        protected getNameProperty() { return AthleteRow.nameProperty; }
        protected getService() { return AthleteService.baseUrl; }

        protected form = new AthleteForm(this.idPrefix);


        private athleteGrid: AthleteGrid;

        private assessmentGrid: Assessment.WellBeingGrid;

        private physioGrid: AthletePhysio.AthletePhysioGrid;

        private athleteWeight: AthleteWeight.AthleteWeightGrid;

        private athleteRPE: RPE.RpeGrid;

        private audit: Audit.AuditLogGrid;


        private athleteNote: AthleteNote.AthleteNoteGrid;


        constructor() {
            super();

            
            // var chk = this.element[0].innerHTML;
            //  this.element[0].innerHTML = this.element[0].innerHTML.replace('<span class="button-inner">Select File</span>', '');

            //  this.element[0].innerHTML = this.element[0].innerHTML.replace("User Image<", "TEST FRED<");

            this.assessmentGrid = new Assessment.WellBeingGrid(this.byId('UpdateAthleteGrid'));
            this.assessmentGrid.element.flexHeightOnly(1);

            this.physioGrid = new AthletePhysio.AthletePhysioGrid(this.byId('UpdateAthletePhysio'));
            this.physioGrid.element.flexHeightOnly(1);

            this.audit = new Audit.AuditLogGrid(this.byId('ActionAuditGrid'));
            this.audit.element.flexHeightOnly(1);

            this.athleteWeight = new AthleteWeight.AthleteWeightGrid(this.byId('UpdateAthleteWeight'));
            this.athleteWeight.element.flexHeightOnly(1);

            this.athleteRPE = new RPE.RpeGrid(this.byId('AthleteRPEGrid'));
            this.athleteRPE.element.flexHeightOnly(1);

            this.athleteNote = new AthleteNote.AthleteNoteGrid(this.byId('AthleteNoteGrid'));
            this.athleteNote.element.flexHeightOnly(1);

            this.tabs.bind('tabsactivate', () => this.arrange());

            this.tabs.on('tabsactivate', (e, i) => {
                this.arrange();
            });


            Serenity.SubDialogHelper.triggerDataChange(this);
            

        }


        loadEntity(entity: AthleteRow) {

            super.loadEntity(entity);

            var athid = this.entity.Id;

            this.audit.rowid = athid;
            this.audit.TableName = "[dbo].[Athlete]";

            //Serenity.TabsExtensions.setDisabled(this.tabs, 'Updates', this.isNewOrDeleted());
            this.assessmentGrid.AthleteId = athid;
            this.physioGrid.AthleteId = athid;
            this.athleteWeight.AthleteUsername = this.entity.Username;
            this.athleteNote.AthleteId = athid;
            this.athleteRPE.AthleteUsername = this.entity.Username;

        }


       


        protected getToolbarButtons(): Serenity.ToolButton[] {
            let buttons = super.getToolbarButtons();

           
            buttons.push(Serene2.Common.ReportHelper.createToolButton({
                title: 'Statement',
                cssClass: 'export-pdf-button',
                reportKey: 'Athlete.AthleteReport',
              //  extension: 'html',

                getParams: () => ({ Id: this.get_entityId() })
            }));


            return buttons;

        }


        protected updateInterface(): void {

            super.updateInterface();
            /*
            var user = Serene2.Authorization.userDefinition;
            Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), true);

            if (user.Username == "apope" || user.Username == "pd363" || user.Username == "nas217" || user.Username == "sh444" ) {
                
                Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), false);

            }
           */
            
     
           
        }

    }
}