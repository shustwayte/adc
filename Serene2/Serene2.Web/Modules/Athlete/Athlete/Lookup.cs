﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Serene2.Modules.Athlete.Athlete
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Web;


    

    [LookupScript("Username")]
    public class Username : RowLookupScript<Serene2.Athlete.Entities.AthleteRow>
    {
        public Username()
        {
            IdField = TextField = "Username";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Athlete.Entities.AthleteRow.Fields;
            query.Distinct(true)
                .Select(fld.Username)
                .Where(
                    new Criteria(fld.Username) != "" &
                    new Criteria(fld.Username).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }



    [LookupScript("dateofdisplay")]
    public class DateLoggedLookup : RowLookupScript<Serene2.Assessment.Entities.WellBeingRow>
    {
        public DateLoggedLookup()
        {
            IdField = TextField = "dateofdisplay";
        }

        protected override void PrepareQuery(SqlQuery query)
        {

            if (Serene2.Global.MyGlobals.IsAthlete != "1")
            {
                var fld = Serene2.Assessment.Entities.WellBeingRow.Fields;
                query.Distinct(true)
                    .Select(fld.dateofdisplay)
                    .Where(
                        new Criteria(fld.dateofdisplay) != "" &
                        new Criteria(fld.dateofdisplay).IsNotNull());
            }

            if (Serene2.Global.MyGlobals.IsAthlete == "1")
            {
                var user = (UserDefinition)Serenity.Authorization.UserDefinition;

                var fld = Serene2.Assessment.Entities.WellBeingRow.Fields;
                query.Distinct(true)
                    .Select(fld.dateofdisplay)
                    .Where(
                        new Criteria(fld.dateofdisplay) != "" &
                        new Criteria(fld.AthleteUsername) == user.Username &
                        new Criteria(fld.dateofdisplay).IsNotNull());
            }
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

    [LookupScript("DisplayName")]
    public class AthleteLookup : RowLookupScript<Serene2.Assessment.Entities.WellBeingRow>
    {
        public AthleteLookup()
        {
            IdField = TextField = "DisplayName";
        }

        protected override void PrepareQuery(SqlQuery query)
        {

            if (Serene2.Global.MyGlobals.IsAthlete != "1")
            {
                var fld = Serene2.Assessment.Entities.WellBeingRow.Fields;
                query.Distinct(true)
                    .Select(fld.DisplayName)
                    .Where(
                        new Criteria(fld.DisplayName) != "" &
                        new Criteria(fld.DisplayName).IsNotNull());
            }

            if (Serene2.Global.MyGlobals.IsAthlete == "1")
            {

                var user = (UserDefinition)Serenity.Authorization.UserDefinition;

                var fld = Serene2.Assessment.Entities.WellBeingRow.Fields;
                query.Distinct(true)
                    .Select(fld.DisplayName)
                    .Where(
                        new Criteria(fld.DisplayName) != "" &
                         new Criteria(fld.AthleteUsername) == user.Username &
                        new Criteria(fld.DisplayName).IsNotNull());
            }



        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }


    [LookupScript("LastAss"), DisplayFormat("dd/MM/yyyy")]
    public class LastLookup : RowLookupScript<Serene2.Athlete.Entities.AthleteRow>
    {
        public LastLookup()
        {
            IdField = TextField = "lastassessment";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Athlete.Entities.AthleteRow.Fields;
            query.Distinct(true)

                //.Select(String.Format(fld.lastassessment.ToString(),"dd/MM/yyyy") )
                .Select("convert(varchar,convert(datetime,dbo.AthleteLoggedAssessmentDate(AthLogged.Username)),103) as lastassessment")
                .Select("convert(varchar(10),dbo.AthleteLoggedAssessmentDate(AthLogged.Username),112)")
                .Where(
                    new Criteria(fld.lastassessment) != "" &
                    new Criteria(fld.lastassessment).IsNotNull())
                .OrderBy("convert(varchar(10),dbo.AthleteLoggedAssessmentDate(AthLogged.Username),112)", desc: true);
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }


    [LookupScript("Trainer")]
    public class TrainersLookup : RowLookupScript<Serene2.Setup.Entities.TrainersRow>
    {
        public TrainersLookup()
        {
            IdField = TextField = "Trainer";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.TrainersRow.Fields;
            query.Distinct(true)
                .Select(fld.Trainer)
                .Where(
                    new Criteria(fld.Trainer) != "" &
                    new Criteria(fld.Trainer).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

    [LookupScript("Sport")]
    public class SportsLookup : RowLookupScript<Serene2.Setup.Entities.SportsRow>
    {
        public SportsLookup()
        {
            IdField = TextField = "Sport";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.SportsRow.Fields;
            query.Distinct(true)
                .Select(fld.Sport)
                .Where(
                    new Criteria(fld.Sport) != "" &
                    new Criteria(fld.Sport).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }

    [LookupScript("EntryDate")]
    public class EntryDateLookup : RowLookupScript<Serene2.AthletePhysio.Entities.AthletePhysioRow>
    {
        public EntryDateLookup()
        {
            IdField = TextField = "EntryDateLkup";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.AthletePhysio.Entities.AthletePhysioRow.Fields;
            query.Distinct(true)
                .Select(fld.EntryDateLkup)
                .Where(
                    new Criteria(fld.EntryDateLkup) != "" &
                    new Criteria(fld.EntryDateLkup).IsNotNull())
                .OrderBy(fld.EntryDateLkup);
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }


    [LookupScript("Year")]
    public class YearsLookup : RowLookupScript<Serene2.Setup.Entities.AthleteYearRow>
    {
        public YearsLookup()
        {
            IdField = TextField = "Year";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.AthleteYearRow.Fields;
            query.Distinct(true)
                .Select(fld.Year)
                .Where(
                    new Criteria(fld.Year) != "" &
                    new Criteria(fld.Year).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }


    [LookupScript("Gener")]
    public class GenderLookup  : RowLookupScript<Serene2.Setup.Entities.GenderRow>
    {
        public GenderLookup()
        {
            IdField = TextField = "Gender";
        }

        protected override void PrepareQuery(SqlQuery query)
        {
            var fld = Serene2.Setup.Entities.GenderRow.Fields;
            query.Distinct(true)
                .Select(fld.Gender)
                .Where(
                    new Criteria(fld.Gender) != "" &
                    new Criteria(fld.Gender).IsNotNull());
        }

        protected override void ApplyOrder(SqlQuery query)
        {
        }
    }
}
