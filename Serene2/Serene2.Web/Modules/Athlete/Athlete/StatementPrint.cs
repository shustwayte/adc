﻿//namespace Serene2.Modules.Transactions.StuTransactions

namespace Serene2.Athlete
{
    using Assessment.Entities;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Reporting;
    using System;
    using System.Collections.Generic;

    [Report("Athlete.AthleteReport")]
    [ReportDesign(MVC.Views.Athlete.Athlete_.StatementPrint)]
    [RequiredPermission("AthleteReport")]
    public class InvoicePrint : IReport, ICustomizeHtmlToPdf
    {
        public Int32 Id { get; set; }

        public object GetData()
        {
            var data = new StatementPrintData();

            using (var connection = SqlConnections.NewFor<WellBeingRow>())
            {
                var o = WellBeingRow.Fields;

                data.Order = connection.TryById<WellBeingRow>(this.Id, q => q
                     .SelectTableFields()
                     .Select(o.AcademicStressAnswer)
                     .Select(o.AthleteUsername)
                     .Select(o.ChangeDailyRoutineAnswer)) ?? new WellBeingRow();


                var od = WellBeingRow.Fields;
                data.Details = connection.List<WellBeingRow>(q => q
                    .SelectTableFields()
                  //  .Select(od.Sport)
                    .Select(od.dateof)
                    .Select(od.Status)
                    .Select(od.InjStatusAnswer)
                    .Select(od.WeightKg)
                    
                    .Select(od.SleepAnswer)
                    .Select(od.ChangeDailyRoutineAnswer)
                    .Select(od.SoughtMedicalAttentionAnswer)
                    .Select(od.TightnessPainAnswer)
                    .Select(od.AcademicStressAnswer)
                    

                    .Where(od.AthleteId == this.Id));

                var c = Athlete.Entities.AthleteRow.Fields;
                data.Customer = connection.TryFirst<Athlete.Entities.AthleteRow>(c.Id == this.Id)
                    ?? new Athlete.Entities.AthleteRow();
            }

            return data;
        }

        public void Customize(IHtmlToPdfOptions options)
        {
            // you may customize HTML to PDF converter (WKHTML) parameters here, e.g. 
            // options.MarginsAll = "2cm";
        }
    }

    public class StatementPrintData
    {
        public WellBeingRow Order { get; set; }

        public List<WellBeingRow> Details { get; set; }
        public Athlete.Entities.AthleteRow Customer { get; set; }
    }
}