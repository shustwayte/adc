﻿namespace Serene2.Athlete {

    @Serenity.Decorators.registerClass()
    export class AthleteGridAthlete extends Serenity.EntityGrid<AthleteRow, any> {
        protected getColumnsKey() { return 'Athlete.Athlete'; }
        protected getDialogType() { return AthleteDialog; }
        protected getIdProperty() { return AthleteRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteRow.localTextPrefix; }
        protected getService() { return AthleteService.baseUrl; }

        constructor(container: JQuery) {
            super(container);

        }

        /**
         * This method is called to get list of buttons to be created.
         */
        public getButtons(): Serenity.ToolButton[] {

            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            return buttons;
        }

    }
}