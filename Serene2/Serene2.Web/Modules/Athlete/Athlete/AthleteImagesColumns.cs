﻿
namespace Serene2.Athlete.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    using Modules.Athlete.Athlete;

    [ColumnsScript("Athlete.AthleteImages")]
    [BasedOnRow(typeof(Entities.AthleteRow))]
    public class AthleteImageColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 Id { get; set; }

        [EditLink, DisplayName("Athlete")]
        public string DisplayName { get; set; }
      //  public Boolean Live { get; set; }

        [InlineImageFormatter, Width(200)]
        public String UserImage { get; set; }

        [InlineImageFormatter, Width(200)]
        public String ManualImage { get; set; }

        [Width(100), DisplayName("Attendence")]
        public Int32 Attendence { get; set; }

        public String AttendToday { get; set; }

        [TestFormatter, Width(150), DisplayName("Last 2 Assessment")]
        public String lastTwoAssessment { get; set; }

        [DisplayName("Last 2 Assessment"), FilterOnly, QuickFilter]
        public AthleteFlagStatus lastTwoAssessmentId { get; set; }

        [DisplayName("Attend Today"), FilterOnly, QuickFilter]
        public AthleteAttendStatus AthleteLoggedAssessmentToday { get; set; }

        //public String Username { get; set; }

        public String StuId { get; set; }

        /*
        public String FirstName { get; set; }
        public String LastName { get; set; }
        */
        [Width(130), DisplayName("Sport"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.SportsLookup)), QuickFilter]
        public string Sport { get; set; }

        [Width(130), DisplayName("Year"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.YearsLookup)), QuickFilter]
        public string Year { get; set; }

       //[Width(130), DisplayName("Trainer"), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.TrainersLookup)), QuickFilter]
        //public string Trainer { get; set; }

        [Width(150), TrainerListFormatter, QuickFilter]
        public string Trainers { get; set; }
    }
}