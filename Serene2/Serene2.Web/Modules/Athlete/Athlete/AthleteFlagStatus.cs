﻿
namespace Serene2.Athlete
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using Serenity.ComponentModel;

    using System.ComponentModel;

    [EnumKey("Athlete.AthleteFlagStatus")]
    public enum AthleteFlagStatus
    {
        [Description("Attention")]
        Attention = 1,
        [Description("Pass")]
        Pass = 0
        
    }

    [EnumKey("Athlete.AthleteAttendStatus")]
    public enum AthleteAttendStatus
    {
        [Description("Attend")]
        Attend = 1,
        [Description("Absent")]
        Missing = 0

    }
}