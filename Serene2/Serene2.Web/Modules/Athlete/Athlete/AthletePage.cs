﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Athlete/Athlete", typeof(Serene2.Athlete.Pages.AthleteController))]

namespace Serene2.Athlete.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Athlete/Athlete"), Route("{action=index}")]
    public class AthleteController : Controller
    {
        [PageAuthorize("Athlete")]
        public ActionResult Index()
        {
            return View("~/Modules/Athlete/Athlete/AthleteIndex.cshtml");
        }
    }
}