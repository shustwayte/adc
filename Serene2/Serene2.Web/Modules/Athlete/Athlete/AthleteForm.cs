﻿
namespace Serene2.Athlete.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    using Modules.Athlete.Athlete;

    [FormScript("Athlete.Athlete")]
    [BasedOnRow(typeof(Entities.AthleteRow))]
    public class AthleteForm
    {
        
        [Category("Details")]

        public String FirstName { get; set; }
        public String LastName { get; set; }


       // public Boolean Paid { get; set; }

        public Int32 SportId { get; set; }

        public Int32 positionId { get; set; }

        public Int32 YearId { get; set; }

        public Boolean Live { get; set; }

        // public Int32 TrainerId { get; set; }
        [DisplayName("Coaches")]
        public List<Int32> Trainers { get; set; }

        [Category("Admin Data")]

        [Width(100), DisplayName("Attendence")]
        public Int32 Attendence { get; set; }

        public String AttendToday { get; set; }

        // public Boolean Live { get; set; }
        public String Username { get; set; }
        public String StuId { get; set; }

        public Int32 GenderId { get; set; }

        [Category("Image")]
        public String UserImage { get; set; }

        public String ManualImage { get; set; }
    }
}