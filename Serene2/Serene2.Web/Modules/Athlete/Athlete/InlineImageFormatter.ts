﻿namespace Serene2.Athlete {

    @Serenity.Decorators.registerFormatter()
    export class InlineImageFormatter
        implements Slick.Formatter, Serenity.IInitializeColumn {

        format(ctx: Slick.FormatterContext): string {

            var file = (this.fileProperty ? ctx.item[this.fileProperty] : ctx.value) as string;
            if (!file || !file.length)
                return "";

            //var unc = "file:///\\\\isad.isadroot.ex.ac.uk\\uoe\\PS\\AS\\SS\\Card";
            //unc = unc + " Office Database\\IDCard\\IDPro7Data\\PR_STUDENT\\Images\\";
            //var unc = "file:///S:\\";
            var unc = "Photo/";                        

            //let href = Q.resolveUrl(unc + file);

            var href = "";

            if (file.substring(0, 9) == "UserImage") {
                href=  Q.resolveUrl('~/upload/') + file;
            }
            else {
                href = unc + file;
            }

            
            let src = Q.resolveUrl('~/upload/' + file);

            //return `<a class="inline-image" target='_blank' href="${href}">` + `<img src="${href}" style='max-height: 145px; max-width: 100%;' /></a>`;

            return `<img src="${href}" style='max-height: 145px; max-width: 100%;' />`;
        }

        initializeColumn(column: Slick.Column): void {
            if (this.fileProperty) {
                column.referencedFields = column.referencedFields || [];
                column.referencedFields.push(this.fileProperty);
            }
        }

        @Serenity.Decorators.option()
        public fileProperty: string;

        @Serenity.Decorators.option()
        public thumb: boolean;
    }



}