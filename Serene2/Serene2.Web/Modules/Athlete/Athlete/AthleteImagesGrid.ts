﻿
namespace Serene2.Athlete {

    @Serenity.Decorators.registerClass()
    export class AthleteImagesGrid extends Serenity.EntityGrid<AthleteRow, any> {
        protected getColumnsKey() { return 'Athlete.AthleteImages'; }
        protected getDialogType() { return AthleteDialog; }
        protected getIdProperty() { return AthleteRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteRow.localTextPrefix; }
        protected getService() { return AthleteService.baseUrl; }

        protected lastTwoAssessmentIdFilter: Serenity.EnumEditor;
        protected AthleteLoggedAssessmentTodayFilter: Serenity.EnumEditor;

        constructor(container: JQuery) {
            super(container);

        }

        protected createQuickFilters() {
            super.createQuickFilters();

            let fld = AthleteRow.Fields;
            this.lastTwoAssessmentIdFilter = this.findQuickFilter(Serenity.EnumEditor, fld.lastTwoAssessmentId);
            this.AthleteLoggedAssessmentTodayFilter = this.findQuickFilter(Serenity.EnumEditor, fld.AthleteLoggedAssessmentToday);
        }

        public set_lastTwoAssessmentId(value: number): void {
            this.lastTwoAssessmentIdFilter.value = value == null ? '' : value.toString();
        }

        public set_AthleteLoggedAssessmentToday(value: number): void {
            this.AthleteLoggedAssessmentTodayFilter.value = value == null ? '' : value.toString();
        }

        protected getSlickOptions(): Slick.GridOptions {
            let opt = super.getSlickOptions();
            opt.rowHeight = 150;
            return opt;
        }

        protected getButtons() {
            var buttons = super.getButtons();

            buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Athlete/Athlete/ListExcel',
                separator: true
            }));

            buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
        }

        protected getColumns() {
            var columns = super.getColumns();

            var user = Serene2.Authorization.userDefinition;


            columns.unshift({
                field: 'View',
                name: '',
                format: ctx => '<a class="inline-action view-details" title="View Details">' +
                    '<i class="fa fa-edit text-red"></i></a> ',
                width: 24,
                minWidth: 24,
                maxWidth: 24
            });

            if (user.isAthlete == true || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                columns.splice(1, 0, {
                    field: 'New Assessment',
                    name: '',
                    format: ctx => '<a class="inline-action new-assessment" title="New Daily Monitoring">' +
                        '<i class="fa fa-plus text-orange"></i></a> ',
                    width: 24,
                    minWidth: 24,
                    maxWidth: 24
                });
            }

            if (user.Roles.indexOf("Physio") >= 0 || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                columns.splice(1, 0, {
                    field: 'New Physio',
                    name: '',
                    format: ctx => '<a class="inline-action new-physio" title="New Physio">' +
                        '<i class="fa fa-road text-blue"></i></a> ',
                    width: 24,
                    minWidth: 24,
                    maxWidth: 24
                });
            }

            return columns;
        }


        protected onClick(e: JQueryEventObject, row: number, cell: number) {
            super.onClick(e, row, cell);

            if (e.isDefaultPrevented())
                return;

            var item = this.itemAt(row);

            var userid = item.Id;
            var Fname = item.FirstName;
            var Lname = item.LastName;

            var target = $(e.target);

            // if user clicks "i" element, e.g. icon
            if (target.parent().hasClass('inline-action'))
                target = target.parent();

            if (target.hasClass('inline-action')) {
                e.preventDefault();

                if (target.hasClass('view-details')) {
                    this.editItem(item.Id);
                }

                if (target.hasClass('new-physio')) {


                    var dlg = new AthletePhysio.AthletePhysioDialog();
                    this.initDialog(dlg);
                    dlg.loadEntityAndOpenDialog(<AthletePhysio.AthletePhysioRow>{
                        AthleteId: userid,
                        AthleteFirstName: Fname,
                        AthleteLastName: Lname,
                        EntryDate: Q.formatDate(new Date(), 'yyyy-MM-dd')
                    });

                }

                if (target.hasClass('new-assessment')) {


                    var dlg2 = new Assessment.WellBeingDialog();
                    this.initDialog(dlg2);
                    dlg2.loadEntityAndOpenDialog(<Assessment.WellBeingRow>{
                        AthleteId: userid,
                        Sleep: 2, // NO
                        ChangeDailyRoutine: 2,
                        SoughtMedicalAttention: 2,
                        TightnessPain: 2,
                        AcademicStress: 2,
                        dateof: Q.formatDate(new Date(), 'yyyy-MM-dd')

                    });

                }

            }

        }

    }

}
