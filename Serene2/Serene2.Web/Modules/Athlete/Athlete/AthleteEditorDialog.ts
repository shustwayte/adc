﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Athlete {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteEditorDialog extends Common.GridEditorDialog<AthleteRow> {
        protected getFormKey() { return AthleteForm.formKey; }
                protected getLocalTextPrefix() { return AthleteRow.localTextPrefix; }
        protected getNameProperty() { return AthleteRow.nameProperty; }
        protected form = new AthleteForm(this.idPrefix);

      
    }
       
}