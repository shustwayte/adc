﻿

namespace Serene2.Athlete.Repositories
{
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System;
    using System.Data;

    using MyRow = Entities.AthleteRow;

    using System.Collections.Generic;

    public class AthleteRepository
    {

        
        private static MyRow.RowFields fld { get { return MyRow.Fields; } }

        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Create);
        }

        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Update);
        }

        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyDeleteHandler().Process(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRetrieveHandler().Process(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {

            if (request.EqualityFilter.ContainsKey("Live"))
            {

                if ((request.EqualityFilter)["Live"] == null)
                {
                    request.EqualityFilter.Remove("Live");
                    request.EqualityFilter.Add("Live", 1);
                }

                else
                {
                    string g = (request.EqualityFilter)["Live"].ToString();


                    var user = Authorization.Username;

                    if (user == "apope" || user == "pd363" || user == "nas217" || user == "sh44499" || user == "admin")
                    {

                        if (g.ToString() == "True")
                        {

                            request.EqualityFilter.Remove("Live");
                            request.EqualityFilter.Add("Live", 1);

                        }

                        if (g.ToString() == "False")
                        {
                            request.EqualityFilter.Remove("Live");
                            request.EqualityFilter.Add("Live", 0);
                        }

                        if (g.ToString() == "" || g.ToString() == "null")
                        {

                            request.EqualityFilter.Remove("Live");
                            request.EqualityFilter.Add("Live", 1);

                        }
                    }

                    else
                    {

                        request.EqualityFilter.Remove("Live");
                        request.EqualityFilter.Add("Live", 1);
                    }
                }
            }
           
            /*
                        var containstext = request.ContainsText;
                        if(containstext==null )
                        {
                            if (request.EqualityFilter.ContainsKey("Live"))
                            {

                            }
                            else
                            {
                                request.EqualityFilter.Add("Live", 1);

                            }
                        }
                        */

            return new MyListHandler().Process(connection, request);
           
        }

        
           
        

        private class MySaveHandler : SaveRequestHandler<MyRow> { }
        private class MyDeleteHandler : DeleteRequestHandler<MyRow> { }
        private class MyRetrieveHandler : RetrieveRequestHandler<MyRow> { }
        private class MyListHandler : ListRequestHandler<MyRow> { }
    }
}