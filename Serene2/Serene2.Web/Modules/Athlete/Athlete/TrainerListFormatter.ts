﻿namespace Serene2.Athlete {

    @Serenity.Decorators.registerFormatter()
    export class TrainerListFormatter implements Slick.Formatter {
        format(ctx: Slick.FormatterContext) {
            var idList = ctx.value as string[];
            if (!idList || !idList.length)
                return "";

            var byId = Setup.TrainersRow.getLookup().itemById;
            let z: Setup.TrainersRow;
            return idList.map(x => ((z = byId[x]) ? z.Trainer : x)).join(", ");
        }
    }
}