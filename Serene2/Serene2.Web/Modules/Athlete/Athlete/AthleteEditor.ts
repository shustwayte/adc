﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Athlete {
    
    @Serenity.Decorators.registerClass()
    export class AthleteEditor extends Common.GridEditorBase<AthleteRow> {
        protected getColumnsKey() { return 'Athlete.Athlete'; }
        protected getDialogType() { return AthleteEditorDialog; }
                protected getLocalTextPrefix() { return AthleteRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}