﻿

namespace Serene2.Athlete.Entities
{
    using Modules.Athlete.Athlete;
    using Modules.Common.BaseClass;
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Athlete"), InstanceName("Athlete"), TwoLevelCached]
    [ReadPermission("Athlete")]
    [ModifyPermission("Athlete")]
    
    [LookupScript("WebApp.Athlete")]
    public sealed class AthleteRow : Row, IIdRow, INameRow , IAuditLog
    {
        [DisplayName("Id"), Column("Id"), Identity, ForeignKey("AthleteLoggedToday", "athleteid"), LeftJoin("AthLogged")]

        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        


        [DisplayName("Live"), Column("live")]
        public Boolean? Live
        {
            get { return Fields.Live[this]; }
            set { Fields.Live[this] = value; }
        }

        [DisplayName("Username"), Column("Username"), Size(10), QuickSearch, Updatable(false)]
        public String Username
        {
            get { return Fields.Username[this]; }
            set { Fields.Username[this] = value; }
        }

        
        [DisplayName("Stu Id"), Column("stuID"), Size(50)] //, Updatable(false)]
        public String StuId
        {
            get { return Fields.StuId[this]; }
            set { Fields.StuId[this] = value; }
        }

        [DisplayName("First Name"), Size(250), QuickSearch] //, Updatable(false)]
        public String FirstName
        {
            get { return Fields.FirstName[this]; }
            set { Fields.FirstName[this] = value; }
        }

        [DisplayName("Last Name"), Size(250), QuickSearch] //, Updatable(false)]
        public String LastName
        {
            get { return Fields.LastName[this]; }
            set { Fields.LastName[this] = value; }
        }


        [DisplayName("DisplayName"), Expression("[LastName] + ', ' + [FirstName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }


        [DisplayName("Gender"), Column("GenderID"), ForeignKey("Gender", "GenderID"), LeftJoin("jGender")] //, Updatable(false)]
        [LookupEditor(typeof(Serene2.Setup.Entities.GenderRow)), TextualField("Gender")]
        public Int32? GenderId
        {
            get { return Fields.GenderId[this]; }
            set { Fields.GenderId[this] = value; }
        }

        
        [DisplayName("Gender"), Expression("jGender.[Gender]")] //, Updatable(false)]
        public String Gender
        {
            get { return Fields.Gender[this]; }
            set { Fields.Gender[this] = value; }
        }


        //[DisplayName("Sport"), Column("SportID"), ForeignKey("[dbo].[Sports]", "SportID"), LeftJoin("jSport"), TextualField("Sport")]
        [DisplayName("Sport"), ForeignKey("Sports", "SportID"), LeftJoin("jSport")]
        [LookupEditor(typeof(Serene2.Setup.Entities.SportsRow)), TextualField("SportName")]
        public Int32? SportId
        {
            get { return Fields.SportId[this]; }
            set { Fields.SportId[this] = value; }
        }

        [DisplayName("Sport"), Expression("jSport.[Sport]"), LookupInclude]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }



        //[DisplayName("Sport"), Column("SportID"), ForeignKey("[dbo].[Sports]", "SportID"), LeftJoin("jSport"), TextualField("Sport")]
        [DisplayName("Position"), ForeignKey("AthletePosition", "Id"), LeftJoin("jAthletePosition")]
        [LookupEditor(typeof(Serene2.Setup.Entities.AthletePositionRow)), TextualField("Position")]
        public Int32? positionId
        {
            get { return Fields.positionId[this]; }
            set { Fields.positionId[this] = value; }
        }

        [DisplayName("Position"), Expression("jAthletePosition.[position]"), LookupInclude]
        public String Position
        {
            get { return Fields.Position[this]; }
            set { Fields.Position[this] = value; }
        }



        [DisplayName("Trainer"), ForeignKey("Trainers", "TrainerID"), LeftJoin("jTrainer")]
        [LookupEditor(typeof(Serene2.Setup.Entities.TrainersRow)), TextualField("TrainerName")]
        public Int32? TrainerId
        {
            get { return Fields.TrainerId[this]; }
            set { Fields.TrainerId[this] = value; }
        }



        [LookupEditor(typeof(Setup.Entities.TrainersRow), Multiple = true), NotMapped]
        [LinkingSetRelation(typeof(Setup.Entities.TrainersListRow), "athID", "TrainerId")]
        [MinSelectLevel(SelectLevel.Details)]
        [DisplayName("Trainers"), Updatable(false)]
        public System.Collections.Generic.List<Int32> Trainers
        {
            get { return Fields.Trainers[this]; }
            set { Fields.Trainers[this] = value; }
        }


        [DisplayName("Trainer"), Expression("jTrainer.[Trainer]"), LookupInclude, Updatable(false)]
        public String Trainer
        {
            get { return Fields.Trainer[this]; }
            set { Fields.Trainer[this] = value; }
        }
        
        [DisplayName("Year"), ForeignKey("AthleteYear", "YearID"), LeftJoin("jAthYear")]
        [LookupEditor(typeof(Serene2.Setup.Entities.AthleteYearRow)), TextualField("YearName")]
        public Int32? YearId
        {
            get { return Fields.YearID[this]; }
            set { Fields.YearID[this] = value; }
        }
        [DisplayName("Year"), Expression("jAthYear.[Year]"), LookupInclude]
        public String Year
        {
            get { return Fields.Year[this]; }
            set { Fields.Year[this] = value; }
        }


        [DisplayName("System Image"), Size(100), ReadOnly(true), ImageUploadEditor()]
        //[ImageUploadEditor(FilenameFormat = "UserImage/~", CopyToHistory = true)]
        
        //  \\isad.isadroot.ex.ac.uk\uoe\PS\AS\SS\Card Office Database\IDCard\IDPro7Data\PR_STUDENT\Images\
        public String UserImage
        {
            get { return Fields.UserImage[this]; }
            set { Fields.UserImage[this] = value; }
        }

        [DisplayName("Attendence"), Expression(" AthLogged.[attendence] "), Updatable(false)]
        
        public Int32? Attendence
        {
            get { return Fields.Attendence[this]; }
            set { Fields.Attendence[this] = value; }
        }


        [DisplayName("Logged Today"), Expression("  isnull(AthLogged.[AthleteLoggedAssessmentToday],0)  "), Updatable(false)]

        public AthleteAttendStatus? AthleteLoggedAssessmentToday
        {
            get { return (AthleteAttendStatus?)Fields.AthleteLoggedAssessmentToday[this]; }
            set { Fields.AthleteLoggedAssessmentToday[this] = (Int32?)value; }
        }


        [DisplayName("Logged Today"), Expression("  case when isnull(AthLogged.[AthleteLoggedAssessmentToday],0) = 1 then 'Attended' else 'Absent' end "), Updatable(false)]

        public String AttendToday
        {
            get { return Fields.AttendToday[this]; }
            set { Fields.AttendToday[this] = value; }
        }


        [DisplayName("Last Two Assessment"), Expression(" case when AthLogged.[recentAssessmentPass] = 1 then 'Attention' when AthLogged.[recentAssessmentPass] = 0 then 'Pass' else 'Pass' end "), Updatable(false)]
        public String lastTwoAssessment
        {
            get { return Fields.lastTwoAssessment[this]; }
            set { Fields.lastTwoAssessment[this] = value; }
        }
        
        /*
        [DisplayName("Last Two Assessment ID"), Expression(" [recentAssessmentPass] "), Updatable(false)]
        public Int32? lastTwoAssessmentId
        {
            get { return Fields.lastTwoAssessmentId[this]; }
            set { Fields.lastTwoAssessmentId[this] = value; }
        }
        */
        [DisplayName("Last Two Assessment ID"), Expression(" isnull(AthLogged.[recentAssessmentPass],0) "), Updatable(false)]
        public AthleteFlagStatus? lastTwoAssessmentId
        {
            get { return (AthleteFlagStatus?)Fields.lastTwoAssessmentId[this]; }
            set { Fields.lastTwoAssessmentId[this] = (Int32?)value; }
        }


        [DisplayName("Uploaded Image"), Size(100)]
        [ImageUploadEditor(FilenameFormat = "UserImage/~", CopyToHistory = true)]
        public String ManualImage
        {
            get { return Fields.ManualImage[this]; }
            set { Fields.ManualImage[this] = value; }
        }


        [Expression(" dbo.AthleteLoggedAssessmentDate(AthLogged.Username)  "), DisplayFormat("dd/MM/yyyy")]
        public String lastassessment
        {
            get { return Fields.lastassessment[this]; }
            set { Fields.lastassessment[this] = value; }
        }


      

        [DisplayName("Paid"), QuickSearch]
        public Int16? Paid
        {
            get { return Fields.Paid[this]; }
            set { Fields.Paid[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Username; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public BooleanField Live;
            public StringField Username;
            public StringField StuId;
            public StringField FirstName;
            public StringField LastName;
            public Int32Field SportId;

            public Int32Field TrainerId;

            public ListField<Int32> Trainers;

            public Int32Field GenderId;

            public StringField Sport;

            public StringField Trainer;

            public StringField Gender;


            public Int32Field YearID;
            public StringField Year;

            public StringField DisplayName;

            public StringField UserImage;

            public Int32Field Attendence;

            public StringField lastTwoAssessment;

            public Int32Field lastTwoAssessmentId;

            public Int32Field AthleteLoggedAssessmentToday;

            public StringField lastassessment;
            

            public StringField AttendToday;

            public StringField ManualImage;

            public Int32Field positionId;
            public StringField Position;

            public Int16Field Paid;

            public RowFields()
                : base("[dbo].[Athlete]")
            {
                LocalTextPrefix = "Athlete.Athlete";
            }
        }
    }
}