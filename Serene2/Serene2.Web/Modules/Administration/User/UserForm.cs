﻿namespace Serene2.Administration.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [FormScript("Administration.User")]
    [BasedOnRow(typeof(Entities.UserRow))]
    public class UserForm
    {
        [Category("Details")]
        public String Username { get; set; }
        public String DisplayName { get; set; }

        public Boolean isAthlete { get; set; }

        public Boolean isLive { get; set; }
        public Boolean Paid { get; set; }


        [EmailEditor]
        public String Email { get; set; }
        public String UserImage { get; set; }

        [PasswordEditor, Required(true), Visible(false)]
        public String Password { get; set; }
        [PasswordEditor, OneWay, Required(true), Visible(false)]
        public String PasswordConfirm { get; set; }
        [OneWay]
        public string Source { get; set; }

        

    }
}