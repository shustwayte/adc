﻿namespace Serene2
{
    using Serenity.ComponentModel;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This data will be available from script code using a dynamic script.
    /// Add properties you need from script code and set them in UserEndpoint.GetUserData.
    /// </summary>
    [ScriptInclude]
    public class ScriptUserDefinition
    {
        public String Username { get; set; }
        public String DisplayName { get; set; }
        public Boolean IsAdmin { get; set; }
        public Boolean isAthlete { get; set; }


        public String AthleteID { get; set; }

        public String ID { get; set; }
        public Dictionary<string, bool> Permissions { get; set; }

        public List<string> Roles { get; set; }

        public String AthleteAlreadyLoggedAss { get; set; }

        public String AthleteAlreadyLoggedRPE { get; set; }

        public String recentAssessmentPass { get; set; }

    }
}