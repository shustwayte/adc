﻿namespace Serene2.Administration
{
    using Serenity;
    using Serenity.Abstractions;
    using Serenity.Data;
    using System;
    using System.Data;
    using MyRow = Entities.UserRow;

    using Entities;
    using System.Collections.Generic;
    
    public class UserRetrieveService : IUserRetrieveService
    {
        private static MyRow.RowFields fld { get { return MyRow.Fields; } }

        private UserDefinition GetFirst(IDbConnection connection, BaseCriteria criteria)
        {
            
            var user = connection.TrySingle<Entities.UserRow>(criteria);

            if(user  != null)
            {
                var Athfld = Serene2.Athlete.Entities.AthleteRow.Fields;
                var AthIDValue = "";


                using (var connectionD = SqlConnections.NewByKey("Default"))
                {
                    var AthID = "";
                    connectionD.List<Athlete.Entities.AthleteRow>(q => q
                            .Select(Athfld.Id)
                            .Where(new Criteria(Athfld.Username) == user.Username))
                            .ForEach(x => AthID = x.Id.ToString());

                    AthIDValue = AthID;
                }


                var UserRolefld = Serene2.Administration.Entities.UserRoleRow.Fields;
                var isPhysio = "";

                using (var connectionD = SqlConnections.NewByKey("Default"))
                {
                    var Physio = "";
                    connectionD.List<Administration.Entities.UserRoleRow>(q => q
                            .Select(UserRolefld.UserId)
                            .Where(new Criteria(UserRolefld.Username) == user.Username
                            && UserRolefld.RoleId == 5))
                            .ForEach(x => Physio = x.UserId.ToString());
                    isPhysio = Physio;
                }


                var AssessmentTodayfld = Serene2.AthleteLoggedToday.Entities.AthleteLoggedTodayRow.Fields;
                var AthleteAlreadyLoggedAss = "";
                var AthleteAlreadyLoggedRPE = "";

                using (var connectionD = SqlConnections.NewByKey("Default"))
                {
                    var AthleteAlreadyLogged = "";
                    connectionD.List<AthleteLoggedToday.Entities.AthleteLoggedTodayRow>(q => q
                            .Select(AssessmentTodayfld.AthleteLoggedAssessmentToday)
                            .Where(new Criteria(AssessmentTodayfld.Username) == user.Username))
                            .ForEach(x => AthleteAlreadyLogged = x.AthleteLoggedAssessmentToday.ToString());
                    AthleteAlreadyLoggedAss = AthleteAlreadyLogged;
                }


                using (var connectionD = SqlConnections.NewByKey("Default"))
                {
                    var AthleteAlreadyLogged = "";
                    connectionD.List<AthleteLoggedToday.Entities.AthleteLoggedTodayRow>(q => q
                            .Select(AssessmentTodayfld.AthleteLoggedRPEToday)
                            .Where(new Criteria(AssessmentTodayfld.Username) == user.Username))
                            .ForEach(x => AthleteAlreadyLogged = x.AthleteLoggedRPEToday.ToString());
                    AthleteAlreadyLoggedRPE = AthleteAlreadyLogged;
                }


                var recentAssessmentPassfld = Serene2.AthleteLoggedToday.Entities.AthleteLoggedTodayRow.Fields;
                var recentAssessmentValue = "";

                using (var connectionD = SqlConnections.NewByKey("Default"))
                {
                    var recentAssessmentPassflag = "";
                    connectionD.List<AthleteLoggedToday.Entities.AthleteLoggedTodayRow>(q => q
                            .Select(recentAssessmentPassfld.recentAssessmentPass)
                            .Where(new Criteria(recentAssessmentPassfld.Username) == user.Username))
                            .ForEach(x => recentAssessmentPassflag = x.recentAssessmentPass.ToString());
                    recentAssessmentValue = recentAssessmentPassflag;
                }


                if (user != null)
                    return new UserDefinition
                    {
                        UserId = user.UserId.Value,
                        Username = user.Username,
                        Email = user.Email,
                        UserImage = user.UserImage,
                        DisplayName = user.DisplayName,
                        IsActive = user.IsActive.Value,
                        Source = user.Source,
                        PasswordHash = user.PasswordHash,
                        PasswordSalt = user.PasswordSalt,
                        UpdateDate = user.UpdateDate,
                        LastDirectoryUpdate = user.LastDirectoryUpdate,
                        isAthlete = user.isAthlete.Value,
                        isPhysio = isPhysio,
                        AthleteID = AthIDValue,
                        AthleteAlreadyLoggedAss = AthleteAlreadyLoggedAss,
                        AthleteAlreadyLoggedRPE = AthleteAlreadyLoggedRPE,
                        recentAssessmentPass = recentAssessmentValue
                    };

                return null;

            }

            else
            {
                return null;
            }
        }

        public IUserDefinition ById(string id)
        {
            TwoLevelCache.ExpireGroupItems("UserDefinition");
            return TwoLevelCache.Get<UserDefinition>("UserByID_" + id, TimeSpan.Zero, TimeSpan.Zero, fld.GenerationKey, () =>
            {
                using (var connection = SqlConnections.NewByKey("Default"))
                    return GetFirst(connection, new Criteria(fld.UserId) == Int32.Parse(id));
            });
        }

        public IUserDefinition ByUsername(string username)
        {
            if (username.IsEmptyOrNull())
                return null;
            //TwoLevelCache.ExpireGroupItems("UserDefinition");
            return TwoLevelCache.Get<UserDefinition>("UserByName_" + username.ToLowerInvariant(),
                  TimeSpan.FromMilliseconds(5), TimeSpan.FromMilliseconds(5), fld.GenerationKey, () =>
                //TimeSpan.Zero, TimeSpan.FromDays(1), fld.GenerationKey, () =>
                {
                using (var connection = SqlConnections.NewByKey("Default"))
                    return GetFirst(connection, new Criteria(fld.Username) == username);
            });
        }

        public static void RemoveCachedUser(int? userId, string username)
        {
            if (userId != null)
                TwoLevelCache.Remove("UserByID_" + userId);

            if (username != null)
                TwoLevelCache.Remove("UserByName_" + username.ToLowerInvariant());
        }
    }
}