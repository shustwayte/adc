﻿namespace Serene2.Administration {

    @Serenity.Decorators.registerClass()
    export class UserGrid extends Serenity.EntityGrid<UserRow, any> {
        protected getColumnsKey() { return "Administration.User"; }
        protected getDialogType() { return UserDialog; }
        protected getIdProperty() { return UserRow.idProperty; }
       // protected getIsActiveProperty() { return UserRow.isActiveProperty; }
        protected getLocalTextPrefix() { return UserRow.localTextPrefix; }
        protected getService() { return UserService.baseUrl; }

        private pendingChanges: Q.Dictionary<any> = {};

        private rowSelection: Serenity.GridRowSelectionMixin;

        constructor(container: JQuery) {
            super(container);

            this.slickContainer.on('change', '.edit:input', (e) => this.inputsChange(e));
        }

        protected createSlickGrid() {
            var grid = super.createSlickGrid();

            // need to register this plugin for grouping or you'll have errors
            grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());

            this.view.setSummaryOptions({
                aggregators: [
                    new Slick.Aggregators.Sum('isAthlete'),
                    new Slick.Aggregators.Sum('isLive'),
                    new Slick.Aggregators.Sum('Paid')
                ]
            });

            return grid;
        }

        protected getSlickOptions() {
            var opt = super.getSlickOptions();
            opt.showFooterRow = true;
            return opt;
        }

        protected usePager() {
            return false;
        }


        protected createToolbarExtensions() {
            super.createToolbarExtensions();
            this.rowSelection = new Serenity.GridRowSelectionMixin(this);
        }

        private inputsChange(e: JQueryEventObject) {
            var cell = this.slickGrid.getCellFromEvent(e);
            var item = this.itemAt(cell.row);
            var input = $(e.target);
            var field = input.data('field');
            var text = Q.coalesce(Q.trimToNull(input.val()), '0');


            var pending = this.pendingChanges[item.UserId];

            var effective = this.getEffectiveValue(item, field);
            
            var oldText: string;
           
            if (input.hasClass("numeric"))
                oldText = Q.formatNumber(effective, '0.##');
            else
                oldText = effective as string;
          
            var value;


            if (field === 'UnitPrice') {
                value = Q.parseDecimal(text);
                if (value == null || isNaN(value)) {
                    Q.notifyError(Q.text('Validation.Decimal'), '', null);
                    input.val(oldText);
                    input.focus();
                    return;
                }
            }
            else if (input.hasClass("numeric")) {
                var i = Q.parseDecimal(text);
                if (isNaN(i) || i > 32767 || i < 0) {
                    Q.notifyError(Q.text('Validation.Decimal'), '', null);
                    input.val(oldText);
                    input.focus();
                    return;
                }
                value = i;
            }
            else
                value = text;

            if (field == "isLive") {
                if (item.isLive == 1) {
                    value = 0;
                }
                else {
                    value = 1;
                }
            }

            if (field == "Paid") {
                if (item.Paid == 1) {
                    value = 0;
                }
                else {
                    value = 1;
                }
            }


           /*
            //if (input.val(oldText)[0].getAttribute("checked").toString() == "true") {
            if (oldText == "1") {
                value = 1;
            }
            else
            {
                value = 0;
            }
           */

            if (!pending) {
                this.pendingChanges[item.UserId] = pending = {};
            }

            pending[field] = value;
            item[field] = value;
            this.view.refresh();

  //          if (input.hasClass("numeric"))
//                value = Q.formatNumber(value, '0.##');

            input.val(value).addClass('dirty');

            this.setSaveButtonState();
        }

        protected getButtons(): Serenity.ToolButton[] {

            // call base method to get list of buttons
            // by default, base entity grid adds a few buttons, 
            // add, refresh, column selection in order.
            var buttons = super.getButtons();

          //  buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

           

            buttons.push({
                title: 'Save Changes',
                cssClass: 'apply-changes-button disabled',
                onClick: e => this.saveClick(),
                separator: true
            });

            return buttons;
        }

        protected onViewProcessData(response) {
            this.pendingChanges = {};
            this.setSaveButtonState();
            return super.onViewProcessData(response);
        }


        private numericInputFormatter(ctx) {
            var klass = 'edit checkbox';
            var item = ctx.item as UserRow;
            var pending = this.pendingChanges[item.UserId];

            if (pending && pending[ctx.column.field] !== undefined) {
                klass += ' dirty';
            }

            var value = this.getEffectiveValue(item, ctx.column.field) as number;

            if (item.isLive == 1 )
                {
                return "<input type='checkbox' class='" + klass +
                    "' data-field='" + ctx.column.field +
                    "' checked />";
            }
            else
            {
                return "<input type='checkbox' class='" + klass +
                    "' data-field='" + ctx.column.field +
                    "'  />";
            }


            
        }


        private numericInputFormatterPaid(ctx) {
            var klass = 'edit checkbox';
            var item = ctx.item as UserRow;
            var pending = this.pendingChanges[item.UserId];

            if (pending && pending[ctx.column.field] !== undefined) {
                klass += ' dirty';
            }

            var value = this.getEffectiveValue(item, ctx.column.field) as number;

            if (item.Paid == 1) {
                return "<input type='checkbox' class='" + klass +
                    "' data-field='" + ctx.column.field +
                    "' checked />";
            }
            else {
                return "<input type='checkbox' class='" + klass +
                    "' data-field='" + ctx.column.field +
                    "'  />";
            }



        }

        private stringInputFormatter(ctx) {
            var klass = 'edit string';
            var item = ctx.item as UserRow;
            var pending = this.pendingChanges[item.UserId];
            var column = ctx.column as Slick.Column;

            if (pending && pending[column.field] !== undefined) {
                klass += ' dirty';
            }

            var value = this.getEffectiveValue(item, column.field) as string;

          
            return "<input type='text' class='" + klass +
                "' data-field='" + column.field +
                "' value='" + Q.htmlEncode(value) +
                "' maxlength='" + column.sourceItem.maxLength + "'/>";

        }

        protected getColumns() {
            var columns = super.getColumns();
            var num = ctx => this.numericInputFormatter(ctx);
            var num2 = ctx => this.numericInputFormatterPaid(ctx);
            
            var str = ctx => this.stringInputFormatter(ctx);
            var fld = UserRow.Fields;

            Q.first(columns, x => x.field === fld.isLive).format = num;

            var user = Serene2.Authorization.userDefinition;

            if (user.Username == "apope" || user.Username == "pd363" || user.Username == "nas217" || user.Username == "crh207" || user.Username == "sh444") {

                Q.first(columns, x => x.field === fld.Paid).format = num2;
            }

            Q.first(columns, x => x.field === 'isAthlete')
                .groupTotalsFormatter = (totals, col) =>
                    (totals.sum ? ('Athletes: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');

            Q.first(columns, x => x.field === 'isLive')
                .groupTotalsFormatter = (totals, col) =>
                    (totals.sum ? ('Live: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');

            Q.first(columns, x => x.field === 'Paid')
                .groupTotalsFormatter = (totals, col) =>
                    (totals.sum ? ('Paid: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');


            return columns;
        }


        private selectFormatter(ctx: Slick.FormatterContext, idField: string, lookup: Q.Lookup<any>) {
            var fld = UserRow.Fields;
            var klass = 'edit';
            var item = ctx.item as UserRow;
            var pending = this.pendingChanges[item.UserId];
            var column = ctx.column as Slick.Column;

            if (pending && pending[idField] !== undefined) {
                klass += ' dirty';
            }

            var value = this.getEffectiveValue(item, idField);
            var markup = "<select class='" + klass +
                "' data-field='" + idField +
                "' style='width: 100%; max-width: 100%'>";
            for (var c of lookup.items) {
                let id = c[lookup.idField];
                markup += "<option value='" + id + "'"
                if (id == value) {
                    markup += " selected";
                }
                markup += ">" + Q.htmlEncode(c[lookup.textField]) + "</option>";
            }
            return markup + "</select>";
        }

        private getEffectiveValue(item, field): any {
            var pending = this.pendingChanges[item.ProductID];
            if (pending && pending[field] !== undefined) {
                return pending[field];
            }

            return item[field];
        }

        private setSaveButtonState() {
            this.toolbar.findButton('apply-changes-button').toggleClass('disabled',
                Object.keys(this.pendingChanges).length === 0);
        }

        private saveClick() {
            if (Object.keys(this.pendingChanges).length === 0) {
                return;
            }

            // this calls save service for all modified rows, one by one
            // you could write a batch update service
            var keys = Object.keys(this.pendingChanges);
            var current = -1;
            var self = this;

            (function saveNext() {
                if (++current >= keys.length) {
                    self.refresh();
                    return;
                }

                var key = keys[current];
                var entity = Q.deepClone(self.pendingChanges[key]);
                entity.UserId = key;
                Q.serviceRequest('Administration/User/Update', {
                    EntityId: key,
                    Entity: entity
                }, (response) => {
                    delete self.pendingChanges[key];
                    saveNext();
                });
            })();
        }

        protected getDefaultSortBy() {
            return [UserRow.Fields.Username];
        }
    }
}