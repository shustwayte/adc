﻿
namespace Serene2.Administration.Forms
{
    using Serenity.ComponentModel;
    using System;

    [ColumnsScript("Administration.User")]
    [BasedOnRow(typeof(Entities.UserRow))]
    public class UserColumns
    {
        //[EditLink, AlignRight, Width(55)]
        //public String UserId { get; set; }
        [EditLink, Width(150)]
        public String Username { get; set; }
        [Width(150)]
        public String DisplayName { get; set; }
        [Width(250)]
        public String Email { get; set; }
        [Width(100)]
        public String Source { get; set; }

        [Width(150), QuickFilter]
        public Boolean isAthlete { get; set; }

        [Width(150), QuickFilter]
        public Boolean isLive { get; set; }

        [Width(150), QuickFilter]
        public Boolean Paid { get; set; }

        [Width(130), LookupEditor(typeof(Serene2.Modules.Athlete.Athlete.SportsLookup)), QuickFilter, QuickFilterOption("multiple", true)]

        public String Sport { get; set; }
    }
}