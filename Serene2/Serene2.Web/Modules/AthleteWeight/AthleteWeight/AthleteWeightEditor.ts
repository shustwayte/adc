﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.AthleteWeight {
    
    @Serenity.Decorators.registerClass()
    export class AthleteWeightEditor extends Common.GridEditorBase<AthleteWeightRow> {
        protected getColumnsKey() { return 'AthleteWeight.AthleteWeight'; }
        protected getDialogType() { return AthleteWeightEditorDialog; }
                protected getLocalTextPrefix() { return AthleteWeightRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}