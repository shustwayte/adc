﻿

namespace Serene2.AthleteWeight.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Athlete Weight"), InstanceName("Athlete Weight"), TwoLevelCached]
    [ReadPermission("AthleteWeight")]
    [ModifyPermission("AthleteWeight")]
    public sealed class AthleteWeightRow : Row, IIdRow, INameRow
    {
        
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? id
        {
            get { return Fields.id[this]; }
            set { Fields.id[this] = value; }
        }
        
        [DisplayName("Avg Weight"), Column("avgWeight")]
        public Double? avgWeight
        {
            get { return Fields.avgWeight[this]; }
            set { Fields.avgWeight[this] = value; }
        }

        [DisplayName("Datefor"), Column("datefor"), Size(34), QuickSearch]
        public String datefor
        {
            get { return Fields.datefor[this]; }
            set { Fields.datefor[this] = value; }
        }
        
        IIdField IIdRow.IdField
        {
            get { return Fields.id; }
        }
        

        StringField INameRow.NameField
        {
            get { return Fields.datefor; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteWeightRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field id;
            public DoubleField avgWeight;
            public StringField datefor;

            public RowFields()
                : base("[dbo].[AthleteWeight]")
            {
                LocalTextPrefix = "AthleteWeight.AthleteWeight";
            }
        }
    }
}