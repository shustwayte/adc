﻿

namespace Serene2.AthleteWeight.Repositories
{
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System;
    using System.Data;
    using MyRow = Entities.AthleteWeightRow;

    using System.Collections.Generic;

    public class AthleteWeightRepository
    {
        private static MyRow.RowFields fld { get { return MyRow.Fields; } }
        /*
        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Create);
        }

        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Update);
        }

        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyDeleteHandler().Process(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRetrieveHandler().Process(connection, request);
        }
        */
        public ListResponse<MyRow> List(IDbConnection connection,
            ListRequest request)

        //AthleteWeightListRequest request)
        
        {
            var AthleteUsername = "";


            if (request.EqualityFilter != null)
            {
                if (request.EqualityFilter.ContainsKey("AthleteUsername") == true)
                {
                    AthleteUsername = request.EqualityFilter.Get("AthleteUsername").ToString();
                    request.EqualityFilter.Clear();
                }
            }
            

        var g = new MyListHandler().Process(connection, request);

            {


                if (AthleteUsername == "")
                {

                    var user = (UserDefinition)Serenity.Authorization.UserDefinition;

                    if (user.isAthlete == 1)
                    {
                        var data = connection.Query<MyRow>("[dbo].[p_Athletes_AVG_Weight]",
                                 param: new
                                 {
                                     username = user.Username,
                                     format = "data"
                                 },
                                 commandType: System.Data.CommandType.StoredProcedure);

                        var response = new ListResponse<MyRow>();
                        response.Entities = (List<MyRow>)data;

                        //return response;

                        g.Entities = (List<MyRow>)data;

                    }

                    if (user.isAthlete != 1)
                    {
                        var data = connection.Query<MyRow>("[dbo].[p_Athletes_AVG_Weight]",
                                 param: new
                                 {
                                     username = user.Username,
                                     format = "data"
                                 },
                                 commandType: System.Data.CommandType.StoredProcedure);

                        var response = new ListResponse<MyRow>();
                        response.Entities = (List<MyRow>)data;

                        //return response;

                        g.Entities = (List<MyRow>)data;
                    }
                }

                if(AthleteUsername != "")
                {
                    
                        var data = connection.Query<MyRow>("[dbo].[p_Athletes_AVG_Weight]",
                                    param: new
                                    {
                                        username = AthleteUsername,
                                        format = "data"
                                    },
                                    commandType: System.Data.CommandType.StoredProcedure);

                        var response = new ListResponse<MyRow>();
                        response.Entities = (List<MyRow>)data;

                        //return response;

                        g.Entities = (List<MyRow>)data;
                   
                }

                return g;
            }
        }
        /*
        private class MySaveHandler : SaveRequestHandler<MyRow> { }
        private class MyDeleteHandler : DeleteRequestHandler<MyRow> { }
        private class MyRetrieveHandler : RetrieveRequestHandler<MyRow> { }
        */
        private class MyListHandler : ListRequestHandler<MyRow> { }
        
    }
}