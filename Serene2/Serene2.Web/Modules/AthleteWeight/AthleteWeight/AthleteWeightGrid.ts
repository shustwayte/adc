﻿
namespace Serene2.AthleteWeight {
    
    @Serenity.Decorators.registerClass()
    export class AthleteWeightGrid extends Serenity.EntityGrid<AthleteWeightRow, any> {
        protected getColumnsKey() { return 'AthleteWeight.AthleteWeight'; }
        protected getDialogType() { return AthleteWeightDialog; }
        protected getIdProperty() { return AthleteWeightRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteWeightRow.localTextPrefix; }
        protected getService() { return AthleteWeightService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getButtons(): Serenity.ToolButton[] {

            var buttons = super.getButtons();

            buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'AthleteWeight/AthleteWeight/ListExcel',
                separator: true
            }));

            buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "column-picker-button"), 1);
            

            return buttons;
        }

        //protected createToolbar(): void {
        //}

        protected createQuickSearchInput(): void {
        }


        private _AthleteUsername: string;

        get AthleteUsername() {
            return this._AthleteUsername;
        }

        set AthleteUsername(value: string) {
            if (this._AthleteUsername !== value) {
                this._AthleteUsername = value;
                this.setEquality('AthleteUsername', value);
                this.refresh();
            }
        }
       

    }
}