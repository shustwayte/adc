﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "AthleteWeight/AthleteWeight", typeof(Serene2.AthleteWeight.Pages.AthleteWeightController))]

namespace Serene2.AthleteWeight.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("AthleteWeight/AthleteWeight"), Route("{action=index}")]
    public class AthleteWeightController : Controller
    {
        [PageAuthorize("AthleteWeight")]
        public ActionResult Index()
        {
            return View("~/Modules/AthleteWeight/AthleteWeight/AthleteWeightIndex.cshtml");
        }
    }
}