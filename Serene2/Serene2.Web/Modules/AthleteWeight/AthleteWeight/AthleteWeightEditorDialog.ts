﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.AthleteWeight {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteWeightEditorDialog extends Common.GridEditorDialog<AthleteWeightRow> {
        protected getFormKey() { return AthleteWeightForm.formKey; }
                protected getLocalTextPrefix() { return AthleteWeightRow.localTextPrefix; }
        protected getNameProperty() { return AthleteWeightRow.nameProperty; }
        protected form = new AthleteWeightForm(this.idPrefix);
    }
}