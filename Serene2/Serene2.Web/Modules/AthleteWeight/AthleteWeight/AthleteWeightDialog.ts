﻿
namespace Serene2.AthleteWeight {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteWeightDialog extends Serenity.EntityDialog<AthleteWeightRow, any> {
        protected getFormKey() { return AthleteWeightForm.formKey; }
        //protected getIdProperty() { return AthleteWeightRow.idProperty; }
        protected getIdProperty() { return "__id"; }
        protected getLocalTextPrefix() { return AthleteWeightRow.localTextPrefix; }
        protected getNameProperty() { return AthleteWeightRow.nameProperty; }
        protected getService() { return AthleteWeightService.baseUrl; }

        protected form = new AthleteWeightForm(this.idPrefix);
        

    }
}