﻿
namespace Serene2.AthleteWeight.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("AthleteWeight.AthleteWeight")]
    [BasedOnRow(typeof(Entities.AthleteWeightRow))]
    public class AthleteWeightForm
    {
        public Double avgWeight { get; set; }
        public String datefor { get; set; }
    }
}