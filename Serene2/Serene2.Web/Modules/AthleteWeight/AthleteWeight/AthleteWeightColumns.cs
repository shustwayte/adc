﻿
namespace Serene2.AthleteWeight.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("AthleteWeight.AthleteWeight")]
    [BasedOnRow(typeof(Entities.AthleteWeightRow))]
    public class AthleteWeightColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 id { get; set; }
        [DisplayName("Avg Weight")]
        public Double avgWeight { get; set; }
        [DisplayName("Date")]
        public String datefor { get; set; }
    }
}