﻿
namespace Serene2.SportsTrainerList.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("SportsTrainerList.SportsCoachList")]
    [BasedOnRow(typeof(Entities.SportsCoachListRow))]
    public class SportsCoachListColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        public Int32 SportId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}