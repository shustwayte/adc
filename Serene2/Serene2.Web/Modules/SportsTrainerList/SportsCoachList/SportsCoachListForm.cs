﻿
namespace Serene2.SportsTrainerList.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("SportsTrainerList.SportsCoachList")]
    [BasedOnRow(typeof(Entities.SportsCoachListRow))]
    public class SportsCoachListForm
    {
        public Int32 SportId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}