﻿
namespace Serene2.SportsTrainerList {
    
    @Serenity.Decorators.registerClass()
    export class SportsCoachListGrid extends Serenity.EntityGrid<SportsCoachListRow, any> {
        protected getColumnsKey() { return 'SportsTrainerList.SportsCoachList'; }
        protected getDialogType() { return SportsCoachListDialog; }
        protected getIdProperty() { return SportsCoachListRow.idProperty; }
        protected getLocalTextPrefix() { return SportsCoachListRow.localTextPrefix; }
        protected getService() { return SportsCoachListService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}