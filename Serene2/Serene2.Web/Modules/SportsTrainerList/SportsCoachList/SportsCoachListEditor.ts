﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.SportsTrainerList {
    
    @Serenity.Decorators.registerClass()
    export class SportsCoachListEditor extends Common.GridEditorBase<SportsCoachListRow> {
        protected getColumnsKey() { return 'SportsTrainerList.SportsCoachList'; }
        protected getDialogType() { return SportsCoachListEditorDialog; }
                protected getLocalTextPrefix() { return SportsCoachListRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}