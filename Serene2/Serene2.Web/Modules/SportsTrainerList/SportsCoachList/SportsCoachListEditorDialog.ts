﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.SportsTrainerList {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SportsCoachListEditorDialog extends Common.GridEditorDialog<SportsCoachListRow> {
        protected getFormKey() { return SportsCoachListForm.formKey; }
                protected getLocalTextPrefix() { return SportsCoachListRow.localTextPrefix; }
        protected form = new SportsCoachListForm(this.idPrefix);
    }
}