﻿

namespace Serene2.SportsTrainerList.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("SportsCoachList"), InstanceName("SportsCoachList"), TwoLevelCached]
    [ReadPermission("SportsTrainerList")]
    [ModifyPermission("Setup")]


    [LookupScript("WebApp.SportsTrainerList")]
    public sealed class SportsCoachListRow : Row, IIdRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Sport"), Column("sportID"), ForeignKey("[dbo].[Sports]", "SportID"), LeftJoin("jSport"), TextualField("Sport")]
        public Int32? SportId
        {
            get { return Fields.SportId[this]; }
            set { Fields.SportId[this] = value; }
        }

        [DisplayName("Trainer Id")]
        public Int32? TrainerId
        {
            get { return Fields.TrainerId[this]; }
            set { Fields.TrainerId[this] = value; }
        }

        [DisplayName("Sport"), Expression("jSport.[Sport]")]
        public String Sport
        {
            get { return Fields.Sport[this]; }
            set { Fields.Sport[this] = value; }
        }

        [DisplayName("Sport Sessions Per Week"), Expression("jSport.[SessionsPerWeek]")]
        public Int32? SportSessionsPerWeek
        {
            get { return Fields.SportSessionsPerWeek[this]; }
            set { Fields.SportSessionsPerWeek[this] = value; }
        }

        [DisplayName("Sport Sandccoach Id"), Expression("jSport.[sandccoachId]")]
        public Int32? SportSandccoachId
        {
            get { return Fields.SportSandccoachId[this]; }
            set { Fields.SportSandccoachId[this] = value; }
        }

        [DisplayName("Sport Scoach Id"), Expression("jSport.[scoachId]")]
        public Int32? SportScoachId
        {
            get { return Fields.SportScoachId[this]; }
            set { Fields.SportScoachId[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SportsCoachListRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field SportId;
            public Int32Field TrainerId;

            public StringField Sport;
            public Int32Field SportSessionsPerWeek;
            public Int32Field SportSandccoachId;
            public Int32Field SportScoachId;

            public RowFields()
                : base("[dbo].[SportsCoachList]")
            {
                LocalTextPrefix = "Setup.SportsCoachList";
            }
        }
    }
}