﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "SportsTrainerList/SportsCoachList", typeof(Serene2.SportsTrainerList.Pages.SportsCoachListController))]

namespace Serene2.SportsTrainerList.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("SportsTrainerList/SportsCoachList"), Route("{action=index}")]
    public class SportsCoachListController : Controller
    {
        [PageAuthorize("SportsTrainerList")]
        public ActionResult Index()
        {
            return View("~/Modules/SportsTrainerList/SportsCoachList/SportsCoachListIndex.cshtml");
        }
    }
}