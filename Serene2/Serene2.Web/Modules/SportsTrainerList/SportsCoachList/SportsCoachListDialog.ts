﻿
namespace Serene2.SportsTrainerList {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SportsCoachListDialog extends Serenity.EntityDialog<SportsCoachListRow, any> {
        protected getFormKey() { return SportsCoachListForm.formKey; }
        protected getIdProperty() { return SportsCoachListRow.idProperty; }
        protected getLocalTextPrefix() { return SportsCoachListRow.localTextPrefix; }
        protected getService() { return SportsCoachListService.baseUrl; }

        protected form = new SportsCoachListForm(this.idPrefix);

    }
}