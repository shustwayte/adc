﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "RPE/Rpe", typeof(Serene2.RPE.Pages.RpeController))]

namespace Serene2.RPE.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("RPE/Rpe"), Route("{action=index}")]
    public class RpeController : Controller
    {
        [PageAuthorize("RPE")]
        public ActionResult Index()
        {
            return View("~/Modules/RPE/Rpe/RpeIndex.cshtml");
        }
    }
}