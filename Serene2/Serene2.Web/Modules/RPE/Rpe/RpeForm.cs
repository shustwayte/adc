﻿
namespace Serene2.RPE.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("RPE.Rpe")]
    [BasedOnRow(typeof(Entities.RpeRow))]
    public class RpeForm
    {
        [Category("RPE")]
        [Visible(false)]
        public Int32 AthleteId { get; set; }
        [DisplayName("RPE Rating ")]
        //[DateYearEditor(MinYear = "1", MaxYear = "10")]
        public Int32 RpEvAlue { get; set; }
        [Visible(false)]
        public DateTime Daterecorded { get; set; }
        [Visible(false)]
        public String AthleteUsername { get; set; }
        
        
    }
}