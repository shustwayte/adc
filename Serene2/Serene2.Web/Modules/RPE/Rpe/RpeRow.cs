﻿

namespace Serene2.RPE.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("RPE"), InstanceName("RPE"), TwoLevelCached]
    [ReadPermission("RPE")]
    [ModifyPermission("RPE")]
    public sealed class RpeRow : Row, IIdRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athlete Id"), Column("athleteID")]
        public Int32? AthleteId
        {
            get { return Fields.AthleteId[this]; }
            set { Fields.AthleteId[this] = value; }
        }

        [DisplayName("Rp Ev Alue"), Column("RPEvALUE")]
        public Int32? RpEvAlue
        {
            get { return Fields.RpEvAlue[this]; }
            set { Fields.RpEvAlue[this] = value; }
        }

        [DisplayName("Daterecorded"), Column("daterecorded"), SortOrder(1,true)]
        public DateTime? Daterecorded
        {
            get { return Fields.Daterecorded[this]; }
            set { Fields.Daterecorded[this] = value; }
        }


        [DisplayName("AthleteUsername"), Column("AthleteUsername")]
        public String AthleteUsername
        {
            get { return Fields.AthleteUsername[this]; }
            set { Fields.AthleteUsername[this] = value; }
        }
        

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public RpeRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthleteId;
            public Int32Field RpEvAlue;
            public DateTimeField Daterecorded;

            public StringField AthleteUsername;

            public RowFields()
                : base("[dbo].[RPE]")
            {
                LocalTextPrefix = "RPE.Rpe";
            }
        }
    }
}