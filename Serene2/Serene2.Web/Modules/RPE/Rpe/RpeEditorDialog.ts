﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.RPE {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class RpeEditorDialog extends Common.GridEditorDialog<RpeRow> {
        protected getFormKey() { return RpeForm.formKey; }
                protected getLocalTextPrefix() { return RpeRow.localTextPrefix; }
        protected form = new RpeForm(this.idPrefix);
    }


}