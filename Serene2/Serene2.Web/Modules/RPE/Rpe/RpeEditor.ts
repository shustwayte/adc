﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.RPE {

    @Serenity.Decorators.registerClass()
    export class RpeEditor extends Common.GridEditorBase<RpeRow> {
        protected getColumnsKey() { return 'RPE.Rpe'; }
        protected getDialogType() { return RpeEditorDialog; }
        protected getLocalTextPrefix() { return RpeRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);

        }
    }
}