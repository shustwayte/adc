﻿
namespace Serene2.RPE {
    
    @Serenity.Decorators.registerClass()
    export class RpeGrid extends Serenity.EntityGrid<RpeRow, any> {
        protected getColumnsKey() { return 'RPE.Rpe'; }
        protected getDialogType() { return RpeDialog; }
        protected getIdProperty() { return RpeRow.idProperty; }
        protected getLocalTextPrefix() { return RpeRow.localTextPrefix; }
        protected getService() { return RpeService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        private _AthleteUsername: string;

        get AthleteUsername() {
            return this._AthleteUsername;
        }

        set AthleteUsername(value: string) {
            if (this._AthleteUsername !== value) {
                this._AthleteUsername = value;
                this.setEquality('AthleteUsername', value);
                this.refresh();
            }
        }
    }
}