﻿
namespace Serene2.RPE {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class RpeDialog extends Serenity.EntityDialog<RpeRow, any> {
        protected getFormKey() { return RpeForm.formKey; }
        protected getIdProperty() { return RpeRow.idProperty; }
        protected getLocalTextPrefix() { return RpeRow.localTextPrefix; }
        protected getService() { return RpeService.baseUrl; }

        protected form = new RpeForm(this.idPrefix);

        

        loadEntity(entity: RPE.RpeRow) {

            super.loadEntity(entity);

            var user = Serene2.Authorization.userDefinition;

            if (user.isAthlete == true) {

                this.form.Daterecorded.element.toggleClass('disabled', true);
                this.form.Daterecorded.element.toggleClass('readOnly', true);
                this.form.Daterecorded.element.toggleClass('isDisabled', true);

                Serenity.EditorUtils.setReadonly(this.element.find('.Daterecorded .editor'), true);

                //this.deleteButton.hide();
                this.deleteButton.toggleClass('disabled', true);

            }
            
        }

   
        
        onSaveSuccess(response) {

            super.onSaveSuccess(response);

            var currenturl = window.location.href;

            if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                var user = Serene2.Authorization.userDefinition;
                if (user.isAthlete == true) {
                    location.href = "/";
                    // location.reload();
                }
            }
        }


        protected afterLoadEntity() {
            super.afterLoadEntity();

            var elID = this.uniqueName + '_RpEvAlue';
            
            //var elID = 's2id_Serene2_RPE_RpeDialog7_RpEvAlue';
                        
            var input = this.element.find('#' + elID);
            input.attr({
                'data-provide': 'slider',
                'data-slider-ticks': '[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]', // TODO: create dynamically
                
                //'data-slider-tooltips': '["0 - Rest", "1 - Really easy", "2 - Easy", "3 - Moderate", "4 - Sort of hard", "5 - Hard", "6", "7 - Really hard", "8", "9 - Really, really hard", "10 - Just like my hardest game"]', // TODO: create dynamically
                //'data-slider-ticks-labels': '["Rest", "Really easy", "Easy", "Moderate", "Sort of hard", "Hard", "", "Really hard", "", "Really, really hard", "Just like my hardest game"]', // TODO: create dynamically
                'data-slider-min': 0, //this.options.min,
                'data-slider-max': 10, //this.options.max,
                'data-slider-step': 1, //this.options.step,
                'data-slider-value': this.entity.RpEvAlue
            });

            input.append('<script>$("#' + elID + '").bootstrapSlider({tooltip: "always"});</script>');
            input.append('<script>$("#' + elID + '").bootstrapSlider({ formatter: function (value) {  if(value == "0"){ return "0 - Rest" } if (value == "1") { return "1 - Really easy" } if (value == "2") { return "2 - Easy" } if (value == "3") { return "3 - Moderate" } if (value == "4") { return "4 - Sort of hard" } if (value == "5") { return "5 - Hard" } if (value == "6") { return "6" } if (value == "7") { return "7 - Really hard" } if (value == "8") { return "8" } if (value == "9") { return "9 - Really, really hard" } if (value == "10") { return "10 - Just like my hardest game" } } }); </script>');
            
        }
        
    }

}