﻿

/**
 * This is an Slider editor widget
 *  
 */

namespace Serene2.Default {

    @Serenity.Decorators.element("<div/>")
    @Serenity.Decorators.registerEditor([Serenity.IGetEditValue, Serenity.ISetEditValue])
    export class SliderEditor extends Serenity.Widget<SliderEditorOptions>
        implements Serenity.IGetEditValue, Serenity.ISetEditValue {

        constructor(container: JQuery, options: SliderEditorOptions) {
            super(container, options);

            this.updateElementContent();
        }

        private updateElementContent() {
            var ticks: string;
            var tickslabels: string;

            // Here you can dynamically create ticks and ticks labels
            //for (var t of this.options.ticks) {
            //    [...]
            //}

            var elID = 'sld' + this.uniqueName;

            this.element.append('<input id="' + elID + '" />');
            var input = this.element.find('#' + elID);
            input.attr({
                'data-provide': 'slider',
                'data-slider-ticks': '[1, 2, 3, 4]', // TODO: create dynamically
                //'data-slider-ticks-labels': '["One", "Two", "Three", "Four"]', // TODO: create dynamically
                'data-slider-min': 1, //this.options.min,
                'data-slider-max': 4, //this.options.max,
                'data-slider-step': 1, //this.options.step,
                'data-slider-value': 1
            });
            input.append('<script>$("#' + elID + '").slider({tooltip: "always"});</script>');
        }

        // Implement get and set
        public get value(): number {
            var sld = this.element.find('#sld' + this.uniqueName);
            return Q.toId(sld.slider('getValue'));
        }

        public set value(value: number) {
            var sld = this.element.find('#sld' + this.uniqueName);
            sld.slider('setValue', value);
        }

        public getEditValue(property, target) {
            target[property.name] = this.value;
        }

        public setEditValue(source, property) {
            this.value = source[property.name];
        }
    }

    export interface SliderEditorOptions {
        min: number;
        max: number;
        step: number;
        ticks: number[];
        tickslabels: string[];
    }
}
