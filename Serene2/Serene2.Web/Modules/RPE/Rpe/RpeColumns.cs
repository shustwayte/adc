﻿
namespace Serene2.RPE.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("RPE.Rpe")]
    [BasedOnRow(typeof(Entities.RpeRow))]
    public class RpeColumns
    {
        [EditLink, DisplayName("Date Logged"), Width(150)]
        //public Int32 Id { get; set; }
        //public Int32 AthleteId { get; set; }
        public DateTime Daterecorded { get; set; }
        [DisplayName("RPE Value"), Width(150)]
        public Int32 RpEvAlue { get; set; }
        
    }
}