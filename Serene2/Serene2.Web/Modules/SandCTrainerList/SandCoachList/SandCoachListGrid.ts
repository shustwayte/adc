﻿
namespace Serene2.SandCTrainerList {
    
    @Serenity.Decorators.registerClass()
    export class SandCoachListGrid extends Serenity.EntityGrid<SandCoachListRow, any> {
        protected getColumnsKey() { return 'SandCTrainerList.SandCoachList'; }
        protected getDialogType() { return SandCoachListDialog; }
        protected getIdProperty() { return SandCoachListRow.idProperty; }
        protected getLocalTextPrefix() { return SandCoachListRow.localTextPrefix; }
        protected getService() { return SandCoachListService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}