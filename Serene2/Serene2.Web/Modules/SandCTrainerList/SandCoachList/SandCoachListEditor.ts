﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.SandCTrainerList {
    
    @Serenity.Decorators.registerClass()
    export class SandCoachListEditor extends Common.GridEditorBase<SandCoachListRow> {
        protected getColumnsKey() { return 'SandCTrainerList.SandCoachList'; }
        protected getDialogType() { return SandCoachListEditorDialog; }
                protected getLocalTextPrefix() { return SandCoachListRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}