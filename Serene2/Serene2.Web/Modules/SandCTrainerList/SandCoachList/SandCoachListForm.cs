﻿
namespace Serene2.SandCTrainerList.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("SandCTrainerList.SandCoachList")]
    [BasedOnRow(typeof(Entities.SandCoachListRow))]
    public class SandCoachListForm
    {
        public Int32 SportId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}