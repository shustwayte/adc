﻿
namespace Serene2.SandCTrainerList.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("SandCTrainerList.SandCoachList")]
    [BasedOnRow(typeof(Entities.SandCoachListRow))]
    public class SandCoachListColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        public Int32 SportId { get; set; }
        public Int32 TrainerId { get; set; }
    }
}