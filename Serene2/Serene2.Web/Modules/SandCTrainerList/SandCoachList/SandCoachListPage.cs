﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "SandCTrainerList/SandCoachList", typeof(Serene2.SandCTrainerList.Pages.SandCoachListController))]

namespace Serene2.SandCTrainerList.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("SandCTrainerList/SandCoachList"), Route("{action=index}")]
    public class SandCoachListController : Controller
    {
        [PageAuthorize("SandCTrainerList")]
        public ActionResult Index()
        {
            return View("~/Modules/SandCTrainerList/SandCoachList/SandCoachListIndex.cshtml");
        }
    }
}