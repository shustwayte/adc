﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.SandCTrainerList {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SandCoachListEditorDialog extends Common.GridEditorDialog<SandCoachListRow> {
        protected getFormKey() { return SandCoachListForm.formKey; }
                protected getLocalTextPrefix() { return SandCoachListRow.localTextPrefix; }
        protected form = new SandCoachListForm(this.idPrefix);
    }
}