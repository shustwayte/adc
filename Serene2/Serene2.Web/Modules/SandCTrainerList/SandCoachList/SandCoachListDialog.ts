﻿
namespace Serene2.SandCTrainerList {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class SandCoachListDialog extends Serenity.EntityDialog<SandCoachListRow, any> {
        protected getFormKey() { return SandCoachListForm.formKey; }
        protected getIdProperty() { return SandCoachListRow.idProperty; }
        protected getLocalTextPrefix() { return SandCoachListRow.localTextPrefix; }
        protected getService() { return SandCoachListService.baseUrl; }

        protected form = new SandCoachListForm(this.idPrefix);

    }
}