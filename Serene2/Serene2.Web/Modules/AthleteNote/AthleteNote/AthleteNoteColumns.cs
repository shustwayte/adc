﻿
namespace Serene2.AthleteNote.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("AthleteNote.AthleteNote")]
    [BasedOnRow(typeof(Entities.AthleteNoteRow))]
    public class AthleteNoteColumns
    {
        //[EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        //public Int32 Id { get; set; }
        //public Int32 AthleteId { get; set; }
        public DateTime EntryDate { get; set; }
        [EditLink, Width(300)]
        public String Notes { get; set; }
        [DisplayName("Entered By")]
        public String Username { get; set; }
    }
}