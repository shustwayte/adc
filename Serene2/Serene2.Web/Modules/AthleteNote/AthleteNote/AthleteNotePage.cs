﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "AthleteNote/AthleteNote", typeof(Serene2.AthleteNote.Pages.AthleteNoteController))]

namespace Serene2.AthleteNote.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("AthleteNote/AthleteNote"), Route("{action=index}")]
    public class AthleteNoteController : Controller
    {
        [PageAuthorize("AthleteNote")]
        public ActionResult Index()
        {
            return View("~/Modules/AthleteNote/AthleteNote/AthleteNoteIndex.cshtml");
        }
    }
}