﻿

namespace Serene2.AthleteNote.Entities
{
    using Modules.Common.BaseClass;
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Note"), InstanceName("Note"), TwoLevelCached]
    [ReadPermission("AthleteNote")]
    [ModifyPermission("AthleteNote")]
    public sealed class AthleteNoteRow : Row, IIdRow, INameRow, IAuditLog
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athlete Id"), Column("athleteID"), Visible(false)]
        public Int32? AthleteId
        {
            get { return Fields.AthleteId[this]; }
            set { Fields.AthleteId[this] = value; }
        }

        [DisplayName("Entry Date"), Column("entryDate"), Updatable(false), ReadOnly(true)]
        public DateTime? EntryDate
        {
            get { return Fields.EntryDate[this]; }
            set { Fields.EntryDate[this] = value; }
        }

        [DisplayName("Notes"), Column("notes"), Size(4000), QuickSearch]
        public String Notes
        {
            get { return Fields.Notes[this]; }
            set { Fields.Notes[this] = value; }
        }

        [DisplayName("Username"), Column("username"), Size(100), Updatable(false), ReadOnly(true)]
        public String Username
        {
            get { return Fields.Username[this]; }
            set { Fields.Username[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Notes; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteNoteRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field AthleteId;
            public DateTimeField EntryDate;
            public StringField Notes;
            public StringField Username;

            public RowFields()
                : base("[dbo].[AthleteNote]")
            {
                LocalTextPrefix = "AthleteNote.AthleteNote";
            }
        }
    }
}