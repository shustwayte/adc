﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.AthleteNote {
    
    @Serenity.Decorators.registerClass()
    export class AthleteNoteEditor extends Common.GridEditorBase<AthleteNoteRow> {
        protected getColumnsKey() { return 'AthleteNote.AthleteNote'; }
        protected getDialogType() { return AthleteNoteEditorDialog; }
                protected getLocalTextPrefix() { return AthleteNoteRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}