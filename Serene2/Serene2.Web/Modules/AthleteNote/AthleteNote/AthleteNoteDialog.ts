﻿
namespace Serene2.AthleteNote {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteNoteDialog extends Serenity.EntityDialog<AthleteNoteRow, any> {
        protected getFormKey() { return AthleteNoteForm.formKey; }
        protected getIdProperty() { return AthleteNoteRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteNoteRow.localTextPrefix; }
        protected getNameProperty() { return AthleteNoteRow.nameProperty; }
        protected getService() { return AthleteNoteService.baseUrl; }

        protected form = new AthleteNoteForm(this.idPrefix);

        private audit: Audit.AuditLogGrid;


        loadEntity(entity: AthleteNote.AthleteNoteRow) {

            super.loadEntity(entity);

                this.form.EntryDate.element.toggleClass('disabled', true);
                this.form.EntryDate.element.toggleClass('readOnly', true);
                this.form.EntryDate.element.toggleClass('isDisabled', true);

                this.form.Username.element.toggleClass('disabled', true);
                this.form.Username.element.toggleClass('readOnly', true);
                this.form.Username.element.toggleClass('isDisabled', true);

            }

           
           


        }


    }
