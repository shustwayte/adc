﻿
namespace Serene2.AthleteNote.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("AthleteNote.AthleteNote")]
    [BasedOnRow(typeof(Entities.AthleteNoteRow))]
    public class AthleteNoteForm
    {
        [Visible(false)]
        public Int32 AthleteId { get; set; }
        public DateTime EntryDate { get; set; }
        [DisplayName("Note"), TextAreaEditor(Rows = 5)]

        public String Notes { get; set; }
        public String Username { get; set; }
    }
}