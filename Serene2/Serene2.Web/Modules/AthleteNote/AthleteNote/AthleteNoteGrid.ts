﻿
namespace Serene2.AthleteNote {
    
    @Serenity.Decorators.registerClass()
    export class AthleteNoteGrid extends Serenity.EntityGrid<AthleteNoteRow, any> {
        protected getColumnsKey() { return 'AthleteNote.AthleteNote'; }
        protected getDialogType() { return AthleteNoteDialog; }
        protected getIdProperty() { return AthleteNoteRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteNoteRow.localTextPrefix; }
        protected getService() { return AthleteNoteService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }


        private _AthleteId: number;

        get AthleteId() {
            return this._AthleteId;
        }


       
        set AthleteId(value: number) {
            if (this._AthleteId !== value) {
                this._AthleteId = value;
                

                var user = Serene2.Authorization.userDefinition;

                if (user.Roles.indexOf("Sports Coach") > 0 || user.Roles.indexOf("S&C Coach") > 0) { //|| user.Roles.indexOf("Admin") >= 0) {
                    this.setEquality('AthleteId', value);
                    this.refresh();
                }
                else {
                    this.setEquality('AthleteId', 0);
                    this.refresh();
                }
            }
        }

        protected getButtons(): Serenity.ToolButton[] {

            var buttons = super.getButtons();

            var user = Serene2.Authorization.userDefinition;

            if (user.Roles.indexOf("Sports Coach") > 0 || user.Roles.indexOf("S&C Coach") > 0) { //|| user.Roles.indexOf("Admin") >= 0) {
                //this.refresh();
            }
            else {
                buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);
                buttons.splice(Q.indexOf(buttons, x => x.cssClass == "column-picker-button"), 1);
            }

            return buttons;
        }

        protected addButtonClick() {

            var user = Serene2.Authorization.userDefinition;

            this.editItem(<AthleteNote.AthleteNoteRow>{
                EntryDate: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                AthleteId: this.AthleteId,
                Username: user.Username
            });




        }

    }
}