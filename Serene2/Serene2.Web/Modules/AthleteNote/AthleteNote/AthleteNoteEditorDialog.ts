﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.AthleteNote {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteNoteEditorDialog extends Common.GridEditorDialog<AthleteNoteRow> {
        protected getFormKey() { return AthleteNoteForm.formKey; }
                protected getLocalTextPrefix() { return AthleteNoteRow.localTextPrefix; }
        protected getNameProperty() { return AthleteNoteRow.nameProperty; }
        protected form = new AthleteNoteForm(this.idPrefix);
    }
}