﻿
namespace Serene2.AthleteLoggedToday.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("AthleteLoggedToday.AthleteLoggedToday")]
    [BasedOnRow(typeof(Entities.AthleteLoggedTodayRow))]
    public class AthleteLoggedTodayForm
    {
        public Int32 Athleteid { get; set; }
        public String Username { get; set; }
        public Int32 AthleteLoggedAssessmentToday { get; set; }
    }
}