﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.AthleteLoggedToday {
    
    @Serenity.Decorators.registerClass()
    export class AthleteLoggedTodayEditor extends Common.GridEditorBase<AthleteLoggedTodayRow> {
        protected getColumnsKey() { return 'AthleteLoggedToday.AthleteLoggedToday'; }
        protected getDialogType() { return AthleteLoggedTodayEditorDialog; }
                protected getLocalTextPrefix() { return AthleteLoggedTodayRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}