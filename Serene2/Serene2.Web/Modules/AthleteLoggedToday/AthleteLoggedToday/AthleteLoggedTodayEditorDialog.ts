﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.AthleteLoggedToday {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteLoggedTodayEditorDialog extends Common.GridEditorDialog<AthleteLoggedTodayRow> {
        protected getFormKey() { return AthleteLoggedTodayForm.formKey; }
                protected getLocalTextPrefix() { return AthleteLoggedTodayRow.localTextPrefix; }
        protected getNameProperty() { return AthleteLoggedTodayRow.nameProperty; }
        protected form = new AthleteLoggedTodayForm(this.idPrefix);
    }
}