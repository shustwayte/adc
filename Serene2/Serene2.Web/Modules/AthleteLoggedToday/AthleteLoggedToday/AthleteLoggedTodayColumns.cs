﻿
namespace Serene2.AthleteLoggedToday.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("AthleteLoggedToday.AthleteLoggedToday")]
    [BasedOnRow(typeof(Entities.AthleteLoggedTodayRow))]
    public class AthleteLoggedTodayColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        public Int32 Athleteid { get; set; }
        [EditLink]
        public String Username { get; set; }
        public Int32 AthleteLoggedAssessmentToday { get; set; }
    }
}