﻿
namespace Serene2.AthleteLoggedToday {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AthleteLoggedTodayDialog extends Serenity.EntityDialog<AthleteLoggedTodayRow, any> {
        protected getFormKey() { return AthleteLoggedTodayForm.formKey; }
        protected getIdProperty() { return AthleteLoggedTodayRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteLoggedTodayRow.localTextPrefix; }
        protected getNameProperty() { return AthleteLoggedTodayRow.nameProperty; }
        protected getService() { return AthleteLoggedTodayService.baseUrl; }

        protected form = new AthleteLoggedTodayForm(this.idPrefix);

    }
}