﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "AthleteLoggedToday/AthleteLoggedToday", typeof(Serene2.AthleteLoggedToday.Pages.AthleteLoggedTodayController))]

namespace Serene2.AthleteLoggedToday.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("AthleteLoggedToday/AthleteLoggedToday"), Route("{action=index}")]
    public class AthleteLoggedTodayController : Controller
    {
        [PageAuthorize("AthleteLoggedToday")]
        public ActionResult Index()
        {
            return View("~/Modules/AthleteLoggedToday/AthleteLoggedToday/AthleteLoggedTodayIndex.cshtml");
        }
    }
}