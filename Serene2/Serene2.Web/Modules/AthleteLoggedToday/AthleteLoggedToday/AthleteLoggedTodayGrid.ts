﻿
namespace Serene2.AthleteLoggedToday {
    
    @Serenity.Decorators.registerClass()
    export class AthleteLoggedTodayGrid extends Serenity.EntityGrid<AthleteLoggedTodayRow, any> {
        protected getColumnsKey() { return 'AthleteLoggedToday.AthleteLoggedToday'; }
        protected getDialogType() { return AthleteLoggedTodayDialog; }
        protected getIdProperty() { return AthleteLoggedTodayRow.idProperty; }
        protected getLocalTextPrefix() { return AthleteLoggedTodayRow.localTextPrefix; }
        protected getService() { return AthleteLoggedTodayService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}