﻿

namespace Serene2.AthleteLoggedToday.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("AthleteLoggedToday"), InstanceName("AthleteLoggedToday"), TwoLevelCached]
    [ReadPermission("AthleteLoggedToday")]
    [ModifyPermission("AthleteLoggedToday")]
    public sealed class AthleteLoggedTodayRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Athleteid"), Column("athleteid"), ForeignKey("[dbo].[Athlete]", "Id"), LeftJoin("jAthleteid"), TextualField("AthleteidUsername")]
        public Int32? Athleteid
        {
            get { return Fields.Athleteid[this]; }
            set { Fields.Athleteid[this] = value; }
        }

        [DisplayName("Username"), Column("username"), Size(20), QuickSearch]
        public String Username
        {
            get { return Fields.Username[this]; }
            set { Fields.Username[this] = value; }
        }

        [DisplayName("Athlete Logged Assessment Today")]
        public Int32? AthleteLoggedAssessmentToday
        {
            get { return Fields.AthleteLoggedAssessmentToday[this]; }
            set { Fields.AthleteLoggedAssessmentToday[this] = value; }
        }


        [DisplayName("Athlete Logged RPE Today")]
        public Int32? AthleteLoggedRPEToday
        {
            get { return Fields.AthleteLoggedRPEToday[this]; }
            set { Fields.AthleteLoggedRPEToday[this] = value; }
        }

        [DisplayName("Athleteid Live"), Expression("jAthleteid.[live]")]
        public Int32? AthleteidLive
        {
            get { return Fields.AthleteidLive[this]; }
            set { Fields.AthleteidLive[this] = value; }
        }

        [DisplayName("Athleteid Username"), Expression("jAthleteid.[Username]")]
        public String AthleteidUsername
        {
            get { return Fields.AthleteidUsername[this]; }
            set { Fields.AthleteidUsername[this] = value; }
        }

        [DisplayName("Athleteid Stu Id"), Expression("jAthleteid.[stuID]")]
        public String AthleteidStuId
        {
            get { return Fields.AthleteidStuId[this]; }
            set { Fields.AthleteidStuId[this] = value; }
        }

        [DisplayName("Athleteid First Name"), Expression("jAthleteid.[FirstName]")]
        public String AthleteidFirstName
        {
            get { return Fields.AthleteidFirstName[this]; }
            set { Fields.AthleteidFirstName[this] = value; }
        }

        [DisplayName("Athleteid Last Name"), Expression("jAthleteid.[LastName]")]
        public String AthleteidLastName
        {
            get { return Fields.AthleteidLastName[this]; }
            set { Fields.AthleteidLastName[this] = value; }
        }

        [DisplayName("Athleteid Sport Id"), Expression("jAthleteid.[SportID]")]
        public Int32? AthleteidSportId
        {
            get { return Fields.AthleteidSportId[this]; }
            set { Fields.AthleteidSportId[this] = value; }
        }

        [DisplayName("Athleteid Trainer Id"), Expression("jAthleteid.[TrainerID]")]
        public Int32? AthleteidTrainerId
        {
            get { return Fields.AthleteidTrainerId[this]; }
            set { Fields.AthleteidTrainerId[this] = value; }
        }

        [DisplayName("Athleteid Gender Id"), Expression("jAthleteid.[GenderID]")]
        public Int32? AthleteidGenderId
        {
            get { return Fields.AthleteidGenderId[this]; }
            set { Fields.AthleteidGenderId[this] = value; }
        }

        [DisplayName("Athleteid Year Id"), Expression("jAthleteid.[YearID]")]
        public Int32? AthleteidYearId
        {
            get { return Fields.AthleteidYearId[this]; }
            set { Fields.AthleteidYearId[this] = value; }
        }


        [DisplayName("Assessment Flagged")]
        public Int32? recentAssessmentPass
        {
            get { return Fields.recentAssessmentPass[this]; }
            set { Fields.recentAssessmentPass[this] = value; }
        }

        [DisplayName("Attendence")]
        public Int32? attendence
        {
            get { return Fields.attendence[this]; }
            set { Fields.attendence[this] = value; }
        }

        
        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Username; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AthleteLoggedTodayRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field Athleteid;
            public StringField Username;
            public Int32Field AthleteLoggedAssessmentToday;

            public Int32Field AthleteidLive;
            public StringField AthleteidUsername;
            public StringField AthleteidStuId;
            public StringField AthleteidFirstName;
            public StringField AthleteidLastName;
            public Int32Field AthleteidSportId;
            public Int32Field AthleteidTrainerId;
            public Int32Field AthleteidGenderId;
            public Int32Field AthleteidYearId;

            public Int32Field recentAssessmentPass;
            public Int32Field attendence;


            public Int32Field AthleteLoggedRPEToday;

            public RowFields()
                : base("[dbo].[AthleteLoggedToday]")
            {
                LocalTextPrefix = "AthleteLoggedToday.AthleteLoggedToday";
            }
        }
    }
}