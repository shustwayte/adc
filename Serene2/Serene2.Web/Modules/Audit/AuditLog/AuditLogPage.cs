﻿

[assembly:Serenity.Navigation.NavigationLink(int.MaxValue, "Audit/AuditLog", typeof(Serene2.Audit.Pages.AuditLogController))]

namespace Serene2.Audit.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Audit/AuditLog"), Route("{action=index}")]
    public class AuditLogController : Controller
    {
        [PageAuthorize("Audit")]
        public ActionResult Index()
        {
            return View("~/Modules/Audit/AuditLog/AuditLogIndex.cshtml");
        }
    }
}