﻿
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace Serene2.Audit {
    
    @Serenity.Decorators.registerClass()
    export class AuditLogEditor extends Common.GridEditorBase<AuditLogRow> {
        protected getColumnsKey() { return 'Audit.AuditLog'; }
        protected getDialogType() { return AuditLogEditorDialog; }
                protected getLocalTextPrefix() { return AuditLogRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}