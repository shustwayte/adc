﻿
namespace Serene2.Audit.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Audit.AuditLog")]
    [BasedOnRow(typeof(Entities.AuditLogRow))]
    public class AuditLogForm
    {
        [Category("Details")]
        //public Int32 UserId { get; set; }
        public String Time { get; set; }

        [DisplayName("User")]
        public String DisplayName { get; set; }
        //public String UserName { get; set; }
        public String Action { get; set; }
        //public DateTime ChangedOn { get; set; }


        //public String TableName { get; set; }
        //public Int32 RowId { get; set; }
        //public String Module { get; set; }
        //public String Page { get; set; }
        //public String Changes { get; set; }

        [DisplayName("Change"), TextAreaEditor(Rows = 5)]
        public String ChangesDisplayForm { get; set; }
        /*
        public Int32 UserId { get; set; }
        public String UserName { get; set; }
        public String Action { get; set; }
        public DateTime ChangedOn { get; set; }
        public String TableName { get; set; }
        public Int32 RowId { get; set; }
        public String Module { get; set; }
        public String Page { get; set; }
        public String Changes { get; set; }
        public String ChangesDisplayForm { get; set; }
        */
    }
}