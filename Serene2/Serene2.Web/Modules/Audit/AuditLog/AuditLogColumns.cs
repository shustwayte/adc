﻿
namespace Serene2.Audit.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Audit.AuditLog")]
    [BasedOnRow(typeof(Entities.AuditLogRow))]
    public class AuditLogColumns
    {
        [EditLink]
        //public DateTime ChangedOn { get; set; }
        [Width(120)]
        public String Time { get; set; }
        [DisplayName("User"), Width(120)]
        public String DisplayName { get; set; }
        //[Width(120)]
        //public String UserName { get; set; }
        [Width(120)]
        public String Action { get; set; }

        //public Int32 RowId { get; set; }

        //public String Page { get; set; }
        //public String Changes { get; set; }
        [DisplayName("Change"), Width(200)]
        public String ChangesDisplayForm { get; set; }

        /*
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int64 Id { get; set; }
        public Int32 UserId { get; set; }
        [EditLink]
        public String UserName { get; set; }
        public String Action { get; set; }
        public DateTime ChangedOn { get; set; }
        public String TableName { get; set; }
        public Int32 RowId { get; set; }
        public String Module { get; set; }
        public String Page { get; set; }
        public String Changes { get; set; }
        public String ChangesDisplayForm { get; set; }
        */
    }
}