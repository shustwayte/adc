﻿

namespace Serene2.Audit.Entities
{
    using Newtonsoft.Json;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), DisplayName("Audit Log"), InstanceName("Audit Log"), TwoLevelCached]
    [ReadPermission("Audit:AuditLog:View")]
    [ModifyPermission("Audit")]
    public sealed class AuditLogRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int64? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("User Id"), NotNull, ForeignKey("[dbo].[Users]", "UserId"), LeftJoin("jUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }


        [DisplayName("Display Name"), Expression("jUsers.[DisplayName]"), LookupInclude]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("User Name"), Size(50), NotNull, QuickSearch]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Action"), Size(50), NotNull]
        public String Action
        {
            get { return Fields.Action[this]; }
            set { Fields.Action[this] = value; }
        }

        [DisplayName("Changed On"), NotNull, DisplayFormat("dd/MM/yyyy")]
        public DateTime? ChangedOn
        {
            get { return Fields.ChangedOn[this]; }
            set { Fields.ChangedOn[this] = value; }
        }

        [DisplayName("Time"), Expression(" convert(varchar,[ChangedOn],103) + ' ' + convert(varchar(5),[ChangedOn],108) "), SortOrder(6, descending: true)]
        public String Time
        {
            get { return Fields.Time[this]; }
            set { Fields.Time[this] = value; }
        }


        [DisplayName("Table Name"), Size(50), NotNull]
        public String TableName
        {
            get { return Fields.TableName[this]; }
            set { Fields.TableName[this] = value; }
        }

        [DisplayName("Row Id"), NotNull]
        public Int32? RowId
        {
            get { return Fields.RowId[this]; }
            set { Fields.RowId[this] = value; }
        }

        [DisplayName("Module"), Size(500)]
        public String Module
        {
            get { return Fields.Module[this]; }
            set { Fields.Module[this] = value; }
        }

        [DisplayName("Page"), Size(500)]
        public String Page
        {
            get { return Fields.Page[this]; }
            set { Fields.Page[this] = value; }
        }

        [DisplayName("Changes")]
        public String Changes
        {
            get { return Fields.Changes[this]; }
            set { Fields.Changes[this] = value; }
        }

        [DisplayName("Changes Display Form")]
        public String ChangesDisplayForm
        {
            get { return Fields.ChangesDisplayForm[this]; }
            set { Fields.ChangesDisplayForm[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.UserName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AuditLogRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int64Field Id;
            public Int32Field UserId;
            public StringField UserName;
            public StringField Action;
            public DateTimeField ChangedOn;
            public StringField TableName;
            public Int32Field RowId;
            public StringField Module;
            public StringField Page;
            public StringField Changes;
            public StringField ChangesDisplayForm;

            public StringField DisplayName;

            public StringField Time;
            public RowFields()
                : base("[dbo].[AuditLog]")
            {
                LocalTextPrefix = "Audit.AuditLog";
            }
        }
    }
}