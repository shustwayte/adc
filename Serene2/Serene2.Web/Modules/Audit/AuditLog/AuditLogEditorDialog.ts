﻿
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace Serene2.Audit {
    
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AuditLogEditorDialog extends Common.GridEditorDialog<AuditLogRow> {
        protected getFormKey() { return AuditLogForm.formKey; }
                protected getLocalTextPrefix() { return AuditLogRow.localTextPrefix; }
        protected getNameProperty() { return AuditLogRow.nameProperty; }
        protected form = new AuditLogForm(this.idPrefix);
    }
}