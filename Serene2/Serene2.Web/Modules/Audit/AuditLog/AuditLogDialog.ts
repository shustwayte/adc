﻿
namespace Serene2.Audit {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.responsive()
    export class AuditLogDialog extends Serenity.EntityDialog<AuditLogRow, any> {
        protected getFormKey() { return AuditLogForm.formKey; }
        protected getIdProperty() { return AuditLogRow.idProperty; }
        protected getLocalTextPrefix() { return AuditLogRow.localTextPrefix; }
        protected getNameProperty() { return AuditLogRow.nameProperty; }
        protected getService() { return AuditLogService.baseUrl; }

        protected form = new AuditLogForm(this.idPrefix);


        getToolbarButtons() {

            var buttons = super.getToolbarButtons();

            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);

            return buttons;
        }




        loadEntity(entity: AuditLogRow) {
            super.loadEntity(entity);
            Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
        }

        protected updateInterface() {
            super.updateInterface();
            this.toolbar.findButton('apply-changes-button').hide();
            this.toolbar.findButton('save-and-close-button').hide();
        }

    }
}