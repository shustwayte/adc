﻿
namespace Serene2.Audit {
    
    @Serenity.Decorators.registerClass()
    export class AuditLogGrid extends Serenity.EntityGrid<AuditLogRow, any> {
        protected getColumnsKey() { return 'Audit.AuditLog'; }
        protected getDialogType() { return AuditLogDialog; }
        protected getIdProperty() { return AuditLogRow.idProperty; }
        protected getLocalTextPrefix() { return AuditLogRow.localTextPrefix; }
        protected getService() { return AuditLogService.baseUrl; }

        private assessmentGridForm: Serenity.PropertyGrid;

        constructor(container: JQuery) {
            super(container);
        }

        protected getButtons() {
            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "column-picker-button"), 1);

            return buttons;
        }

        private _rowid: number;

        get rowid() {
            return this._rowid;
        }

        set rowid(value: number) {

            if (value) {
                if (this._rowid !== value) {
                    this._rowid = value;
                    this.setEquality('rowid', value);
                    this.refresh();
                }
            }
            else {
                
                this.setEquality('rowid', 0);
                this.refresh();

            }
        }

        protected createQuickSearchInput(): void {
        }

        private _TableName: string;

        get TableName() {
            return this._TableName;
        }

        set TableName(value: string) {
            if (this._TableName !== value) {
                this._TableName = value;
                this.setEquality('TableName', value);
                this.refresh();
            }
        }

    }
}