﻿declare namespace Serene2.Administration {
    class LanguageDialog extends Serenity.EntityDialog<LanguageRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: LanguageForm;
    }
}
declare namespace Serene2.Administration {
    class LanguageGrid extends Serenity.EntityGrid<LanguageRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof LanguageDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): string[];
    }
}
declare namespace Serene2.Administration {
    class RoleDialog extends Serenity.EntityDialog<RoleRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: RoleForm;
        protected getToolbarButtons(): Serenity.ToolButton[];
        protected updateInterface(): void;
    }
}
declare namespace Serene2.Administration {
    class RoleGrid extends Serenity.EntityGrid<RoleRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof RoleDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): string[];
    }
}
declare namespace Serene2.Administration {
    class RolePermissionDialog extends Serenity.TemplatedDialog<RolePermissionDialogOptions> {
        private permissions;
        constructor(opt: RolePermissionDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface RolePermissionDialogOptions {
        roleID?: number;
        title?: string;
    }
}
declare namespace Serene2.Administration {
    class TranslationGrid extends Serenity.EntityGrid<TranslationItem, any> {
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        private hasChanges;
        private searchText;
        private sourceLanguage;
        private targetLanguage;
        private targetLanguageKey;
        constructor(container: JQuery);
        protected onClick(e: JQueryEventObject, row: number, cell: number): any;
        protected getColumns(): Slick.Column[];
        protected createToolbarExtensions(): void;
        protected saveChanges(language: string): RSVP.Promise<any>;
        protected onViewSubmit(): boolean;
        protected getButtons(): Serenity.ToolButton[];
        protected createQuickSearchInput(): void;
        protected onViewFilter(item: TranslationItem): boolean;
        protected usePager(): boolean;
    }
}
declare namespace Serene2.Authorization {
    let userDefinition: ScriptUserDefinition;
    function hasPermission(permissionKey: string): boolean;
}
declare namespace Serene2.Administration {
    class UserDialog extends Serenity.EntityDialog<UserRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getIsActiveProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: UserForm;
        constructor();
        protected getToolbarButtons(): Serenity.ToolButton[];
        protected updateInterface(): void;
        protected afterLoadEntity(): void;
    }
}
declare namespace Serene2.Administration {
    class UserGrid extends Serenity.EntityGrid<UserRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof UserDialog;
        protected getIdProperty(): string;
        protected getIsActiveProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): string[];
    }
}
declare namespace Serene2.Administration {
    class PermissionCheckEditor extends Serenity.DataGrid<PermissionCheckItem, PermissionCheckEditorOptions> {
        protected getIdProperty(): string;
        private searchText;
        private byParentKey;
        private rolePermissions;
        constructor(container: JQuery, opt: PermissionCheckEditorOptions);
        private getItemGrantRevokeClass(item, grant);
        private getItemEffectiveClass(item);
        protected getColumns(): Slick.Column[];
        setItems(items: PermissionCheckItem[]): void;
        protected onViewSubmit(): boolean;
        protected onViewFilter(item: PermissionCheckItem): boolean;
        private matchContains(item);
        private getDescendants(item, excludeGroups);
        protected onClick(e: any, row: any, cell: any): void;
        private getParentKey(key);
        protected getButtons(): Serenity.ToolButton[];
        protected createToolbarExtensions(): void;
        private getSortedGroupAndPermissionKeys(titleByKey);
        get_value(): UserPermissionRow[];
        set_value(value: UserPermissionRow[]): void;
        get_rolePermissions(): string[];
        set_rolePermissions(value: string[]): void;
    }
    interface PermissionCheckEditorOptions {
        showRevoke?: boolean;
    }
    interface PermissionCheckItem {
        ParentKey?: string;
        Key?: string;
        Title?: string;
        IsGroup?: boolean;
        GrantRevoke?: boolean;
    }
}
declare namespace Serene2.Administration {
    class UserPermissionDialog extends Serenity.TemplatedDialog<UserPermissionDialogOptions> {
        private permissions;
        constructor(opt: UserPermissionDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface UserPermissionDialogOptions {
        userID?: number;
        username?: string;
    }
}
declare namespace Serene2.Administration {
    class RoleCheckEditor extends Serenity.CheckTreeEditor<Serenity.CheckTreeItem<any>, any> {
        private searchText;
        constructor(div: JQuery);
        protected createToolbarExtensions(): void;
        protected getButtons(): any[];
        protected getTreeItems(): Serenity.CheckTreeItem<any>[];
        protected onViewFilter(item: any): boolean;
    }
}
declare namespace Serene2.Administration {
    class UserRoleDialog extends Serenity.TemplatedDialog<UserRoleDialogOptions> {
        private permissions;
        constructor(opt: UserRoleDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface UserRoleDialogOptions {
        userID: number;
        username: string;
    }
}
declare namespace Serene2.Assessment {
    class WellBeingDialog extends Serenity.EntityDialog<WellBeingRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        protected form: WellBeingForm;
        private assessmentGridForm;
        private audit;
        constructor();
        loadEntity(entity: Assessment.WellBeingRow): void;
        onSaveSuccess(response: any): void;
        protected updateInterface(): void;
    }
}
declare namespace Serene2.Common {
    class GridEditorBase<TEntity> extends Serenity.EntityGrid<TEntity, any> implements Serenity.IGetEditValue, Serenity.ISetEditValue {
        protected getIdProperty(): string;
        private nextId;
        constructor(container: JQuery);
        protected id(entity: TEntity): any;
        protected save(opt: Serenity.ServiceOptions<any>, callback: (r: Serenity.ServiceResponse) => void): void;
        protected deleteEntity(id: number): boolean;
        protected validateEntity(row: TEntity, id: number): boolean;
        protected setEntities(items: TEntity[]): void;
        protected getNewEntity(): TEntity;
        protected getButtons(): Serenity.ToolButton[];
        protected editItem(entityOrId: any): void;
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
        value: TEntity[];
        protected getGridCanLoad(): boolean;
        protected usePager(): boolean;
        protected getInitialTitle(): any;
        protected createQuickSearchInput(): void;
    }
}
declare namespace Serene2.Assessment {
    class WellBeingEditor extends Common.GridEditorBase<WellBeingRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof WellBeingEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Common {
    class GridEditorDialog<TEntity> extends Serenity.EntityDialog<TEntity, any> {
        protected getIdProperty(): string;
        onSave: (options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void) => void;
        onDelete: (options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void) => void;
        destroy(): void;
        protected updateInterface(): void;
        protected saveHandler(options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void): void;
        protected deleteHandler(options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void): void;
    }
}
declare namespace Serene2.Assessment {
    class WellBeingEditorDialog extends Common.GridEditorDialog<WellBeingRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected form: WellBeingForm;
    }
}
declare namespace Serene2 {
    class TestFormatter implements Slick.Formatter {
        format(ctx: Slick.FormatterContext): any;
    }
    class AnswerFormatter implements Slick.Formatter {
        format(ctx: Slick.FormatterContext): any;
    }
}
declare namespace Serene2.Assessment {
    class WellBeingGrid extends Serenity.EntityGrid<WellBeingRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof WellBeingDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected createQuickSearchInput(): void;
        protected getButtons(): Serenity.ToolButton[];
        private _AthleteId;
        AthleteId: number;
        protected addButtonClick(): void;
    }
}
declare namespace Serene2.Assessment {
    class WellBeingGridAthlete extends Serenity.EntityGrid<WellBeingRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof WellBeingDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getButtons(): Serenity.ToolButton[];
        private _AthleteId;
        AthleteId: number;
        protected addButtonClick(): void;
        protected getQuickFilters(): Serenity.QuickFilter<Serenity.Widget<any>, any>[];
        protected createQuickFilters(): void;
    }
}
declare namespace Serene2.Athlete {
    class AthleteDialog extends Serenity.EntityDialog<AthleteRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteForm;
        private athleteGrid;
        private assessmentGrid;
        private physioGrid;
        private athleteWeight;
        private audit;
        constructor();
        loadEntity(entity: AthleteRow): void;
        protected getToolbarButtons(): Serenity.ToolButton[];
    }
}
declare namespace Serene2.Athlete {
    class AthleteEditor extends Common.GridEditorBase<AthleteRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Athlete {
    class AthleteEditorDialog extends Common.GridEditorDialog<AthleteRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteForm;
    }
}
declare namespace Serene2.Athlete {
    class AthleteGrid extends Serenity.EntityGrid<AthleteRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        protected lastTwoAssessmentIdFilter: Serenity.EnumEditor;
        protected AthleteLoggedAssessmentTodayFilter: Serenity.EnumEditor;
        constructor(container: JQuery);
        protected createQuickFilters(): void;
        set_lastTwoAssessmentId(value: number): void;
        set_AthleteLoggedAssessmentToday(value: number): void;
        protected getButtons(): Serenity.ToolButton[];
        protected getColumns(): Slick.Column[];
        protected onClick(e: JQueryEventObject, row: number, cell: number): void;
    }
}
declare namespace Serene2.Athlete {
    class AthleteGridAthlete extends Serenity.EntityGrid<AthleteRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        /**
         * This method is called to get list of buttons to be created.
         */
        getButtons(): Serenity.ToolButton[];
    }
}
declare namespace Serene2.Athlete {
    class AthleteImagesGrid extends Serenity.EntityGrid<AthleteRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        protected lastTwoAssessmentIdFilter: Serenity.EnumEditor;
        protected AthleteLoggedAssessmentTodayFilter: Serenity.EnumEditor;
        constructor(container: JQuery);
        protected createQuickFilters(): void;
        set_lastTwoAssessmentId(value: number): void;
        set_AthleteLoggedAssessmentToday(value: number): void;
        protected getSlickOptions(): Slick.GridOptions;
        protected getButtons(): Serenity.ToolButton[];
        protected getColumns(): Slick.Column[];
        protected onClick(e: JQueryEventObject, row: number, cell: number): void;
    }
}
declare namespace Serene2.Athlete {
    class InlineImageFormatter implements Slick.Formatter, Serenity.IInitializeColumn {
        format(ctx: Slick.FormatterContext): string;
        initializeColumn(column: Slick.Column): void;
        fileProperty: string;
        thumb: boolean;
    }
}
declare namespace Serene2.Athlete {
    class TrainerListFormatter implements Slick.Formatter {
        format(ctx: Slick.FormatterContext): string;
    }
}
declare namespace Serene2.AthleteLoggedToday {
    class AthleteLoggedTodayDialog extends Serenity.EntityDialog<AthleteLoggedTodayRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteLoggedTodayForm;
    }
}
declare namespace Serene2.AthleteLoggedToday {
    class AthleteLoggedTodayEditor extends Common.GridEditorBase<AthleteLoggedTodayRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteLoggedTodayEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.AthleteLoggedToday {
    class AthleteLoggedTodayEditorDialog extends Common.GridEditorDialog<AthleteLoggedTodayRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteLoggedTodayForm;
    }
}
declare namespace Serene2.AthleteLoggedToday {
    class AthleteLoggedTodayGrid extends Serenity.EntityGrid<AthleteLoggedTodayRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteLoggedTodayDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.AthletePhysio {
    class AthletePhysioDialog extends Serenity.EntityDialog<AthletePhysioRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthletePhysioForm;
        private audit;
        constructor();
        loadEntity(entity: Assessment.WellBeingRow): void;
    }
}
declare namespace Serene2.AthletePhysio {
    class AthletePhysioEditor extends Common.GridEditorBase<AthletePhysioRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthletePhysioEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.AthletePhysio {
    class AthletePhysioEditorDialog extends Common.GridEditorDialog<AthletePhysioRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthletePhysioForm;
    }
}
declare namespace Serene2.AthletePhysio {
    class AthletePhysioGrid extends Serenity.EntityGrid<AthletePhysioRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthletePhysioDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getButtons(): Serenity.ToolButton[];
        private _AthleteId;
        AthleteId: number;
    }
}
declare namespace Serene2.AthleteWeight {
    class AthleteWeightDialog extends Serenity.EntityDialog<AthleteWeightRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteWeightForm;
    }
}
declare namespace Serene2.AthleteWeight {
    class AthleteWeightEditor extends Common.GridEditorBase<AthleteWeightRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteWeightEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.AthleteWeight {
    class AthleteWeightEditorDialog extends Common.GridEditorDialog<AthleteWeightRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteWeightForm;
    }
}
declare namespace Serene2.AthleteWeight {
    class AthleteWeightGrid extends Serenity.EntityGrid<AthleteWeightRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteWeightDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getButtons(): Serenity.ToolButton[];
        protected createQuickSearchInput(): void;
        private _AthleteUsername;
        AthleteUsername: string;
    }
}
declare namespace Serene2.Audit {
    class AuditLogDialog extends Serenity.EntityDialog<AuditLogRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AuditLogForm;
        getToolbarButtons(): Serenity.ToolButton[];
        loadEntity(entity: AuditLogRow): void;
        protected updateInterface(): void;
    }
}
declare namespace Serene2.Audit {
    class AuditLogEditor extends Common.GridEditorBase<AuditLogRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AuditLogEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Audit {
    class AuditLogEditorDialog extends Common.GridEditorDialog<AuditLogRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AuditLogForm;
    }
}
declare namespace Serene2.Audit {
    class AuditLogGrid extends Serenity.EntityGrid<AuditLogRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AuditLogDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        private assessmentGridForm;
        constructor(container: JQuery);
        protected getButtons(): Serenity.ToolButton[];
        private _rowid;
        rowid: number;
        protected createQuickSearchInput(): void;
        private _TableName;
        TableName: string;
    }
}
declare namespace Serene2 {
    class BasicProgressDialog extends Serenity.TemplatedDialog<any> {
        constructor();
        cancelled: boolean;
        max: number;
        value: number;
        title: string;
        cancelTitle: string;
        getDialogOptions(): JQueryUI.DialogOptions;
        initDialog(): void;
        getTemplate(): string;
    }
}
declare namespace Serene2.Common {
    class BulkServiceAction {
        protected keys: string[];
        protected queue: string[];
        protected queueIndex: number;
        protected progressDialog: BasicProgressDialog;
        protected pendingRequests: number;
        protected completedRequests: number;
        protected errorByKey: Q.Dictionary<Serenity.ServiceError>;
        private successCount;
        private errorCount;
        done: () => void;
        protected createProgressDialog(): void;
        protected getConfirmationFormat(): string;
        protected getConfirmationMessage(targetCount: any): string;
        protected confirm(targetCount: any, action: any): void;
        protected getNothingToProcessMessage(): string;
        protected nothingToProcess(): void;
        protected getParallelRequests(): number;
        protected getBatchSize(): number;
        protected startParallelExecution(): void;
        protected serviceCallCleanup(): void;
        protected executeForBatch(batch: string[]): void;
        protected executeNextBatch(): void;
        protected getAllHadErrorsFormat(): string;
        protected showAllHadErrors(): void;
        protected getSomeHadErrorsFormat(): string;
        protected showSomeHadErrors(): void;
        protected getAllSuccessFormat(): string;
        protected showAllSuccess(): void;
        protected showResults(): void;
        execute(keys: string[]): void;
        get_successCount(): any;
        set_successCount(value: number): void;
        get_errorCount(): any;
        set_errorCount(value: number): void;
    }
}
declare namespace Serene2.DialogUtils {
    function pendingChangesConfirmation(element: JQuery, hasPendingChanges: () => boolean): void;
}
declare namespace Serene2.Common {
    interface ExcelExportOptions {
        grid: Serenity.DataGrid<any, any>;
        service: string;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
    }
    namespace ExcelExportHelper {
        function createToolButton(options: ExcelExportOptions): Serenity.ToolButton;
    }
}
declare namespace Serene2.LanguageList {
    function getValue(): string[][];
}
declare namespace Serene2.Administration {
}
declare namespace Serene2.Administration {
    class LanguageForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface LanguageForm {
        LanguageId: Serenity.StringEditor;
        LanguageName: Serenity.StringEditor;
    }
}
declare namespace Serene2.Administration {
    interface LanguageRow {
        Id?: number;
        LanguageId?: string;
        LanguageName?: string;
    }
    namespace LanguageRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<LanguageRow>;
        namespace Fields {
            const Id: string;
            const LanguageId: string;
            const LanguageName: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace LanguageService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<LanguageRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<LanguageRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<LanguageRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<LanguageRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Administration {
}
declare namespace Serene2.Administration {
    class RoleForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface RoleForm {
        RoleName: Serenity.StringEditor;
    }
}
declare namespace Serene2.Administration {
    interface RolePermissionListRequest extends Serenity.ServiceRequest {
        RoleID?: number;
        Module?: string;
        Submodule?: string;
    }
}
declare namespace Serene2.Administration {
    interface RolePermissionListResponse extends Serenity.ListResponse<string> {
    }
}
declare namespace Serene2.Administration {
    interface RolePermissionRow {
        RolePermissionId?: number;
        RoleId?: number;
        PermissionKey?: string;
        RoleRoleName?: string;
    }
    namespace RolePermissionRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const RolePermissionId: string;
            const RoleId: string;
            const PermissionKey: string;
            const RoleRoleName: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace RolePermissionService {
        const baseUrl: string;
        function Update(request: RolePermissionUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: RolePermissionListRequest, onSuccess?: (response: RolePermissionListResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Update: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Administration {
    interface RolePermissionUpdateRequest extends Serenity.ServiceRequest {
        RoleID?: number;
        Module?: string;
        Submodule?: string;
        Permissions?: string[];
    }
}
declare namespace Serene2.Administration {
    interface RoleRow {
        RoleId?: number;
        RoleName?: string;
    }
    namespace RoleRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<RoleRow>;
        namespace Fields {
            const RoleId: string;
            const RoleName: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace RoleService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<RoleRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<RoleRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<RoleRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<RoleRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Administration {
    interface TranslationItem {
        Key?: string;
        SourceText?: string;
        TargetText?: string;
        CustomText?: string;
    }
}
declare namespace Serene2.Administration {
    interface TranslationListRequest extends Serenity.ListRequest {
        SourceLanguageID?: string;
        TargetLanguageID?: string;
    }
}
declare namespace Serene2.Administration {
    namespace TranslationService {
        const baseUrl: string;
        function List(request: TranslationListRequest, onSuccess?: (response: Serenity.ListResponse<TranslationItem>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: TranslationUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const List: string;
            const Update: string;
        }
    }
}
declare namespace Serene2.Administration {
    interface TranslationUpdateRequest extends Serenity.ServiceRequest {
        TargetLanguageID?: string;
        Translations?: {
            [key: string]: string;
        };
    }
}
declare namespace Serene2.Administration {
}
declare namespace Serene2.Administration {
    class UserForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface UserForm {
        Username: Serenity.StringEditor;
        DisplayName: Serenity.StringEditor;
        isAthlete: Serenity.BooleanEditor;
        Email: Serenity.EmailEditor;
        UserImage: Serenity.ImageUploadEditor;
        Password: Serenity.PasswordEditor;
        PasswordConfirm: Serenity.PasswordEditor;
        Source: Serenity.StringEditor;
    }
}
declare namespace Serene2.Administration {
    interface UserPermissionListRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Module?: string;
        Submodule?: string;
    }
}
declare namespace Serene2.Administration {
    interface UserPermissionRow {
        UserPermissionId?: number;
        UserId?: number;
        PermissionKey?: string;
        Granted?: boolean;
        Username?: string;
        User?: string;
    }
    namespace UserPermissionRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const UserPermissionId: string;
            const UserId: string;
            const PermissionKey: string;
            const Granted: string;
            const Username: string;
            const User: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace UserPermissionService {
        const baseUrl: string;
        function Update(request: UserPermissionUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: UserPermissionListRequest, onSuccess?: (response: Serenity.ListResponse<UserPermissionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListRolePermissions(request: UserPermissionListRequest, onSuccess?: (response: Serenity.ListResponse<string>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListPermissionKeys(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<string>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Update: string;
            const List: string;
            const ListRolePermissions: string;
            const ListPermissionKeys: string;
        }
    }
}
declare namespace Serene2.Administration {
    interface UserPermissionUpdateRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Module?: string;
        Submodule?: string;
        Permissions?: UserPermissionRow[];
    }
}
declare namespace Serene2.Administration {
    interface UserRoleListRequest extends Serenity.ServiceRequest {
        UserID?: number;
    }
}
declare namespace Serene2.Administration {
    interface UserRoleListResponse extends Serenity.ListResponse<number> {
    }
}
declare namespace Serene2.Administration {
    interface UserRoleRow {
        UserRoleId?: number;
        UserId?: number;
        RoleId?: number;
        Username?: string;
        User?: string;
        RoleName?: string;
    }
    namespace UserRoleRow {
        const idProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const UserRoleId: string;
            const UserId: string;
            const RoleId: string;
            const Username: string;
            const User: string;
            const RoleName: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace UserRoleService {
        const baseUrl: string;
        function Update(request: UserRoleUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: UserRoleListRequest, onSuccess?: (response: UserRoleListResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Update: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Administration {
    interface UserRoleUpdateRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Roles?: number[];
    }
}
declare namespace Serene2.Administration {
    interface UserRow {
        UserId?: number;
        Username?: string;
        Source?: string;
        PasswordHash?: string;
        PasswordSalt?: string;
        DisplayName?: string;
        Email?: string;
        UserImage?: string;
        LastDirectoryUpdate?: string;
        IsActive?: number;
        Password?: string;
        PasswordConfirm?: string;
        isAthlete?: number;
        InsertUserId?: number;
        InsertDate?: string;
        UpdateUserId?: number;
        UpdateDate?: string;
    }
    namespace UserRow {
        const idProperty: string;
        const isActiveProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<UserRow>;
        namespace Fields {
            const UserId: string;
            const Username: string;
            const Source: string;
            const PasswordHash: string;
            const PasswordSalt: string;
            const DisplayName: string;
            const Email: string;
            const UserImage: string;
            const LastDirectoryUpdate: string;
            const IsActive: string;
            const Password: string;
            const PasswordConfirm: string;
            const isAthlete: string;
            const InsertUserId: string;
            const InsertDate: string;
            const UpdateUserId: string;
            const UpdateDate: string;
        }
    }
}
declare namespace Serene2.Administration {
    namespace UserService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<UserRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<UserRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Undelete(request: Serenity.UndeleteRequest, onSuccess?: (response: Serenity.UndeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<UserRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<UserRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function GetUserData(request: Serenity.ServiceRequest, onSuccess?: (response: ScriptUserDefinition) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Undelete: string;
            const Retrieve: string;
            const List: string;
            const GetUserData: string;
        }
    }
}
declare namespace Serene2.Assessment {
}
declare namespace Serene2.Assessment {
    class WellBeingForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface WellBeingForm {
        WeightKg: Serenity.DecimalEditor;
        AthleteId: Serenity.IntegerEditor;
        dateof: Serenity.DateEditor;
        InjStatus: Serenity.LookupEditor;
        Sleep: Serenity.LookupEditor;
        ChangeDailyRoutine: Serenity.LookupEditor;
        SoughtMedicalAttention: Serenity.LookupEditor;
        TightnessPain: Serenity.LookupEditor;
        AcademicStress: Serenity.LookupEditor;
    }
}
declare namespace Serene2.Assessment {
    interface WellBeingRow {
        Id?: number;
        AthleteId?: number;
        InjStatus?: number;
        WeightKg?: number;
        Sleep?: number;
        ChangeDailyRoutine?: number;
        SoughtMedicalAttention?: number;
        TightnessPain?: number;
        AcademicStress?: number;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        InjStatusAnswer?: string;
        SleepAnswer?: string;
        ChangeDailyRoutineAnswer?: string;
        SoughtMedicalAttentionAnswer?: string;
        TightnessPainAnswer?: string;
        AcademicStressAnswer?: string;
        DisplayName?: string;
        dateof?: string;
        dateofdisplay?: string;
        Status?: string;
        Sport?: string;
        Trainer?: string;
        Year?: string;
    }
    namespace WellBeingRow {
        const idProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<WellBeingRow>;
        namespace Fields {
            const Id: string;
            const AthleteId: string;
            const InjStatus: string;
            const WeightKg: string;
            const Sleep: string;
            const ChangeDailyRoutine: string;
            const SoughtMedicalAttention: string;
            const TightnessPain: string;
            const AcademicStress: string;
            const AthleteLive: string;
            const AthleteUsername: string;
            const AthleteStuId: string;
            const AthleteFirstName: string;
            const AthleteLastName: string;
            const AthleteSportId: string;
            const AthleteTrainerId: string;
            const AthleteGenderId: string;
            const AthleteYearId: string;
            const InjStatusAnswer: string;
            const SleepAnswer: string;
            const ChangeDailyRoutineAnswer: string;
            const SoughtMedicalAttentionAnswer: string;
            const TightnessPainAnswer: string;
            const AcademicStressAnswer: string;
            const DisplayName: string;
            const dateof: string;
            const dateofdisplay: string;
            const Status: string;
            const Sport: string;
            const Trainer: string;
            const Year: string;
        }
    }
}
declare namespace Serene2.Assessment {
    namespace WellBeingService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<WellBeingRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<WellBeingRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<WellBeingRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<WellBeingRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Athlete {
    enum AthleteAttendStatus {
        Missing = 0,
        Attend = 1,
    }
}
declare namespace Serene2.Athlete {
}
declare namespace Serene2.Athlete {
    enum AthleteFlagStatus {
        Pass = 0,
        Attention = 1,
    }
}
declare namespace Serene2.Athlete {
    class AthleteForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteForm {
        FirstName: Serenity.StringEditor;
        LastName: Serenity.StringEditor;
        SportId: Serenity.LookupEditor;
        YearId: Serenity.LookupEditor;
        Trainers: Serenity.LookupEditor;
        Attendence: Serenity.IntegerEditor;
        AttendToday: Serenity.StringEditor;
        Username: Serenity.StringEditor;
        StuId: Serenity.StringEditor;
        GenderId: Serenity.LookupEditor;
        UserImage: Serenity.ImageUploadEditor;
        ManualImage: Serenity.ImageUploadEditor;
    }
}
declare namespace Serene2.Athlete {
}
declare namespace Serene2.Athlete {
    interface AthleteRow {
        Id?: number;
        Live?: boolean;
        Username?: string;
        StuId?: string;
        FirstName?: string;
        LastName?: string;
        SportId?: number;
        TrainerId?: number;
        Trainers?: number[];
        GenderId?: number;
        Sport?: string;
        Trainer?: string;
        Gender?: string;
        YearId?: number;
        Year?: string;
        DisplayName?: string;
        UserImage?: string;
        Attendence?: number;
        lastTwoAssessment?: string;
        lastTwoAssessmentId?: AthleteFlagStatus;
        AthleteLoggedAssessmentToday?: AthleteAttendStatus;
        lastassessment?: string;
        AttendToday?: string;
        ManualImage?: string;
        positionId?: number;
        Position?: string;
    }
    namespace AthleteRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<AthleteRow>;
        namespace Fields {
            const Id: string;
            const Live: string;
            const Username: string;
            const StuId: string;
            const FirstName: string;
            const LastName: string;
            const SportId: string;
            const TrainerId: string;
            const Trainers: string;
            const GenderId: string;
            const Sport: string;
            const Trainer: string;
            const Gender: string;
            const YearId: string;
            const Year: string;
            const DisplayName: string;
            const UserImage: string;
            const Attendence: string;
            const lastTwoAssessment: string;
            const lastTwoAssessmentId: string;
            const AthleteLoggedAssessmentToday: string;
            const lastassessment: string;
            const AttendToday: string;
            const ManualImage: string;
            const positionId: string;
            const Position: string;
        }
    }
}
declare namespace Serene2.Athlete {
    namespace AthleteService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthleteRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthleteRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthleteRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.AthleteLoggedToday {
}
declare namespace Serene2.AthleteLoggedToday {
    class AthleteLoggedTodayForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteLoggedTodayForm {
        Athleteid: Serenity.IntegerEditor;
        Username: Serenity.StringEditor;
        AthleteLoggedAssessmentToday: Serenity.IntegerEditor;
    }
}
declare namespace Serene2.AthleteLoggedToday {
    interface AthleteLoggedTodayRow {
        Id?: number;
        Athleteid?: number;
        Username?: string;
        AthleteLoggedAssessmentToday?: number;
        AthleteidLive?: number;
        AthleteidUsername?: string;
        AthleteidStuId?: string;
        AthleteidFirstName?: string;
        AthleteidLastName?: string;
        AthleteidSportId?: number;
        AthleteidTrainerId?: number;
        AthleteidGenderId?: number;
        AthleteidYearId?: number;
        recentAssessmentPass?: number;
        attendence?: number;
    }
    namespace AthleteLoggedTodayRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: string;
            const Athleteid: string;
            const Username: string;
            const AthleteLoggedAssessmentToday: string;
            const AthleteidLive: string;
            const AthleteidUsername: string;
            const AthleteidStuId: string;
            const AthleteidFirstName: string;
            const AthleteidLastName: string;
            const AthleteidSportId: string;
            const AthleteidTrainerId: string;
            const AthleteidGenderId: string;
            const AthleteidYearId: string;
            const recentAssessmentPass: string;
            const attendence: string;
        }
    }
}
declare namespace Serene2.AthleteLoggedToday {
    namespace AthleteLoggedTodayService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthleteLoggedTodayRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthleteLoggedTodayRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthleteLoggedTodayRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteLoggedTodayRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.AthletePhysio {
}
declare namespace Serene2.AthletePhysio {
    class AthletePhysioForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthletePhysioForm {
        AthleteId: Serenity.LookupEditor;
        AthleteFirstName: Serenity.StringEditor;
        AthleteLastName: Serenity.StringEditor;
        EntryDate: Serenity.DateEditor;
        PhysioId: Serenity.LookupEditor;
        InjuryId: Serenity.LookupEditor;
        InjuryStatusId: Serenity.LookupEditor;
        Notes: Serenity.TextAreaEditor;
        PhysioPlan: Serenity.TextAreaEditor;
    }
}
declare namespace Serene2.AthletePhysio {
    interface AthletePhysioRow {
        Id?: number;
        AthleteId?: number;
        EntryDate?: string;
        PhysioId?: number;
        InjuryId?: number;
        InjuryStatusId?: number;
        Notes?: string;
        PhysioPlan?: string;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        Physio?: string;
        PhysioUsername?: string;
        Injury?: string;
        InjuryStatus?: string;
        DisplayName?: string;
        history?: string;
        previousHistory?: string;
        objectiveAssessment?: string;
        managementPlan?: string;
        Sport?: string;
    }
    namespace AthletePhysioRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: string;
            const AthleteId: string;
            const EntryDate: string;
            const PhysioId: string;
            const InjuryId: string;
            const InjuryStatusId: string;
            const Notes: string;
            const PhysioPlan: string;
            const AthleteLive: string;
            const AthleteUsername: string;
            const AthleteStuId: string;
            const AthleteFirstName: string;
            const AthleteLastName: string;
            const AthleteSportId: string;
            const AthleteTrainerId: string;
            const AthleteGenderId: string;
            const AthleteYearId: string;
            const Physio: string;
            const PhysioUsername: string;
            const Injury: string;
            const InjuryStatus: string;
            const DisplayName: string;
            const history: string;
            const previousHistory: string;
            const objectiveAssessment: string;
            const managementPlan: string;
            const Sport: string;
        }
    }
}
declare namespace Serene2.AthletePhysio {
    namespace AthletePhysioService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthletePhysioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthletePhysioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthletePhysioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthletePhysioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.AthleteWeight {
}
declare namespace Serene2.AthleteWeight {
    class AthleteWeightForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteWeightForm {
        avgWeight: Serenity.DecimalEditor;
        datefor: Serenity.StringEditor;
    }
}
declare namespace Serene2.AthleteWeight {
    interface AthleteWeightListRequest extends Serenity.ListRequest {
        username?: string;
    }
}
declare namespace Serene2.AthleteWeight {
    interface AthleteWeightRow {
        id?: number;
        avgWeight?: number;
        datefor?: string;
    }
    namespace AthleteWeightRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const id: string;
            const avgWeight: string;
            const datefor: string;
        }
    }
}
declare namespace Serene2.AthleteWeight {
    namespace AthleteWeightService {
        const baseUrl: string;
        function List(request: AthleteWeightListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteWeightRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const List: string;
        }
    }
}
declare namespace Serene2.Audit {
}
declare namespace Serene2.Audit {
    class AuditLogForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AuditLogForm {
        Time: Serenity.StringEditor;
        DisplayName: Serenity.StringEditor;
        Action: Serenity.StringEditor;
        ChangesDisplayForm: Serenity.TextAreaEditor;
    }
}
declare namespace Serene2.Audit {
    interface AuditLogRow {
        Id?: number;
        UserId?: number;
        UserName?: string;
        Action?: string;
        ChangedOn?: string;
        TableName?: string;
        RowId?: number;
        Module?: string;
        Page?: string;
        Changes?: string;
        ChangesDisplayForm?: string;
        DisplayName?: string;
        Time?: string;
    }
    namespace AuditLogRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: string;
            const UserId: string;
            const UserName: string;
            const Action: string;
            const ChangedOn: string;
            const TableName: string;
            const RowId: string;
            const Module: string;
            const Page: string;
            const Changes: string;
            const ChangesDisplayForm: string;
            const DisplayName: string;
            const Time: string;
        }
    }
}
declare namespace Serene2.Audit {
    namespace AuditLogService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AuditLogRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AuditLogRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AuditLogRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AuditLogRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Common.Pages {
    interface UploadResponse extends Serenity.ServiceResponse {
        TemporaryFile?: string;
        Size?: number;
        IsImage?: boolean;
        Width?: number;
        Height?: number;
    }
}
declare namespace Serene2.Common {
    interface UserPreferenceRetrieveRequest extends Serenity.ServiceRequest {
        PreferenceType?: string;
        Name?: string;
    }
}
declare namespace Serene2.Common {
    interface UserPreferenceRetrieveResponse extends Serenity.ServiceResponse {
        Value?: string;
    }
}
declare namespace Serene2.Common {
    interface UserPreferenceRow {
        UserPreferenceId?: number;
        UserId?: number;
        PreferenceType?: string;
        Name?: string;
        Value?: string;
    }
    namespace UserPreferenceRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const UserPreferenceId: string;
            const UserId: string;
            const PreferenceType: string;
            const Name: string;
            const Value: string;
        }
    }
}
declare namespace Serene2.Common {
    namespace UserPreferenceService {
        const baseUrl: string;
        function Update(request: UserPreferenceUpdateRequest, onSuccess?: (response: Serenity.ServiceResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: UserPreferenceRetrieveRequest, onSuccess?: (response: UserPreferenceRetrieveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Update: string;
            const Retrieve: string;
        }
    }
}
declare namespace Serene2.Common {
    interface UserPreferenceUpdateRequest extends Serenity.ServiceRequest {
        PreferenceType?: string;
        Name?: string;
        Value?: string;
    }
}
declare namespace Serene2 {
    interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}
declare namespace Serene2 {
    interface ExcelImportResponse extends Serenity.ServiceResponse {
        Inserted?: number;
        Updated?: number;
        ErrorList?: string[];
    }
}
declare namespace Serene2 {
    interface GetNextNumberRequest extends Serenity.ServiceRequest {
        Prefix?: string;
        Length?: number;
    }
}
declare namespace Serene2 {
    interface GetNextNumberResponse extends Serenity.ServiceResponse {
        Number?: number;
        Serial?: string;
    }
}
declare namespace Serene2.Membership {
    class ChangePasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface ChangePasswordForm {
        OldPassword: Serenity.PasswordEditor;
        NewPassword: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
}
declare namespace Serene2.Membership {
    interface ChangePasswordRequest extends Serenity.ServiceRequest {
        OldPassword?: string;
        NewPassword?: string;
        ConfirmPassword?: string;
    }
}
declare namespace Serene2.Membership {
    class ForgotPasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface ForgotPasswordForm {
        Email: Serenity.EmailEditor;
    }
}
declare namespace Serene2.Membership {
    interface ForgotPasswordRequest extends Serenity.ServiceRequest {
        Email?: string;
    }
}
declare namespace Serene2.Membership {
    class LoginForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface LoginForm {
        Username: Serenity.StringEditor;
        Password: Serenity.PasswordEditor;
    }
}
declare namespace Serene2.Membership {
    interface LoginRequest extends Serenity.ServiceRequest {
        Username?: string;
        Password?: string;
    }
}
declare namespace Serene2.Membership {
    class ResetPasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface ResetPasswordForm {
        NewPassword: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
}
declare namespace Serene2.Membership {
    interface ResetPasswordRequest extends Serenity.ServiceRequest {
        Token?: string;
        NewPassword?: string;
        ConfirmPassword?: string;
    }
}
declare namespace Serene2.Membership {
    class SignUpForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface SignUpForm {
        DisplayName: Serenity.StringEditor;
        Email: Serenity.EmailEditor;
        ConfirmEmail: Serenity.EmailEditor;
        Password: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
}
declare namespace Serene2.Membership {
    interface SignUpRequest extends Serenity.ServiceRequest {
        DisplayName?: string;
        Email?: string;
        Password?: string;
    }
}
declare namespace Serene2 {
    interface ScriptUserDefinition {
        Username?: string;
        DisplayName?: string;
        IsAdmin?: boolean;
        isAthlete?: boolean;
        AthleteID?: string;
        ID?: string;
        Permissions?: {
            [key: string]: boolean;
        };
        Roles?: string[];
        AthleteAlreadyLoggedAss?: string;
        recentAssessmentPass?: string;
    }
}
declare namespace Serene2.Setup {
    class AthletePositionForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthletePositionForm {
        Position: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface AthletePositionRow {
        Id?: number;
        Position?: string;
    }
    namespace AthletePositionRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: any;
            const Position: any;
        }
    }
}
declare namespace Serene2.Setup {
    namespace AthletePositionService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthletePositionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthletePositionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthletePositionRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthletePositionRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteTestingForm {
        AthleteId: Serenity.IntegerEditor;
        Test1Label: Serenity.StringEditor;
        Test1Result: Serenity.DecimalEditor;
        Test2Label: Serenity.StringEditor;
        Test2Result: Serenity.DecimalEditor;
        Test3Label: Serenity.StringEditor;
        Test3Result: Serenity.DecimalEditor;
        Test4Label: Serenity.StringEditor;
        Test4Result: Serenity.DecimalEditor;
        Test5Label: Serenity.StringEditor;
        Test5Result: Serenity.DecimalEditor;
        OnDate: Serenity.DateEditor;
        SportId: Serenity.IntegerEditor;
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingLkUpForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteTestingLkUpForm {
        Test1Label: Serenity.StringEditor;
        Test2Label: Serenity.StringEditor;
        Test3Label: Serenity.StringEditor;
        Test4Label: Serenity.StringEditor;
        Test5Label: Serenity.StringEditor;
        SportId: Serenity.IntegerEditor;
    }
}
declare namespace Serene2.Setup {
    interface AthleteTestingLkUpRow {
        Id?: number;
        Test1Label?: string;
        Test2Label?: string;
        Test3Label?: string;
        Test4Label?: string;
        Test5Label?: string;
        SportId?: number;
    }
    namespace AthleteTestingLkUpRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: any;
            const Test1Label: any;
            const Test2Label: any;
            const Test3Label: any;
            const Test4Label: any;
            const Test5Label: any;
            const SportId: any;
        }
    }
}
declare namespace Serene2.Setup {
    namespace AthleteTestingLkUpService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthleteTestingLkUpRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthleteTestingLkUpRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthleteTestingLkUpRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteTestingLkUpRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
    interface AthleteTestingRow {
        Id?: number;
        AthleteId?: number;
        Test1Label?: string;
        Test1Result?: number;
        Test2Label?: string;
        Test2Result?: number;
        Test3Label?: string;
        Test3Result?: number;
        Test4Label?: string;
        Test4Result?: number;
        Test5Label?: string;
        Test5Result?: number;
        OnDate?: string;
        SportId?: number;
        AthleteLive?: number;
        AthleteUsername?: string;
        AthleteStuId?: string;
        AthleteFirstName?: string;
        AthleteLastName?: string;
        AthleteSportId?: number;
        AthleteTrainerId?: number;
        AthleteGenderId?: number;
        AthleteYearId?: number;
        AthleteUserImage?: string;
        AthleteManualImage?: string;
        AthletePositionId?: number;
        Sport?: string;
        SportSessionsPerWeek?: number;
    }
    namespace AthleteTestingRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        namespace Fields {
            const Id: any;
            const AthleteId: any;
            const Test1Label: any;
            const Test1Result: any;
            const Test2Label: any;
            const Test2Result: any;
            const Test3Label: any;
            const Test3Result: any;
            const Test4Label: any;
            const Test4Result: any;
            const Test5Label: any;
            const Test5Result: any;
            const OnDate: any;
            const SportId: any;
            const AthleteLive: string;
            const AthleteUsername: string;
            const AthleteStuId: string;
            const AthleteFirstName: string;
            const AthleteLastName: string;
            const AthleteSportId: string;
            const AthleteTrainerId: string;
            const AthleteGenderId: string;
            const AthleteYearId: string;
            const AthleteUserImage: string;
            const AthleteManualImage: string;
            const AthletePositionId: string;
            const Sport: string;
            const SportSessionsPerWeek: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace AthleteTestingService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthleteTestingRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthleteTestingRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthleteTestingRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteTestingRow>) => void, opt?: Serenity.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class AthleteYearForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface AthleteYearForm {
        Year: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface AthleteYearRow {
        YearId?: number;
        Year?: string;
    }
    namespace AthleteYearRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<AthleteYearRow>;
        namespace Fields {
            const YearId: string;
            const Year: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace AthleteYearService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<AthleteYearRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AthleteYearRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AthleteYearRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AthleteYearRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class GenderForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface GenderForm {
        Gender: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface GenderRow {
        GenderId?: number;
        Gender?: string;
    }
    namespace GenderRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<GenderRow>;
        namespace Fields {
            const GenderId: string;
            const Gender: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace GenderService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<GenderRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<GenderRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<GenderRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<GenderRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class InjStatusAnswerLkupForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface InjStatusAnswerLkupForm {
        Answer: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface InjStatusAnswerLkupRow {
        Id?: number;
        Answer?: string;
    }
    namespace InjStatusAnswerLkupRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<InjStatusAnswerLkupRow>;
        namespace Fields {
            const Id: string;
            const Answer: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace InjStatusAnswerLkupService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<InjStatusAnswerLkupRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<InjStatusAnswerLkupRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<InjStatusAnswerLkupRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<InjStatusAnswerLkupRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class InjuryForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface InjuryForm {
        Injury: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface InjuryRow {
        Id?: number;
        Injury?: string;
    }
    namespace InjuryRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<InjuryRow>;
        namespace Fields {
            const Id: string;
            const Injury: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace InjuryService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<InjuryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<InjuryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<InjuryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<InjuryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class InjuryStatusForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface InjuryStatusForm {
        InjuryStatus: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface InjuryStatusRow {
        Id?: number;
        InjuryStatus?: string;
    }
    namespace InjuryStatusRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<InjuryStatusRow>;
        namespace Fields {
            const Id: string;
            const InjuryStatus: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace InjuryStatusService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<InjuryStatusRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<InjuryStatusRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<InjuryStatusRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<InjuryStatusRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class PhysioForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface PhysioForm {
        Physio: Serenity.StringEditor;
        Username: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface PhysioRow {
        Id?: number;
        Physio?: string;
        Username?: string;
    }
    namespace PhysioRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<PhysioRow>;
        namespace Fields {
            const Id: string;
            const Physio: string;
            const Username: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace PhysioService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<PhysioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<PhysioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<PhysioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<PhysioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class SportsForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface SportsForm {
        Sport: Serenity.StringEditor;
        SessionsPerWeek: Serenity.IntegerEditor;
    }
}
declare namespace Serene2.Setup {
    interface SportsRow {
        SportId?: number;
        Sport?: string;
        SessionsPerWeek?: number;
    }
    namespace SportsRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<SportsRow>;
        namespace Fields {
            const SportId: string;
            const Sport: string;
            const SessionsPerWeek: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace SportsService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<SportsRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<SportsRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<SportsRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<SportsRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class TrainersForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface TrainersForm {
        Trainer: Serenity.StringEditor;
        username: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class TrainersListForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface TrainersListForm {
        AthId: Serenity.IntegerEditor;
        TrainerId: Serenity.IntegerEditor;
    }
}
declare namespace Serene2.Setup {
    interface TrainersListRow {
        Id?: number;
        AthId?: number;
        TrainerId?: number;
        AthLive?: number;
        AthUsername?: string;
        AthStuId?: string;
        AthFirstName?: string;
        AthLastName?: string;
        AthSportId?: number;
        AthTrainerId?: number;
        AthGenderId?: number;
        AthYearId?: number;
        AthUserImage?: string;
    }
    namespace TrainersListRow {
        const idProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<TrainersListRow>;
        namespace Fields {
            const Id: string;
            const AthId: string;
            const TrainerId: string;
            const AthLive: string;
            const AthUsername: string;
            const AthStuId: string;
            const AthFirstName: string;
            const AthLastName: string;
            const AthSportId: string;
            const AthTrainerId: string;
            const AthGenderId: string;
            const AthYearId: string;
            const AthUserImage: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace TrainersListService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<TrainersListRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<TrainersListRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<TrainersListRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<TrainersListRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
    interface TrainersRow {
        TrainerId?: number;
        Trainer?: string;
        username?: string;
    }
    namespace TrainersRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<TrainersRow>;
        namespace Fields {
            const TrainerId: string;
            const Trainer: string;
            const username: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace TrainersService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<TrainersRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<TrainersRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<TrainersRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<TrainersRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Setup {
}
declare namespace Serene2.Setup {
    class YesNoAnswerLkupForm extends Serenity.PrefixedContext {
        static formKey: string;
    }
    interface YesNoAnswerLkupForm {
        Answer: Serenity.StringEditor;
    }
}
declare namespace Serene2.Setup {
    interface YesNoAnswerLkupRow {
        Id?: number;
        Answer?: string;
    }
    namespace YesNoAnswerLkupRow {
        const idProperty: string;
        const nameProperty: string;
        const localTextPrefix: string;
        const lookupKey: string;
        function getLookup(): Q.Lookup<YesNoAnswerLkupRow>;
        namespace Fields {
            const Id: string;
            const Answer: string;
        }
    }
}
declare namespace Serene2.Setup {
    namespace YesNoAnswerLkupService {
        const baseUrl: string;
        function Create(request: Serenity.SaveRequest<YesNoAnswerLkupRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<YesNoAnswerLkupRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<YesNoAnswerLkupRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<YesNoAnswerLkupRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        namespace Methods {
            const Create: string;
            const Update: string;
            const Delete: string;
            const Retrieve: string;
            const List: string;
        }
    }
}
declare namespace Serene2.Common {
    class LanguageSelection extends Serenity.Widget<any> {
        constructor(select: JQuery, currentLanguage: string);
    }
}
declare namespace Serene2.Common {
    class SidebarSearch extends Serenity.Widget<any> {
        private menuUL;
        constructor(input: JQuery, menuUL: JQuery);
        protected updateMatchFlags(text: string): void;
    }
}
declare namespace Serene2.Common {
    class ThemeSelection extends Serenity.Widget<any> {
        constructor(select: JQuery);
        protected getCurrentTheme(): string;
    }
}
declare var jsPDF: any;
declare namespace Serene2.Common {
    interface PdfExportOptions {
        grid: Serenity.DataGrid<any, any>;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
        reportTitle?: string;
        titleTop?: number;
        titleFontSize?: number;
        fileName?: string;
        pageNumbers?: boolean;
        columnTitles?: {
            [key: string]: string;
        };
        tableOptions?: jsPDF.AutoTableOptions;
        output?: string;
        autoPrint?: boolean;
    }
    namespace PdfExportHelper {
        function exportToPdf(options: PdfExportOptions): void;
        function createToolButton(options: PdfExportOptions): Serenity.ToolButton;
    }
}
declare var jsPDF: any;
declare namespace Serene2.Common {
    class ReportDialog extends Serenity.TemplatedDialog<ReportDialogOptions> {
        private report;
        private propertyItems;
        private propertyGrid;
        constructor(options: ReportDialogOptions);
        protected getDialogButtons(): any;
        protected createPropertyGrid(): void;
        protected loadReport(reportKey: string): void;
        protected updateInterface(): void;
        executeReport(target: string, ext: string, download: boolean): void;
        getToolbarButtons(): {
            title: string;
            cssClass: string;
            onClick: () => void;
        }[];
    }
    interface ReportDialogOptions {
        reportKey: string;
    }
}
declare namespace Serene2.Common {
    interface ReportExecuteOptions {
        reportKey: string;
        download?: boolean;
        extension?: 'pdf' | 'htm' | 'html' | 'xlsx' | 'docx';
        getParams?: () => any;
        params?: {
            [key: string]: any;
        };
        target?: string;
    }
    interface ReportButtonOptions extends ReportExecuteOptions {
        title?: string;
        cssClass?: string;
        icon?: string;
    }
    namespace ReportHelper {
        function createToolButton(options: ReportButtonOptions): Serenity.ToolButton;
        function execute(options: ReportExecuteOptions): void;
    }
}
declare var jsPDF: any;
declare namespace Serene2.Common {
    class ReportPage extends Serenity.Widget<any> {
        private reportKey;
        private propertyItems;
        private propertyGrid;
        constructor(element: JQuery);
        protected updateMatchFlags(text: string): void;
        protected categoryClick(e: any): void;
        protected reportLinkClick(e: any): void;
    }
}
declare namespace Serene2.ScriptInitialization {
}
declare namespace Serene2.Common {
    class UserPreferenceStorage implements Serenity.SettingStorage {
        getItem(key: string): string;
        setItem(key: string, data: string): void;
    }
}
declare namespace Serene2.Membership {
    class ChangePasswordPanel extends Serenity.PropertyPanel<ChangePasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Membership {
    class ForgotPasswordPanel extends Serenity.PropertyPanel<ForgotPasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Membership {
    class LoginPanel extends Serenity.PropertyPanel<LoginRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Membership {
    class ResetPasswordPanel extends Serenity.PropertyPanel<ResetPasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Membership {
    class SignUpPanel extends Serenity.PropertyPanel<SignUpRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthletePositionDialog extends Serenity.EntityDialog<AthletePositionRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthletePositionForm;
    }
}
declare namespace Serene2.Setup {
    class AthletePositionEditor extends Common.GridEditorBase<AthletePositionRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthletePositionEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthletePositionEditorDialog extends Common.GridEditorDialog<AthletePositionRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthletePositionForm;
    }
}
declare namespace Serene2.Setup {
    class AthletePositionGrid extends Serenity.EntityGrid<AthletePositionRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthletePositionDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingDialog extends Serenity.EntityDialog<AthleteTestingRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteTestingForm;
        protected getToolbarButtons(): Serenity.ToolButton[];
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingEditor extends Common.GridEditorBase<AthleteTestingRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteTestingEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingEditorDialog extends Common.GridEditorDialog<AthleteTestingRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteTestingForm;
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingGrid extends Serenity.EntityGrid<AthleteTestingRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteTestingDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingLkUpDialog extends Serenity.EntityDialog<AthleteTestingLkUpRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteTestingLkUpForm;
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingLkUpEditor extends Common.GridEditorBase<AthleteTestingLkUpRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteTestingLkUpEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingLkUpEditorDialog extends Common.GridEditorDialog<AthleteTestingLkUpRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteTestingLkUpForm;
    }
}
declare namespace Serene2.Setup {
    class AthleteTestingLkUpGrid extends Serenity.EntityGrid<AthleteTestingLkUpRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteTestingLkUpDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteYearDialog extends Serenity.EntityDialog<AthleteYearRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AthleteYearForm;
    }
}
declare namespace Serene2.Setup {
    class AthleteYearEditor extends Common.GridEditorBase<AthleteYearRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteYearEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class AthleteYearEditorDialog extends Common.GridEditorDialog<AthleteYearRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: AthleteYearForm;
    }
}
declare namespace Serene2.Setup {
    class AthleteYearGrid extends Serenity.EntityGrid<AthleteYearRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AthleteYearDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class GenderDialog extends Serenity.EntityDialog<GenderRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: GenderForm;
    }
}
declare namespace Serene2.Setup {
    class GenderEditor extends Common.GridEditorBase<GenderRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof GenderEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class GenderEditorDialog extends Common.GridEditorDialog<GenderRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: GenderForm;
    }
}
declare namespace Serene2.Setup {
    class GenderGrid extends Serenity.EntityGrid<GenderRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof GenderDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjStatusAnswerLkupDialog extends Serenity.EntityDialog<InjStatusAnswerLkupRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: InjStatusAnswerLkupForm;
    }
}
declare namespace Serene2.Setup {
    class InjStatusAnswerLkupEditor extends Common.GridEditorBase<InjStatusAnswerLkupRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjStatusAnswerLkupEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjStatusAnswerLkupEditorDialog extends Common.GridEditorDialog<InjStatusAnswerLkupRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: InjStatusAnswerLkupForm;
    }
}
declare namespace Serene2.Setup {
    class InjStatusAnswerLkupGrid extends Serenity.EntityGrid<InjStatusAnswerLkupRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjStatusAnswerLkupDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjuryDialog extends Serenity.EntityDialog<InjuryRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: InjuryForm;
    }
}
declare namespace Serene2.Setup {
    class InjuryEditor extends Common.GridEditorBase<InjuryRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjuryEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjuryEditorDialog extends Common.GridEditorDialog<InjuryRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: InjuryForm;
    }
}
declare namespace Serene2.Setup {
    class InjuryGrid extends Serenity.EntityGrid<InjuryRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjuryDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjuryStatusDialog extends Serenity.EntityDialog<InjuryStatusRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: InjuryStatusForm;
    }
}
declare namespace Serene2.Setup {
    class InjuryStatusEditor extends Common.GridEditorBase<InjuryStatusRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjuryStatusEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class InjuryStatusEditorDialog extends Common.GridEditorDialog<InjuryStatusRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: InjuryStatusForm;
    }
}
declare namespace Serene2.Setup {
    class InjuryStatusGrid extends Serenity.EntityGrid<InjuryStatusRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof InjuryStatusDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class PhysioDialog extends Serenity.EntityDialog<PhysioRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: PhysioForm;
    }
}
declare namespace Serene2.Setup {
    class PhysioEditor extends Common.GridEditorBase<PhysioRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof PhysioEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class PhysioEditorDialog extends Common.GridEditorDialog<PhysioRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: PhysioForm;
    }
}
declare namespace Serene2.Setup {
    class PhysioGrid extends Serenity.EntityGrid<PhysioRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof PhysioDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class SportsDialog extends Serenity.EntityDialog<SportsRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: SportsForm;
    }
}
declare namespace Serene2.Setup {
    class SportsEditor extends Common.GridEditorBase<SportsRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof SportsEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class SportsEditorDialog extends Common.GridEditorDialog<SportsRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: SportsForm;
    }
}
declare namespace Serene2.Setup {
    class SportsGrid extends Serenity.EntityGrid<SportsRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof SportsDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class TrainersDialog extends Serenity.EntityDialog<TrainersRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: TrainersForm;
    }
}
declare namespace Serene2.Setup {
    class TrainersEditor extends Common.GridEditorBase<TrainersRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof TrainersEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class TrainersEditorDialog extends Common.GridEditorDialog<TrainersRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: TrainersForm;
    }
}
declare namespace Serene2.Setup {
    class TrainersGrid extends Serenity.EntityGrid<TrainersRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof TrainersDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class TrainersListDialog extends Serenity.EntityDialog<TrainersListRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        protected form: TrainersListForm;
    }
}
declare namespace Serene2.Setup {
    class TrainersListEditor extends Common.GridEditorBase<TrainersListRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof TrainersListEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class TrainersListEditorDialog extends Common.GridEditorDialog<TrainersListRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected form: TrainersListForm;
    }
}
declare namespace Serene2.Setup {
    class TrainersListGrid extends Serenity.EntityGrid<TrainersListRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof TrainersListDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class YesNoAnswerLkupDialog extends Serenity.EntityDialog<YesNoAnswerLkupRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: YesNoAnswerLkupForm;
    }
}
declare namespace Serene2.Setup {
    class YesNoAnswerLkupEditor extends Common.GridEditorBase<YesNoAnswerLkupRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof YesNoAnswerLkupEditorDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
    }
}
declare namespace Serene2.Setup {
    class YesNoAnswerLkupEditorDialog extends Common.GridEditorDialog<YesNoAnswerLkupRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected form: YesNoAnswerLkupForm;
    }
}
declare namespace Serene2.Setup {
    class YesNoAnswerLkupGrid extends Serenity.EntityGrid<YesNoAnswerLkupRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof YesNoAnswerLkupDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
