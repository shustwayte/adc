﻿var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Serene2;
(function (Serene2) {
    var ScriptInitialization;
    (function (ScriptInitialization) {
        Q.Config.responsiveDialogs = true;
        Q.Config.rootNamespaces.push('Serene2');
        Serenity.DataGrid.defaultPersistanceStorage = window.sessionStorage;
        Serenity.DataGrid.defaultPersistanceStorage = window.localStorage;
    })(ScriptInitialization = Serene2.ScriptInitialization || (Serene2.ScriptInitialization = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var LanguageDialog = /** @class */ (function (_super) {
            __extends(LanguageDialog, _super);
            function LanguageDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.LanguageForm(_this.idPrefix);
                return _this;
            }
            LanguageDialog.prototype.getFormKey = function () { return Administration.LanguageForm.formKey; };
            LanguageDialog.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageDialog.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageDialog.prototype.getNameProperty = function () { return Administration.LanguageRow.nameProperty; };
            LanguageDialog.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageDialog);
            return LanguageDialog;
        }(Serenity.EntityDialog));
        Administration.LanguageDialog = LanguageDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var LanguageGrid = /** @class */ (function (_super) {
            __extends(LanguageGrid, _super);
            function LanguageGrid(container) {
                return _super.call(this, container) || this;
            }
            LanguageGrid.prototype.getColumnsKey = function () { return "Administration.Language"; };
            LanguageGrid.prototype.getDialogType = function () { return Administration.LanguageDialog; };
            LanguageGrid.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageGrid.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageGrid.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageGrid.prototype.getDefaultSortBy = function () {
                return [Administration.LanguageRow.Fields.LanguageName];
            };
            LanguageGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageGrid);
            return LanguageGrid;
        }(Serenity.EntityGrid));
        Administration.LanguageGrid = LanguageGrid;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleDialog = /** @class */ (function (_super) {
            __extends(RoleDialog, _super);
            function RoleDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.RoleForm(_this.idPrefix);
                return _this;
            }
            RoleDialog.prototype.getFormKey = function () { return Administration.RoleForm.formKey; };
            RoleDialog.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleDialog.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleDialog.prototype.getNameProperty = function () { return Administration.RoleRow.nameProperty; };
            RoleDialog.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.RolePermissionDialog.EditButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'icon-lock-open text-green',
                    onClick: function () {
                        new Administration.RolePermissionDialog({
                            roleID: _this.entity.RoleId,
                            title: _this.entity.RoleName
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            RoleDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
            };
            RoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleDialog);
            return RoleDialog;
        }(Serenity.EntityDialog));
        Administration.RoleDialog = RoleDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleGrid = /** @class */ (function (_super) {
            __extends(RoleGrid, _super);
            function RoleGrid(container) {
                return _super.call(this, container) || this;
            }
            RoleGrid.prototype.getColumnsKey = function () { return "Administration.Role"; };
            RoleGrid.prototype.getDialogType = function () { return Administration.RoleDialog; };
            RoleGrid.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleGrid.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleGrid.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleGrid.prototype.getDefaultSortBy = function () {
                return [Administration.RoleRow.Fields.RoleName];
            };
            RoleGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleGrid);
            return RoleGrid;
        }(Serenity.EntityGrid));
        Administration.RoleGrid = RoleGrid;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RolePermissionDialog = /** @class */ (function (_super) {
            __extends(RolePermissionDialog, _super);
            function RolePermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: false
                });
                Administration.RolePermissionService.List({
                    RoleID: _this.options.roleID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.set_value(response.Entities.map(function (x) { return ({ PermissionKey: x }); }));
                });
                return _this;
            }
            RolePermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.RolePermissionService.Update({
                                RoleID: _this.options.roleID,
                                Permissions: _this.permissions.get_value().map(function (x) { return x.PermissionKey; }),
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.RolePermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.RolePermissionDialog.DialogTitle'), this.options.title);
                return opt;
            };
            RolePermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            RolePermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RolePermissionDialog);
            return RolePermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.RolePermissionDialog = RolePermissionDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var TranslationGrid = /** @class */ (function (_super) {
            __extends(TranslationGrid, _super);
            function TranslationGrid(container) {
                var _this = _super.call(this, container) || this;
                _this.element.on('keyup.' + _this.uniqueName + ' change.' + _this.uniqueName, 'input.custom-text', function (e) {
                    var value = Q.trimToNull($(e.target).val());
                    if (value === '') {
                        value = null;
                    }
                    _this.view.getItemById($(e.target).data('key')).CustomText = value;
                    _this.hasChanges = true;
                });
                return _this;
            }
            TranslationGrid.prototype.getIdProperty = function () { return "Key"; };
            TranslationGrid.prototype.getLocalTextPrefix = function () { return "Administration.Translation"; };
            TranslationGrid.prototype.getService = function () { return Administration.TranslationService.baseUrl; };
            TranslationGrid.prototype.onClick = function (e, row, cell) {
                var _this = this;
                _super.prototype.onClick.call(this, e, row, cell);
                if (e.isDefaultPrevented()) {
                    return;
                }
                var item = this.itemAt(row);
                var done;
                if ($(e.target).hasClass('source-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.SourceText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.SourceText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
                if ($(e.target).hasClass('target-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.TargetText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.TargetText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
            };
            TranslationGrid.prototype.getColumns = function () {
                var columns = [];
                columns.push({ field: 'Key', width: 300, sortable: false });
                columns.push({
                    field: 'SourceText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) {
                        return Q.outerHtml($('<a/>')
                            .addClass('source-text')
                            .text(ctx.value || ''));
                    }
                });
                columns.push({
                    field: 'CustomText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<input/>')
                        .addClass('custom-text')
                        .attr('value', ctx.value)
                        .attr('type', 'text')
                        .attr('data-key', ctx.item.Key)); }
                });
                columns.push({
                    field: 'TargetText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<a/>')
                        .addClass('target-text')
                        .text(ctx.value || '')); }
                });
                return columns;
            };
            TranslationGrid.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                var opt = {
                    lookupKey: 'Administration.Language'
                };
                this.sourceLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.SourceLanguage') + ' ---'); },
                    options: opt
                });
                this.sourceLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
                this.targetLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.TargetLanguage') + ' ---'); },
                    options: opt
                });
                this.targetLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
            };
            TranslationGrid.prototype.saveChanges = function (language) {
                var _this = this;
                var translations = {};
                for (var _i = 0, _a = this.getItems(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    translations[item.Key] = item.CustomText;
                }
                return RSVP.resolve(Administration.TranslationService.Update({
                    TargetLanguageID: language,
                    Translations: translations
                })).then(function () {
                    _this.hasChanges = false;
                    language = Q.trimToNull(language) || 'invariant';
                    Q.notifySuccess('User translations in "' + language +
                        '" language are saved to "user.texts.' +
                        language + '.json" ' + 'file under "~/App_Data/texts/"', '');
                });
            };
            TranslationGrid.prototype.onViewSubmit = function () {
                var request = this.view.params;
                request.SourceLanguageID = this.sourceLanguage.value;
                this.targetLanguageKey = this.targetLanguage.value || '';
                request.TargetLanguageID = this.targetLanguageKey;
                this.hasChanges = false;
                return _super.prototype.onViewSubmit.call(this);
            };
            TranslationGrid.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: Q.text('Db.Administration.Translation.SaveChangesButton'),
                        onClick: function (e) { return _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); }); },
                        cssClass: 'apply-changes-button'
                    }];
            };
            TranslationGrid.prototype.createQuickSearchInput = function () {
                var _this = this;
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, searchText) {
                    _this.searchText = searchText;
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            TranslationGrid.prototype.onViewFilter = function (item) {
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!this.searchText) {
                    return true;
                }
                var sd = Select2.util.stripDiacritics;
                var searching = sd(this.searchText).toLowerCase();
                function match(str) {
                    if (!str)
                        return false;
                    return str.toLowerCase().indexOf(searching) >= 0;
                }
                return Q.isEmptyOrNull(searching) || match(item.Key) || match(item.SourceText) ||
                    match(item.TargetText) || match(item.CustomText);
            };
            TranslationGrid.prototype.usePager = function () {
                return false;
            };
            TranslationGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TranslationGrid);
            return TranslationGrid;
        }(Serenity.EntityGrid));
        Administration.TranslationGrid = TranslationGrid;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserDialog = /** @class */ (function (_super) {
            __extends(UserDialog, _super);
            function UserDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Administration.UserForm(_this.idPrefix);
                _this.form.Password.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value.length < 7)
                        return "Password must be at least 7 characters!";
                });
                _this.form.PasswordConfirm.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value != _this.form.PasswordConfirm.value)
                        return "The passwords entered doesn't match!";
                });
                return _this;
            }
            UserDialog.prototype.getFormKey = function () { return Administration.UserForm.formKey; };
            UserDialog.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            UserDialog.prototype.getIsActiveProperty = function () { return Administration.UserRow.isActiveProperty; };
            UserDialog.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserDialog.prototype.getNameProperty = function () { return Administration.UserRow.nameProperty; };
            UserDialog.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.UserDialog.EditRolesButton'),
                    cssClass: 'edit-roles-button',
                    icon: 'icon-people text-blue',
                    onClick: function () {
                        new Administration.UserRoleDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                buttons.push({
                    title: Q.text('Site.UserDialog.EditPermissionsButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'icon-lock-open text-green',
                    onClick: function () {
                        new Administration.UserPermissionDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            UserDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton('edit-roles-button').toggleClass('disabled', this.isNewOrDeleted());
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
                var user = Serene2.Authorization.userDefinition;
                Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), true);
                if (user.Username == "apope" || user.Username == "pd363" || user.Username == "nas217" || user.Username == "crh207" || user.Username == "sh444") {
                    Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), false);
                }
            };
            UserDialog.prototype.afterLoadEntity = function () {
                _super.prototype.afterLoadEntity.call(this);
                // these fields are only required in new record mode
                this.form.Password.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
                this.form.PasswordConfirm.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
            };
            UserDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserDialog);
            return UserDialog;
        }(Serenity.EntityDialog));
        Administration.UserDialog = UserDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserGrid = /** @class */ (function (_super) {
            __extends(UserGrid, _super);
            function UserGrid(container) {
                var _this = _super.call(this, container) || this;
                _this.pendingChanges = {};
                _this.slickContainer.on('change', '.edit:input', function (e) { return _this.inputsChange(e); });
                return _this;
            }
            UserGrid.prototype.getColumnsKey = function () { return "Administration.User"; };
            UserGrid.prototype.getDialogType = function () { return Administration.UserDialog; };
            UserGrid.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            // protected getIsActiveProperty() { return UserRow.isActiveProperty; }
            UserGrid.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserGrid.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum('isAthlete'),
                        new Slick.Aggregators.Sum('isLive'),
                        new Slick.Aggregators.Sum('Paid')
                    ]
                });
                return grid;
            };
            UserGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            UserGrid.prototype.usePager = function () {
                return false;
            };
            UserGrid.prototype.createToolbarExtensions = function () {
                _super.prototype.createToolbarExtensions.call(this);
                this.rowSelection = new Serenity.GridRowSelectionMixin(this);
            };
            UserGrid.prototype.inputsChange = function (e) {
                var cell = this.slickGrid.getCellFromEvent(e);
                var item = this.itemAt(cell.row);
                var input = $(e.target);
                var field = input.data('field');
                var text = Q.coalesce(Q.trimToNull(input.val()), '0');
                var pending = this.pendingChanges[item.UserId];
                var effective = this.getEffectiveValue(item, field);
                var oldText;
                if (input.hasClass("numeric"))
                    oldText = Q.formatNumber(effective, '0.##');
                else
                    oldText = effective;
                var value;
                if (field === 'UnitPrice') {
                    value = Q.parseDecimal(text);
                    if (value == null || isNaN(value)) {
                        Q.notifyError(Q.text('Validation.Decimal'), '', null);
                        input.val(oldText);
                        input.focus();
                        return;
                    }
                }
                else if (input.hasClass("numeric")) {
                    var i = Q.parseDecimal(text);
                    if (isNaN(i) || i > 32767 || i < 0) {
                        Q.notifyError(Q.text('Validation.Decimal'), '', null);
                        input.val(oldText);
                        input.focus();
                        return;
                    }
                    value = i;
                }
                else
                    value = text;
                if (field == "isLive") {
                    if (item.isLive == 1) {
                        value = 0;
                    }
                    else {
                        value = 1;
                    }
                }
                if (field == "Paid") {
                    if (item.Paid == 1) {
                        value = 0;
                    }
                    else {
                        value = 1;
                    }
                }
                /*
                 //if (input.val(oldText)[0].getAttribute("checked").toString() == "true") {
                 if (oldText == "1") {
                     value = 1;
                 }
                 else
                 {
                     value = 0;
                 }
                */
                if (!pending) {
                    this.pendingChanges[item.UserId] = pending = {};
                }
                pending[field] = value;
                item[field] = value;
                this.view.refresh();
                //          if (input.hasClass("numeric"))
                //                value = Q.formatNumber(value, '0.##');
                input.val(value).addClass('dirty');
                this.setSaveButtonState();
            };
            UserGrid.prototype.getButtons = function () {
                var _this = this;
                // call base method to get list of buttons
                // by default, base entity grid adds a few buttons, 
                // add, refresh, column selection in order.
                var buttons = _super.prototype.getButtons.call(this);
                //  buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);
                buttons.push({
                    title: 'Save Changes',
                    cssClass: 'apply-changes-button disabled',
                    onClick: function (e) { return _this.saveClick(); },
                    separator: true
                });
                return buttons;
            };
            UserGrid.prototype.onViewProcessData = function (response) {
                this.pendingChanges = {};
                this.setSaveButtonState();
                return _super.prototype.onViewProcessData.call(this, response);
            };
            UserGrid.prototype.numericInputFormatter = function (ctx) {
                var klass = 'edit checkbox';
                var item = ctx.item;
                var pending = this.pendingChanges[item.UserId];
                if (pending && pending[ctx.column.field] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, ctx.column.field);
                if (item.isLive == 1) {
                    return "<input type='checkbox' class='" + klass +
                        "' data-field='" + ctx.column.field +
                        "' checked />";
                }
                else {
                    return "<input type='checkbox' class='" + klass +
                        "' data-field='" + ctx.column.field +
                        "'  />";
                }
            };
            UserGrid.prototype.numericInputFormatterPaid = function (ctx) {
                var klass = 'edit checkbox';
                var item = ctx.item;
                var pending = this.pendingChanges[item.UserId];
                if (pending && pending[ctx.column.field] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, ctx.column.field);
                if (item.Paid == 1) {
                    return "<input type='checkbox' class='" + klass +
                        "' data-field='" + ctx.column.field +
                        "' checked />";
                }
                else {
                    return "<input type='checkbox' class='" + klass +
                        "' data-field='" + ctx.column.field +
                        "'  />";
                }
            };
            UserGrid.prototype.stringInputFormatter = function (ctx) {
                var klass = 'edit string';
                var item = ctx.item;
                var pending = this.pendingChanges[item.UserId];
                var column = ctx.column;
                if (pending && pending[column.field] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, column.field);
                return "<input type='text' class='" + klass +
                    "' data-field='" + column.field +
                    "' value='" + Q.htmlEncode(value) +
                    "' maxlength='" + column.sourceItem.maxLength + "'/>";
            };
            UserGrid.prototype.getColumns = function () {
                var _this = this;
                var columns = _super.prototype.getColumns.call(this);
                var num = function (ctx) { return _this.numericInputFormatter(ctx); };
                var num2 = function (ctx) { return _this.numericInputFormatterPaid(ctx); };
                var str = function (ctx) { return _this.stringInputFormatter(ctx); };
                var fld = Administration.UserRow.Fields;
                Q.first(columns, function (x) { return x.field === fld.isLive; }).format = num;
                var user = Serene2.Authorization.userDefinition;
                if (user.Username == "apope" || user.Username == "pd363" || user.Username == "nas217" || user.Username == "crh207" || user.Username == "sh444") {
                    Q.first(columns, function (x) { return x.field === fld.Paid; }).format = num2;
                }
                Q.first(columns, function (x) { return x.field === 'isAthlete'; })
                    .groupTotalsFormatter = function (totals, col) {
                    return (totals.sum ? ('Athletes: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');
                };
                Q.first(columns, function (x) { return x.field === 'isLive'; })
                    .groupTotalsFormatter = function (totals, col) {
                    return (totals.sum ? ('Live: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');
                };
                Q.first(columns, function (x) { return x.field === 'Paid'; })
                    .groupTotalsFormatter = function (totals, col) {
                    return (totals.sum ? ('Paid: ' + Q.coalesce(Q.formatNumber(totals.sum[col.field], '0.'), '')) : '');
                };
                return columns;
            };
            UserGrid.prototype.selectFormatter = function (ctx, idField, lookup) {
                var fld = Administration.UserRow.Fields;
                var klass = 'edit';
                var item = ctx.item;
                var pending = this.pendingChanges[item.UserId];
                var column = ctx.column;
                if (pending && pending[idField] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, idField);
                var markup = "<select class='" + klass +
                    "' data-field='" + idField +
                    "' style='width: 100%; max-width: 100%'>";
                for (var _i = 0, _a = lookup.items; _i < _a.length; _i++) {
                    var c = _a[_i];
                    var id = c[lookup.idField];
                    markup += "<option value='" + id + "'";
                    if (id == value) {
                        markup += " selected";
                    }
                    markup += ">" + Q.htmlEncode(c[lookup.textField]) + "</option>";
                }
                return markup + "</select>";
            };
            UserGrid.prototype.getEffectiveValue = function (item, field) {
                var pending = this.pendingChanges[item.ProductID];
                if (pending && pending[field] !== undefined) {
                    return pending[field];
                }
                return item[field];
            };
            UserGrid.prototype.setSaveButtonState = function () {
                this.toolbar.findButton('apply-changes-button').toggleClass('disabled', Object.keys(this.pendingChanges).length === 0);
            };
            UserGrid.prototype.saveClick = function () {
                if (Object.keys(this.pendingChanges).length === 0) {
                    return;
                }
                // this calls save service for all modified rows, one by one
                // you could write a batch update service
                var keys = Object.keys(this.pendingChanges);
                var current = -1;
                var self = this;
                (function saveNext() {
                    if (++current >= keys.length) {
                        self.refresh();
                        return;
                    }
                    var key = keys[current];
                    var entity = Q.deepClone(self.pendingChanges[key]);
                    entity.UserId = key;
                    Q.serviceRequest('Administration/User/Update', {
                        EntityId: key,
                        Entity: entity
                    }, function (response) {
                        delete self.pendingChanges[key];
                        saveNext();
                    });
                })();
            };
            UserGrid.prototype.getDefaultSortBy = function () {
                return [Administration.UserRow.Fields.Username];
            };
            UserGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], UserGrid);
            return UserGrid;
        }(Serenity.EntityGrid));
        Administration.UserGrid = UserGrid;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var PermissionCheckEditor = /** @class */ (function (_super) {
            __extends(PermissionCheckEditor, _super);
            function PermissionCheckEditor(container, opt) {
                var _this = _super.call(this, container, opt) || this;
                _this.rolePermissions = {};
                var titleByKey = {};
                var permissionKeys = _this.getSortedGroupAndPermissionKeys(titleByKey);
                var items = permissionKeys.map(function (key) { return ({
                    Key: key,
                    ParentKey: _this.getParentKey(key),
                    Title: titleByKey[key],
                    GrantRevoke: null,
                    IsGroup: key.charAt(key.length - 1) === ':'
                }); });
                _this.byParentKey = Q.toGrouping(items, function (x) { return x.ParentKey; });
                _this.setItems(items);
                return _this;
            }
            PermissionCheckEditor.prototype.getIdProperty = function () { return "Key"; };
            PermissionCheckEditor.prototype.getItemGrantRevokeClass = function (item, grant) {
                if (!item.IsGroup) {
                    return ((item.GrantRevoke === grant) ? ' checked' : '');
                }
                var desc = this.getDescendants(item, true);
                var granted = desc.filter(function (x) { return x.GrantRevoke === grant; });
                if (!granted.length) {
                    return '';
                }
                if (desc.length === granted.length) {
                    return 'checked';
                }
                return 'checked partial';
            };
            PermissionCheckEditor.prototype.getItemEffectiveClass = function (item) {
                var _this = this;
                if (item.IsGroup) {
                    var desc = this.getDescendants(item, true);
                    var grantCount = Q.count(desc, function (x) { return x.GrantRevoke === true ||
                        (x.GrantRevoke == null && _this.rolePermissions[x.Key]); });
                    if (grantCount === desc.length || desc.length === 0) {
                        return 'allow';
                    }
                    if (grantCount === 0) {
                        return 'deny';
                    }
                    return 'partial';
                }
                var granted = item.GrantRevoke === true ||
                    (item.GrantRevoke == null && this.rolePermissions[item.Key]);
                return (granted ? ' allow' : ' deny');
            };
            PermissionCheckEditor.prototype.getColumns = function () {
                var _this = this;
                var columns = [{
                        name: Q.text('Site.UserPermissionDialog.Permission'),
                        field: 'Title',
                        format: Serenity.SlickFormatting.treeToggle(function () { return _this.view; }, function (x) { return x.Key; }, function (ctx) {
                            var item = ctx.item;
                            var klass = _this.getItemEffectiveClass(item);
                            return '<span class="effective-permission ' + klass + '">' + Q.htmlEncode(ctx.value) + '</span>';
                        }),
                        width: 495,
                        sortable: false
                    }, {
                        name: Q.text('Site.UserPermissionDialog.Grant'), field: 'Grant',
                        format: function (ctx) {
                            var item1 = ctx.item;
                            var klass1 = _this.getItemGrantRevokeClass(item1, true);
                            return "<span class='check-box grant no-float " + klass1 + "'></span>";
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    }];
                if (this.options.showRevoke) {
                    columns.push({
                        name: Q.text('Site.UserPermissionDialog.Revoke'), field: 'Revoke',
                        format: function (ctx) {
                            var item2 = ctx.item;
                            var klass2 = _this.getItemGrantRevokeClass(item2, false);
                            return '<span class="check-box revoke no-float ' + klass2 + '"></span>';
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    });
                }
                return columns;
            };
            PermissionCheckEditor.prototype.setItems = function (items) {
                Serenity.SlickTreeHelper.setIndents(items, function (x) { return x.Key; }, function (x) { return x.ParentKey; }, false);
                this.view.setItems(items, true);
            };
            PermissionCheckEditor.prototype.onViewSubmit = function () {
                return false;
            };
            PermissionCheckEditor.prototype.onViewFilter = function (item) {
                var _this = this;
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!Serenity.SlickTreeHelper.filterById(item, this.view, function (x) { return x.ParentKey; }))
                    return false;
                if (this.searchText) {
                    return this.matchContains(item) || item.IsGroup && Q.any(this.getDescendants(item, false), function (x) { return _this.matchContains(x); });
                }
                return true;
            };
            PermissionCheckEditor.prototype.matchContains = function (item) {
                return Select2.util.stripDiacritics(item.Title || '').toLowerCase().indexOf(this.searchText) >= 0;
            };
            PermissionCheckEditor.prototype.getDescendants = function (item, excludeGroups) {
                var result = [];
                var stack = [item];
                while (stack.length > 0) {
                    var i = stack.pop();
                    var children = this.byParentKey[i.Key];
                    if (!children)
                        continue;
                    for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                        var child = children_1[_i];
                        if (!excludeGroups || !child.IsGroup) {
                            result.push(child);
                        }
                        stack.push(child);
                    }
                }
                return result;
            };
            PermissionCheckEditor.prototype.onClick = function (e, row, cell) {
                _super.prototype.onClick.call(this, e, row, cell);
                if (!e.isDefaultPrevented()) {
                    Serenity.SlickTreeHelper.toggleClick(e, row, cell, this.view, function (x) { return x.Key; });
                }
                if (e.isDefaultPrevented()) {
                    return;
                }
                var target = $(e.target);
                var grant = target.hasClass('grant');
                if (grant || target.hasClass('revoke')) {
                    e.preventDefault();
                    var item = this.itemAt(row);
                    var checkedOrPartial = target.hasClass('checked') || target.hasClass('partial');
                    if (checkedOrPartial) {
                        grant = null;
                    }
                    else {
                        grant = grant !== checkedOrPartial;
                    }
                    if (item.IsGroup) {
                        for (var _i = 0, _a = this.getDescendants(item, true); _i < _a.length; _i++) {
                            var d = _a[_i];
                            d.GrantRevoke = grant;
                        }
                    }
                    else
                        item.GrantRevoke = grant;
                    this.slickGrid.invalidate();
                }
            };
            PermissionCheckEditor.prototype.getParentKey = function (key) {
                if (key.charAt(key.length - 1) === ':') {
                    key = key.substr(0, key.length - 1);
                }
                var idx = key.lastIndexOf(':');
                if (idx >= 0) {
                    return key.substr(0, idx + 1);
                }
                return null;
            };
            PermissionCheckEditor.prototype.getButtons = function () {
                return [];
            };
            PermissionCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(Q.trimToNull(text) || '').toLowerCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            PermissionCheckEditor.prototype.getSortedGroupAndPermissionKeys = function (titleByKey) {
                var keys = Q.getRemoteData('Administration.PermissionKeys').Entities;
                var titleWithGroup = {};
                for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                    var k = keys_1[_i];
                    var s = k;
                    if (!s) {
                        continue;
                    }
                    if (s.charAt(s.length - 1) == ':') {
                        s = s.substr(0, s.length - 1);
                        if (s.length === 0) {
                            continue;
                        }
                    }
                    if (titleByKey[s]) {
                        continue;
                    }
                    titleByKey[s] = Q.coalesce(Q.tryGetText('Permission.' + s), s);
                    var parts = s.split(':');
                    var group = '';
                    var groupTitle = '';
                    for (var i = 0; i < parts.length - 1; i++) {
                        group = group + parts[i] + ':';
                        var txt = Q.tryGetText('Permission.' + group);
                        if (txt == null) {
                            txt = parts[i];
                        }
                        titleByKey[group] = txt;
                        groupTitle = groupTitle + titleByKey[group] + ':';
                        titleWithGroup[group] = groupTitle;
                    }
                    titleWithGroup[s] = groupTitle + titleByKey[s];
                }
                keys = Object.keys(titleByKey);
                keys = keys.sort(function (x, y) { return Q.turkishLocaleCompare(titleWithGroup[x], titleWithGroup[y]); });
                return keys;
            };
            PermissionCheckEditor.prototype.get_value = function () {
                var result = [];
                for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    if (item.GrantRevoke != null && item.Key.charAt(item.Key.length - 1) != ':') {
                        result.push({ PermissionKey: item.Key, Granted: item.GrantRevoke });
                    }
                }
                return result;
            };
            PermissionCheckEditor.prototype.set_value = function (value) {
                for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    item.GrantRevoke = null;
                }
                if (value != null) {
                    for (var _b = 0, value_1 = value; _b < value_1.length; _b++) {
                        var row = value_1[_b];
                        var r = this.view.getItemById(row.PermissionKey);
                        if (r) {
                            r.GrantRevoke = Q.coalesce(row.Granted, true);
                        }
                    }
                }
                this.setItems(this.getItems());
            };
            PermissionCheckEditor.prototype.get_rolePermissions = function () {
                return Object.keys(this.rolePermissions);
            };
            PermissionCheckEditor.prototype.set_rolePermissions = function (value) {
                this.rolePermissions = {};
                if (value) {
                    for (var _i = 0, value_2 = value; _i < value_2.length; _i++) {
                        var k = value_2[_i];
                        this.rolePermissions[k] = true;
                    }
                }
                this.setItems(this.getItems());
            };
            PermissionCheckEditor = __decorate([
                Serenity.Decorators.registerEditor([Serenity.IGetEditValue, Serenity.ISetEditValue])
            ], PermissionCheckEditor);
            return PermissionCheckEditor;
        }(Serenity.DataGrid));
        Administration.PermissionCheckEditor = PermissionCheckEditor;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserPermissionDialog = /** @class */ (function (_super) {
            __extends(UserPermissionDialog, _super);
            function UserPermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: true
                });
                Administration.UserPermissionService.List({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.set_value(response.Entities);
                });
                Administration.UserPermissionService.ListRolePermissions({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null,
                }, function (response) {
                    _this.permissions.set_rolePermissions(response.Entities);
                });
                return _this;
            }
            UserPermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.UserPermissionService.Update({
                                UserID: _this.options.userID,
                                Permissions: _this.permissions.get_value(),
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.UserPermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.UserPermissionDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserPermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            UserPermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserPermissionDialog);
            return UserPermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserPermissionDialog = UserPermissionDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleCheckEditor = /** @class */ (function (_super) {
            __extends(RoleCheckEditor, _super);
            function RoleCheckEditor(div) {
                return _super.call(this, div) || this;
            }
            RoleCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(text || '').toUpperCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            RoleCheckEditor.prototype.getButtons = function () {
                return [];
            };
            RoleCheckEditor.prototype.getTreeItems = function () {
                return Administration.RoleRow.getLookup().items.map(function (role) { return ({
                    id: role.RoleId.toString(),
                    text: role.RoleName
                }); });
            };
            RoleCheckEditor.prototype.onViewFilter = function (item) {
                return _super.prototype.onViewFilter.call(this, item) &&
                    (Q.isEmptyOrNull(this.searchText) ||
                        Select2.util.stripDiacritics(item.text || '')
                            .toUpperCase().indexOf(this.searchText) >= 0);
            };
            RoleCheckEditor = __decorate([
                Serenity.Decorators.registerEditor()
            ], RoleCheckEditor);
            return RoleCheckEditor;
        }(Serenity.CheckTreeEditor));
        Administration.RoleCheckEditor = RoleCheckEditor;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserRoleDialog = /** @class */ (function (_super) {
            __extends(UserRoleDialog, _super);
            function UserRoleDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.RoleCheckEditor(_this.byId('Roles'));
                Administration.UserRoleService.List({
                    UserID: _this.options.userID
                }, function (response) {
                    _this.permissions.value = response.Entities.map(function (x) { return x.toString(); });
                });
                return _this;
            }
            UserRoleDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [{
                        text: Q.text('Dialogs.OkButton'),
                        click: function () {
                            Q.serviceRequest('Administration/UserRole/Update', {
                                UserID: _this.options.userID,
                                Roles: _this.permissions.value.map(function (x) { return parseInt(x, 10); })
                            }, function (response) {
                                _this.dialogClose();
                                Q.notifySuccess(Q.text('Site.UserRoleDialog.SaveSuccess'));
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }];
                opt.title = Q.format(Q.text('Site.UserRoleDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserRoleDialog.prototype.getTemplate = function () {
                return "<div id='~_Roles'></div>";
            };
            UserRoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserRoleDialog);
            return UserRoleDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserRoleDialog = UserRoleDialog;
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingDialog = /** @class */ (function (_super) {
            __extends(WellBeingDialog, _super);
            function WellBeingDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Assessment.WellBeingForm(_this.idPrefix);
                _this.audit = new Serene2.Audit.AuditLogGrid(_this.byId('ActionAuditGrid'));
                _this.audit.element.flexHeightOnly(1);
                _this.tabs.bind('tabsactivate', function () { return _this.arrange(); });
                _this.tabs.on('tabsactivate', function (e, i) {
                    _this.arrange();
                });
                return _this;
            }
            WellBeingDialog.prototype.getFormKey = function () { return Assessment.WellBeingForm.formKey; };
            WellBeingDialog.prototype.getIdProperty = function () { return Assessment.WellBeingRow.idProperty; };
            WellBeingDialog.prototype.getLocalTextPrefix = function () { return Assessment.WellBeingRow.localTextPrefix; };
            WellBeingDialog.prototype.getService = function () { return Assessment.WellBeingService.baseUrl; };
            WellBeingDialog.prototype.loadEntity = function (entity) {
                var _this = this;
                _super.prototype.loadEntity.call(this, entity);
                var user = Serene2.Authorization.userDefinition;
                if (user.isAthlete == true) {
                    this.form.dateof.element.toggleClass('disabled', true);
                    this.form.dateof.element.toggleClass('readOnly', true);
                    this.form.dateof.element.toggleClass('isDisabled', true);
                    Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);
                    //this.deleteButton.hide();
                    this.deleteButton.toggleClass('disabled', true);
                }
                var athid = this.entity.Id;
                this.audit.rowid = athid;
                this.audit.TableName = "[dbo].[wellBeing]";
                /*
                            if (!athid) {
                                // just load an empty entity
                                this.assessmentGridForm.load({});
                                return;
                            }
                */
                // load  record
                if (athid) {
                    Assessment.WellBeingService.Retrieve({
                        EntityId: athid
                    }, function (response) {
                        _this.assessmentGridForm.load(response.Entity);
                    });
                }
            };
            WellBeingDialog.prototype.onSaveSuccess = function (response) {
                _super.prototype.onSaveSuccess.call(this, response);
                var currenturl = window.location.href;
                if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                    var user = Serene2.Authorization.userDefinition;
                    if (user.isAthlete == true) {
                        location.href = "/";
                        // location.reload();
                    }
                }
            };
            WellBeingDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                var user = Serene2.Authorization.userDefinition;
                var currenturl = window.location.href;
                if (currenturl.indexOf("Assessment/WellBeing") > 0) {
                    if (user.isAthlete == true) {
                        this.form.dateof.element.toggleClass('disabled', true);
                        this.form.dateof.element.toggleClass('readOnly', true);
                        this.form.dateof.element.toggleClass('isDisabled', true);
                        Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);
                        //this.deleteButton.hide();
                        this.deleteButton.toggleClass('disabled', true);
                    }
                }
                if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                    if (user.isAthlete == true) {
                        if (user.AthleteAlreadyLoggedAss == "0" || user.AthleteAlreadyLoggedAss == "") {
                            this.form.dateof.element.toggleClass('disabled', true);
                            this.form.dateof.element.toggleClass('readOnly', true);
                            this.form.dateof.element.toggleClass('isDisabled', true);
                            Serenity.EditorUtils.setReadonly(this.element.find('.dateof .editor'), true);
                            //this.deleteButton.hide();
                            this.deleteButton.toggleClass('disabled', true);
                        }
                        if (user.AthleteAlreadyLoggedAss != "0" && user.AthleteAlreadyLoggedAss != "") {
                            Q.information("Today's assessment already logged. Enjoy your workout!!", function () { });
                            this.onDialogClose();
                        }
                    }
                }
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
            };
            WellBeingDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], WellBeingDialog);
            return WellBeingDialog;
        }(Serenity.EntityDialog));
        Assessment.WellBeingDialog = WellBeingDialog;
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var GridEditorBase = /** @class */ (function (_super) {
            __extends(GridEditorBase, _super);
            function GridEditorBase(container) {
                var _this = _super.call(this, container) || this;
                _this.nextId = 1;
                return _this;
            }
            GridEditorBase.prototype.getIdProperty = function () { return "__id"; };
            GridEditorBase.prototype.id = function (entity) {
                return entity[this.getIdProperty()];
            };
            GridEditorBase.prototype.save = function (opt, callback) {
                var _this = this;
                var request = opt.request;
                var row = Q.deepClone(request.Entity);
                var id = this.id(row);
                if (id == null) {
                    row[this.getIdProperty()] = "`" + this.nextId++;
                }
                if (!this.validateEntity(row, id)) {
                    return;
                }
                var items = this.view.getItems().slice();
                if (id == null) {
                    items.push(row);
                }
                else {
                    var index = Q.indexOf(items, function (x) { return _this.id(x) === id; });
                    items[index] = Q.deepClone({}, items[index], row);
                }
                this.setEntities(items);
                callback({});
            };
            GridEditorBase.prototype.deleteEntity = function (id) {
                this.view.deleteItem(id);
                return true;
            };
            GridEditorBase.prototype.validateEntity = function (row, id) {
                return true;
            };
            GridEditorBase.prototype.setEntities = function (items) {
                this.view.setItems(items, true);
            };
            GridEditorBase.prototype.getNewEntity = function () {
                return {};
            };
            GridEditorBase.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: this.getAddButtonCaption(),
                        cssClass: 'add-button',
                        onClick: function () {
                            _this.createEntityDialog(_this.getItemType(), function (dlg) {
                                var dialog = dlg;
                                dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                                dialog.loadEntityAndOpenDialog(_this.getNewEntity());
                            });
                        }
                    }];
            };
            GridEditorBase.prototype.editItem = function (entityOrId) {
                var _this = this;
                var id = entityOrId;
                var item = this.view.getItemById(id);
                this.createEntityDialog(this.getItemType(), function (dlg) {
                    var dialog = dlg;
                    dialog.onDelete = function (opt, callback) {
                        if (!_this.deleteEntity(id)) {
                            return;
                        }
                        callback({});
                    };
                    dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                    dialog.loadEntityAndOpenDialog(item);
                });
                ;
            };
            GridEditorBase.prototype.getEditValue = function (property, target) {
                target[property.name] = this.value;
            };
            GridEditorBase.prototype.setEditValue = function (source, property) {
                this.value = source[property.name];
            };
            Object.defineProperty(GridEditorBase.prototype, "value", {
                get: function () {
                    var p = this.getIdProperty();
                    return this.view.getItems().map(function (x) {
                        var y = Q.deepClone(x);
                        var id = y[p];
                        if (id && id.toString().charAt(0) == '`')
                            delete y[p];
                        return y;
                    });
                },
                set: function (value) {
                    var _this = this;
                    var p = this.getIdProperty();
                    this.view.setItems((value || []).map(function (x) {
                        var y = Q.deepClone(x);
                        if (y[p] == null)
                            y[p] = "`" + _this.nextId++;
                        return y;
                    }), true);
                },
                enumerable: true,
                configurable: true
            });
            GridEditorBase.prototype.getGridCanLoad = function () {
                return false;
            };
            GridEditorBase.prototype.usePager = function () {
                return false;
            };
            GridEditorBase.prototype.getInitialTitle = function () {
                return null;
            };
            GridEditorBase.prototype.createQuickSearchInput = function () {
            };
            GridEditorBase = __decorate([
                Serenity.Decorators.registerClass([Serenity.IGetEditValue, Serenity.ISetEditValue]),
                Serenity.Decorators.editor(),
                Serenity.Decorators.element("<div/>")
            ], GridEditorBase);
            return GridEditorBase;
        }(Serenity.EntityGrid));
        Common.GridEditorBase = GridEditorBase;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingEditor = /** @class */ (function (_super) {
            __extends(WellBeingEditor, _super);
            function WellBeingEditor(container) {
                return _super.call(this, container) || this;
            }
            WellBeingEditor.prototype.getColumnsKey = function () { return 'Assessment.WellBeing'; };
            WellBeingEditor.prototype.getDialogType = function () { return Assessment.WellBeingEditorDialog; };
            WellBeingEditor.prototype.getLocalTextPrefix = function () { return Assessment.WellBeingRow.localTextPrefix; };
            WellBeingEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], WellBeingEditor);
            return WellBeingEditor;
        }(Serene2.Common.GridEditorBase));
        Assessment.WellBeingEditor = WellBeingEditor;
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var GridEditorDialog = /** @class */ (function (_super) {
            __extends(GridEditorDialog, _super);
            function GridEditorDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GridEditorDialog.prototype.getIdProperty = function () { return "__id"; };
            GridEditorDialog.prototype.destroy = function () {
                this.onSave = null;
                this.onDelete = null;
                _super.prototype.destroy.call(this);
            };
            GridEditorDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // apply changes button doesn't work properly with in-memory grids yet
                if (this.applyChangesButton) {
                    this.applyChangesButton.hide();
                }
            };
            GridEditorDialog.prototype.saveHandler = function (options, callback) {
                this.onSave && this.onSave(options, callback);
            };
            GridEditorDialog.prototype.deleteHandler = function (options, callback) {
                this.onDelete && this.onDelete(options, callback);
            };
            GridEditorDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], GridEditorDialog);
            return GridEditorDialog;
        }(Serenity.EntityDialog));
        Common.GridEditorDialog = GridEditorDialog;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingEditorDialog = /** @class */ (function (_super) {
            __extends(WellBeingEditorDialog, _super);
            function WellBeingEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Assessment.WellBeingForm(_this.idPrefix);
                return _this;
            }
            WellBeingEditorDialog.prototype.getFormKey = function () { return Assessment.WellBeingForm.formKey; };
            WellBeingEditorDialog.prototype.getLocalTextPrefix = function () { return Assessment.WellBeingRow.localTextPrefix; };
            WellBeingEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], WellBeingEditorDialog);
            return WellBeingEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Assessment.WellBeingEditorDialog = WellBeingEditorDialog;
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var TestFormatter = /** @class */ (function () {
        function TestFormatter() {
        }
        TestFormatter.prototype.format = function (ctx) {
            if ((ctx.value == null || String(ctx.value).length == 0)) {
                return ctx.value;
            }
            var testNumber = ctx.value;
            if (testNumber == "Pass") {
                return "<div style='height:100%; background-color:green;text-align:center;color: white;'>" + testNumber + '</div>';
            }
            if (testNumber == "Attention") {
                return "<div style='height:100%; background-color:red;text-align:center;color: white;'>" + testNumber + '</div>';
            }
            //return "<div style='height:100%;  background-color:white'>" + testNumber + '</div>';
        };
        TestFormatter = __decorate([
            Serenity.Decorators.registerFormatter()
        ], TestFormatter);
        return TestFormatter;
    }());
    Serene2.TestFormatter = TestFormatter;
    var AnswerFormatter = /** @class */ (function () {
        function AnswerFormatter() {
        }
        AnswerFormatter.prototype.format = function (ctx) {
            if ((ctx.value == null || String(ctx.value).length == 0)) {
                return ctx.value;
            }
            var testNumber = ctx.value;
            if (testNumber == "Fit to Train and Play") {
                return "<div style='height:100%; background-color:green;'>" + testNumber + '</div>';
            }
            if (testNumber == "Restricted Training and Playing") {
                return "<div style='height:100%; background-color:orange;'>" + testNumber + '</div>';
            }
            if (testNumber == "No Training or Playing") {
                return "<div style='height:100%; background-color:red;'>" + testNumber + '</div>';
            }
            if (testNumber == "Yes") {
                return "<div style='height:100%; background-color:red;'>" + testNumber + '</div>';
            }
            return "<div style='height:100%;  background-color:white'>" + testNumber + '</div>';
        };
        return AnswerFormatter;
    }());
    Serene2.AnswerFormatter = AnswerFormatter;
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingGrid = /** @class */ (function (_super) {
            __extends(WellBeingGrid, _super);
            function WellBeingGrid(container) {
                return _super.call(this, container) || this;
            }
            WellBeingGrid.prototype.getColumnsKey = function () { return 'Assessment.WellBeing'; };
            WellBeingGrid.prototype.getDialogType = function () { return Assessment.WellBeingDialog; };
            WellBeingGrid.prototype.getIdProperty = function () { return Assessment.WellBeingRow.idProperty; };
            WellBeingGrid.prototype.getLocalTextPrefix = function () { return Assessment.WellBeingRow.localTextPrefix; };
            WellBeingGrid.prototype.getService = function () { return Assessment.WellBeingService.baseUrl; };
            WellBeingGrid.prototype.createQuickSearchInput = function () {
            };
            WellBeingGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Assessment/WellBeing/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                return buttons;
            };
            Object.defineProperty(WellBeingGrid.prototype, "AthleteId", {
                get: function () {
                    return this._AthleteId;
                },
                set: function (value) {
                    if (this._AthleteId !== value) {
                        this._AthleteId = value;
                        this.setEquality('AthleteId', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            WellBeingGrid.prototype.addButtonClick = function () {
                var user = Serene2.Authorization.userDefinition;
                this.editItem({
                    dateof: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                    AthleteId: Serene2.Athlete.AthleteRow.getLookup().items
                        .filter(function (x) { return x.Username === user.Username; })[0].Id,
                    InjStatus: 1,
                    Sleep: 2,
                    ChangeDailyRoutine: 2,
                    SoughtMedicalAttention: 2,
                    TightnessPain: 2,
                    AcademicStress: 2
                });
            };
            WellBeingGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], WellBeingGrid);
            return WellBeingGrid;
        }(Serenity.EntityGrid));
        Assessment.WellBeingGrid = WellBeingGrid;
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingGridAthlete = /** @class */ (function (_super) {
            __extends(WellBeingGridAthlete, _super);
            function WellBeingGridAthlete(container) {
                return _super.call(this, container) || this;
            }
            WellBeingGridAthlete.prototype.getColumnsKey = function () { return 'Assessment.WellBeing'; };
            WellBeingGridAthlete.prototype.getDialogType = function () { return Assessment.WellBeingDialog; };
            WellBeingGridAthlete.prototype.getIdProperty = function () { return Assessment.WellBeingRow.idProperty; };
            WellBeingGridAthlete.prototype.getLocalTextPrefix = function () { return Assessment.WellBeingRow.localTextPrefix; };
            WellBeingGridAthlete.prototype.getService = function () { return Assessment.WellBeingService.baseUrl; };
            WellBeingGridAthlete.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Assessment/WellBeing/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            Object.defineProperty(WellBeingGridAthlete.prototype, "AthleteId", {
                get: function () {
                    return this._AthleteId;
                },
                set: function (value) {
                    if (this._AthleteId !== value) {
                        this._AthleteId = value;
                        this.setEquality('AthleteId', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            WellBeingGridAthlete.prototype.addButtonClick = function () {
                var user = Serene2.Authorization.userDefinition.Username;
                this.editItem({
                    dateof: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                    AthleteId: Serene2.Athlete.AthleteRow.getLookup().items
                        .filter(function (x) { return x.Username === user; })[0].Id,
                    InjStatus: 1,
                    Sleep: 2,
                    ChangeDailyRoutine: 2,
                    SoughtMedicalAttention: 2,
                    TightnessPain: 2,
                    AcademicStress: 2
                });
            };
            WellBeingGridAthlete.prototype.getQuickFilters = function () {
                // get quick filter list from base class
                var filters = _super.prototype.getQuickFilters.call(this);
                // get a reference to order row field names
                var fld = Assessment.WellBeingRow.Fields;
                //  remove filters
                var myStringArray = filters;
                var arrayLength = myStringArray.length;
                myStringArray.splice(2, arrayLength);
                return myStringArray;
            };
            WellBeingGridAthlete.prototype.createQuickFilters = function () {
                _super.prototype.createQuickFilters.call(this);
            };
            WellBeingGridAthlete = __decorate([
                Serenity.Decorators.registerClass()
            ], WellBeingGridAthlete);
            return WellBeingGridAthlete;
        }(Serenity.EntityGrid));
        Assessment.WellBeingGridAthlete = WellBeingGridAthlete;
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteDialog = /** @class */ (function (_super) {
            __extends(AthleteDialog, _super);
            function AthleteDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Athlete.AthleteForm(_this.idPrefix);
                // var chk = this.element[0].innerHTML;
                //  this.element[0].innerHTML = this.element[0].innerHTML.replace('<span class="button-inner">Select File</span>', '');
                //  this.element[0].innerHTML = this.element[0].innerHTML.replace("User Image<", "TEST FRED<");
                _this.assessmentGrid = new Serene2.Assessment.WellBeingGrid(_this.byId('UpdateAthleteGrid'));
                _this.assessmentGrid.element.flexHeightOnly(1);
                _this.physioGrid = new Serene2.AthletePhysio.AthletePhysioGrid(_this.byId('UpdateAthletePhysio'));
                _this.physioGrid.element.flexHeightOnly(1);
                _this.audit = new Serene2.Audit.AuditLogGrid(_this.byId('ActionAuditGrid'));
                _this.audit.element.flexHeightOnly(1);
                _this.athleteWeight = new Serene2.AthleteWeight.AthleteWeightGrid(_this.byId('UpdateAthleteWeight'));
                _this.athleteWeight.element.flexHeightOnly(1);
                _this.athleteRPE = new Serene2.RPE.RpeGrid(_this.byId('AthleteRPEGrid'));
                _this.athleteRPE.element.flexHeightOnly(1);
                _this.athleteNote = new Serene2.AthleteNote.AthleteNoteGrid(_this.byId('AthleteNoteGrid'));
                _this.athleteNote.element.flexHeightOnly(1);
                _this.tabs.bind('tabsactivate', function () { return _this.arrange(); });
                _this.tabs.on('tabsactivate', function (e, i) {
                    _this.arrange();
                });
                Serenity.SubDialogHelper.triggerDataChange(_this);
                return _this;
            }
            AthleteDialog.prototype.getFormKey = function () { return Athlete.AthleteForm.formKey; };
            AthleteDialog.prototype.getIdProperty = function () { return Athlete.AthleteRow.idProperty; };
            AthleteDialog.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteDialog.prototype.getNameProperty = function () { return Athlete.AthleteRow.nameProperty; };
            AthleteDialog.prototype.getService = function () { return Athlete.AthleteService.baseUrl; };
            AthleteDialog.prototype.loadEntity = function (entity) {
                _super.prototype.loadEntity.call(this, entity);
                var athid = this.entity.Id;
                this.audit.rowid = athid;
                this.audit.TableName = "[dbo].[Athlete]";
                //Serenity.TabsExtensions.setDisabled(this.tabs, 'Updates', this.isNewOrDeleted());
                this.assessmentGrid.AthleteId = athid;
                this.physioGrid.AthleteId = athid;
                this.athleteWeight.AthleteUsername = this.entity.Username;
                this.athleteNote.AthleteId = athid;
                this.athleteRPE.AthleteUsername = this.entity.Username;
            };
            AthleteDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push(Serene2.Common.ReportHelper.createToolButton({
                    title: 'Statement',
                    cssClass: 'export-pdf-button',
                    reportKey: 'Athlete.AthleteReport',
                    //  extension: 'html',
                    getParams: function () { return ({ Id: _this.get_entityId() }); }
                }));
                return buttons;
            };
            AthleteDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                /*
                var user = Serene2.Authorization.userDefinition;
                Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), true);
    
                if (user.Username == "apope" || user.Username == "pd363" || user.Username == "nas217" || user.Username == "sh444" ) {
                    
                    Serenity.EditorUtils.setReadonly(this.element.find('.Paid .editor'), false);
    
                }
               */
            };
            AthleteDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteDialog);
            return AthleteDialog;
        }(Serenity.EntityDialog));
        Athlete.AthleteDialog = AthleteDialog;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteEditor = /** @class */ (function (_super) {
            __extends(AthleteEditor, _super);
            function AthleteEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteEditor.prototype.getColumnsKey = function () { return 'Athlete.Athlete'; };
            AthleteEditor.prototype.getDialogType = function () { return Athlete.AthleteEditorDialog; };
            AthleteEditor.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteEditor);
            return AthleteEditor;
        }(Serene2.Common.GridEditorBase));
        Athlete.AthleteEditor = AthleteEditor;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteEditorDialog, _super);
            function AthleteEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Athlete.AthleteForm(_this.idPrefix);
                return _this;
            }
            AthleteEditorDialog.prototype.getFormKey = function () { return Athlete.AthleteForm.formKey; };
            AthleteEditorDialog.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteEditorDialog.prototype.getNameProperty = function () { return Athlete.AthleteRow.nameProperty; };
            AthleteEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteEditorDialog);
            return AthleteEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Athlete.AthleteEditorDialog = AthleteEditorDialog;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteGrid = /** @class */ (function (_super) {
            __extends(AthleteGrid, _super);
            function AthleteGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteGrid.prototype.getColumnsKey = function () { return 'Athlete.Athlete'; };
            AthleteGrid.prototype.getDialogType = function () { return Athlete.AthleteDialog; };
            AthleteGrid.prototype.getIdProperty = function () { return Athlete.AthleteRow.idProperty; };
            AthleteGrid.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteGrid.prototype.getService = function () { return Athlete.AthleteService.baseUrl; };
            AthleteGrid.prototype.createQuickFilters = function () {
                _super.prototype.createQuickFilters.call(this);
                var fld = Athlete.AthleteRow.Fields;
                this.lastTwoAssessmentIdFilter = this.findQuickFilter(Serenity.EnumEditor, fld.lastTwoAssessmentId);
                this.AthleteLoggedAssessmentTodayFilter = this.findQuickFilter(Serenity.EnumEditor, fld.AthleteLoggedAssessmentToday);
            };
            AthleteGrid.prototype.getQuickFilters = function () {
                // get quick filter list from base class
                var filters = _super.prototype.getQuickFilters.call(this);
                var fld = Athlete.AthleteRow.Fields;
                Q.first(filters, function (x) { return x.field == fld.Live; }).init = function (w) {
                    // enum editor has a string value, so need to call toString()
                    w.value = "1";
                };
                var filter = Q.first(filters, function (x) { return x.field == fld.lastassessment; });
                filter.title = "Assessment Taken";
                filter.handler = function (h) {
                    if (h.active) {
                        h.request.Criteria = Serenity.Criteria.and(h.request.Criteria, [[fld.lastassessment], 'like', h.value.substring(12, 6) + "/" + h.value.substring(3, 5) + "/" + h.value.substring(0, 2) + '%']);
                    }
                };
                return filters;
            };
            AthleteGrid.prototype.set_lastTwoAssessmentId = function (value) {
                this.lastTwoAssessmentIdFilter.value = value == null ? '' : value.toString();
            };
            AthleteGrid.prototype.set_AthleteLoggedAssessmentToday = function (value) {
                this.AthleteLoggedAssessmentTodayFilter.value = value == null ? '' : value.toString();
            };
            AthleteGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                var user = Serene2.Authorization.userDefinition;
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Athlete/Athlete/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            AthleteGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                var user = Serene2.Authorization.userDefinition;
                var fld = Athlete.AthleteRow.Fields;
                columns.unshift({
                    field: 'View',
                    name: '',
                    format: function (ctx) { return '<a class="inline-action view-details" title="View Details">' +
                        '<i class="fa fa-edit text-red"></i></a> '; },
                    width: 24,
                    minWidth: 24,
                    maxWidth: 24
                });
                if (user.isAthlete == true || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                    columns.splice(1, 0, {
                        field: 'New Assessment',
                        name: '',
                        format: function (ctx) { return '<a class="inline-action new-assessment" title="New Daily Monitoring">' +
                            '<i class="fa fa-plus text-orange"></i></a> '; },
                        width: 24,
                        minWidth: 24,
                        maxWidth: 24
                    });
                }
                if (user.Roles.indexOf("Physio") >= 0 || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                    columns.splice(1, 0, {
                        field: 'New Physio',
                        name: '',
                        format: function (ctx) { return '<a class="inline-action new-physio" title="New Physio">' +
                            '<i class="fa fa-road text-blue"></i></a> '; },
                        width: 24,
                        minWidth: 24,
                        maxWidth: 24
                    });
                }
                return columns;
            };
            AthleteGrid.prototype.onClick = function (e, row, cell) {
                _super.prototype.onClick.call(this, e, row, cell);
                if (e.isDefaultPrevented())
                    return;
                var item = this.itemAt(row);
                var userid = item.Id;
                var Fname = item.FirstName;
                var Lname = item.LastName;
                var target = $(e.target);
                // if user clicks "i" element, e.g. icon
                if (target.parent().hasClass('inline-action'))
                    target = target.parent();
                if (target.hasClass('inline-action')) {
                    e.preventDefault();
                    if (target.hasClass('view-details')) {
                        this.editItem(item.Id);
                    }
                    if (target.hasClass('new-physio')) {
                        var dlg = new Serene2.AthletePhysio.AthletePhysioDialog();
                        this.initDialog(dlg);
                        dlg.loadEntityAndOpenDialog({
                            AthleteId: userid,
                            AthleteFirstName: Fname,
                            AthleteLastName: Lname,
                            EntryDate: Q.formatDate(new Date(), 'yyyy-MM-dd')
                        });
                    }
                    if (target.hasClass('new-assessment')) {
                        var dlg2 = new Serene2.Assessment.WellBeingDialog();
                        this.initDialog(dlg2);
                        dlg2.loadEntityAndOpenDialog({
                            AthleteId: userid,
                            Sleep: 2,
                            ChangeDailyRoutine: 2,
                            SoughtMedicalAttention: 2,
                            TightnessPain: 2,
                            AcademicStress: 2,
                            dateof: Q.formatDate(new Date(), 'yyyy-MM-dd')
                        });
                    }
                }
            };
            AthleteGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteGrid);
            return AthleteGrid;
        }(Serenity.EntityGrid));
        Athlete.AthleteGrid = AthleteGrid;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteGridAthlete = /** @class */ (function (_super) {
            __extends(AthleteGridAthlete, _super);
            function AthleteGridAthlete(container) {
                return _super.call(this, container) || this;
            }
            AthleteGridAthlete.prototype.getColumnsKey = function () { return 'Athlete.Athlete'; };
            AthleteGridAthlete.prototype.getDialogType = function () { return Athlete.AthleteDialog; };
            AthleteGridAthlete.prototype.getIdProperty = function () { return Athlete.AthleteRow.idProperty; };
            AthleteGridAthlete.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteGridAthlete.prototype.getService = function () { return Athlete.AthleteService.baseUrl; };
            /**
             * This method is called to get list of buttons to be created.
             */
            AthleteGridAthlete.prototype.getButtons = function () {
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                return buttons;
            };
            AthleteGridAthlete = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteGridAthlete);
            return AthleteGridAthlete;
        }(Serenity.EntityGrid));
        Athlete.AthleteGridAthlete = AthleteGridAthlete;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteImagesGrid = /** @class */ (function (_super) {
            __extends(AthleteImagesGrid, _super);
            function AthleteImagesGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteImagesGrid.prototype.getColumnsKey = function () { return 'Athlete.AthleteImages'; };
            AthleteImagesGrid.prototype.getDialogType = function () { return Athlete.AthleteDialog; };
            AthleteImagesGrid.prototype.getIdProperty = function () { return Athlete.AthleteRow.idProperty; };
            AthleteImagesGrid.prototype.getLocalTextPrefix = function () { return Athlete.AthleteRow.localTextPrefix; };
            AthleteImagesGrid.prototype.getService = function () { return Athlete.AthleteService.baseUrl; };
            AthleteImagesGrid.prototype.createQuickFilters = function () {
                _super.prototype.createQuickFilters.call(this);
                var fld = Athlete.AthleteRow.Fields;
                this.lastTwoAssessmentIdFilter = this.findQuickFilter(Serenity.EnumEditor, fld.lastTwoAssessmentId);
                this.AthleteLoggedAssessmentTodayFilter = this.findQuickFilter(Serenity.EnumEditor, fld.AthleteLoggedAssessmentToday);
            };
            AthleteImagesGrid.prototype.set_lastTwoAssessmentId = function (value) {
                this.lastTwoAssessmentIdFilter.value = value == null ? '' : value.toString();
            };
            AthleteImagesGrid.prototype.set_AthleteLoggedAssessmentToday = function (value) {
                this.AthleteLoggedAssessmentTodayFilter.value = value == null ? '' : value.toString();
            };
            AthleteImagesGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.rowHeight = 150;
                return opt;
            };
            AthleteImagesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Athlete/Athlete/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            AthleteImagesGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                var user = Serene2.Authorization.userDefinition;
                columns.unshift({
                    field: 'View',
                    name: '',
                    format: function (ctx) { return '<a class="inline-action view-details" title="View Details">' +
                        '<i class="fa fa-edit text-red"></i></a> '; },
                    width: 24,
                    minWidth: 24,
                    maxWidth: 24
                });
                if (user.isAthlete == true || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                    columns.splice(1, 0, {
                        field: 'New Assessment',
                        name: '',
                        format: function (ctx) { return '<a class="inline-action new-assessment" title="New Daily Monitoring">' +
                            '<i class="fa fa-plus text-orange"></i></a> '; },
                        width: 24,
                        minWidth: 24,
                        maxWidth: 24
                    });
                }
                if (user.Roles.indexOf("Physio") >= 0 || user.IsAdmin == true || user.Roles.indexOf("Admin") >= 0) {
                    columns.splice(1, 0, {
                        field: 'New Physio',
                        name: '',
                        format: function (ctx) { return '<a class="inline-action new-physio" title="New Physio">' +
                            '<i class="fa fa-road text-blue"></i></a> '; },
                        width: 24,
                        minWidth: 24,
                        maxWidth: 24
                    });
                }
                return columns;
            };
            AthleteImagesGrid.prototype.onClick = function (e, row, cell) {
                _super.prototype.onClick.call(this, e, row, cell);
                if (e.isDefaultPrevented())
                    return;
                var item = this.itemAt(row);
                var userid = item.Id;
                var Fname = item.FirstName;
                var Lname = item.LastName;
                var target = $(e.target);
                // if user clicks "i" element, e.g. icon
                if (target.parent().hasClass('inline-action'))
                    target = target.parent();
                if (target.hasClass('inline-action')) {
                    e.preventDefault();
                    if (target.hasClass('view-details')) {
                        this.editItem(item.Id);
                    }
                    if (target.hasClass('new-physio')) {
                        var dlg = new Serene2.AthletePhysio.AthletePhysioDialog();
                        this.initDialog(dlg);
                        dlg.loadEntityAndOpenDialog({
                            AthleteId: userid,
                            AthleteFirstName: Fname,
                            AthleteLastName: Lname,
                            EntryDate: Q.formatDate(new Date(), 'yyyy-MM-dd')
                        });
                    }
                    if (target.hasClass('new-assessment')) {
                        var dlg2 = new Serene2.Assessment.WellBeingDialog();
                        this.initDialog(dlg2);
                        dlg2.loadEntityAndOpenDialog({
                            AthleteId: userid,
                            Sleep: 2,
                            ChangeDailyRoutine: 2,
                            SoughtMedicalAttention: 2,
                            TightnessPain: 2,
                            AcademicStress: 2,
                            dateof: Q.formatDate(new Date(), 'yyyy-MM-dd')
                        });
                    }
                }
            };
            AthleteImagesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteImagesGrid);
            return AthleteImagesGrid;
        }(Serenity.EntityGrid));
        Athlete.AthleteImagesGrid = AthleteImagesGrid;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var InlineImageFormatter = /** @class */ (function () {
            function InlineImageFormatter() {
            }
            InlineImageFormatter.prototype.format = function (ctx) {
                var file = (this.fileProperty ? ctx.item[this.fileProperty] : ctx.value);
                if (!file || !file.length)
                    return "";
                //var unc = "file:///\\\\isad.isadroot.ex.ac.uk\\uoe\\PS\\AS\\SS\\Card";
                //unc = unc + " Office Database\\IDCard\\IDPro7Data\\PR_STUDENT\\Images\\";
                //var unc = "file:///S:\\";
                var unc = "Photo/";
                //let href = Q.resolveUrl(unc + file);
                var href = "";
                if (file.substring(0, 9) == "UserImage") {
                    href = Q.resolveUrl('~/upload/') + file;
                }
                else {
                    href = unc + file;
                }
                var src = Q.resolveUrl('~/upload/' + file);
                //return `<a class="inline-image" target='_blank' href="${href}">` + `<img src="${href}" style='max-height: 145px; max-width: 100%;' /></a>`;
                return "<img src=\"" + href + "\" style='max-height: 145px; max-width: 100%;' />";
            };
            InlineImageFormatter.prototype.initializeColumn = function (column) {
                if (this.fileProperty) {
                    column.referencedFields = column.referencedFields || [];
                    column.referencedFields.push(this.fileProperty);
                }
            };
            __decorate([
                Serenity.Decorators.option()
            ], InlineImageFormatter.prototype, "fileProperty", void 0);
            __decorate([
                Serenity.Decorators.option()
            ], InlineImageFormatter.prototype, "thumb", void 0);
            InlineImageFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], InlineImageFormatter);
            return InlineImageFormatter;
        }());
        Athlete.InlineImageFormatter = InlineImageFormatter;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var TrainerListFormatter = /** @class */ (function () {
            function TrainerListFormatter() {
            }
            TrainerListFormatter.prototype.format = function (ctx) {
                var idList = ctx.value;
                if (!idList || !idList.length)
                    return "";
                var byId = Serene2.Setup.TrainersRow.getLookup().itemById;
                var z;
                return idList.map(function (x) { return ((z = byId[x]) ? z.Trainer : x); }).join(", ");
            };
            TrainerListFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], TrainerListFormatter);
            return TrainerListFormatter;
        }());
        Athlete.TrainerListFormatter = TrainerListFormatter;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayDialog = /** @class */ (function (_super) {
            __extends(AthleteLoggedTodayDialog, _super);
            function AthleteLoggedTodayDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteLoggedToday.AthleteLoggedTodayForm(_this.idPrefix);
                return _this;
            }
            AthleteLoggedTodayDialog.prototype.getFormKey = function () { return AthleteLoggedToday.AthleteLoggedTodayForm.formKey; };
            AthleteLoggedTodayDialog.prototype.getIdProperty = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.idProperty; };
            AthleteLoggedTodayDialog.prototype.getLocalTextPrefix = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.localTextPrefix; };
            AthleteLoggedTodayDialog.prototype.getNameProperty = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.nameProperty; };
            AthleteLoggedTodayDialog.prototype.getService = function () { return AthleteLoggedToday.AthleteLoggedTodayService.baseUrl; };
            AthleteLoggedTodayDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteLoggedTodayDialog);
            return AthleteLoggedTodayDialog;
        }(Serenity.EntityDialog));
        AthleteLoggedToday.AthleteLoggedTodayDialog = AthleteLoggedTodayDialog;
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayEditor = /** @class */ (function (_super) {
            __extends(AthleteLoggedTodayEditor, _super);
            function AthleteLoggedTodayEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteLoggedTodayEditor.prototype.getColumnsKey = function () { return 'AthleteLoggedToday.AthleteLoggedToday'; };
            AthleteLoggedTodayEditor.prototype.getDialogType = function () { return AthleteLoggedToday.AthleteLoggedTodayEditorDialog; };
            AthleteLoggedTodayEditor.prototype.getLocalTextPrefix = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.localTextPrefix; };
            AthleteLoggedTodayEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteLoggedTodayEditor);
            return AthleteLoggedTodayEditor;
        }(Serene2.Common.GridEditorBase));
        AthleteLoggedToday.AthleteLoggedTodayEditor = AthleteLoggedTodayEditor;
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteLoggedTodayEditorDialog, _super);
            function AthleteLoggedTodayEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteLoggedToday.AthleteLoggedTodayForm(_this.idPrefix);
                return _this;
            }
            AthleteLoggedTodayEditorDialog.prototype.getFormKey = function () { return AthleteLoggedToday.AthleteLoggedTodayForm.formKey; };
            AthleteLoggedTodayEditorDialog.prototype.getLocalTextPrefix = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.localTextPrefix; };
            AthleteLoggedTodayEditorDialog.prototype.getNameProperty = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.nameProperty; };
            AthleteLoggedTodayEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteLoggedTodayEditorDialog);
            return AthleteLoggedTodayEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        AthleteLoggedToday.AthleteLoggedTodayEditorDialog = AthleteLoggedTodayEditorDialog;
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayGrid = /** @class */ (function (_super) {
            __extends(AthleteLoggedTodayGrid, _super);
            function AthleteLoggedTodayGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteLoggedTodayGrid.prototype.getColumnsKey = function () { return 'AthleteLoggedToday.AthleteLoggedToday'; };
            AthleteLoggedTodayGrid.prototype.getDialogType = function () { return AthleteLoggedToday.AthleteLoggedTodayDialog; };
            AthleteLoggedTodayGrid.prototype.getIdProperty = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.idProperty; };
            AthleteLoggedTodayGrid.prototype.getLocalTextPrefix = function () { return AthleteLoggedToday.AthleteLoggedTodayRow.localTextPrefix; };
            AthleteLoggedTodayGrid.prototype.getService = function () { return AthleteLoggedToday.AthleteLoggedTodayService.baseUrl; };
            AthleteLoggedTodayGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteLoggedTodayGrid);
            return AthleteLoggedTodayGrid;
        }(Serenity.EntityGrid));
        AthleteLoggedToday.AthleteLoggedTodayGrid = AthleteLoggedTodayGrid;
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteDialog = /** @class */ (function (_super) {
            __extends(AthleteNoteDialog, _super);
            function AthleteNoteDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteNote.AthleteNoteForm(_this.idPrefix);
                return _this;
            }
            AthleteNoteDialog.prototype.getFormKey = function () { return AthleteNote.AthleteNoteForm.formKey; };
            AthleteNoteDialog.prototype.getIdProperty = function () { return AthleteNote.AthleteNoteRow.idProperty; };
            AthleteNoteDialog.prototype.getLocalTextPrefix = function () { return AthleteNote.AthleteNoteRow.localTextPrefix; };
            AthleteNoteDialog.prototype.getNameProperty = function () { return AthleteNote.AthleteNoteRow.nameProperty; };
            AthleteNoteDialog.prototype.getService = function () { return AthleteNote.AthleteNoteService.baseUrl; };
            AthleteNoteDialog.prototype.loadEntity = function (entity) {
                _super.prototype.loadEntity.call(this, entity);
                this.form.EntryDate.element.toggleClass('disabled', true);
                this.form.EntryDate.element.toggleClass('readOnly', true);
                this.form.EntryDate.element.toggleClass('isDisabled', true);
                this.form.Username.element.toggleClass('disabled', true);
                this.form.Username.element.toggleClass('readOnly', true);
                this.form.Username.element.toggleClass('isDisabled', true);
            };
            AthleteNoteDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteNoteDialog);
            return AthleteNoteDialog;
        }(Serenity.EntityDialog));
        AthleteNote.AthleteNoteDialog = AthleteNoteDialog;
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteEditor = /** @class */ (function (_super) {
            __extends(AthleteNoteEditor, _super);
            function AthleteNoteEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteNoteEditor.prototype.getColumnsKey = function () { return 'AthleteNote.AthleteNote'; };
            AthleteNoteEditor.prototype.getDialogType = function () { return AthleteNote.AthleteNoteEditorDialog; };
            AthleteNoteEditor.prototype.getLocalTextPrefix = function () { return AthleteNote.AthleteNoteRow.localTextPrefix; };
            AthleteNoteEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteNoteEditor);
            return AthleteNoteEditor;
        }(Serene2.Common.GridEditorBase));
        AthleteNote.AthleteNoteEditor = AthleteNoteEditor;
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteNoteEditorDialog, _super);
            function AthleteNoteEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteNote.AthleteNoteForm(_this.idPrefix);
                return _this;
            }
            AthleteNoteEditorDialog.prototype.getFormKey = function () { return AthleteNote.AthleteNoteForm.formKey; };
            AthleteNoteEditorDialog.prototype.getLocalTextPrefix = function () { return AthleteNote.AthleteNoteRow.localTextPrefix; };
            AthleteNoteEditorDialog.prototype.getNameProperty = function () { return AthleteNote.AthleteNoteRow.nameProperty; };
            AthleteNoteEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteNoteEditorDialog);
            return AthleteNoteEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        AthleteNote.AthleteNoteEditorDialog = AthleteNoteEditorDialog;
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteGrid = /** @class */ (function (_super) {
            __extends(AthleteNoteGrid, _super);
            function AthleteNoteGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteNoteGrid.prototype.getColumnsKey = function () { return 'AthleteNote.AthleteNote'; };
            AthleteNoteGrid.prototype.getDialogType = function () { return AthleteNote.AthleteNoteDialog; };
            AthleteNoteGrid.prototype.getIdProperty = function () { return AthleteNote.AthleteNoteRow.idProperty; };
            AthleteNoteGrid.prototype.getLocalTextPrefix = function () { return AthleteNote.AthleteNoteRow.localTextPrefix; };
            AthleteNoteGrid.prototype.getService = function () { return AthleteNote.AthleteNoteService.baseUrl; };
            Object.defineProperty(AthleteNoteGrid.prototype, "AthleteId", {
                get: function () {
                    return this._AthleteId;
                },
                set: function (value) {
                    if (this._AthleteId !== value) {
                        this._AthleteId = value;
                        var user = Serene2.Authorization.userDefinition;
                        if (user.Roles.indexOf("Sports Coach") > 0 || user.Roles.indexOf("S&C Coach") > 0) { //|| user.Roles.indexOf("Admin") >= 0) {
                            this.setEquality('AthleteId', value);
                            this.refresh();
                        }
                        else {
                            this.setEquality('AthleteId', 0);
                            this.refresh();
                        }
                    }
                },
                enumerable: true,
                configurable: true
            });
            AthleteNoteGrid.prototype.getButtons = function () {
                var buttons = _super.prototype.getButtons.call(this);
                var user = Serene2.Authorization.userDefinition;
                if (user.Roles.indexOf("Sports Coach") > 0 || user.Roles.indexOf("S&C Coach") > 0) { //|| user.Roles.indexOf("Admin") >= 0) {
                    //this.refresh();
                }
                else {
                    buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                    buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "column-picker-button"; }), 1);
                }
                return buttons;
            };
            AthleteNoteGrid.prototype.addButtonClick = function () {
                var user = Serene2.Authorization.userDefinition;
                this.editItem({
                    EntryDate: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                    AthleteId: this.AthleteId,
                    Username: user.Username
                });
            };
            AthleteNoteGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteNoteGrid);
            return AthleteNoteGrid;
        }(Serenity.EntityGrid));
        AthleteNote.AthleteNoteGrid = AthleteNoteGrid;
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioDialog = /** @class */ (function (_super) {
            __extends(AthletePhysioDialog, _super);
            function AthletePhysioDialog() {
                var _this = _super.call(this) || this;
                _this.form = new AthletePhysio.AthletePhysioForm(_this.idPrefix);
                _this.audit = new Serene2.Audit.AuditLogGrid(_this.byId('ActionAuditGrid'));
                _this.audit.element.flexHeightOnly(1);
                _this.tabs.bind('tabsactivate', function () { return _this.arrange(); });
                _this.tabs.on('tabsactivate', function (e, i) {
                    _this.arrange();
                });
                return _this;
            }
            AthletePhysioDialog.prototype.getFormKey = function () { return AthletePhysio.AthletePhysioForm.formKey; };
            AthletePhysioDialog.prototype.getIdProperty = function () { return AthletePhysio.AthletePhysioRow.idProperty; };
            AthletePhysioDialog.prototype.getLocalTextPrefix = function () { return AthletePhysio.AthletePhysioRow.localTextPrefix; };
            AthletePhysioDialog.prototype.getNameProperty = function () { return AthletePhysio.AthletePhysioRow.nameProperty; };
            AthletePhysioDialog.prototype.getService = function () { return AthletePhysio.AthletePhysioService.baseUrl; };
            AthletePhysioDialog.prototype.loadEntity = function (entity) {
                _super.prototype.loadEntity.call(this, entity);
                Serenity.EditorUtils.setReadonly(this.element.find('.AthleteFirstName .editor'), true);
                Serenity.EditorUtils.setReadonly(this.element.find('.AthleteLastName .editor'), true);
                var athid = this.entity.Id;
                this.audit.rowid = athid;
                this.audit.TableName = "[dbo].[AthletePhysio]";
                var user = Serene2.Authorization.userDefinition;
                if (user.Username != "physioStaff") {
                    this.element.find('.history .editor').hide();
                    this.element.find('.previousHistory .editor').hide();
                    this.element.find('.objectiveAssessment .editor').hide();
                    this.element.find('.managementPlan .editor').hide();
                }
            };
            AthletePhysioDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthletePhysioDialog);
            return AthletePhysioDialog;
        }(Serenity.EntityDialog));
        AthletePhysio.AthletePhysioDialog = AthletePhysioDialog;
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioEditor = /** @class */ (function (_super) {
            __extends(AthletePhysioEditor, _super);
            function AthletePhysioEditor(container) {
                return _super.call(this, container) || this;
            }
            AthletePhysioEditor.prototype.getColumnsKey = function () { return 'AthletePhysio.AthletePhysio'; };
            AthletePhysioEditor.prototype.getDialogType = function () { return AthletePhysio.AthletePhysioEditorDialog; };
            AthletePhysioEditor.prototype.getLocalTextPrefix = function () { return AthletePhysio.AthletePhysioRow.localTextPrefix; };
            AthletePhysioEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthletePhysioEditor);
            return AthletePhysioEditor;
        }(Serene2.Common.GridEditorBase));
        AthletePhysio.AthletePhysioEditor = AthletePhysioEditor;
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioEditorDialog = /** @class */ (function (_super) {
            __extends(AthletePhysioEditorDialog, _super);
            function AthletePhysioEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthletePhysio.AthletePhysioForm(_this.idPrefix);
                return _this;
            }
            AthletePhysioEditorDialog.prototype.getFormKey = function () { return AthletePhysio.AthletePhysioForm.formKey; };
            AthletePhysioEditorDialog.prototype.getLocalTextPrefix = function () { return AthletePhysio.AthletePhysioRow.localTextPrefix; };
            AthletePhysioEditorDialog.prototype.getNameProperty = function () { return AthletePhysio.AthletePhysioRow.nameProperty; };
            AthletePhysioEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthletePhysioEditorDialog);
            return AthletePhysioEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        AthletePhysio.AthletePhysioEditorDialog = AthletePhysioEditorDialog;
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioGrid = /** @class */ (function (_super) {
            __extends(AthletePhysioGrid, _super);
            function AthletePhysioGrid(container) {
                return _super.call(this, container) || this;
            }
            AthletePhysioGrid.prototype.getColumnsKey = function () { return 'AthletePhysio.AthletePhysio'; };
            AthletePhysioGrid.prototype.getDialogType = function () { return AthletePhysio.AthletePhysioDialog; };
            AthletePhysioGrid.prototype.getIdProperty = function () { return AthletePhysio.AthletePhysioRow.idProperty; };
            AthletePhysioGrid.prototype.getLocalTextPrefix = function () { return AthletePhysio.AthletePhysioRow.localTextPrefix; };
            AthletePhysioGrid.prototype.getService = function () { return AthletePhysio.AthletePhysioService.baseUrl; };
            AthletePhysioGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'AthletePhysio/AthletePhysio/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                return buttons;
            };
            Object.defineProperty(AthletePhysioGrid.prototype, "AthleteId", {
                get: function () {
                    return this._AthleteId;
                },
                set: function (value) {
                    if (this._AthleteId !== value) {
                        this._AthleteId = value;
                        this.setEquality('AthleteId', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            AthletePhysioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthletePhysioGrid);
            return AthletePhysioGrid;
        }(Serenity.EntityGrid));
        AthletePhysio.AthletePhysioGrid = AthletePhysioGrid;
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightDialog = /** @class */ (function (_super) {
            __extends(AthleteWeightDialog, _super);
            function AthleteWeightDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteWeight.AthleteWeightForm(_this.idPrefix);
                return _this;
            }
            AthleteWeightDialog.prototype.getFormKey = function () { return AthleteWeight.AthleteWeightForm.formKey; };
            //protected getIdProperty() { return AthleteWeightRow.idProperty; }
            AthleteWeightDialog.prototype.getIdProperty = function () { return "__id"; };
            AthleteWeightDialog.prototype.getLocalTextPrefix = function () { return AthleteWeight.AthleteWeightRow.localTextPrefix; };
            AthleteWeightDialog.prototype.getNameProperty = function () { return AthleteWeight.AthleteWeightRow.nameProperty; };
            AthleteWeightDialog.prototype.getService = function () { return AthleteWeight.AthleteWeightService.baseUrl; };
            AthleteWeightDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteWeightDialog);
            return AthleteWeightDialog;
        }(Serenity.EntityDialog));
        AthleteWeight.AthleteWeightDialog = AthleteWeightDialog;
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightEditor = /** @class */ (function (_super) {
            __extends(AthleteWeightEditor, _super);
            function AthleteWeightEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteWeightEditor.prototype.getColumnsKey = function () { return 'AthleteWeight.AthleteWeight'; };
            AthleteWeightEditor.prototype.getDialogType = function () { return AthleteWeight.AthleteWeightEditorDialog; };
            AthleteWeightEditor.prototype.getLocalTextPrefix = function () { return AthleteWeight.AthleteWeightRow.localTextPrefix; };
            AthleteWeightEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteWeightEditor);
            return AthleteWeightEditor;
        }(Serene2.Common.GridEditorBase));
        AthleteWeight.AthleteWeightEditor = AthleteWeightEditor;
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteWeightEditorDialog, _super);
            function AthleteWeightEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new AthleteWeight.AthleteWeightForm(_this.idPrefix);
                return _this;
            }
            AthleteWeightEditorDialog.prototype.getFormKey = function () { return AthleteWeight.AthleteWeightForm.formKey; };
            AthleteWeightEditorDialog.prototype.getLocalTextPrefix = function () { return AthleteWeight.AthleteWeightRow.localTextPrefix; };
            AthleteWeightEditorDialog.prototype.getNameProperty = function () { return AthleteWeight.AthleteWeightRow.nameProperty; };
            AthleteWeightEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteWeightEditorDialog);
            return AthleteWeightEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        AthleteWeight.AthleteWeightEditorDialog = AthleteWeightEditorDialog;
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightGrid = /** @class */ (function (_super) {
            __extends(AthleteWeightGrid, _super);
            function AthleteWeightGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteWeightGrid.prototype.getColumnsKey = function () { return 'AthleteWeight.AthleteWeight'; };
            AthleteWeightGrid.prototype.getDialogType = function () { return AthleteWeight.AthleteWeightDialog; };
            AthleteWeightGrid.prototype.getIdProperty = function () { return AthleteWeight.AthleteWeightRow.idProperty; };
            AthleteWeightGrid.prototype.getLocalTextPrefix = function () { return AthleteWeight.AthleteWeightRow.localTextPrefix; };
            AthleteWeightGrid.prototype.getService = function () { return AthleteWeight.AthleteWeightService.baseUrl; };
            AthleteWeightGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(Serene2.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'AthleteWeight/AthleteWeight/ListExcel',
                    separator: true
                }));
                buttons.push(Serene2.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "column-picker-button"; }), 1);
                return buttons;
            };
            //protected createToolbar(): void {
            //}
            AthleteWeightGrid.prototype.createQuickSearchInput = function () {
            };
            Object.defineProperty(AthleteWeightGrid.prototype, "AthleteUsername", {
                get: function () {
                    return this._AthleteUsername;
                },
                set: function (value) {
                    if (this._AthleteUsername !== value) {
                        this._AthleteUsername = value;
                        this.setEquality('AthleteUsername', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            AthleteWeightGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteWeightGrid);
            return AthleteWeightGrid;
        }(Serenity.EntityGrid));
        AthleteWeight.AthleteWeightGrid = AthleteWeightGrid;
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogDialog = /** @class */ (function (_super) {
            __extends(AuditLogDialog, _super);
            function AuditLogDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Audit.AuditLogForm(_this.idPrefix);
                return _this;
            }
            AuditLogDialog.prototype.getFormKey = function () { return Audit.AuditLogForm.formKey; };
            AuditLogDialog.prototype.getIdProperty = function () { return Audit.AuditLogRow.idProperty; };
            AuditLogDialog.prototype.getLocalTextPrefix = function () { return Audit.AuditLogRow.localTextPrefix; };
            AuditLogDialog.prototype.getNameProperty = function () { return Audit.AuditLogRow.nameProperty; };
            AuditLogDialog.prototype.getService = function () { return Audit.AuditLogService.baseUrl; };
            AuditLogDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "delete-button"; }), 1);
                return buttons;
            };
            AuditLogDialog.prototype.loadEntity = function (entity) {
                _super.prototype.loadEntity.call(this, entity);
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
            };
            AuditLogDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton('apply-changes-button').hide();
                this.toolbar.findButton('save-and-close-button').hide();
            };
            AuditLogDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AuditLogDialog);
            return AuditLogDialog;
        }(Serenity.EntityDialog));
        Audit.AuditLogDialog = AuditLogDialog;
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogEditor = /** @class */ (function (_super) {
            __extends(AuditLogEditor, _super);
            function AuditLogEditor(container) {
                return _super.call(this, container) || this;
            }
            AuditLogEditor.prototype.getColumnsKey = function () { return 'Audit.AuditLog'; };
            AuditLogEditor.prototype.getDialogType = function () { return Audit.AuditLogEditorDialog; };
            AuditLogEditor.prototype.getLocalTextPrefix = function () { return Audit.AuditLogRow.localTextPrefix; };
            AuditLogEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AuditLogEditor);
            return AuditLogEditor;
        }(Serene2.Common.GridEditorBase));
        Audit.AuditLogEditor = AuditLogEditor;
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogEditorDialog = /** @class */ (function (_super) {
            __extends(AuditLogEditorDialog, _super);
            function AuditLogEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Audit.AuditLogForm(_this.idPrefix);
                return _this;
            }
            AuditLogEditorDialog.prototype.getFormKey = function () { return Audit.AuditLogForm.formKey; };
            AuditLogEditorDialog.prototype.getLocalTextPrefix = function () { return Audit.AuditLogRow.localTextPrefix; };
            AuditLogEditorDialog.prototype.getNameProperty = function () { return Audit.AuditLogRow.nameProperty; };
            AuditLogEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AuditLogEditorDialog);
            return AuditLogEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Audit.AuditLogEditorDialog = AuditLogEditorDialog;
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogGrid = /** @class */ (function (_super) {
            __extends(AuditLogGrid, _super);
            function AuditLogGrid(container) {
                return _super.call(this, container) || this;
            }
            AuditLogGrid.prototype.getColumnsKey = function () { return 'Audit.AuditLog'; };
            AuditLogGrid.prototype.getDialogType = function () { return Audit.AuditLogDialog; };
            AuditLogGrid.prototype.getIdProperty = function () { return Audit.AuditLogRow.idProperty; };
            AuditLogGrid.prototype.getLocalTextPrefix = function () { return Audit.AuditLogRow.localTextPrefix; };
            AuditLogGrid.prototype.getService = function () { return Audit.AuditLogService.baseUrl; };
            AuditLogGrid.prototype.getButtons = function () {
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "column-picker-button"; }), 1);
                return buttons;
            };
            Object.defineProperty(AuditLogGrid.prototype, "rowid", {
                get: function () {
                    return this._rowid;
                },
                set: function (value) {
                    if (value) {
                        if (this._rowid !== value) {
                            this._rowid = value;
                            this.setEquality('rowid', value);
                            this.refresh();
                        }
                    }
                    else {
                        this.setEquality('rowid', 0);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            AuditLogGrid.prototype.createQuickSearchInput = function () {
            };
            Object.defineProperty(AuditLogGrid.prototype, "TableName", {
                get: function () {
                    return this._TableName;
                },
                set: function (value) {
                    if (this._TableName !== value) {
                        this._TableName = value;
                        this.setEquality('TableName', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            AuditLogGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AuditLogGrid);
            return AuditLogGrid;
        }(Serenity.EntityGrid));
        Audit.AuditLogGrid = AuditLogGrid;
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var BasicProgressDialog = /** @class */ (function (_super) {
        __extends(BasicProgressDialog, _super);
        function BasicProgressDialog() {
            var _this = _super.call(this) || this;
            _this.byId('ProgressBar').progressbar({
                max: 100,
                value: 0,
                change: function (e, v) {
                    _this.byId('ProgressLabel').text(_this.value + ' / ' + _this.max);
                }
            });
            return _this;
        }
        Object.defineProperty(BasicProgressDialog.prototype, "max", {
            get: function () {
                return this.byId('ProgressBar').progressbar().progressbar('option', 'max');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('option', 'max', value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "value", {
            get: function () {
                return this.byId('ProgressBar').progressbar('value');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('value', value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "title", {
            get: function () {
                return this.element.dialog().dialog('option', 'title');
            },
            set: function (value) {
                this.element.dialog().dialog('option', 'title', value);
            },
            enumerable: true,
            configurable: true
        });
        BasicProgressDialog.prototype.getDialogOptions = function () {
            var _this = this;
            var opt = _super.prototype.getDialogOptions.call(this);
            opt.title = Q.text('Site.BasicProgressDialog.PleaseWait');
            opt.width = 600;
            opt.buttons = [{
                    text: Q.text('Dialogs.CancelButton'),
                    click: function () {
                        _this.cancelled = true;
                        _this.element.closest('.ui-dialog')
                            .find('.ui-dialog-buttonpane .ui-button')
                            .attr('disabled', 'disabled')
                            .css('opacity', '0.5');
                        _this.element.dialog('option', 'title', Q.trimToNull(_this.cancelTitle) ||
                            Q.text('Site.BasicProgressDialog.CancelTitle'));
                    }
                }];
            return opt;
        };
        BasicProgressDialog.prototype.initDialog = function () {
            _super.prototype.initDialog.call(this);
            this.element.closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        };
        BasicProgressDialog.prototype.getTemplate = function () {
            return ("<div class='s-DialogContent s-BasicProgressDialogContent'>" +
                "<div id='~_StatusText' class='status-text' ></div>" +
                "<div id='~_ProgressBar' class='progress-bar'>" +
                "<div id='~_ProgressLabel' class='progress-label' ></div>" +
                "</div>" +
                "</div>");
        };
        return BasicProgressDialog;
    }(Serenity.TemplatedDialog));
    Serene2.BasicProgressDialog = BasicProgressDialog;
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var BulkServiceAction = /** @class */ (function () {
            function BulkServiceAction() {
            }
            BulkServiceAction.prototype.createProgressDialog = function () {
                this.progressDialog = new Serene2.BasicProgressDialog();
                this.progressDialog.dialogOpen();
                this.progressDialog.max = this.keys.length;
                this.progressDialog.value = 0;
            };
            BulkServiceAction.prototype.getConfirmationFormat = function () {
                return Q.text('Site.BulkServiceAction.ConfirmationFormat');
            };
            BulkServiceAction.prototype.getConfirmationMessage = function (targetCount) {
                return Q.format(this.getConfirmationFormat(), targetCount);
            };
            BulkServiceAction.prototype.confirm = function (targetCount, action) {
                Q.confirm(this.getConfirmationMessage(targetCount), action);
            };
            BulkServiceAction.prototype.getNothingToProcessMessage = function () {
                return Q.text('Site.BulkServiceAction.NothingToProcess');
            };
            BulkServiceAction.prototype.nothingToProcess = function () {
                Q.notifyError(this.getNothingToProcessMessage());
            };
            BulkServiceAction.prototype.getParallelRequests = function () {
                return 1;
            };
            BulkServiceAction.prototype.getBatchSize = function () {
                return 1;
            };
            BulkServiceAction.prototype.startParallelExecution = function () {
                this.createProgressDialog();
                this.successCount = 0;
                this.errorCount = 0;
                this.pendingRequests = 0;
                this.completedRequests = 0;
                this.errorCount = 0;
                this.errorByKey = {};
                this.queue = this.keys.slice();
                this.queueIndex = 0;
                var parallelRequests = this.getParallelRequests();
                while (parallelRequests-- > 0) {
                    this.executeNextBatch();
                }
            };
            BulkServiceAction.prototype.serviceCallCleanup = function () {
                this.pendingRequests--;
                this.completedRequests++;
                var title = Q.text((this.progressDialog.cancelled ?
                    'Site.BasicProgressDialog.CancelTitle' : 'Site.BasicProgressDialog.PleaseWait'));
                title += ' (';
                if (this.successCount > 0) {
                    title += Q.format(Q.text('Site.BulkServiceAction.SuccessCount'), this.successCount);
                }
                if (this.errorCount > 0) {
                    if (this.successCount > 0) {
                        title += ', ';
                    }
                    title += Q.format(Q.text('Site.BulkServiceAction.ErrorCount'), this.errorCount);
                }
                this.progressDialog.title = title + ')';
                this.progressDialog.value = this.successCount + this.errorCount;
                if (!this.progressDialog.cancelled && this.progressDialog.value < this.keys.length) {
                    this.executeNextBatch();
                }
                else if (this.pendingRequests === 0) {
                    this.progressDialog.dialogClose();
                    this.showResults();
                    if (this.done) {
                        this.done();
                        this.done = null;
                    }
                }
            };
            BulkServiceAction.prototype.executeForBatch = function (batch) {
            };
            BulkServiceAction.prototype.executeNextBatch = function () {
                var batchSize = this.getBatchSize();
                var batch = [];
                while (true) {
                    if (batch.length >= batchSize) {
                        break;
                    }
                    if (this.queueIndex >= this.queue.length) {
                        break;
                    }
                    batch.push(this.queue[this.queueIndex++]);
                }
                if (batch.length > 0) {
                    this.pendingRequests++;
                    this.executeForBatch(batch);
                }
            };
            BulkServiceAction.prototype.getAllHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.AllHadErrorsFormat');
            };
            BulkServiceAction.prototype.showAllHadErrors = function () {
                Q.notifyError(Q.format(this.getAllHadErrorsFormat(), this.errorCount));
            };
            BulkServiceAction.prototype.getSomeHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.SomeHadErrorsFormat');
            };
            BulkServiceAction.prototype.showSomeHadErrors = function () {
                Q.notifyWarning(Q.format(this.getSomeHadErrorsFormat(), this.successCount, this.errorCount));
            };
            BulkServiceAction.prototype.getAllSuccessFormat = function () {
                return Q.text('Site.BulkServiceAction.AllSuccessFormat');
            };
            BulkServiceAction.prototype.showAllSuccess = function () {
                Q.notifySuccess(Q.format(this.getAllSuccessFormat(), this.successCount));
            };
            BulkServiceAction.prototype.showResults = function () {
                if (this.errorCount === 0 && this.successCount === 0) {
                    this.nothingToProcess();
                    return;
                }
                if (this.errorCount > 0 && this.successCount === 0) {
                    this.showAllHadErrors();
                    return;
                }
                if (this.errorCount > 0) {
                    this.showSomeHadErrors();
                    return;
                }
                this.showAllSuccess();
            };
            BulkServiceAction.prototype.execute = function (keys) {
                var _this = this;
                this.keys = keys;
                if (this.keys.length === 0) {
                    this.nothingToProcess();
                    return;
                }
                this.confirm(this.keys.length, function () { return _this.startParallelExecution(); });
            };
            BulkServiceAction.prototype.get_successCount = function () {
                return this.successCount;
            };
            BulkServiceAction.prototype.set_successCount = function (value) {
                this.successCount = value;
            };
            BulkServiceAction.prototype.get_errorCount = function () {
                return this.errorCount;
            };
            BulkServiceAction.prototype.set_errorCount = function (value) {
                this.errorCount = value;
            };
            return BulkServiceAction;
        }());
        Common.BulkServiceAction = BulkServiceAction;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var DialogUtils;
    (function (DialogUtils) {
        function pendingChangesConfirmation(element, hasPendingChanges) {
            element.bind('dialogbeforeclose', function (e) {
                if (!Serenity.WX.hasOriginalEvent(e) || !hasPendingChanges()) {
                    return;
                }
                e.preventDefault();
                Q.confirm('You have pending changes. Save them?', function () { return element.find('div.save-and-close-button').click(); }, {
                    onNo: function () {
                        element.dialog().dialog('close');
                    }
                });
            });
        }
        DialogUtils.pendingChangesConfirmation = pendingChangesConfirmation;
    })(DialogUtils = Serene2.DialogUtils || (Serene2.DialogUtils = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var ExcelExportHelper;
        (function (ExcelExportHelper) {
            function createToolButton(options) {
                return {
                    hint: Q.coalesce(options.title, 'Excel'),
                    title: Q.coalesce(options.hint, ''),
                    cssClass: 'export-xlsx-button',
                    onClick: function () {
                        if (!options.onViewSubmit()) {
                            return;
                        }
                        var grid = options.grid;
                        var request = Q.deepClone(grid.getView().params);
                        request.Take = 0;
                        request.Skip = 0;
                        var sortBy = grid.getView().sortBy;
                        if (sortBy) {
                            request.Sort = sortBy;
                        }
                        request.IncludeColumns = [];
                        var columns = grid.getGrid().getColumns();
                        for (var _i = 0, columns_1 = columns; _i < columns_1.length; _i++) {
                            var column = columns_1[_i];
                            request.IncludeColumns.push(column.id || column.field);
                        }
                        Q.postToService({ service: options.service, request: request, target: '_blank' });
                    },
                    separator: options.separator
                };
            }
            ExcelExportHelper.createToolButton = createToolButton;
        })(ExcelExportHelper = Common.ExcelExportHelper || (Common.ExcelExportHelper = {}));
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var LanguageList;
    (function (LanguageList) {
        function getValue() {
            var result = [];
            for (var _i = 0, _a = Serene2.Administration.LanguageRow.getLookup().items; _i < _a.length; _i++) {
                var k = _a[_i];
                if (k.LanguageId !== 'en') {
                    result.push([k.Id.toString(), k.LanguageName]);
                }
            }
            return result;
        }
        LanguageList.getValue = getValue;
    })(LanguageList = Serene2.LanguageList || (Serene2.LanguageList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var LanguageSelection = /** @class */ (function (_super) {
            __extends(LanguageSelection, _super);
            function LanguageSelection(select, currentLanguage) {
                var _this = _super.call(this, select) || this;
                currentLanguage = Q.coalesce(currentLanguage, 'en');
                _this.change(function (e) {
                    $.cookie('LanguagePreference', select.val(), {
                        path: Q.Config.applicationPath,
                        expires: 365
                    });
                    window.location.reload(true);
                });
                Q.getLookupAsync('Administration.Language').then(function (x) {
                    if (!Q.any(x.items, function (z) { return z.LanguageId === currentLanguage; })) {
                        var idx = currentLanguage.lastIndexOf('-');
                        if (idx >= 0) {
                            currentLanguage = currentLanguage.substr(0, idx);
                            if (!Q.any(x.items, function (y) { return y.LanguageId === currentLanguage; })) {
                                currentLanguage = 'en';
                            }
                        }
                        else {
                            currentLanguage = 'en';
                        }
                    }
                    for (var _i = 0, _a = x.items; _i < _a.length; _i++) {
                        var l = _a[_i];
                        Q.addOption(select, l.LanguageId, l.LanguageName);
                    }
                    select.val(currentLanguage);
                });
                return _this;
            }
            return LanguageSelection;
        }(Serenity.Widget));
        Common.LanguageSelection = LanguageSelection;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var SidebarSearch = /** @class */ (function (_super) {
            __extends(SidebarSearch, _super);
            function SidebarSearch(input, menuUL) {
                var _this = _super.call(this, input) || this;
                new Serenity.QuickSearchInput(input, {
                    onSearch: function (field, text, success) {
                        _this.updateMatchFlags(text);
                        success(true);
                    }
                });
                _this.menuUL = menuUL;
                return _this;
            }
            SidebarSearch.prototype.updateMatchFlags = function (text) {
                var liList = this.menuUL.find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (text == null) {
                    liList.show();
                    liList.removeClass('expanded');
                    return;
                }
                var parts = text.replace(',', ' ').split(' ').filter(function (x) { return !Q.isTrimmedEmpty(x); });
                for (var i = 0; i < parts.length; i++) {
                    parts[i] = Q.trimToNull(Select2.util.stripDiacritics(parts[i]).toUpperCase());
                }
                var items = liList;
                items.each(function (idx, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
                        var p = parts_1[_i];
                        if (p != null && !(title.indexOf(p) !== -1)) {
                            x.addClass('non-match');
                            break;
                        }
                    }
                });
                var matchingItems = items.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                liList.addClass('expanded');
            };
            return SidebarSearch;
        }(Serenity.Widget));
        Common.SidebarSearch = SidebarSearch;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var ThemeSelection = /** @class */ (function (_super) {
            __extends(ThemeSelection, _super);
            function ThemeSelection(select) {
                var _this = _super.call(this, select) || this;
                _this.change(function (e) {
                    $.cookie('ThemePreference', select.val(), {
                        path: Q.Config.applicationPath,
                        expires: 365
                    });
                    $('body').removeClass('skin-' + _this.getCurrentTheme());
                    $('body').addClass('skin-' + select.val());
                });
                Q.addOption(select, 'blue', Q.text('Site.Layout.ThemeBlue'));
                Q.addOption(select, 'blue-light', Q.text('Site.Layout.ThemeBlueLight'));
                Q.addOption(select, 'purple', Q.text('Site.Layout.ThemePurple'));
                Q.addOption(select, 'purple-light', Q.text('Site.Layout.ThemePurpleLight'));
                Q.addOption(select, 'red', Q.text('Site.Layout.ThemeRed'));
                Q.addOption(select, 'red-light', Q.text('Site.Layout.ThemeRedLight'));
                Q.addOption(select, 'green', Q.text('Site.Layout.ThemeGreen'));
                Q.addOption(select, 'green-light', Q.text('Site.Layout.ThemeGreenLight'));
                Q.addOption(select, 'yellow', Q.text('Site.Layout.ThemeYellow'));
                Q.addOption(select, 'yellow-light', Q.text('Site.Layout.ThemeYellowLight'));
                Q.addOption(select, 'black', Q.text('Site.Layout.ThemeBlack'));
                Q.addOption(select, 'black-light', Q.text('Site.Layout.ThemeBlackLight'));
                select.val(_this.getCurrentTheme());
                return _this;
            }
            ThemeSelection.prototype.getCurrentTheme = function () {
                var skinClass = Q.first(($('body').attr('class') || '').split(' '), function (x) { return Q.startsWith(x, 'skin-'); });
                if (skinClass) {
                    return skinClass.substr(5);
                }
                return 'blue';
            };
            return ThemeSelection;
        }(Serenity.Widget));
        Common.ThemeSelection = ThemeSelection;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var PdfExportHelper;
        (function (PdfExportHelper) {
            function toAutoTableColumns(srcColumns, columnStyles, columnTitles) {
                return srcColumns.map(function (src) {
                    var col = {
                        dataKey: src.id || src.field,
                        title: src.name || ''
                    };
                    if (columnTitles && columnTitles[col.dataKey] != null)
                        col.title = columnTitles[col.dataKey];
                    var style = {};
                    if ((src.cssClass || '').indexOf("align-right") >= 0)
                        style.halign = 'right';
                    else if ((src.cssClass || '').indexOf("align-center") >= 0)
                        style.halign = 'center';
                    columnStyles[col.dataKey] = style;
                    return col;
                });
            }
            function toAutoTableData(entities, keys, srcColumns) {
                var el = document.createElement('span');
                var row = 0;
                return entities.map(function (item) {
                    var dst = {};
                    for (var cell = 0; cell < srcColumns.length; cell++) {
                        var src = srcColumns[cell];
                        var fld = src.field || '';
                        var key = keys[cell];
                        var txt = void 0;
                        var html = void 0;
                        if (src.formatter) {
                            html = src.formatter(row, cell, item[fld], src, item);
                        }
                        else if (src.format) {
                            html = src.format({ row: row, cell: cell, item: item, value: item[fld] });
                        }
                        else {
                            dst[key] = item[fld];
                            continue;
                        }
                        if (!html || (html.indexOf('<') < 0 && html.indexOf('&') < 0))
                            dst[key] = html;
                        else {
                            el.innerHTML = html;
                            if (el.children.length == 1 &&
                                $(el.children[0]).is(":input")) {
                                dst[key] = $(el.children[0]).val();
                            }
                            else if (el.children.length == 1 &&
                                $(el.children).is('.check-box')) {
                                dst[key] = $(el.children).hasClass("checked") ? "X" : "";
                            }
                            else
                                dst[key] = el.textContent || '';
                        }
                    }
                    row++;
                    return dst;
                });
            }
            function exportToPdf(options) {
                var g = options.grid;
                if (!options.onViewSubmit())
                    return;
                includeAutoTable();
                var request = Q.deepClone(g.view.params);
                request.Take = 0;
                request.Skip = 0;
                var sortBy = g.view.sortBy;
                if (sortBy != null)
                    request.Sort = sortBy;
                var gridColumns = g.slickGrid.getColumns();
                gridColumns = gridColumns.filter(function (x) { return x.id !== "__select__"; });
                request.IncludeColumns = [];
                for (var _i = 0, gridColumns_1 = gridColumns; _i < gridColumns_1.length; _i++) {
                    var column = gridColumns_1[_i];
                    request.IncludeColumns.push(column.id || column.field);
                }
                Q.serviceCall({
                    url: g.view.url,
                    request: request,
                    onSuccess: function (response) {
                        var doc = new jsPDF('l', 'pt');
                        var srcColumns = gridColumns;
                        var columnStyles = {};
                        var columns = toAutoTableColumns(srcColumns, columnStyles, options.columnTitles);
                        var keys = columns.map(function (x) { return x.dataKey; });
                        var entities = response.Entities || [];
                        var data = toAutoTableData(entities, keys, srcColumns);
                        doc.setFontSize(options.titleFontSize || 10);
                        doc.setFontStyle('bold');
                        var reportTitle = options.reportTitle || g.getTitle() || "Report";
                        doc.autoTableText(reportTitle, doc.internal.pageSize.width / 2, options.titleTop || 25, { halign: 'center' });
                        var totalPagesExp = "{{T}}";
                        var pageNumbers = options.pageNumbers == null || options.pageNumbers;
                        var autoOptions = $.extend({
                            margin: { top: 25, left: 25, right: 25, bottom: pageNumbers ? 25 : 30 },
                            startY: 60,
                            styles: {
                                fontSize: 8,
                                overflow: 'linebreak',
                                cellPadding: 2,
                                valign: 'middle'
                            },
                            columnStyles: columnStyles
                        }, options.tableOptions);
                        if (pageNumbers) {
                            var footer = function (data) {
                                var str = data.pageCount;
                                // Total page number plugin only available in jspdf v1.0+
                                if (typeof doc.putTotalPages === 'function') {
                                    str = str + " / " + totalPagesExp;
                                }
                                doc.autoTableText(str, doc.internal.pageSize.width / 2, doc.internal.pageSize.height - autoOptions.margin.bottom, {
                                    halign: 'center'
                                });
                            };
                            autoOptions.afterPageContent = footer;
                        }
                        doc.autoTable(columns, data, autoOptions);
                        if (typeof doc.putTotalPages === 'function') {
                            doc.putTotalPages(totalPagesExp);
                        }
                        if (!options.output || options.output == "file") {
                            var fileName = options.reportTitle || "{0}_{1}.pdf";
                            fileName = Q.format(fileName, g.getTitle() || "report", Q.formatDate(new Date(), "yyyyMMdd_HHmm"));
                            doc.save(fileName);
                            return;
                        }
                        if (options.autoPrint)
                            doc.autoPrint();
                        var output = options.output;
                        if (output == 'newwindow' || '_blank')
                            output = 'dataurlnewwindow';
                        else if (output == 'window')
                            output = 'datauri';
                        doc.output(output);
                    }
                });
            }
            PdfExportHelper.exportToPdf = exportToPdf;
            function createToolButton(options) {
                return {
                    title: options.title || '',
                    hint: options.hint || 'PDF',
                    cssClass: 'export-pdf-button',
                    onClick: function () { return exportToPdf(options); },
                    separator: options.separator
                };
            }
            PdfExportHelper.createToolButton = createToolButton;
            function includeJsPDF() {
                if (typeof jsPDF !== "undefined")
                    return;
                var script = $("jsPDFScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.min.js"))
                    .appendTo(document.head);
            }
            function includeAutoTable() {
                includeJsPDF();
                if (typeof jsPDF === "undefined" ||
                    typeof jsPDF.API == "undefined" ||
                    typeof jsPDF.API.autoTable !== "undefined")
                    return;
                var script = $("jsPDFAutoTableScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFAutoTableScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.plugin.autotable.min.js"))
                    .appendTo(document.head);
            }
        })(PdfExportHelper = Common.PdfExportHelper || (Common.PdfExportHelper = {}));
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var ReportDialog = /** @class */ (function (_super) {
            __extends(ReportDialog, _super);
            function ReportDialog(options) {
                var _this = _super.call(this, options) || this;
                _this.updateInterface();
                _this.loadReport(_this.options.reportKey);
                return _this;
            }
            ReportDialog.prototype.getDialogButtons = function () {
                return null;
            };
            ReportDialog.prototype.createPropertyGrid = function () {
                this.propertyGrid && this.byId('PropertyGrid').html('').attr('class', '');
                this.propertyGrid = new Serenity.PropertyGrid(this.byId('PropertyGrid'), {
                    idPrefix: this.idPrefix,
                    useCategories: true,
                    items: this.propertyItems
                }).init(null);
            };
            ReportDialog.prototype.loadReport = function (reportKey) {
                var _this = this;
                Q.serviceCall({
                    url: Q.resolveUrl('~/Report/Retrieve'),
                    request: {
                        ReportKey: reportKey
                    },
                    onSuccess: function (response) {
                        _this.report = response;
                        _this.element.dialog().dialog('option', 'title', _this.report.Title);
                        _this.createPropertyGrid();
                        _this.propertyGrid.load(_this.report.InitialSettings || {});
                        _this.updateInterface();
                        _this.dialogOpen();
                    }
                });
            };
            ReportDialog.prototype.updateInterface = function () {
                this.toolbar.findButton('print-preview-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-pdf-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-xlsx-button')
                    .toggle(this.report && this.report.IsDataOnlyReport);
            };
            ReportDialog.prototype.executeReport = function (target, ext, download) {
                if (!this.validateForm()) {
                    return;
                }
                var opt = {};
                this.propertyGrid.save(opt);
                Common.ReportHelper.execute({
                    download: download,
                    reportKey: this.report.ReportKey,
                    extension: ext,
                    target: target,
                    params: opt
                });
            };
            ReportDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                return [
                    {
                        title: 'Preview',
                        cssClass: 'print-preview-button',
                        onClick: function () { return _this.executeReport('_blank', null, false); }
                    },
                    {
                        title: 'PDF',
                        cssClass: 'export-pdf-button',
                        onClick: function () { return _this.executeReport('_blank', 'pdf', true); }
                    },
                    {
                        title: 'Excel',
                        cssClass: 'export-xlsx-button',
                        onClick: function () { return _this.executeReport('_blank', 'xlsx', true); }
                    }
                ];
            };
            return ReportDialog;
        }(Serenity.TemplatedDialog));
        Common.ReportDialog = ReportDialog;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var ReportHelper;
        (function (ReportHelper) {
            function createToolButton(options) {
                return {
                    title: Q.coalesce(options.title, 'Report'),
                    cssClass: Q.coalesce(options.cssClass, 'print-button'),
                    icon: options.icon,
                    onClick: function () {
                        ReportHelper.execute(options);
                    }
                };
            }
            ReportHelper.createToolButton = createToolButton;
            function execute(options) {
                var opt = options.getParams ? options.getParams() : options.params;
                Q.postToUrl({
                    url: '~/Report/' + (options.download ? 'Download' : 'Render'),
                    params: {
                        key: options.reportKey,
                        ext: Q.coalesce(options.extension, 'pdf'),
                        opt: opt ? $.toJSON(opt) : ''
                    },
                    target: Q.coalesce(options.target, '_blank')
                });
            }
            ReportHelper.execute = execute;
        })(ReportHelper = Common.ReportHelper || (Common.ReportHelper = {}));
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var ReportPage = /** @class */ (function (_super) {
            __extends(ReportPage, _super);
            function ReportPage(element) {
                var _this = _super.call(this, element) || this;
                $('.report-link', element).click(function (e) { return _this.reportLinkClick(e); });
                $('div.line', element).click(function (e) { return _this.categoryClick(e); });
                new Serenity.QuickSearchInput($('.s-QuickSearchBar input', element), {
                    onSearch: function (field, text, done) {
                        _this.updateMatchFlags(text);
                        done(true);
                    }
                });
                return _this;
            }
            ReportPage.prototype.updateMatchFlags = function (text) {
                var liList = $('.report-list', this.element).find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (!text) {
                    liList.children('ul').hide();
                    liList.show().removeClass('expanded');
                    return;
                }
                text = Select2.util.stripDiacritics(text).toUpperCase();
                var reportItems = liList.filter('.report-item');
                reportItems.each(function (ix, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    if (title.indexOf(text) < 0) {
                        x.addClass('non-match');
                    }
                });
                var matchingItems = reportItems.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                if (visibles.length <= 100) {
                    liList.children('ul').show();
                    liList.addClass('expanded');
                }
            };
            ReportPage.prototype.categoryClick = function (e) {
                var li = $(e.target).closest('li');
                if (li.hasClass('expanded')) {
                    li.find('ul').hide('fast');
                    li.removeClass('expanded');
                    li.find('li').removeClass('expanded');
                }
                else {
                    li.addClass('expanded');
                    li.children('ul').show('fast');
                    if (li.children('ul').children('li').length === 1 && !li.children('ul').children('li').hasClass('expanded')) {
                        li.children('ul').children('li').children('.line').click();
                    }
                }
            };
            ReportPage.prototype.reportLinkClick = function (e) {
                e.preventDefault();
                new Common.ReportDialog({
                    reportKey: $(e.target).data('key')
                }).dialogOpen();
            };
            return ReportPage;
        }(Serenity.Widget));
        Common.ReportPage = ReportPage;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var UserPreferenceStorage = /** @class */ (function () {
            function UserPreferenceStorage() {
            }
            UserPreferenceStorage.prototype.getItem = function (key) {
                var value;
                Common.UserPreferenceService.Retrieve({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key
                }, function (response) { return value = response.Value; }, {
                    async: false
                });
                return value;
            };
            UserPreferenceStorage.prototype.setItem = function (key, data) {
                Common.UserPreferenceService.Update({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key,
                    Value: data
                });
            };
            return UserPreferenceStorage;
        }());
        Common.UserPreferenceStorage = UserPreferenceStorage;
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var LoginPanel = /** @class */ (function (_super) {
            __extends(LoginPanel, _super);
            function LoginPanel(container) {
                var _this = _super.call(this, container) || this;
                $(function () {
                    $('body').vegas({
                        delay: 10000,
                        cover: true,
                        overlay: Q.resolveUrl("~/scripts/vegas/overlays/01.png"),
                        slides: [
                            { src: Q.resolveUrl('~/content/site/slides/home.png'), transition: 'fade' }
                            /*
                                { src: Q.resolveUrl('~/content/site/slides/slide1.jpg'), transition: 'fade' },
                                { src: Q.resolveUrl('~/content/site/slides/slide2.jpg'), transition: 'fade' },
                                { src: Q.resolveUrl('~/content/site/slides/slide3.jpg'), transition: 'zoomOut' },
                                { src: Q.resolveUrl('~/content/site/slides/slide4.jpg'), transition: 'blur' },
                                { src: Q.resolveUrl('~/content/site/slides/slide5.jpg'), transition: 'swirlLeft' }
                            */
                        ]
                    });
                });
                _this.form = new Membership.LoginForm(_this.idPrefix);
                _this.byId('LoginButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/Login'),
                        request: request,
                        onSuccess: function (response) {
                            var q = Q.parseQueryString();
                            var returnUrl = q['returnUrl'] || q['ReturnUrl'];
                            if (returnUrl) {
                                window.location.href = returnUrl;
                            }
                            else {
                                window.location.href = Q.resolveUrl('~/');
                            }
                        }
                    });
                });
                return _this;
            }
            LoginPanel.prototype.getFormKey = function () { return Membership.LoginForm.formKey; };
            LoginPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], LoginPanel);
            return LoginPanel;
        }(Serenity.PropertyPanel));
        Membership.LoginPanel = LoginPanel;
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeDialog = /** @class */ (function (_super) {
            __extends(RpeDialog, _super);
            function RpeDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new RPE.RpeForm(_this.idPrefix);
                return _this;
            }
            RpeDialog.prototype.getFormKey = function () { return RPE.RpeForm.formKey; };
            RpeDialog.prototype.getIdProperty = function () { return RPE.RpeRow.idProperty; };
            RpeDialog.prototype.getLocalTextPrefix = function () { return RPE.RpeRow.localTextPrefix; };
            RpeDialog.prototype.getService = function () { return RPE.RpeService.baseUrl; };
            RpeDialog.prototype.loadEntity = function (entity) {
                _super.prototype.loadEntity.call(this, entity);
                var user = Serene2.Authorization.userDefinition;
                if (user.isAthlete == true) {
                    this.form.Daterecorded.element.toggleClass('disabled', true);
                    this.form.Daterecorded.element.toggleClass('readOnly', true);
                    this.form.Daterecorded.element.toggleClass('isDisabled', true);
                    Serenity.EditorUtils.setReadonly(this.element.find('.Daterecorded .editor'), true);
                    //this.deleteButton.hide();
                    this.deleteButton.toggleClass('disabled', true);
                }
            };
            RpeDialog.prototype.onSaveSuccess = function (response) {
                _super.prototype.onSaveSuccess.call(this, response);
                var currenturl = window.location.href;
                if (currenturl.indexOf("Assessment/WellBeing") == -1) {
                    var user = Serene2.Authorization.userDefinition;
                    if (user.isAthlete == true) {
                        location.href = "/";
                        // location.reload();
                    }
                }
            };
            RpeDialog.prototype.afterLoadEntity = function () {
                _super.prototype.afterLoadEntity.call(this);
                var elID = this.uniqueName + '_RpEvAlue';
                //var elID = 's2id_Serene2_RPE_RpeDialog7_RpEvAlue';
                var input = this.element.find('#' + elID);
                input.attr({
                    'data-provide': 'slider',
                    'data-slider-ticks': '[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]',
                    //'data-slider-tooltips': '["0 - Rest", "1 - Really easy", "2 - Easy", "3 - Moderate", "4 - Sort of hard", "5 - Hard", "6", "7 - Really hard", "8", "9 - Really, really hard", "10 - Just like my hardest game"]', // TODO: create dynamically
                    //'data-slider-ticks-labels': '["Rest", "Really easy", "Easy", "Moderate", "Sort of hard", "Hard", "", "Really hard", "", "Really, really hard", "Just like my hardest game"]', // TODO: create dynamically
                    'data-slider-min': 0,
                    'data-slider-max': 10,
                    'data-slider-step': 1,
                    'data-slider-value': this.entity.RpEvAlue
                });
                input.append('<script>$("#' + elID + '").bootstrapSlider({tooltip: "always"});</script>');
                input.append('<script>$("#' + elID + '").bootstrapSlider({ formatter: function (value) {  if(value == "0"){ return "0 - Rest" } if (value == "1") { return "1 - Really easy" } if (value == "2") { return "2 - Easy" } if (value == "3") { return "3 - Moderate" } if (value == "4") { return "4 - Sort of hard" } if (value == "5") { return "5 - Hard" } if (value == "6") { return "6" } if (value == "7") { return "7 - Really hard" } if (value == "8") { return "8" } if (value == "9") { return "9 - Really, really hard" } if (value == "10") { return "10 - Just like my hardest game" } } }); </script>');
            };
            RpeDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], RpeDialog);
            return RpeDialog;
        }(Serenity.EntityDialog));
        RPE.RpeDialog = RpeDialog;
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeEditor = /** @class */ (function (_super) {
            __extends(RpeEditor, _super);
            function RpeEditor(container) {
                return _super.call(this, container) || this;
            }
            RpeEditor.prototype.getColumnsKey = function () { return 'RPE.Rpe'; };
            RpeEditor.prototype.getDialogType = function () { return RPE.RpeEditorDialog; };
            RpeEditor.prototype.getLocalTextPrefix = function () { return RPE.RpeRow.localTextPrefix; };
            RpeEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], RpeEditor);
            return RpeEditor;
        }(Serene2.Common.GridEditorBase));
        RPE.RpeEditor = RpeEditor;
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeEditorDialog = /** @class */ (function (_super) {
            __extends(RpeEditorDialog, _super);
            function RpeEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new RPE.RpeForm(_this.idPrefix);
                return _this;
            }
            RpeEditorDialog.prototype.getFormKey = function () { return RPE.RpeForm.formKey; };
            RpeEditorDialog.prototype.getLocalTextPrefix = function () { return RPE.RpeRow.localTextPrefix; };
            RpeEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], RpeEditorDialog);
            return RpeEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        RPE.RpeEditorDialog = RpeEditorDialog;
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeGrid = /** @class */ (function (_super) {
            __extends(RpeGrid, _super);
            function RpeGrid(container) {
                return _super.call(this, container) || this;
            }
            RpeGrid.prototype.getColumnsKey = function () { return 'RPE.Rpe'; };
            RpeGrid.prototype.getDialogType = function () { return RPE.RpeDialog; };
            RpeGrid.prototype.getIdProperty = function () { return RPE.RpeRow.idProperty; };
            RpeGrid.prototype.getLocalTextPrefix = function () { return RPE.RpeRow.localTextPrefix; };
            RpeGrid.prototype.getService = function () { return RPE.RpeService.baseUrl; };
            Object.defineProperty(RpeGrid.prototype, "AthleteUsername", {
                get: function () {
                    return this._AthleteUsername;
                },
                set: function (value) {
                    if (this._AthleteUsername !== value) {
                        this._AthleteUsername = value;
                        this.setEquality('AthleteUsername', value);
                        this.refresh();
                    }
                },
                enumerable: true,
                configurable: true
            });
            RpeGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], RpeGrid);
            return RpeGrid;
        }(Serenity.EntityGrid));
        RPE.RpeGrid = RpeGrid;
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
/**
 * This is an Slider editor widget
 *
 */
var Serene2;
(function (Serene2) {
    var Default;
    (function (Default) {
        var SliderEditor = /** @class */ (function (_super) {
            __extends(SliderEditor, _super);
            function SliderEditor(container, options) {
                var _this = _super.call(this, container, options) || this;
                _this.updateElementContent();
                return _this;
            }
            SliderEditor.prototype.updateElementContent = function () {
                var ticks;
                var tickslabels;
                // Here you can dynamically create ticks and ticks labels
                //for (var t of this.options.ticks) {
                //    [...]
                //}
                var elID = 'sld' + this.uniqueName;
                this.element.append('<input id="' + elID + '" />');
                var input = this.element.find('#' + elID);
                input.attr({
                    'data-provide': 'slider',
                    'data-slider-ticks': '[1, 2, 3, 4]',
                    //'data-slider-ticks-labels': '["One", "Two", "Three", "Four"]', // TODO: create dynamically
                    'data-slider-min': 1,
                    'data-slider-max': 4,
                    'data-slider-step': 1,
                    'data-slider-value': 1
                });
                input.append('<script>$("#' + elID + '").slider({tooltip: "always"});</script>');
            };
            Object.defineProperty(SliderEditor.prototype, "value", {
                // Implement get and set
                get: function () {
                    var sld = this.element.find('#sld' + this.uniqueName);
                    return Q.toId(sld.slider('getValue'));
                },
                set: function (value) {
                    var sld = this.element.find('#sld' + this.uniqueName);
                    sld.slider('setValue', value);
                },
                enumerable: true,
                configurable: true
            });
            SliderEditor.prototype.getEditValue = function (property, target) {
                target[property.name] = this.value;
            };
            SliderEditor.prototype.setEditValue = function (source, property) {
                this.value = source[property.name];
            };
            SliderEditor = __decorate([
                Serenity.Decorators.element("<div/>"),
                Serenity.Decorators.registerEditor([Serenity.IGetEditValue, Serenity.ISetEditValue])
            ], SliderEditor);
            return SliderEditor;
        }(Serenity.Widget));
        Default.SliderEditor = SliderEditor;
    })(Default = Serene2.Default || (Serene2.Default = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListDialog = /** @class */ (function (_super) {
            __extends(SandCoachListDialog, _super);
            function SandCoachListDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new SandCTrainerList.SandCoachListForm(_this.idPrefix);
                return _this;
            }
            SandCoachListDialog.prototype.getFormKey = function () { return SandCTrainerList.SandCoachListForm.formKey; };
            SandCoachListDialog.prototype.getIdProperty = function () { return SandCTrainerList.SandCoachListRow.idProperty; };
            SandCoachListDialog.prototype.getLocalTextPrefix = function () { return SandCTrainerList.SandCoachListRow.localTextPrefix; };
            SandCoachListDialog.prototype.getService = function () { return SandCTrainerList.SandCoachListService.baseUrl; };
            SandCoachListDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SandCoachListDialog);
            return SandCoachListDialog;
        }(Serenity.EntityDialog));
        SandCTrainerList.SandCoachListDialog = SandCoachListDialog;
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListEditor = /** @class */ (function (_super) {
            __extends(SandCoachListEditor, _super);
            function SandCoachListEditor(container) {
                return _super.call(this, container) || this;
            }
            SandCoachListEditor.prototype.getColumnsKey = function () { return 'SandCTrainerList.SandCoachList'; };
            SandCoachListEditor.prototype.getDialogType = function () { return SandCTrainerList.SandCoachListEditorDialog; };
            SandCoachListEditor.prototype.getLocalTextPrefix = function () { return SandCTrainerList.SandCoachListRow.localTextPrefix; };
            SandCoachListEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], SandCoachListEditor);
            return SandCoachListEditor;
        }(Serene2.Common.GridEditorBase));
        SandCTrainerList.SandCoachListEditor = SandCoachListEditor;
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListEditorDialog = /** @class */ (function (_super) {
            __extends(SandCoachListEditorDialog, _super);
            function SandCoachListEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new SandCTrainerList.SandCoachListForm(_this.idPrefix);
                return _this;
            }
            SandCoachListEditorDialog.prototype.getFormKey = function () { return SandCTrainerList.SandCoachListForm.formKey; };
            SandCoachListEditorDialog.prototype.getLocalTextPrefix = function () { return SandCTrainerList.SandCoachListRow.localTextPrefix; };
            SandCoachListEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SandCoachListEditorDialog);
            return SandCoachListEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        SandCTrainerList.SandCoachListEditorDialog = SandCoachListEditorDialog;
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListGrid = /** @class */ (function (_super) {
            __extends(SandCoachListGrid, _super);
            function SandCoachListGrid(container) {
                return _super.call(this, container) || this;
            }
            SandCoachListGrid.prototype.getColumnsKey = function () { return 'SandCTrainerList.SandCoachList'; };
            SandCoachListGrid.prototype.getDialogType = function () { return SandCTrainerList.SandCoachListDialog; };
            SandCoachListGrid.prototype.getIdProperty = function () { return SandCTrainerList.SandCoachListRow.idProperty; };
            SandCoachListGrid.prototype.getLocalTextPrefix = function () { return SandCTrainerList.SandCoachListRow.localTextPrefix; };
            SandCoachListGrid.prototype.getService = function () { return SandCTrainerList.SandCoachListService.baseUrl; };
            SandCoachListGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SandCoachListGrid);
            return SandCoachListGrid;
        }(Serenity.EntityGrid));
        SandCTrainerList.SandCoachListGrid = SandCoachListGrid;
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionDialog = /** @class */ (function (_super) {
            __extends(AthletePositionDialog, _super);
            function AthletePositionDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthletePositionForm(_this.idPrefix);
                return _this;
            }
            AthletePositionDialog.prototype.getFormKey = function () { return Setup.AthletePositionForm.formKey; };
            AthletePositionDialog.prototype.getIdProperty = function () { return Setup.AthletePositionRow.idProperty; };
            AthletePositionDialog.prototype.getLocalTextPrefix = function () { return Setup.AthletePositionRow.localTextPrefix; };
            AthletePositionDialog.prototype.getNameProperty = function () { return Setup.AthletePositionRow.nameProperty; };
            AthletePositionDialog.prototype.getService = function () { return Setup.AthletePositionService.baseUrl; };
            AthletePositionDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthletePositionDialog);
            return AthletePositionDialog;
        }(Serenity.EntityDialog));
        Setup.AthletePositionDialog = AthletePositionDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionEditor = /** @class */ (function (_super) {
            __extends(AthletePositionEditor, _super);
            function AthletePositionEditor(container) {
                return _super.call(this, container) || this;
            }
            AthletePositionEditor.prototype.getColumnsKey = function () { return 'Setup.AthletePosition'; };
            AthletePositionEditor.prototype.getDialogType = function () { return Setup.AthletePositionEditorDialog; };
            AthletePositionEditor.prototype.getLocalTextPrefix = function () { return Setup.AthletePositionRow.localTextPrefix; };
            AthletePositionEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthletePositionEditor);
            return AthletePositionEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.AthletePositionEditor = AthletePositionEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionEditorDialog = /** @class */ (function (_super) {
            __extends(AthletePositionEditorDialog, _super);
            function AthletePositionEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthletePositionForm(_this.idPrefix);
                return _this;
            }
            AthletePositionEditorDialog.prototype.getFormKey = function () { return Setup.AthletePositionForm.formKey; };
            AthletePositionEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.AthletePositionRow.localTextPrefix; };
            AthletePositionEditorDialog.prototype.getNameProperty = function () { return Setup.AthletePositionRow.nameProperty; };
            AthletePositionEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthletePositionEditorDialog);
            return AthletePositionEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.AthletePositionEditorDialog = AthletePositionEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionGrid = /** @class */ (function (_super) {
            __extends(AthletePositionGrid, _super);
            function AthletePositionGrid(container) {
                return _super.call(this, container) || this;
            }
            AthletePositionGrid.prototype.getColumnsKey = function () { return 'Setup.AthletePosition'; };
            AthletePositionGrid.prototype.getDialogType = function () { return Setup.AthletePositionDialog; };
            AthletePositionGrid.prototype.getIdProperty = function () { return Setup.AthletePositionRow.idProperty; };
            AthletePositionGrid.prototype.getLocalTextPrefix = function () { return Setup.AthletePositionRow.localTextPrefix; };
            AthletePositionGrid.prototype.getService = function () { return Setup.AthletePositionService.baseUrl; };
            AthletePositionGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthletePositionGrid);
            return AthletePositionGrid;
        }(Serenity.EntityGrid));
        Setup.AthletePositionGrid = AthletePositionGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingDialog = /** @class */ (function (_super) {
            __extends(AthleteTestingDialog, _super);
            function AthleteTestingDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteTestingForm(_this.idPrefix);
                return _this;
            }
            AthleteTestingDialog.prototype.getFormKey = function () { return Setup.AthleteTestingForm.formKey; };
            AthleteTestingDialog.prototype.getIdProperty = function () { return Setup.AthleteTestingRow.idProperty; };
            AthleteTestingDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingRow.localTextPrefix; };
            AthleteTestingDialog.prototype.getNameProperty = function () { return Setup.AthleteTestingRow.nameProperty; };
            AthleteTestingDialog.prototype.getService = function () { return Setup.AthleteTestingService.baseUrl; };
            AthleteTestingDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteTestingDialog);
            return AthleteTestingDialog;
        }(Serenity.EntityDialog));
        Setup.AthleteTestingDialog = AthleteTestingDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingEditor = /** @class */ (function (_super) {
            __extends(AthleteTestingEditor, _super);
            function AthleteTestingEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteTestingEditor.prototype.getColumnsKey = function () { return 'Setup.AthleteTesting'; };
            AthleteTestingEditor.prototype.getDialogType = function () { return Setup.AthleteTestingEditorDialog; };
            AthleteTestingEditor.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingRow.localTextPrefix; };
            AthleteTestingEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteTestingEditor);
            return AthleteTestingEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.AthleteTestingEditor = AthleteTestingEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteTestingEditorDialog, _super);
            function AthleteTestingEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteTestingForm(_this.idPrefix);
                return _this;
            }
            AthleteTestingEditorDialog.prototype.getFormKey = function () { return Setup.AthleteTestingForm.formKey; };
            AthleteTestingEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingRow.localTextPrefix; };
            AthleteTestingEditorDialog.prototype.getNameProperty = function () { return Setup.AthleteTestingRow.nameProperty; };
            AthleteTestingEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteTestingEditorDialog);
            return AthleteTestingEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.AthleteTestingEditorDialog = AthleteTestingEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingGrid = /** @class */ (function (_super) {
            __extends(AthleteTestingGrid, _super);
            function AthleteTestingGrid(container) {
                var _this = _super.call(this, container) || this;
                _this.pendingChanges = {};
                _this.slickContainer.on('change', '.edit:input', function (e) { return _this.inputsChange(e); });
                return _this;
            }
            AthleteTestingGrid.prototype.getColumnsKey = function () { return 'Setup.AthleteTesting'; };
            AthleteTestingGrid.prototype.getDialogType = function () { return Setup.AthleteTestingDialog; };
            AthleteTestingGrid.prototype.getIdProperty = function () { return Setup.AthleteTestingRow.idProperty; };
            AthleteTestingGrid.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingRow.localTextPrefix; };
            AthleteTestingGrid.prototype.getService = function () { return Setup.AthleteTestingService.baseUrl; };
            AthleteTestingGrid.prototype.createToolbarExtensions = function () {
                _super.prototype.createToolbarExtensions.call(this);
                this.rowSelection = new Serenity.GridRowSelectionMixin(this);
            };
            AthleteTestingGrid.prototype.getButtons = function () {
                var _this = this;
                // call base method to get list of buttons
                // by default, base entity grid adds a few buttons, 
                // add, refresh, column selection in order.
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                // add our import button
                buttons.push({
                    title: 'Create Test',
                    cssClass: 'approve-button',
                    onClick: function () {
                        // open import dialog, let it handle rest
                        var dialog = new Serene2.AthleteTesting.CreateTestDialog();
                        dialog.element.on('dialogclose', function () {
                            _this.refresh();
                            dialog = null;
                        });
                        dialog.dialogOpen();
                    }
                });
                buttons.push({
                    title: 'Save Changes',
                    cssClass: 'apply-changes-button disabled',
                    onClick: function (e) { return _this.saveClick(); },
                    separator: true
                });
                return buttons;
            };
            AthleteTestingGrid.prototype.onViewProcessData = function (response) {
                this.pendingChanges = {};
                this.setSaveButtonState();
                return _super.prototype.onViewProcessData.call(this, response);
            };
            AthleteTestingGrid.prototype.numericInputFormatter = function (ctx) {
                var klass = 'edit numeric';
                var item = ctx.item;
                var pending = this.pendingChanges[item.Id];
                if (pending && pending[ctx.column.field] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, ctx.column.field);
                return "<input type='text' class='" + klass +
                    "' data-field='" + ctx.column.field +
                    "' value='" + Q.formatNumber(value, '0.##') + "'/>";
            };
            AthleteTestingGrid.prototype.stringInputFormatter = function (ctx) {
                var klass = 'edit string';
                var item = ctx.item;
                var pending = this.pendingChanges[item.Id];
                var column = ctx.column;
                if (pending && pending[column.field] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, column.field);
                return "<input type='text' class='" + klass +
                    "' data-field='" + column.field +
                    "' value='" + Q.htmlEncode(value) +
                    "' maxlength='" + column.sourceItem.maxLength + "'/>";
            };
            AthleteTestingGrid.prototype.selectFormatter = function (ctx, idField, lookup) {
                var fld = Setup.AthleteTestingRow.Fields;
                var klass = 'edit';
                var item = ctx.item;
                var pending = this.pendingChanges[item.Id];
                var column = ctx.column;
                if (pending && pending[idField] !== undefined) {
                    klass += ' dirty';
                }
                var value = this.getEffectiveValue(item, idField);
                var markup = "<select class='" + klass +
                    "' data-field='" + idField +
                    "' style='width: 100%; max-width: 100%'>";
                for (var _i = 0, _a = lookup.items; _i < _a.length; _i++) {
                    var c = _a[_i];
                    var id = c[lookup.idField];
                    markup += "<option value='" + id + "'";
                    if (id == value) {
                        markup += " selected";
                    }
                    markup += ">" + Q.htmlEncode(c[lookup.textField]) + "</option>";
                }
                return markup + "</select>";
            };
            AthleteTestingGrid.prototype.getEffectiveValue = function (item, field) {
                var pending = this.pendingChanges[item.ProductID];
                if (pending && pending[field] !== undefined) {
                    return pending[field];
                }
                return item[field];
            };
            AthleteTestingGrid.prototype.getColumns = function () {
                var _this = this;
                var columns = _super.prototype.getColumns.call(this);
                var num = function (ctx) { return _this.numericInputFormatter(ctx); };
                var str = function (ctx) { return _this.stringInputFormatter(ctx); };
                var fld = Setup.AthleteTestingRow.Fields;
                Q.first(columns, function (x) { return x.field === fld.Test1Result; }).format = num;
                Q.first(columns, function (x) { return x.field === fld.Test2Result; }).format = num;
                Q.first(columns, function (x) { return x.field === fld.Test3Result; }).format = num;
                Q.first(columns, function (x) { return x.field === fld.Test4Result; }).format = num;
                Q.first(columns, function (x) { return x.field === fld.Test5Result; }).format = num;
                return columns;
            };
            AthleteTestingGrid.prototype.inputsChange = function (e) {
                var cell = this.slickGrid.getCellFromEvent(e);
                var item = this.itemAt(cell.row);
                var input = $(e.target);
                var field = input.data('field');
                var text = Q.coalesce(Q.trimToNull(input.val()), '0');
                var pending = this.pendingChanges[item.Id];
                var effective = this.getEffectiveValue(item, field);
                var oldText;
                if (input.hasClass("numeric"))
                    oldText = Q.formatNumber(effective, '0.##');
                else
                    oldText = effective;
                var value;
                if (field === 'UnitPrice') {
                    value = Q.parseDecimal(text);
                    if (value == null || isNaN(value)) {
                        Q.notifyError(Q.text('Validation.Decimal'), '', null);
                        input.val(oldText);
                        input.focus();
                        return;
                    }
                }
                else if (input.hasClass("numeric")) {
                    var i = Q.parseDecimal(text);
                    if (isNaN(i) || i > 32767 || i < 0) {
                        Q.notifyError(Q.text('Validation.Decimal'), '', null);
                        input.val(oldText);
                        input.focus();
                        return;
                    }
                    value = i;
                }
                else
                    value = text;
                if (!pending) {
                    this.pendingChanges[item.Id] = pending = {};
                }
                pending[field] = value;
                item[field] = value;
                this.view.refresh();
                if (input.hasClass("numeric"))
                    value = Q.formatNumber(value, '0.##');
                input.val(value).addClass('dirty');
                this.setSaveButtonState();
            };
            AthleteTestingGrid.prototype.setSaveButtonState = function () {
                this.toolbar.findButton('apply-changes-button').toggleClass('disabled', Object.keys(this.pendingChanges).length === 0);
            };
            AthleteTestingGrid.prototype.saveClick = function () {
                if (Object.keys(this.pendingChanges).length === 0) {
                    return;
                }
                // this calls save service for all modified rows, one by one
                // you could write a batch update service
                var keys = Object.keys(this.pendingChanges);
                var current = -1;
                var self = this;
                (function saveNext() {
                    if (++current >= keys.length) {
                        self.refresh();
                        return;
                    }
                    var key = keys[current];
                    var entity = Q.deepClone(self.pendingChanges[key]);
                    entity.Id = key;
                    Q.serviceRequest('Setup/AthleteTesting/Update', {
                        EntityId: key,
                        Entity: entity
                    }, function (response) {
                        delete self.pendingChanges[key];
                        saveNext();
                    });
                })();
            };
            AthleteTestingGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteTestingGrid);
            return AthleteTestingGrid;
        }(Serenity.EntityGrid));
        Setup.AthleteTestingGrid = AthleteTestingGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteTesting;
    (function (AthleteTesting) {
        var CreateTestDialog = /** @class */ (function (_super) {
            __extends(CreateTestDialog, _super);
            function CreateTestDialog() {
                var _this = _super.call(this) || this;
                _this.formImp = new AthleteTesting.CreateTestForm(_this.idPrefix);
                return _this;
            }
            CreateTestDialog.prototype.getDialogTitle = function () {
                return "Create Test";
            };
            CreateTestDialog.prototype.getDialogButtons = function () {
                var _this = this;
                return [
                    {
                        text: 'Create Test',
                        click: function () {
                            if (!_this.validateBeforeSave())
                                return;
                            if (_this.formImp.Sport.value == null) {
                                Q.notifyError("Please select a sport!");
                                return;
                            }
                            Serene2.Setup.AthleteTestingService.ExecuteCreateTest({
                                Sport: _this.formImp.Sport.value
                            }, function (response) {
                                Q.information(Q.text(response.outcome), function () {
                                });
                                /*
                                 Q.notifySuccess(
                                     'Inserted: ' + (response.Inserted || 0) +
                                     ', Updated: ' + (response.Updated || 0));
                                */
                                if (response.ErrorList.length > 0) {
                                    Q.warning(response.ErrorList.join(',\r\n '));
                                }
                                _this.dialogClose();
                            });
                        },
                    },
                    {
                        text: 'Cancel',
                        click: function () { return _this.dialogClose(); }
                    }
                ];
            };
            CreateTestDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], CreateTestDialog);
            return CreateTestDialog;
        }(Serenity.PropertyDialog));
        AthleteTesting.CreateTestDialog = CreateTestDialog;
    })(AthleteTesting = Serene2.AthleteTesting || (Serene2.AthleteTesting = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteTesting;
    (function (AthleteTesting) {
        /**
         * Our select editor with hardcoded values.
         *
         * When you define a new editor type, make sure you build
         * and transform templates for it to be available
         * in server side forms, e.g. [HardCodedValuesEditor]
         */
        var HardcodedValuesEditor = /** @class */ (function (_super) {
            __extends(HardcodedValuesEditor, _super);
            function HardcodedValuesEditor(container) {
                var _this = _super.call(this, container, null) || this;
                // add option accepts a key (id) value and display text value
                _this.addOption("key1", "Text 1");
                _this.addOption("key2", "Text 2");
                // you may also use addItem which accepts a Select2Item parameter
                _this.addItem({
                    id: "key3",
                    text: "Text 3"
                });
                // don't let selecting this one (disabled)
                _this.addItem({
                    id: "key4",
                    text: "Text 4",
                    disabled: true
                });
                return _this;
            }
            HardcodedValuesEditor = __decorate([
                Serenity.Decorators.registerEditor()
            ], HardcodedValuesEditor);
            return HardcodedValuesEditor;
        }(Serenity.Select2Editor));
        AthleteTesting.HardcodedValuesEditor = HardcodedValuesEditor;
    })(AthleteTesting = Serene2.AthleteTesting || (Serene2.AthleteTesting = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpDialog = /** @class */ (function (_super) {
            __extends(AthleteTestingLkUpDialog, _super);
            function AthleteTestingLkUpDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteTestingLkUpForm(_this.idPrefix);
                return _this;
            }
            AthleteTestingLkUpDialog.prototype.getFormKey = function () { return Setup.AthleteTestingLkUpForm.formKey; };
            AthleteTestingLkUpDialog.prototype.getIdProperty = function () { return Setup.AthleteTestingLkUpRow.idProperty; };
            AthleteTestingLkUpDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingLkUpRow.localTextPrefix; };
            AthleteTestingLkUpDialog.prototype.getNameProperty = function () { return Setup.AthleteTestingLkUpRow.nameProperty; };
            AthleteTestingLkUpDialog.prototype.getService = function () { return Setup.AthleteTestingLkUpService.baseUrl; };
            AthleteTestingLkUpDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteTestingLkUpDialog);
            return AthleteTestingLkUpDialog;
        }(Serenity.EntityDialog));
        Setup.AthleteTestingLkUpDialog = AthleteTestingLkUpDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpEditor = /** @class */ (function (_super) {
            __extends(AthleteTestingLkUpEditor, _super);
            function AthleteTestingLkUpEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteTestingLkUpEditor.prototype.getColumnsKey = function () { return 'Setup.AthleteTestingLkUp'; };
            AthleteTestingLkUpEditor.prototype.getDialogType = function () { return Setup.AthleteTestingLkUpEditorDialog; };
            AthleteTestingLkUpEditor.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingLkUpRow.localTextPrefix; };
            AthleteTestingLkUpEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteTestingLkUpEditor);
            return AthleteTestingLkUpEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.AthleteTestingLkUpEditor = AthleteTestingLkUpEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteTestingLkUpEditorDialog, _super);
            function AthleteTestingLkUpEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteTestingLkUpForm(_this.idPrefix);
                return _this;
            }
            AthleteTestingLkUpEditorDialog.prototype.getFormKey = function () { return Setup.AthleteTestingLkUpForm.formKey; };
            AthleteTestingLkUpEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingLkUpRow.localTextPrefix; };
            AthleteTestingLkUpEditorDialog.prototype.getNameProperty = function () { return Setup.AthleteTestingLkUpRow.nameProperty; };
            AthleteTestingLkUpEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteTestingLkUpEditorDialog);
            return AthleteTestingLkUpEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.AthleteTestingLkUpEditorDialog = AthleteTestingLkUpEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpGrid = /** @class */ (function (_super) {
            __extends(AthleteTestingLkUpGrid, _super);
            function AthleteTestingLkUpGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteTestingLkUpGrid.prototype.getColumnsKey = function () { return 'Setup.AthleteTestingLkUp'; };
            AthleteTestingLkUpGrid.prototype.getDialogType = function () { return Setup.AthleteTestingLkUpDialog; };
            AthleteTestingLkUpGrid.prototype.getIdProperty = function () { return Setup.AthleteTestingLkUpRow.idProperty; };
            AthleteTestingLkUpGrid.prototype.getLocalTextPrefix = function () { return Setup.AthleteTestingLkUpRow.localTextPrefix; };
            AthleteTestingLkUpGrid.prototype.getService = function () { return Setup.AthleteTestingLkUpService.baseUrl; };
            AthleteTestingLkUpGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteTestingLkUpGrid);
            return AthleteTestingLkUpGrid;
        }(Serenity.EntityGrid));
        Setup.AthleteTestingLkUpGrid = AthleteTestingLkUpGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearDialog = /** @class */ (function (_super) {
            __extends(AthleteYearDialog, _super);
            function AthleteYearDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteYearForm(_this.idPrefix);
                return _this;
            }
            AthleteYearDialog.prototype.getFormKey = function () { return Setup.AthleteYearForm.formKey; };
            AthleteYearDialog.prototype.getIdProperty = function () { return Setup.AthleteYearRow.idProperty; };
            AthleteYearDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteYearRow.localTextPrefix; };
            AthleteYearDialog.prototype.getNameProperty = function () { return Setup.AthleteYearRow.nameProperty; };
            AthleteYearDialog.prototype.getService = function () { return Setup.AthleteYearService.baseUrl; };
            AthleteYearDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteYearDialog);
            return AthleteYearDialog;
        }(Serenity.EntityDialog));
        Setup.AthleteYearDialog = AthleteYearDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearEditor = /** @class */ (function (_super) {
            __extends(AthleteYearEditor, _super);
            function AthleteYearEditor(container) {
                return _super.call(this, container) || this;
            }
            AthleteYearEditor.prototype.getColumnsKey = function () { return 'Setup.AthleteYear'; };
            AthleteYearEditor.prototype.getDialogType = function () { return Setup.AthleteYearEditorDialog; };
            AthleteYearEditor.prototype.getLocalTextPrefix = function () { return Setup.AthleteYearRow.localTextPrefix; };
            AthleteYearEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteYearEditor);
            return AthleteYearEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.AthleteYearEditor = AthleteYearEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearEditorDialog = /** @class */ (function (_super) {
            __extends(AthleteYearEditorDialog, _super);
            function AthleteYearEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.AthleteYearForm(_this.idPrefix);
                return _this;
            }
            AthleteYearEditorDialog.prototype.getFormKey = function () { return Setup.AthleteYearForm.formKey; };
            AthleteYearEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.AthleteYearRow.localTextPrefix; };
            AthleteYearEditorDialog.prototype.getNameProperty = function () { return Setup.AthleteYearRow.nameProperty; };
            AthleteYearEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AthleteYearEditorDialog);
            return AthleteYearEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.AthleteYearEditorDialog = AthleteYearEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearGrid = /** @class */ (function (_super) {
            __extends(AthleteYearGrid, _super);
            function AthleteYearGrid(container) {
                return _super.call(this, container) || this;
            }
            AthleteYearGrid.prototype.getColumnsKey = function () { return 'Setup.AthleteYear'; };
            AthleteYearGrid.prototype.getDialogType = function () { return Setup.AthleteYearDialog; };
            AthleteYearGrid.prototype.getIdProperty = function () { return Setup.AthleteYearRow.idProperty; };
            AthleteYearGrid.prototype.getLocalTextPrefix = function () { return Setup.AthleteYearRow.localTextPrefix; };
            AthleteYearGrid.prototype.getService = function () { return Setup.AthleteYearService.baseUrl; };
            AthleteYearGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AthleteYearGrid);
            return AthleteYearGrid;
        }(Serenity.EntityGrid));
        Setup.AthleteYearGrid = AthleteYearGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderDialog = /** @class */ (function (_super) {
            __extends(GenderDialog, _super);
            function GenderDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.GenderForm(_this.idPrefix);
                return _this;
            }
            GenderDialog.prototype.getFormKey = function () { return Setup.GenderForm.formKey; };
            GenderDialog.prototype.getIdProperty = function () { return Setup.GenderRow.idProperty; };
            GenderDialog.prototype.getLocalTextPrefix = function () { return Setup.GenderRow.localTextPrefix; };
            GenderDialog.prototype.getNameProperty = function () { return Setup.GenderRow.nameProperty; };
            GenderDialog.prototype.getService = function () { return Setup.GenderService.baseUrl; };
            GenderDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], GenderDialog);
            return GenderDialog;
        }(Serenity.EntityDialog));
        Setup.GenderDialog = GenderDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderEditor = /** @class */ (function (_super) {
            __extends(GenderEditor, _super);
            function GenderEditor(container) {
                return _super.call(this, container) || this;
            }
            GenderEditor.prototype.getColumnsKey = function () { return 'Setup.Gender'; };
            GenderEditor.prototype.getDialogType = function () { return Setup.GenderEditorDialog; };
            GenderEditor.prototype.getLocalTextPrefix = function () { return Setup.GenderRow.localTextPrefix; };
            GenderEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], GenderEditor);
            return GenderEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.GenderEditor = GenderEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderEditorDialog = /** @class */ (function (_super) {
            __extends(GenderEditorDialog, _super);
            function GenderEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.GenderForm(_this.idPrefix);
                return _this;
            }
            GenderEditorDialog.prototype.getFormKey = function () { return Setup.GenderForm.formKey; };
            GenderEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.GenderRow.localTextPrefix; };
            GenderEditorDialog.prototype.getNameProperty = function () { return Setup.GenderRow.nameProperty; };
            GenderEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], GenderEditorDialog);
            return GenderEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.GenderEditorDialog = GenderEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderGrid = /** @class */ (function (_super) {
            __extends(GenderGrid, _super);
            function GenderGrid(container) {
                return _super.call(this, container) || this;
            }
            GenderGrid.prototype.getColumnsKey = function () { return 'Setup.Gender'; };
            GenderGrid.prototype.getDialogType = function () { return Setup.GenderDialog; };
            GenderGrid.prototype.getIdProperty = function () { return Setup.GenderRow.idProperty; };
            GenderGrid.prototype.getLocalTextPrefix = function () { return Setup.GenderRow.localTextPrefix; };
            GenderGrid.prototype.getService = function () { return Setup.GenderService.baseUrl; };
            GenderGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], GenderGrid);
            return GenderGrid;
        }(Serenity.EntityGrid));
        Setup.GenderGrid = GenderGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupDialog = /** @class */ (function (_super) {
            __extends(InjStatusAnswerLkupDialog, _super);
            function InjStatusAnswerLkupDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjStatusAnswerLkupForm(_this.idPrefix);
                return _this;
            }
            InjStatusAnswerLkupDialog.prototype.getFormKey = function () { return Setup.InjStatusAnswerLkupForm.formKey; };
            InjStatusAnswerLkupDialog.prototype.getIdProperty = function () { return Setup.InjStatusAnswerLkupRow.idProperty; };
            InjStatusAnswerLkupDialog.prototype.getLocalTextPrefix = function () { return Setup.InjStatusAnswerLkupRow.localTextPrefix; };
            InjStatusAnswerLkupDialog.prototype.getNameProperty = function () { return Setup.InjStatusAnswerLkupRow.nameProperty; };
            InjStatusAnswerLkupDialog.prototype.getService = function () { return Setup.InjStatusAnswerLkupService.baseUrl; };
            InjStatusAnswerLkupDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjStatusAnswerLkupDialog);
            return InjStatusAnswerLkupDialog;
        }(Serenity.EntityDialog));
        Setup.InjStatusAnswerLkupDialog = InjStatusAnswerLkupDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupEditor = /** @class */ (function (_super) {
            __extends(InjStatusAnswerLkupEditor, _super);
            function InjStatusAnswerLkupEditor(container) {
                return _super.call(this, container) || this;
            }
            InjStatusAnswerLkupEditor.prototype.getColumnsKey = function () { return 'Setup.InjStatusAnswerLkup'; };
            InjStatusAnswerLkupEditor.prototype.getDialogType = function () { return Setup.InjStatusAnswerLkupEditorDialog; };
            InjStatusAnswerLkupEditor.prototype.getLocalTextPrefix = function () { return Setup.InjStatusAnswerLkupRow.localTextPrefix; };
            InjStatusAnswerLkupEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], InjStatusAnswerLkupEditor);
            return InjStatusAnswerLkupEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.InjStatusAnswerLkupEditor = InjStatusAnswerLkupEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupEditorDialog = /** @class */ (function (_super) {
            __extends(InjStatusAnswerLkupEditorDialog, _super);
            function InjStatusAnswerLkupEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjStatusAnswerLkupForm(_this.idPrefix);
                return _this;
            }
            InjStatusAnswerLkupEditorDialog.prototype.getFormKey = function () { return Setup.InjStatusAnswerLkupForm.formKey; };
            InjStatusAnswerLkupEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.InjStatusAnswerLkupRow.localTextPrefix; };
            InjStatusAnswerLkupEditorDialog.prototype.getNameProperty = function () { return Setup.InjStatusAnswerLkupRow.nameProperty; };
            InjStatusAnswerLkupEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjStatusAnswerLkupEditorDialog);
            return InjStatusAnswerLkupEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.InjStatusAnswerLkupEditorDialog = InjStatusAnswerLkupEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupGrid = /** @class */ (function (_super) {
            __extends(InjStatusAnswerLkupGrid, _super);
            function InjStatusAnswerLkupGrid(container) {
                return _super.call(this, container) || this;
            }
            InjStatusAnswerLkupGrid.prototype.getColumnsKey = function () { return 'Setup.InjStatusAnswerLkup'; };
            InjStatusAnswerLkupGrid.prototype.getDialogType = function () { return Setup.InjStatusAnswerLkupDialog; };
            InjStatusAnswerLkupGrid.prototype.getIdProperty = function () { return Setup.InjStatusAnswerLkupRow.idProperty; };
            InjStatusAnswerLkupGrid.prototype.getLocalTextPrefix = function () { return Setup.InjStatusAnswerLkupRow.localTextPrefix; };
            InjStatusAnswerLkupGrid.prototype.getService = function () { return Setup.InjStatusAnswerLkupService.baseUrl; };
            InjStatusAnswerLkupGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], InjStatusAnswerLkupGrid);
            return InjStatusAnswerLkupGrid;
        }(Serenity.EntityGrid));
        Setup.InjStatusAnswerLkupGrid = InjStatusAnswerLkupGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryDialog = /** @class */ (function (_super) {
            __extends(InjuryDialog, _super);
            function InjuryDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjuryForm(_this.idPrefix);
                return _this;
            }
            InjuryDialog.prototype.getFormKey = function () { return Setup.InjuryForm.formKey; };
            InjuryDialog.prototype.getIdProperty = function () { return Setup.InjuryRow.idProperty; };
            InjuryDialog.prototype.getLocalTextPrefix = function () { return Setup.InjuryRow.localTextPrefix; };
            InjuryDialog.prototype.getNameProperty = function () { return Setup.InjuryRow.nameProperty; };
            InjuryDialog.prototype.getService = function () { return Setup.InjuryService.baseUrl; };
            InjuryDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjuryDialog);
            return InjuryDialog;
        }(Serenity.EntityDialog));
        Setup.InjuryDialog = InjuryDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryEditor = /** @class */ (function (_super) {
            __extends(InjuryEditor, _super);
            function InjuryEditor(container) {
                return _super.call(this, container) || this;
            }
            InjuryEditor.prototype.getColumnsKey = function () { return 'Setup.Injury'; };
            InjuryEditor.prototype.getDialogType = function () { return Setup.InjuryEditorDialog; };
            InjuryEditor.prototype.getLocalTextPrefix = function () { return Setup.InjuryRow.localTextPrefix; };
            InjuryEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], InjuryEditor);
            return InjuryEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.InjuryEditor = InjuryEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryEditorDialog = /** @class */ (function (_super) {
            __extends(InjuryEditorDialog, _super);
            function InjuryEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjuryForm(_this.idPrefix);
                return _this;
            }
            InjuryEditorDialog.prototype.getFormKey = function () { return Setup.InjuryForm.formKey; };
            InjuryEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.InjuryRow.localTextPrefix; };
            InjuryEditorDialog.prototype.getNameProperty = function () { return Setup.InjuryRow.nameProperty; };
            InjuryEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjuryEditorDialog);
            return InjuryEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.InjuryEditorDialog = InjuryEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryGrid = /** @class */ (function (_super) {
            __extends(InjuryGrid, _super);
            function InjuryGrid(container) {
                return _super.call(this, container) || this;
            }
            InjuryGrid.prototype.getColumnsKey = function () { return 'Setup.Injury'; };
            InjuryGrid.prototype.getDialogType = function () { return Setup.InjuryDialog; };
            InjuryGrid.prototype.getIdProperty = function () { return Setup.InjuryRow.idProperty; };
            InjuryGrid.prototype.getLocalTextPrefix = function () { return Setup.InjuryRow.localTextPrefix; };
            InjuryGrid.prototype.getService = function () { return Setup.InjuryService.baseUrl; };
            InjuryGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], InjuryGrid);
            return InjuryGrid;
        }(Serenity.EntityGrid));
        Setup.InjuryGrid = InjuryGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusDialog = /** @class */ (function (_super) {
            __extends(InjuryStatusDialog, _super);
            function InjuryStatusDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjuryStatusForm(_this.idPrefix);
                return _this;
            }
            InjuryStatusDialog.prototype.getFormKey = function () { return Setup.InjuryStatusForm.formKey; };
            InjuryStatusDialog.prototype.getIdProperty = function () { return Setup.InjuryStatusRow.idProperty; };
            InjuryStatusDialog.prototype.getLocalTextPrefix = function () { return Setup.InjuryStatusRow.localTextPrefix; };
            InjuryStatusDialog.prototype.getNameProperty = function () { return Setup.InjuryStatusRow.nameProperty; };
            InjuryStatusDialog.prototype.getService = function () { return Setup.InjuryStatusService.baseUrl; };
            InjuryStatusDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjuryStatusDialog);
            return InjuryStatusDialog;
        }(Serenity.EntityDialog));
        Setup.InjuryStatusDialog = InjuryStatusDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusEditor = /** @class */ (function (_super) {
            __extends(InjuryStatusEditor, _super);
            function InjuryStatusEditor(container) {
                return _super.call(this, container) || this;
            }
            InjuryStatusEditor.prototype.getColumnsKey = function () { return 'Setup.InjuryStatus'; };
            InjuryStatusEditor.prototype.getDialogType = function () { return Setup.InjuryStatusEditorDialog; };
            InjuryStatusEditor.prototype.getLocalTextPrefix = function () { return Setup.InjuryStatusRow.localTextPrefix; };
            InjuryStatusEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], InjuryStatusEditor);
            return InjuryStatusEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.InjuryStatusEditor = InjuryStatusEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusEditorDialog = /** @class */ (function (_super) {
            __extends(InjuryStatusEditorDialog, _super);
            function InjuryStatusEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.InjuryStatusForm(_this.idPrefix);
                return _this;
            }
            InjuryStatusEditorDialog.prototype.getFormKey = function () { return Setup.InjuryStatusForm.formKey; };
            InjuryStatusEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.InjuryStatusRow.localTextPrefix; };
            InjuryStatusEditorDialog.prototype.getNameProperty = function () { return Setup.InjuryStatusRow.nameProperty; };
            InjuryStatusEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InjuryStatusEditorDialog);
            return InjuryStatusEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.InjuryStatusEditorDialog = InjuryStatusEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusGrid = /** @class */ (function (_super) {
            __extends(InjuryStatusGrid, _super);
            function InjuryStatusGrid(container) {
                return _super.call(this, container) || this;
            }
            InjuryStatusGrid.prototype.getColumnsKey = function () { return 'Setup.InjuryStatus'; };
            InjuryStatusGrid.prototype.getDialogType = function () { return Setup.InjuryStatusDialog; };
            InjuryStatusGrid.prototype.getIdProperty = function () { return Setup.InjuryStatusRow.idProperty; };
            InjuryStatusGrid.prototype.getLocalTextPrefix = function () { return Setup.InjuryStatusRow.localTextPrefix; };
            InjuryStatusGrid.prototype.getService = function () { return Setup.InjuryStatusService.baseUrl; };
            InjuryStatusGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], InjuryStatusGrid);
            return InjuryStatusGrid;
        }(Serenity.EntityGrid));
        Setup.InjuryStatusGrid = InjuryStatusGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioDialog = /** @class */ (function (_super) {
            __extends(PhysioDialog, _super);
            function PhysioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.PhysioForm(_this.idPrefix);
                return _this;
            }
            PhysioDialog.prototype.getFormKey = function () { return Setup.PhysioForm.formKey; };
            PhysioDialog.prototype.getIdProperty = function () { return Setup.PhysioRow.idProperty; };
            PhysioDialog.prototype.getLocalTextPrefix = function () { return Setup.PhysioRow.localTextPrefix; };
            PhysioDialog.prototype.getNameProperty = function () { return Setup.PhysioRow.nameProperty; };
            PhysioDialog.prototype.getService = function () { return Setup.PhysioService.baseUrl; };
            PhysioDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], PhysioDialog);
            return PhysioDialog;
        }(Serenity.EntityDialog));
        Setup.PhysioDialog = PhysioDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioEditor = /** @class */ (function (_super) {
            __extends(PhysioEditor, _super);
            function PhysioEditor(container) {
                return _super.call(this, container) || this;
            }
            PhysioEditor.prototype.getColumnsKey = function () { return 'Setup.Physio'; };
            PhysioEditor.prototype.getDialogType = function () { return Setup.PhysioEditorDialog; };
            PhysioEditor.prototype.getLocalTextPrefix = function () { return Setup.PhysioRow.localTextPrefix; };
            PhysioEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], PhysioEditor);
            return PhysioEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.PhysioEditor = PhysioEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioEditorDialog = /** @class */ (function (_super) {
            __extends(PhysioEditorDialog, _super);
            function PhysioEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.PhysioForm(_this.idPrefix);
                return _this;
            }
            PhysioEditorDialog.prototype.getFormKey = function () { return Setup.PhysioForm.formKey; };
            PhysioEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.PhysioRow.localTextPrefix; };
            PhysioEditorDialog.prototype.getNameProperty = function () { return Setup.PhysioRow.nameProperty; };
            PhysioEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], PhysioEditorDialog);
            return PhysioEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.PhysioEditorDialog = PhysioEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioGrid = /** @class */ (function (_super) {
            __extends(PhysioGrid, _super);
            function PhysioGrid(container) {
                return _super.call(this, container) || this;
            }
            PhysioGrid.prototype.getColumnsKey = function () { return 'Setup.Physio'; };
            PhysioGrid.prototype.getDialogType = function () { return Setup.PhysioDialog; };
            PhysioGrid.prototype.getIdProperty = function () { return Setup.PhysioRow.idProperty; };
            PhysioGrid.prototype.getLocalTextPrefix = function () { return Setup.PhysioRow.localTextPrefix; };
            PhysioGrid.prototype.getService = function () { return Setup.PhysioService.baseUrl; };
            PhysioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PhysioGrid);
            return PhysioGrid;
        }(Serenity.EntityGrid));
        Setup.PhysioGrid = PhysioGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsDialog = /** @class */ (function (_super) {
            __extends(SportsDialog, _super);
            function SportsDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.SportsForm(_this.idPrefix);
                return _this;
            }
            SportsDialog.prototype.getFormKey = function () { return Setup.SportsForm.formKey; };
            SportsDialog.prototype.getIdProperty = function () { return Setup.SportsRow.idProperty; };
            SportsDialog.prototype.getLocalTextPrefix = function () { return Setup.SportsRow.localTextPrefix; };
            SportsDialog.prototype.getNameProperty = function () { return Setup.SportsRow.nameProperty; };
            SportsDialog.prototype.getService = function () { return Setup.SportsService.baseUrl; };
            SportsDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SportsDialog);
            return SportsDialog;
        }(Serenity.EntityDialog));
        Setup.SportsDialog = SportsDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsEditor = /** @class */ (function (_super) {
            __extends(SportsEditor, _super);
            function SportsEditor(container) {
                return _super.call(this, container) || this;
            }
            SportsEditor.prototype.getColumnsKey = function () { return 'Setup.Sports'; };
            SportsEditor.prototype.getDialogType = function () { return Setup.SportsEditorDialog; };
            SportsEditor.prototype.getLocalTextPrefix = function () { return Setup.SportsRow.localTextPrefix; };
            SportsEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], SportsEditor);
            return SportsEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.SportsEditor = SportsEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsEditorDialog = /** @class */ (function (_super) {
            __extends(SportsEditorDialog, _super);
            function SportsEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.SportsForm(_this.idPrefix);
                return _this;
            }
            SportsEditorDialog.prototype.getFormKey = function () { return Setup.SportsForm.formKey; };
            SportsEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.SportsRow.localTextPrefix; };
            SportsEditorDialog.prototype.getNameProperty = function () { return Setup.SportsRow.nameProperty; };
            SportsEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SportsEditorDialog);
            return SportsEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.SportsEditorDialog = SportsEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsGrid = /** @class */ (function (_super) {
            __extends(SportsGrid, _super);
            function SportsGrid(container) {
                return _super.call(this, container) || this;
            }
            SportsGrid.prototype.getColumnsKey = function () { return 'Setup.Sports'; };
            SportsGrid.prototype.getDialogType = function () { return Setup.SportsDialog; };
            SportsGrid.prototype.getIdProperty = function () { return Setup.SportsRow.idProperty; };
            SportsGrid.prototype.getLocalTextPrefix = function () { return Setup.SportsRow.localTextPrefix; };
            SportsGrid.prototype.getService = function () { return Setup.SportsService.baseUrl; };
            SportsGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SportsGrid);
            return SportsGrid;
        }(Serenity.EntityGrid));
        Setup.SportsGrid = SportsGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var TrainerSandCListFormatter = /** @class */ (function () {
            function TrainerSandCListFormatter() {
            }
            TrainerSandCListFormatter.prototype.format = function (ctx) {
                var idList = ctx.value;
                if (!idList || !idList.length)
                    return "";
                var byId = Serene2.Setup.TrainersRow.getLookup().itemById;
                var z;
                return idList.map(function (x) { return ((z = byId[x]) ? z.Trainer : x); }).join(", ");
            };
            TrainerSandCListFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], TrainerSandCListFormatter);
            return TrainerSandCListFormatter;
        }());
        Athlete.TrainerSandCListFormatter = TrainerSandCListFormatter;
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Sports;
    (function (Sports) {
        var TrainerSportsListFormatter = /** @class */ (function () {
            function TrainerSportsListFormatter() {
            }
            TrainerSportsListFormatter.prototype.format = function (ctx) {
                var idList = ctx.value;
                if (!idList || !idList.length)
                    return "";
                var byId = Serene2.Setup.TrainersRow.getLookup().itemById;
                var z;
                return idList.map(function (x) { return ((z = byId[x]) ? z.Trainer : x); }).join(", ");
            };
            TrainerSportsListFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], TrainerSportsListFormatter);
            return TrainerSportsListFormatter;
        }());
        Sports.TrainerSportsListFormatter = TrainerSportsListFormatter;
    })(Sports = Serene2.Sports || (Serene2.Sports = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersDialog = /** @class */ (function (_super) {
            __extends(TrainersDialog, _super);
            function TrainersDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.TrainersForm(_this.idPrefix);
                return _this;
            }
            TrainersDialog.prototype.getFormKey = function () { return Setup.TrainersForm.formKey; };
            TrainersDialog.prototype.getIdProperty = function () { return Setup.TrainersRow.idProperty; };
            TrainersDialog.prototype.getLocalTextPrefix = function () { return Setup.TrainersRow.localTextPrefix; };
            TrainersDialog.prototype.getNameProperty = function () { return Setup.TrainersRow.nameProperty; };
            TrainersDialog.prototype.getService = function () { return Setup.TrainersService.baseUrl; };
            TrainersDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TrainersDialog);
            return TrainersDialog;
        }(Serenity.EntityDialog));
        Setup.TrainersDialog = TrainersDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersEditor = /** @class */ (function (_super) {
            __extends(TrainersEditor, _super);
            function TrainersEditor(container) {
                return _super.call(this, container) || this;
            }
            TrainersEditor.prototype.getColumnsKey = function () { return 'Setup.Trainers'; };
            TrainersEditor.prototype.getDialogType = function () { return Setup.TrainersEditorDialog; };
            TrainersEditor.prototype.getLocalTextPrefix = function () { return Setup.TrainersRow.localTextPrefix; };
            TrainersEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], TrainersEditor);
            return TrainersEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.TrainersEditor = TrainersEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersEditorDialog = /** @class */ (function (_super) {
            __extends(TrainersEditorDialog, _super);
            function TrainersEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.TrainersForm(_this.idPrefix);
                return _this;
            }
            TrainersEditorDialog.prototype.getFormKey = function () { return Setup.TrainersForm.formKey; };
            TrainersEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.TrainersRow.localTextPrefix; };
            TrainersEditorDialog.prototype.getNameProperty = function () { return Setup.TrainersRow.nameProperty; };
            TrainersEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TrainersEditorDialog);
            return TrainersEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.TrainersEditorDialog = TrainersEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersGrid = /** @class */ (function (_super) {
            __extends(TrainersGrid, _super);
            function TrainersGrid(container) {
                return _super.call(this, container) || this;
            }
            TrainersGrid.prototype.getColumnsKey = function () { return 'Setup.Trainers'; };
            TrainersGrid.prototype.getDialogType = function () { return Setup.TrainersDialog; };
            TrainersGrid.prototype.getIdProperty = function () { return Setup.TrainersRow.idProperty; };
            TrainersGrid.prototype.getLocalTextPrefix = function () { return Setup.TrainersRow.localTextPrefix; };
            TrainersGrid.prototype.getService = function () { return Setup.TrainersService.baseUrl; };
            TrainersGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TrainersGrid);
            return TrainersGrid;
        }(Serenity.EntityGrid));
        Setup.TrainersGrid = TrainersGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListDialog = /** @class */ (function (_super) {
            __extends(TrainersListDialog, _super);
            function TrainersListDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.TrainersListForm(_this.idPrefix);
                return _this;
            }
            TrainersListDialog.prototype.getFormKey = function () { return Setup.TrainersListForm.formKey; };
            TrainersListDialog.prototype.getIdProperty = function () { return Setup.TrainersListRow.idProperty; };
            TrainersListDialog.prototype.getLocalTextPrefix = function () { return Setup.TrainersListRow.localTextPrefix; };
            TrainersListDialog.prototype.getService = function () { return Setup.TrainersListService.baseUrl; };
            TrainersListDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TrainersListDialog);
            return TrainersListDialog;
        }(Serenity.EntityDialog));
        Setup.TrainersListDialog = TrainersListDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListEditor = /** @class */ (function (_super) {
            __extends(TrainersListEditor, _super);
            function TrainersListEditor(container) {
                return _super.call(this, container) || this;
            }
            TrainersListEditor.prototype.getColumnsKey = function () { return 'Setup.TrainersList'; };
            TrainersListEditor.prototype.getDialogType = function () { return Setup.TrainersListEditorDialog; };
            TrainersListEditor.prototype.getLocalTextPrefix = function () { return Setup.TrainersListRow.localTextPrefix; };
            TrainersListEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], TrainersListEditor);
            return TrainersListEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.TrainersListEditor = TrainersListEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListEditorDialog = /** @class */ (function (_super) {
            __extends(TrainersListEditorDialog, _super);
            function TrainersListEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.TrainersListForm(_this.idPrefix);
                return _this;
            }
            TrainersListEditorDialog.prototype.getFormKey = function () { return Setup.TrainersListForm.formKey; };
            TrainersListEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.TrainersListRow.localTextPrefix; };
            TrainersListEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TrainersListEditorDialog);
            return TrainersListEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.TrainersListEditorDialog = TrainersListEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListGrid = /** @class */ (function (_super) {
            __extends(TrainersListGrid, _super);
            function TrainersListGrid(container) {
                return _super.call(this, container) || this;
            }
            TrainersListGrid.prototype.getColumnsKey = function () { return 'Setup.TrainersList'; };
            TrainersListGrid.prototype.getDialogType = function () { return Setup.TrainersListDialog; };
            TrainersListGrid.prototype.getIdProperty = function () { return Setup.TrainersListRow.idProperty; };
            TrainersListGrid.prototype.getLocalTextPrefix = function () { return Setup.TrainersListRow.localTextPrefix; };
            TrainersListGrid.prototype.getService = function () { return Setup.TrainersListService.baseUrl; };
            TrainersListGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TrainersListGrid);
            return TrainersListGrid;
        }(Serenity.EntityGrid));
        Setup.TrainersListGrid = TrainersListGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupDialog = /** @class */ (function (_super) {
            __extends(YesNoAnswerLkupDialog, _super);
            function YesNoAnswerLkupDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.YesNoAnswerLkupForm(_this.idPrefix);
                return _this;
            }
            YesNoAnswerLkupDialog.prototype.getFormKey = function () { return Setup.YesNoAnswerLkupForm.formKey; };
            YesNoAnswerLkupDialog.prototype.getIdProperty = function () { return Setup.YesNoAnswerLkupRow.idProperty; };
            YesNoAnswerLkupDialog.prototype.getLocalTextPrefix = function () { return Setup.YesNoAnswerLkupRow.localTextPrefix; };
            YesNoAnswerLkupDialog.prototype.getNameProperty = function () { return Setup.YesNoAnswerLkupRow.nameProperty; };
            YesNoAnswerLkupDialog.prototype.getService = function () { return Setup.YesNoAnswerLkupService.baseUrl; };
            YesNoAnswerLkupDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], YesNoAnswerLkupDialog);
            return YesNoAnswerLkupDialog;
        }(Serenity.EntityDialog));
        Setup.YesNoAnswerLkupDialog = YesNoAnswerLkupDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupEditor = /** @class */ (function (_super) {
            __extends(YesNoAnswerLkupEditor, _super);
            function YesNoAnswerLkupEditor(container) {
                return _super.call(this, container) || this;
            }
            YesNoAnswerLkupEditor.prototype.getColumnsKey = function () { return 'Setup.YesNoAnswerLkup'; };
            YesNoAnswerLkupEditor.prototype.getDialogType = function () { return Setup.YesNoAnswerLkupEditorDialog; };
            YesNoAnswerLkupEditor.prototype.getLocalTextPrefix = function () { return Setup.YesNoAnswerLkupRow.localTextPrefix; };
            YesNoAnswerLkupEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], YesNoAnswerLkupEditor);
            return YesNoAnswerLkupEditor;
        }(Serene2.Common.GridEditorBase));
        Setup.YesNoAnswerLkupEditor = YesNoAnswerLkupEditor;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupEditorDialog = /** @class */ (function (_super) {
            __extends(YesNoAnswerLkupEditorDialog, _super);
            function YesNoAnswerLkupEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Setup.YesNoAnswerLkupForm(_this.idPrefix);
                return _this;
            }
            YesNoAnswerLkupEditorDialog.prototype.getFormKey = function () { return Setup.YesNoAnswerLkupForm.formKey; };
            YesNoAnswerLkupEditorDialog.prototype.getLocalTextPrefix = function () { return Setup.YesNoAnswerLkupRow.localTextPrefix; };
            YesNoAnswerLkupEditorDialog.prototype.getNameProperty = function () { return Setup.YesNoAnswerLkupRow.nameProperty; };
            YesNoAnswerLkupEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], YesNoAnswerLkupEditorDialog);
            return YesNoAnswerLkupEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        Setup.YesNoAnswerLkupEditorDialog = YesNoAnswerLkupEditorDialog;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupGrid = /** @class */ (function (_super) {
            __extends(YesNoAnswerLkupGrid, _super);
            function YesNoAnswerLkupGrid(container) {
                return _super.call(this, container) || this;
            }
            YesNoAnswerLkupGrid.prototype.getColumnsKey = function () { return 'Setup.YesNoAnswerLkup'; };
            YesNoAnswerLkupGrid.prototype.getDialogType = function () { return Setup.YesNoAnswerLkupDialog; };
            YesNoAnswerLkupGrid.prototype.getIdProperty = function () { return Setup.YesNoAnswerLkupRow.idProperty; };
            YesNoAnswerLkupGrid.prototype.getLocalTextPrefix = function () { return Setup.YesNoAnswerLkupRow.localTextPrefix; };
            YesNoAnswerLkupGrid.prototype.getService = function () { return Setup.YesNoAnswerLkupService.baseUrl; };
            YesNoAnswerLkupGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], YesNoAnswerLkupGrid);
            return YesNoAnswerLkupGrid;
        }(Serenity.EntityGrid));
        Setup.YesNoAnswerLkupGrid = YesNoAnswerLkupGrid;
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListDialog = /** @class */ (function (_super) {
            __extends(SportsCoachListDialog, _super);
            function SportsCoachListDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new SportsTrainerList.SportsCoachListForm(_this.idPrefix);
                return _this;
            }
            SportsCoachListDialog.prototype.getFormKey = function () { return SportsTrainerList.SportsCoachListForm.formKey; };
            SportsCoachListDialog.prototype.getIdProperty = function () { return SportsTrainerList.SportsCoachListRow.idProperty; };
            SportsCoachListDialog.prototype.getLocalTextPrefix = function () { return SportsTrainerList.SportsCoachListRow.localTextPrefix; };
            SportsCoachListDialog.prototype.getService = function () { return SportsTrainerList.SportsCoachListService.baseUrl; };
            SportsCoachListDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SportsCoachListDialog);
            return SportsCoachListDialog;
        }(Serenity.EntityDialog));
        SportsTrainerList.SportsCoachListDialog = SportsCoachListDialog;
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListEditor = /** @class */ (function (_super) {
            __extends(SportsCoachListEditor, _super);
            function SportsCoachListEditor(container) {
                return _super.call(this, container) || this;
            }
            SportsCoachListEditor.prototype.getColumnsKey = function () { return 'SportsTrainerList.SportsCoachList'; };
            SportsCoachListEditor.prototype.getDialogType = function () { return SportsTrainerList.SportsCoachListEditorDialog; };
            SportsCoachListEditor.prototype.getLocalTextPrefix = function () { return SportsTrainerList.SportsCoachListRow.localTextPrefix; };
            SportsCoachListEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], SportsCoachListEditor);
            return SportsCoachListEditor;
        }(Serene2.Common.GridEditorBase));
        SportsTrainerList.SportsCoachListEditor = SportsCoachListEditor;
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListEditorDialog = /** @class */ (function (_super) {
            __extends(SportsCoachListEditorDialog, _super);
            function SportsCoachListEditorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new SportsTrainerList.SportsCoachListForm(_this.idPrefix);
                return _this;
            }
            SportsCoachListEditorDialog.prototype.getFormKey = function () { return SportsTrainerList.SportsCoachListForm.formKey; };
            SportsCoachListEditorDialog.prototype.getLocalTextPrefix = function () { return SportsTrainerList.SportsCoachListRow.localTextPrefix; };
            SportsCoachListEditorDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SportsCoachListEditorDialog);
            return SportsCoachListEditorDialog;
        }(Serene2.Common.GridEditorDialog));
        SportsTrainerList.SportsCoachListEditorDialog = SportsCoachListEditorDialog;
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListGrid = /** @class */ (function (_super) {
            __extends(SportsCoachListGrid, _super);
            function SportsCoachListGrid(container) {
                return _super.call(this, container) || this;
            }
            SportsCoachListGrid.prototype.getColumnsKey = function () { return 'SportsTrainerList.SportsCoachList'; };
            SportsCoachListGrid.prototype.getDialogType = function () { return SportsTrainerList.SportsCoachListDialog; };
            SportsCoachListGrid.prototype.getIdProperty = function () { return SportsTrainerList.SportsCoachListRow.idProperty; };
            SportsCoachListGrid.prototype.getLocalTextPrefix = function () { return SportsTrainerList.SportsCoachListRow.localTextPrefix; };
            SportsCoachListGrid.prototype.getService = function () { return SportsTrainerList.SportsCoachListService.baseUrl; };
            SportsCoachListGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SportsCoachListGrid);
            return SportsCoachListGrid;
        }(Serenity.EntityGrid));
        SportsTrainerList.SportsCoachListGrid = SportsCoachListGrid;
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Authorization;
    (function (Authorization) {
        Object.defineProperty(Authorization, 'userDefinition', {
            get: function () {
                return Q.getRemoteData('UserData');
            }
        });
        function hasPermission(permissionKey) {
            var ud = Authorization.userDefinition;
            return ud.Username === 'admin' || !!ud.Permissions[permissionKey];
        }
        Authorization.hasPermission = hasPermission;
    })(Authorization = Serene2.Authorization || (Serene2.Authorization = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var LanguageForm = /** @class */ (function (_super) {
            __extends(LanguageForm, _super);
            function LanguageForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LanguageForm.formKey = 'Administration.Language';
            return LanguageForm;
        }(Serenity.PrefixedContext));
        Administration.LanguageForm = LanguageForm;
        [['LanguageId', function () { return Serenity.StringEditor; }], ['LanguageName', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(LanguageForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var LanguageRow;
        (function (LanguageRow) {
            LanguageRow.idProperty = 'Id';
            LanguageRow.nameProperty = 'LanguageName';
            LanguageRow.localTextPrefix = 'Administration.Language';
            LanguageRow.lookupKey = 'Administration.Language';
            function getLookup() {
                return Q.getLookup('Administration.Language');
            }
            LanguageRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = LanguageRow.Fields || (LanguageRow.Fields = {}));
            ['Id', 'LanguageId', 'LanguageName'].forEach(function (x) { return Fields[x] = x; });
        })(LanguageRow = Administration.LanguageRow || (Administration.LanguageRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var LanguageService;
        (function (LanguageService) {
            LanguageService.baseUrl = 'Administration/Language';
            var Methods;
            (function (Methods) {
            })(Methods = LanguageService.Methods || (LanguageService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                LanguageService[x] = function (r, s, o) { return Q.serviceRequest(LanguageService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = LanguageService.baseUrl + '/' + x;
            });
        })(LanguageService = Administration.LanguageService || (Administration.LanguageService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleForm = /** @class */ (function (_super) {
            __extends(RoleForm, _super);
            function RoleForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            RoleForm.formKey = 'Administration.Role';
            return RoleForm;
        }(Serenity.PrefixedContext));
        Administration.RoleForm = RoleForm;
        [['RoleName', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(RoleForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RolePermissionRow;
        (function (RolePermissionRow) {
            RolePermissionRow.idProperty = 'RolePermissionId';
            RolePermissionRow.nameProperty = 'PermissionKey';
            RolePermissionRow.localTextPrefix = 'Administration.RolePermission';
            var Fields;
            (function (Fields) {
            })(Fields = RolePermissionRow.Fields || (RolePermissionRow.Fields = {}));
            ['RolePermissionId', 'RoleId', 'PermissionKey', 'RoleRoleName'].forEach(function (x) { return Fields[x] = x; });
        })(RolePermissionRow = Administration.RolePermissionRow || (Administration.RolePermissionRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RolePermissionService;
        (function (RolePermissionService) {
            RolePermissionService.baseUrl = 'Administration/RolePermission';
            var Methods;
            (function (Methods) {
            })(Methods = RolePermissionService.Methods || (RolePermissionService.Methods = {}));
            ['Update', 'List'].forEach(function (x) {
                RolePermissionService[x] = function (r, s, o) { return Q.serviceRequest(RolePermissionService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = RolePermissionService.baseUrl + '/' + x;
            });
        })(RolePermissionService = Administration.RolePermissionService || (Administration.RolePermissionService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleRow;
        (function (RoleRow) {
            RoleRow.idProperty = 'RoleId';
            RoleRow.nameProperty = 'RoleName';
            RoleRow.localTextPrefix = 'Administration.Role';
            RoleRow.lookupKey = 'Administration.Role';
            function getLookup() {
                return Q.getLookup('Administration.Role');
            }
            RoleRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = RoleRow.Fields || (RoleRow.Fields = {}));
            ['RoleId', 'RoleName'].forEach(function (x) { return Fields[x] = x; });
        })(RoleRow = Administration.RoleRow || (Administration.RoleRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var RoleService;
        (function (RoleService) {
            RoleService.baseUrl = 'Administration/Role';
            var Methods;
            (function (Methods) {
            })(Methods = RoleService.Methods || (RoleService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                RoleService[x] = function (r, s, o) { return Q.serviceRequest(RoleService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = RoleService.baseUrl + '/' + x;
            });
        })(RoleService = Administration.RoleService || (Administration.RoleService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var TranslationService;
        (function (TranslationService) {
            TranslationService.baseUrl = 'Administration/Translation';
            var Methods;
            (function (Methods) {
            })(Methods = TranslationService.Methods || (TranslationService.Methods = {}));
            ['List', 'Update'].forEach(function (x) {
                TranslationService[x] = function (r, s, o) { return Q.serviceRequest(TranslationService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = TranslationService.baseUrl + '/' + x;
            });
        })(TranslationService = Administration.TranslationService || (Administration.TranslationService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserForm = /** @class */ (function (_super) {
            __extends(UserForm, _super);
            function UserForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            UserForm.formKey = 'Administration.User';
            return UserForm;
        }(Serenity.PrefixedContext));
        Administration.UserForm = UserForm;
        [['Username', function () { return Serenity.StringEditor; }], ['DisplayName', function () { return Serenity.StringEditor; }], ['isAthlete', function () { return Serenity.BooleanEditor; }], ['isLive', function () { return Serenity.BooleanEditor; }], ['Paid', function () { return Serenity.BooleanEditor; }], ['Email', function () { return Serenity.EmailEditor; }], ['UserImage', function () { return Serenity.ImageUploadEditor; }], ['Password', function () { return Serenity.PasswordEditor; }], ['PasswordConfirm', function () { return Serenity.PasswordEditor; }], ['Source', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(UserForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserPermissionRow;
        (function (UserPermissionRow) {
            UserPermissionRow.idProperty = 'UserPermissionId';
            UserPermissionRow.nameProperty = 'PermissionKey';
            UserPermissionRow.localTextPrefix = 'Administration.UserPermission';
            var Fields;
            (function (Fields) {
            })(Fields = UserPermissionRow.Fields || (UserPermissionRow.Fields = {}));
            ['UserPermissionId', 'UserId', 'PermissionKey', 'Granted', 'Username', 'User'].forEach(function (x) { return Fields[x] = x; });
        })(UserPermissionRow = Administration.UserPermissionRow || (Administration.UserPermissionRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserPermissionService;
        (function (UserPermissionService) {
            UserPermissionService.baseUrl = 'Administration/UserPermission';
            var Methods;
            (function (Methods) {
            })(Methods = UserPermissionService.Methods || (UserPermissionService.Methods = {}));
            ['Update', 'List', 'ListRolePermissions', 'ListPermissionKeys'].forEach(function (x) {
                UserPermissionService[x] = function (r, s, o) { return Q.serviceRequest(UserPermissionService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = UserPermissionService.baseUrl + '/' + x;
            });
        })(UserPermissionService = Administration.UserPermissionService || (Administration.UserPermissionService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserRoleRow;
        (function (UserRoleRow) {
            UserRoleRow.idProperty = 'UserRoleId';
            UserRoleRow.localTextPrefix = 'Administration.UserRole';
            var Fields;
            (function (Fields) {
            })(Fields = UserRoleRow.Fields || (UserRoleRow.Fields = {}));
            ['UserRoleId', 'UserId', 'RoleId', 'Username', 'User', 'RoleName'].forEach(function (x) { return Fields[x] = x; });
        })(UserRoleRow = Administration.UserRoleRow || (Administration.UserRoleRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserRoleService;
        (function (UserRoleService) {
            UserRoleService.baseUrl = 'Administration/UserRole';
            var Methods;
            (function (Methods) {
            })(Methods = UserRoleService.Methods || (UserRoleService.Methods = {}));
            ['Update', 'List'].forEach(function (x) {
                UserRoleService[x] = function (r, s, o) { return Q.serviceRequest(UserRoleService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = UserRoleService.baseUrl + '/' + x;
            });
        })(UserRoleService = Administration.UserRoleService || (Administration.UserRoleService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserRow;
        (function (UserRow) {
            UserRow.idProperty = 'UserId';
            UserRow.isActiveProperty = 'IsActive';
            UserRow.nameProperty = 'Username';
            UserRow.localTextPrefix = 'Administration.User';
            UserRow.lookupKey = 'Administration.User';
            function getLookup() {
                return Q.getLookup('Administration.User');
            }
            UserRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = UserRow.Fields || (UserRow.Fields = {}));
            ['UserId', 'Username', 'Source', 'PasswordHash', 'PasswordSalt', 'DisplayName', 'Email', 'UserImage', 'LastDirectoryUpdate', 'IsActive', 'Password', 'PasswordConfirm', 'isAthlete', 'isLive', 'Sport', 'Paid', 'InsertUserId', 'InsertDate', 'UpdateUserId', 'UpdateDate'].forEach(function (x) { return Fields[x] = x; });
        })(UserRow = Administration.UserRow || (Administration.UserRow = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Administration;
    (function (Administration) {
        var UserService;
        (function (UserService) {
            UserService.baseUrl = 'Administration/User';
            var Methods;
            (function (Methods) {
            })(Methods = UserService.Methods || (UserService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Undelete', 'Retrieve', 'List', 'GetUserData'].forEach(function (x) {
                UserService[x] = function (r, s, o) { return Q.serviceRequest(UserService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = UserService.baseUrl + '/' + x;
            });
        })(UserService = Administration.UserService || (Administration.UserService = {}));
    })(Administration = Serene2.Administration || (Serene2.Administration = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingForm = /** @class */ (function (_super) {
            __extends(WellBeingForm, _super);
            function WellBeingForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            WellBeingForm.formKey = 'Assessment.WellBeing';
            return WellBeingForm;
        }(Serenity.PrefixedContext));
        Assessment.WellBeingForm = WellBeingForm;
        [['WeightKg', function () { return Serenity.DecimalEditor; }], ['AthleteId', function () { return Serenity.IntegerEditor; }], ['dateof', function () { return Serenity.DateEditor; }], ['InjStatus', function () { return Serenity.LookupEditor; }], ['Sleep', function () { return Serenity.LookupEditor; }], ['ChangeDailyRoutine', function () { return Serenity.LookupEditor; }], ['SoughtMedicalAttention', function () { return Serenity.LookupEditor; }], ['TightnessPain', function () { return Serenity.LookupEditor; }], ['AcademicStress', function () { return Serenity.LookupEditor; }]].forEach(function (x) { return Object.defineProperty(WellBeingForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingRow;
        (function (WellBeingRow) {
            WellBeingRow.idProperty = 'Id';
            WellBeingRow.localTextPrefix = 'Assessment.WellBeing';
            WellBeingRow.lookupKey = 'WebApp.Assessment';
            function getLookup() {
                return Q.getLookup('WebApp.Assessment');
            }
            WellBeingRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = WellBeingRow.Fields || (WellBeingRow.Fields = {}));
            ['Id', 'AthleteId', 'InjStatus', 'WeightKg', 'Sleep', 'ChangeDailyRoutine', 'SoughtMedicalAttention', 'TightnessPain', 'AcademicStress', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'InjStatusAnswer', 'SleepAnswer', 'ChangeDailyRoutineAnswer', 'SoughtMedicalAttentionAnswer', 'TightnessPainAnswer', 'AcademicStressAnswer', 'DisplayName', 'dateof', 'dateofdisplay', 'Status', 'Sport', 'Trainer', 'Year'].forEach(function (x) { return Fields[x] = x; });
        })(WellBeingRow = Assessment.WellBeingRow || (Assessment.WellBeingRow = {}));
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Assessment;
    (function (Assessment) {
        var WellBeingService;
        (function (WellBeingService) {
            WellBeingService.baseUrl = 'Assessment/WellBeing';
            var Methods;
            (function (Methods) {
            })(Methods = WellBeingService.Methods || (WellBeingService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                WellBeingService[x] = function (r, s, o) { return Q.serviceRequest(WellBeingService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = WellBeingService.baseUrl + '/' + x;
            });
        })(WellBeingService = Assessment.WellBeingService || (Assessment.WellBeingService = {}));
    })(Assessment = Serene2.Assessment || (Serene2.Assessment = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteAttendStatus;
        (function (AthleteAttendStatus) {
            AthleteAttendStatus[AthleteAttendStatus["Missing"] = 0] = "Missing";
            AthleteAttendStatus[AthleteAttendStatus["Attend"] = 1] = "Attend";
        })(AthleteAttendStatus = Athlete.AthleteAttendStatus || (Athlete.AthleteAttendStatus = {}));
        Serenity.Decorators.registerEnum(AthleteAttendStatus, 'Athlete.AthleteAttendStatus');
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteFlagStatus;
        (function (AthleteFlagStatus) {
            AthleteFlagStatus[AthleteFlagStatus["Pass"] = 0] = "Pass";
            AthleteFlagStatus[AthleteFlagStatus["Attention"] = 1] = "Attention";
        })(AthleteFlagStatus = Athlete.AthleteFlagStatus || (Athlete.AthleteFlagStatus = {}));
        Serenity.Decorators.registerEnum(AthleteFlagStatus, 'Athlete.AthleteFlagStatus');
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteForm = /** @class */ (function (_super) {
            __extends(AthleteForm, _super);
            function AthleteForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteForm.formKey = 'Athlete.Athlete';
            return AthleteForm;
        }(Serenity.PrefixedContext));
        Athlete.AthleteForm = AthleteForm;
        [['FirstName', function () { return Serenity.StringEditor; }], ['LastName', function () { return Serenity.StringEditor; }], ['SportId', function () { return Serenity.LookupEditor; }], ['positionId', function () { return Serenity.LookupEditor; }], ['YearId', function () { return Serenity.LookupEditor; }], ['Live', function () { return Serenity.BooleanEditor; }], ['Trainers', function () { return Serenity.LookupEditor; }], ['Attendence', function () { return Serenity.IntegerEditor; }], ['AttendToday', function () { return Serenity.StringEditor; }], ['Username', function () { return Serenity.StringEditor; }], ['StuId', function () { return Serenity.StringEditor; }], ['GenderId', function () { return Serenity.LookupEditor; }], ['UserImage', function () { return Serenity.ImageUploadEditor; }], ['ManualImage', function () { return Serenity.ImageUploadEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteRow;
        (function (AthleteRow) {
            AthleteRow.idProperty = 'Id';
            AthleteRow.nameProperty = 'Username';
            AthleteRow.localTextPrefix = 'Athlete.Athlete';
            AthleteRow.lookupKey = 'WebApp.Athlete';
            function getLookup() {
                return Q.getLookup('WebApp.Athlete');
            }
            AthleteRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = AthleteRow.Fields || (AthleteRow.Fields = {}));
            ['Id', 'Live', 'Username', 'StuId', 'FirstName', 'LastName', 'SportId', 'TrainerId', 'Trainers', 'GenderId', 'Sport', 'Trainer', 'Gender', 'YearId', 'Year', 'DisplayName', 'UserImage', 'Attendence', 'lastTwoAssessment', 'lastTwoAssessmentId', 'AthleteLoggedAssessmentToday', 'lastassessment', 'AttendToday', 'ManualImage', 'positionId', 'Position', 'Paid'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteRow = Athlete.AthleteRow || (Athlete.AthleteRow = {}));
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Athlete;
    (function (Athlete) {
        var AthleteService;
        (function (AthleteService) {
            AthleteService.baseUrl = 'Athlete/Athlete';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteService.Methods || (AthleteService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteService[x] = function (r, s, o) { return Q.serviceRequest(AthleteService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteService.baseUrl + '/' + x;
            });
        })(AthleteService = Athlete.AthleteService || (Athlete.AthleteService = {}));
    })(Athlete = Serene2.Athlete || (Serene2.Athlete = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayForm = /** @class */ (function (_super) {
            __extends(AthleteLoggedTodayForm, _super);
            function AthleteLoggedTodayForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteLoggedTodayForm.formKey = 'AthleteLoggedToday.AthleteLoggedToday';
            return AthleteLoggedTodayForm;
        }(Serenity.PrefixedContext));
        AthleteLoggedToday.AthleteLoggedTodayForm = AthleteLoggedTodayForm;
        [['Athleteid', function () { return Serenity.IntegerEditor; }], ['Username', function () { return Serenity.StringEditor; }], ['AthleteLoggedAssessmentToday', function () { return Serenity.IntegerEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteLoggedTodayForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayRow;
        (function (AthleteLoggedTodayRow) {
            AthleteLoggedTodayRow.idProperty = 'Id';
            AthleteLoggedTodayRow.nameProperty = 'Username';
            AthleteLoggedTodayRow.localTextPrefix = 'AthleteLoggedToday.AthleteLoggedToday';
            var Fields;
            (function (Fields) {
            })(Fields = AthleteLoggedTodayRow.Fields || (AthleteLoggedTodayRow.Fields = {}));
            ['Id', 'Athleteid', 'Username', 'AthleteLoggedAssessmentToday', 'AthleteidLive', 'AthleteidUsername', 'AthleteidStuId', 'AthleteidFirstName', 'AthleteidLastName', 'AthleteidSportId', 'AthleteidTrainerId', 'AthleteidGenderId', 'AthleteidYearId', 'recentAssessmentPass', 'attendence', 'AthleteLoggedRPEToday'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteLoggedTodayRow = AthleteLoggedToday.AthleteLoggedTodayRow || (AthleteLoggedToday.AthleteLoggedTodayRow = {}));
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteLoggedToday;
    (function (AthleteLoggedToday) {
        var AthleteLoggedTodayService;
        (function (AthleteLoggedTodayService) {
            AthleteLoggedTodayService.baseUrl = 'AthleteLoggedToday/AthleteLoggedToday';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteLoggedTodayService.Methods || (AthleteLoggedTodayService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteLoggedTodayService[x] = function (r, s, o) { return Q.serviceRequest(AthleteLoggedTodayService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteLoggedTodayService.baseUrl + '/' + x;
            });
        })(AthleteLoggedTodayService = AthleteLoggedToday.AthleteLoggedTodayService || (AthleteLoggedToday.AthleteLoggedTodayService = {}));
    })(AthleteLoggedToday = Serene2.AthleteLoggedToday || (Serene2.AthleteLoggedToday = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteForm = /** @class */ (function (_super) {
            __extends(AthleteNoteForm, _super);
            function AthleteNoteForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteNoteForm.formKey = 'AthleteNote.AthleteNote';
            return AthleteNoteForm;
        }(Serenity.PrefixedContext));
        AthleteNote.AthleteNoteForm = AthleteNoteForm;
        [['AthleteId', function () { return Serenity.IntegerEditor; }], ['EntryDate', function () { return Serenity.DateEditor; }], ['Notes', function () { return Serenity.TextAreaEditor; }], ['Username', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteNoteForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteRow;
        (function (AthleteNoteRow) {
            AthleteNoteRow.idProperty = 'Id';
            AthleteNoteRow.nameProperty = 'Notes';
            AthleteNoteRow.localTextPrefix = 'AthleteNote.AthleteNote';
            var Fields;
            (function (Fields) {
            })(Fields = AthleteNoteRow.Fields || (AthleteNoteRow.Fields = {}));
            ['Id', 'AthleteId', 'EntryDate', 'Notes', 'Username'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteNoteRow = AthleteNote.AthleteNoteRow || (AthleteNote.AthleteNoteRow = {}));
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteNote;
    (function (AthleteNote) {
        var AthleteNoteService;
        (function (AthleteNoteService) {
            AthleteNoteService.baseUrl = 'AthleteNote/AthleteNote';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteNoteService.Methods || (AthleteNoteService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteNoteService[x] = function (r, s, o) { return Q.serviceRequest(AthleteNoteService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteNoteService.baseUrl + '/' + x;
            });
        })(AthleteNoteService = AthleteNote.AthleteNoteService || (AthleteNote.AthleteNoteService = {}));
    })(AthleteNote = Serene2.AthleteNote || (Serene2.AthleteNote = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioForm = /** @class */ (function (_super) {
            __extends(AthletePhysioForm, _super);
            function AthletePhysioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthletePhysioForm.formKey = 'AthletePhysio.AthletePhysio';
            return AthletePhysioForm;
        }(Serenity.PrefixedContext));
        AthletePhysio.AthletePhysioForm = AthletePhysioForm;
        [['AthleteId', function () { return Serenity.LookupEditor; }], ['AthleteFirstName', function () { return Serenity.StringEditor; }], ['AthleteLastName', function () { return Serenity.StringEditor; }], ['EntryDate', function () { return Serenity.DateEditor; }], ['PhysioId', function () { return Serenity.LookupEditor; }], ['InjuryId', function () { return Serenity.LookupEditor; }], ['InjuryStatusId', function () { return Serenity.LookupEditor; }], ['Notes', function () { return Serenity.TextAreaEditor; }], ['PhysioPlan', function () { return Serenity.TextAreaEditor; }], ['conClinicRvw', function () { return Serenity.TextAreaEditor; }], ['history', function () { return Serenity.TextAreaEditor; }], ['previousHistory', function () { return Serenity.TextAreaEditor; }], ['objectiveAssessment', function () { return Serenity.TextAreaEditor; }], ['managementPlan', function () { return Serenity.TextAreaEditor; }]].forEach(function (x) { return Object.defineProperty(AthletePhysioForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioRow;
        (function (AthletePhysioRow) {
            AthletePhysioRow.idProperty = 'Id';
            AthletePhysioRow.nameProperty = 'Notes';
            AthletePhysioRow.localTextPrefix = 'AthletePhysio.AthletePhysio';
            AthletePhysioRow.lookupKey = 'EntryDate';
            function getLookup() {
                return Q.getLookup('EntryDate');
            }
            AthletePhysioRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = AthletePhysioRow.Fields || (AthletePhysioRow.Fields = {}));
            ['Id', 'AthleteId', 'EntryDate', 'PhysioId', 'InjuryId', 'InjuryStatusId', 'Notes', 'PhysioPlan', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'Physio', 'PhysioUsername', 'Injury', 'InjuryStatus', 'DisplayName', 'history', 'previousHistory', 'objectiveAssessment', 'managementPlan', 'Sport', 'EntryDateLkup', 'conClinicRvw'].forEach(function (x) { return Fields[x] = x; });
        })(AthletePhysioRow = AthletePhysio.AthletePhysioRow || (AthletePhysio.AthletePhysioRow = {}));
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthletePhysio;
    (function (AthletePhysio) {
        var AthletePhysioService;
        (function (AthletePhysioService) {
            AthletePhysioService.baseUrl = 'AthletePhysio/AthletePhysio';
            var Methods;
            (function (Methods) {
            })(Methods = AthletePhysioService.Methods || (AthletePhysioService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthletePhysioService[x] = function (r, s, o) { return Q.serviceRequest(AthletePhysioService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthletePhysioService.baseUrl + '/' + x;
            });
        })(AthletePhysioService = AthletePhysio.AthletePhysioService || (AthletePhysio.AthletePhysioService = {}));
    })(AthletePhysio = Serene2.AthletePhysio || (Serene2.AthletePhysio = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteTesting;
    (function (AthleteTesting) {
        var CreateTestForm = /** @class */ (function (_super) {
            __extends(CreateTestForm, _super);
            function CreateTestForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            CreateTestForm.formKey = 'AthleteTesting.CreateTest';
            return CreateTestForm;
        }(Serenity.PrefixedContext));
        AthleteTesting.CreateTestForm = CreateTestForm;
        [['Sport', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(CreateTestForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(AthleteTesting = Serene2.AthleteTesting || (Serene2.AthleteTesting = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightForm = /** @class */ (function (_super) {
            __extends(AthleteWeightForm, _super);
            function AthleteWeightForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteWeightForm.formKey = 'AthleteWeight.AthleteWeight';
            return AthleteWeightForm;
        }(Serenity.PrefixedContext));
        AthleteWeight.AthleteWeightForm = AthleteWeightForm;
        [['avgWeight', function () { return Serenity.DecimalEditor; }], ['datefor', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteWeightForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightRow;
        (function (AthleteWeightRow) {
            AthleteWeightRow.idProperty = 'id';
            AthleteWeightRow.nameProperty = 'datefor';
            AthleteWeightRow.localTextPrefix = 'AthleteWeight.AthleteWeight';
            var Fields;
            (function (Fields) {
            })(Fields = AthleteWeightRow.Fields || (AthleteWeightRow.Fields = {}));
            ['id', 'avgWeight', 'datefor'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteWeightRow = AthleteWeight.AthleteWeightRow || (AthleteWeight.AthleteWeightRow = {}));
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var AthleteWeight;
    (function (AthleteWeight) {
        var AthleteWeightService;
        (function (AthleteWeightService) {
            AthleteWeightService.baseUrl = 'AthleteWeight/AthleteWeight';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteWeightService.Methods || (AthleteWeightService.Methods = {}));
            ['List'].forEach(function (x) {
                AthleteWeightService[x] = function (r, s, o) { return Q.serviceRequest(AthleteWeightService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteWeightService.baseUrl + '/' + x;
            });
        })(AthleteWeightService = AthleteWeight.AthleteWeightService || (AthleteWeight.AthleteWeightService = {}));
    })(AthleteWeight = Serene2.AthleteWeight || (Serene2.AthleteWeight = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogForm = /** @class */ (function (_super) {
            __extends(AuditLogForm, _super);
            function AuditLogForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AuditLogForm.formKey = 'Audit.AuditLog';
            return AuditLogForm;
        }(Serenity.PrefixedContext));
        Audit.AuditLogForm = AuditLogForm;
        [['Time', function () { return Serenity.StringEditor; }], ['DisplayName', function () { return Serenity.StringEditor; }], ['Action', function () { return Serenity.StringEditor; }], ['ChangesDisplayForm', function () { return Serenity.TextAreaEditor; }]].forEach(function (x) { return Object.defineProperty(AuditLogForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogRow;
        (function (AuditLogRow) {
            AuditLogRow.idProperty = 'Id';
            AuditLogRow.nameProperty = 'UserName';
            AuditLogRow.localTextPrefix = 'Audit.AuditLog';
            var Fields;
            (function (Fields) {
            })(Fields = AuditLogRow.Fields || (AuditLogRow.Fields = {}));
            ['Id', 'UserId', 'UserName', 'Action', 'ChangedOn', 'TableName', 'RowId', 'Module', 'Page', 'Changes', 'ChangesDisplayForm', 'DisplayName', 'Time'].forEach(function (x) { return Fields[x] = x; });
        })(AuditLogRow = Audit.AuditLogRow || (Audit.AuditLogRow = {}));
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Audit;
    (function (Audit) {
        var AuditLogService;
        (function (AuditLogService) {
            AuditLogService.baseUrl = 'Audit/AuditLog';
            var Methods;
            (function (Methods) {
            })(Methods = AuditLogService.Methods || (AuditLogService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AuditLogService[x] = function (r, s, o) { return Q.serviceRequest(AuditLogService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AuditLogService.baseUrl + '/' + x;
            });
        })(AuditLogService = Audit.AuditLogService || (Audit.AuditLogService = {}));
    })(Audit = Serene2.Audit || (Serene2.Audit = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var UserPreferenceRow;
        (function (UserPreferenceRow) {
            UserPreferenceRow.idProperty = 'UserPreferenceId';
            UserPreferenceRow.nameProperty = 'Name';
            UserPreferenceRow.localTextPrefix = 'Common.UserPreference';
            var Fields;
            (function (Fields) {
            })(Fields = UserPreferenceRow.Fields || (UserPreferenceRow.Fields = {}));
            ['UserPreferenceId', 'UserId', 'PreferenceType', 'Name', 'Value'].forEach(function (x) { return Fields[x] = x; });
        })(UserPreferenceRow = Common.UserPreferenceRow || (Common.UserPreferenceRow = {}));
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Common;
    (function (Common) {
        var UserPreferenceService;
        (function (UserPreferenceService) {
            UserPreferenceService.baseUrl = 'Common/UserPreference';
            var Methods;
            (function (Methods) {
            })(Methods = UserPreferenceService.Methods || (UserPreferenceService.Methods = {}));
            ['Update', 'Retrieve'].forEach(function (x) {
                UserPreferenceService[x] = function (r, s, o) { return Q.serviceRequest(UserPreferenceService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = UserPreferenceService.baseUrl + '/' + x;
            });
        })(UserPreferenceService = Common.UserPreferenceService || (Common.UserPreferenceService = {}));
    })(Common = Serene2.Common || (Serene2.Common = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ChangePasswordForm = /** @class */ (function (_super) {
            __extends(ChangePasswordForm, _super);
            function ChangePasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ChangePasswordForm.formKey = 'Membership.ChangePassword';
            return ChangePasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ChangePasswordForm = ChangePasswordForm;
        [['OldPassword', function () { return Serenity.PasswordEditor; }], ['NewPassword', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(ChangePasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordForm = /** @class */ (function (_super) {
            __extends(ForgotPasswordForm, _super);
            function ForgotPasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ForgotPasswordForm.formKey = 'Membership.ForgotPassword';
            return ForgotPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ForgotPasswordForm = ForgotPasswordForm;
        [['Email', function () { return Serenity.EmailEditor; }]].forEach(function (x) { return Object.defineProperty(ForgotPasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var LoginForm = /** @class */ (function (_super) {
            __extends(LoginForm, _super);
            function LoginForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LoginForm.formKey = 'Membership.Login';
            return LoginForm;
        }(Serenity.PrefixedContext));
        Membership.LoginForm = LoginForm;
        [['Username', function () { return Serenity.StringEditor; }], ['Password', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(LoginForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ResetPasswordForm = /** @class */ (function (_super) {
            __extends(ResetPasswordForm, _super);
            function ResetPasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ResetPasswordForm.formKey = 'Membership.ResetPassword';
            return ResetPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ResetPasswordForm = ResetPasswordForm;
        [['NewPassword', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(ResetPasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var SignUpForm = /** @class */ (function (_super) {
            __extends(SignUpForm, _super);
            function SignUpForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SignUpForm.formKey = 'Membership.SignUp';
            return SignUpForm;
        }(Serenity.PrefixedContext));
        Membership.SignUpForm = SignUpForm;
        [['DisplayName', function () { return Serenity.StringEditor; }], ['Email', function () { return Serenity.EmailEditor; }], ['ConfirmEmail', function () { return Serenity.EmailEditor; }], ['Password', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(SignUpForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeForm = /** @class */ (function (_super) {
            __extends(RpeForm, _super);
            function RpeForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            RpeForm.formKey = 'RPE.Rpe';
            return RpeForm;
        }(Serenity.PrefixedContext));
        RPE.RpeForm = RpeForm;
        [['AthleteId', function () { return Serenity.IntegerEditor; }], ['RpEvAlue', function () { return Serenity.IntegerEditor; }], ['Daterecorded', function () { return Serenity.DateEditor; }], ['AthleteUsername', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(RpeForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeRow;
        (function (RpeRow) {
            RpeRow.idProperty = 'Id';
            RpeRow.localTextPrefix = 'RPE.Rpe';
            var Fields;
            (function (Fields) {
            })(Fields = RpeRow.Fields || (RpeRow.Fields = {}));
            ['Id', 'AthleteId', 'RpEvAlue', 'Daterecorded', 'AthleteUsername'].forEach(function (x) { return Fields[x] = x; });
        })(RpeRow = RPE.RpeRow || (RPE.RpeRow = {}));
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var RPE;
    (function (RPE) {
        var RpeService;
        (function (RpeService) {
            RpeService.baseUrl = 'RPE/Rpe';
            var Methods;
            (function (Methods) {
            })(Methods = RpeService.Methods || (RpeService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                RpeService[x] = function (r, s, o) { return Q.serviceRequest(RpeService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = RpeService.baseUrl + '/' + x;
            });
        })(RpeService = RPE.RpeService || (RPE.RpeService = {}));
    })(RPE = Serene2.RPE || (Serene2.RPE = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListForm = /** @class */ (function (_super) {
            __extends(SandCoachListForm, _super);
            function SandCoachListForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SandCoachListForm.formKey = 'SandCTrainerList.SandCoachList';
            return SandCoachListForm;
        }(Serenity.PrefixedContext));
        SandCTrainerList.SandCoachListForm = SandCoachListForm;
        [['SportId', function () { return Serenity.IntegerEditor; }], ['TrainerId', function () { return Serenity.IntegerEditor; }]].forEach(function (x) { return Object.defineProperty(SandCoachListForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListRow;
        (function (SandCoachListRow) {
            SandCoachListRow.idProperty = 'Id';
            SandCoachListRow.localTextPrefix = 'SandCTrainerList.SandCoachList';
            var Fields;
            (function (Fields) {
            })(Fields = SandCoachListRow.Fields || (SandCoachListRow.Fields = {}));
            ['Id', 'SportId', 'TrainerId', 'Sport', 'SportSessionsPerWeek', 'SportSandccoachId', 'SportScoachId'].forEach(function (x) { return Fields[x] = x; });
        })(SandCoachListRow = SandCTrainerList.SandCoachListRow || (SandCTrainerList.SandCoachListRow = {}));
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SandCTrainerList;
    (function (SandCTrainerList) {
        var SandCoachListService;
        (function (SandCoachListService) {
            SandCoachListService.baseUrl = 'SandCTrainerList/SandCoachList';
            var Methods;
            (function (Methods) {
            })(Methods = SandCoachListService.Methods || (SandCoachListService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                SandCoachListService[x] = function (r, s, o) { return Q.serviceRequest(SandCoachListService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = SandCoachListService.baseUrl + '/' + x;
            });
        })(SandCoachListService = SandCTrainerList.SandCoachListService || (SandCTrainerList.SandCoachListService = {}));
    })(SandCTrainerList = Serene2.SandCTrainerList || (Serene2.SandCTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionForm = /** @class */ (function (_super) {
            __extends(AthletePositionForm, _super);
            function AthletePositionForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthletePositionForm.formKey = 'Setup.AthletePosition';
            return AthletePositionForm;
        }(Serenity.PrefixedContext));
        Setup.AthletePositionForm = AthletePositionForm;
        [['Position', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(AthletePositionForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionRow;
        (function (AthletePositionRow) {
            AthletePositionRow.idProperty = 'Id';
            AthletePositionRow.nameProperty = 'Position';
            AthletePositionRow.localTextPrefix = 'Setup.AthletePosition';
            AthletePositionRow.lookupKey = 'WebApp.Position';
            function getLookup() {
                return Q.getLookup('WebApp.Position');
            }
            AthletePositionRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = AthletePositionRow.Fields || (AthletePositionRow.Fields = {}));
            ['Id', 'Position'].forEach(function (x) { return Fields[x] = x; });
        })(AthletePositionRow = Setup.AthletePositionRow || (Setup.AthletePositionRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthletePositionService;
        (function (AthletePositionService) {
            AthletePositionService.baseUrl = 'Setup/AthletePosition';
            var Methods;
            (function (Methods) {
            })(Methods = AthletePositionService.Methods || (AthletePositionService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthletePositionService[x] = function (r, s, o) { return Q.serviceRequest(AthletePositionService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthletePositionService.baseUrl + '/' + x;
            });
        })(AthletePositionService = Setup.AthletePositionService || (Setup.AthletePositionService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingForm = /** @class */ (function (_super) {
            __extends(AthleteTestingForm, _super);
            function AthleteTestingForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteTestingForm.formKey = 'Setup.AthleteTesting';
            return AthleteTestingForm;
        }(Serenity.PrefixedContext));
        Setup.AthleteTestingForm = AthleteTestingForm;
        [['DisplayName', function () { return Serenity.StringEditor; }], ['Test1Label', function () { return Serenity.StringEditor; }], ['Test1Result', function () { return Serenity.DecimalEditor; }], ['Test2Label', function () { return Serenity.StringEditor; }], ['Test2Result', function () { return Serenity.DecimalEditor; }], ['Test3Label', function () { return Serenity.StringEditor; }], ['Test3Result', function () { return Serenity.DecimalEditor; }], ['Test4Label', function () { return Serenity.StringEditor; }], ['Test4Result', function () { return Serenity.DecimalEditor; }], ['Test5Label', function () { return Serenity.StringEditor; }], ['Test5Result', function () { return Serenity.DecimalEditor; }], ['Sport', function () { return Serenity.StringEditor; }], ['TermDisplay', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteTestingForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpForm = /** @class */ (function (_super) {
            __extends(AthleteTestingLkUpForm, _super);
            function AthleteTestingLkUpForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteTestingLkUpForm.formKey = 'Setup.AthleteTestingLkUp';
            return AthleteTestingLkUpForm;
        }(Serenity.PrefixedContext));
        Setup.AthleteTestingLkUpForm = AthleteTestingLkUpForm;
        [['Test1Label', function () { return Serenity.StringEditor; }], ['Test2Label', function () { return Serenity.StringEditor; }], ['Test3Label', function () { return Serenity.StringEditor; }], ['Test4Label', function () { return Serenity.StringEditor; }], ['Test5Label', function () { return Serenity.StringEditor; }], ['SportId', function () { return Serenity.LookupEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteTestingLkUpForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpRow;
        (function (AthleteTestingLkUpRow) {
            AthleteTestingLkUpRow.idProperty = 'Id';
            AthleteTestingLkUpRow.nameProperty = 'Test1Label';
            AthleteTestingLkUpRow.localTextPrefix = 'Setup.AthleteTestingLkUp';
            var Fields;
            (function (Fields) {
            })(Fields = AthleteTestingLkUpRow.Fields || (AthleteTestingLkUpRow.Fields = {}));
            ['Id', 'Test1Label', 'Test2Label', 'Test3Label', 'Test4Label', 'Test5Label', 'SportId', 'Sport'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteTestingLkUpRow = Setup.AthleteTestingLkUpRow || (Setup.AthleteTestingLkUpRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingLkUpService;
        (function (AthleteTestingLkUpService) {
            AthleteTestingLkUpService.baseUrl = 'Setup/AthleteTestingLkUp';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteTestingLkUpService.Methods || (AthleteTestingLkUpService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteTestingLkUpService[x] = function (r, s, o) { return Q.serviceRequest(AthleteTestingLkUpService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteTestingLkUpService.baseUrl + '/' + x;
            });
        })(AthleteTestingLkUpService = Setup.AthleteTestingLkUpService || (Setup.AthleteTestingLkUpService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingRow;
        (function (AthleteTestingRow) {
            AthleteTestingRow.idProperty = 'Id';
            AthleteTestingRow.nameProperty = 'Test1Label';
            AthleteTestingRow.localTextPrefix = 'Setup.AthleteTesting';
            AthleteTestingRow.lookupKey = 'WebApp.AthleteTesting';
            function getLookup() {
                return Q.getLookup('WebApp.AthleteTesting');
            }
            AthleteTestingRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = AthleteTestingRow.Fields || (AthleteTestingRow.Fields = {}));
            ['Id', 'AthleteId', 'Test1Label', 'Test1Result', 'Test2Label', 'Test2Result', 'Test3Label', 'Test3Result', 'Test4Label', 'Test4Result', 'Test5Label', 'Test5Result', 'OnDate', 'SportId', 'AthleteLive', 'AthleteUsername', 'AthleteStuId', 'AthleteFirstName', 'AthleteLastName', 'AthleteSportId', 'AthleteTrainerId', 'AthleteGenderId', 'AthleteYearId', 'AthleteUserImage', 'AthleteManualImage', 'AthletePositionId', 'Sport', 'SportSessionsPerWeek', 'DisplayName', 'TermDisplay', 'testID'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteTestingRow = Setup.AthleteTestingRow || (Setup.AthleteTestingRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteTestingService;
        (function (AthleteTestingService) {
            AthleteTestingService.baseUrl = 'Setup/AthleteTesting';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteTestingService.Methods || (AthleteTestingService.Methods = {}));
            ['ExecuteCreateTest', 'Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteTestingService[x] = function (r, s, o) { return Q.serviceRequest(AthleteTestingService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteTestingService.baseUrl + '/' + x;
            });
        })(AthleteTestingService = Setup.AthleteTestingService || (Setup.AthleteTestingService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearForm = /** @class */ (function (_super) {
            __extends(AthleteYearForm, _super);
            function AthleteYearForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AthleteYearForm.formKey = 'Setup.AthleteYear';
            return AthleteYearForm;
        }(Serenity.PrefixedContext));
        Setup.AthleteYearForm = AthleteYearForm;
        [['Year', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(AthleteYearForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearRow;
        (function (AthleteYearRow) {
            AthleteYearRow.idProperty = 'YearId';
            AthleteYearRow.nameProperty = 'Year';
            AthleteYearRow.localTextPrefix = 'Setup.AthleteYear';
            AthleteYearRow.lookupKey = 'WebApp.Year';
            function getLookup() {
                return Q.getLookup('WebApp.Year');
            }
            AthleteYearRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = AthleteYearRow.Fields || (AthleteYearRow.Fields = {}));
            ['YearId', 'Year'].forEach(function (x) { return Fields[x] = x; });
        })(AthleteYearRow = Setup.AthleteYearRow || (Setup.AthleteYearRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var AthleteYearService;
        (function (AthleteYearService) {
            AthleteYearService.baseUrl = 'Setup/AthleteYear';
            var Methods;
            (function (Methods) {
            })(Methods = AthleteYearService.Methods || (AthleteYearService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                AthleteYearService[x] = function (r, s, o) { return Q.serviceRequest(AthleteYearService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = AthleteYearService.baseUrl + '/' + x;
            });
        })(AthleteYearService = Setup.AthleteYearService || (Setup.AthleteYearService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderForm = /** @class */ (function (_super) {
            __extends(GenderForm, _super);
            function GenderForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GenderForm.formKey = 'Setup.Gender';
            return GenderForm;
        }(Serenity.PrefixedContext));
        Setup.GenderForm = GenderForm;
        [['Gender', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(GenderForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderRow;
        (function (GenderRow) {
            GenderRow.idProperty = 'GenderId';
            GenderRow.nameProperty = 'Gender';
            GenderRow.localTextPrefix = 'Setup.Gender';
            GenderRow.lookupKey = 'WebApp.Gender';
            function getLookup() {
                return Q.getLookup('WebApp.Gender');
            }
            GenderRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = GenderRow.Fields || (GenderRow.Fields = {}));
            ['GenderId', 'Gender'].forEach(function (x) { return Fields[x] = x; });
        })(GenderRow = Setup.GenderRow || (Setup.GenderRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var GenderService;
        (function (GenderService) {
            GenderService.baseUrl = 'Setup/Gender';
            var Methods;
            (function (Methods) {
            })(Methods = GenderService.Methods || (GenderService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                GenderService[x] = function (r, s, o) { return Q.serviceRequest(GenderService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = GenderService.baseUrl + '/' + x;
            });
        })(GenderService = Setup.GenderService || (Setup.GenderService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupForm = /** @class */ (function (_super) {
            __extends(InjStatusAnswerLkupForm, _super);
            function InjStatusAnswerLkupForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InjStatusAnswerLkupForm.formKey = 'Setup.InjStatusAnswerLkup';
            return InjStatusAnswerLkupForm;
        }(Serenity.PrefixedContext));
        Setup.InjStatusAnswerLkupForm = InjStatusAnswerLkupForm;
        [['Answer', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(InjStatusAnswerLkupForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupRow;
        (function (InjStatusAnswerLkupRow) {
            InjStatusAnswerLkupRow.idProperty = 'Id';
            InjStatusAnswerLkupRow.nameProperty = 'Answer';
            InjStatusAnswerLkupRow.localTextPrefix = 'Setup.InjStatusAnswerLkup';
            InjStatusAnswerLkupRow.lookupKey = 'WebApp.injStatus';
            function getLookup() {
                return Q.getLookup('WebApp.injStatus');
            }
            InjStatusAnswerLkupRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = InjStatusAnswerLkupRow.Fields || (InjStatusAnswerLkupRow.Fields = {}));
            ['Id', 'Answer'].forEach(function (x) { return Fields[x] = x; });
        })(InjStatusAnswerLkupRow = Setup.InjStatusAnswerLkupRow || (Setup.InjStatusAnswerLkupRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjStatusAnswerLkupService;
        (function (InjStatusAnswerLkupService) {
            InjStatusAnswerLkupService.baseUrl = 'Setup/InjStatusAnswerLkup';
            var Methods;
            (function (Methods) {
            })(Methods = InjStatusAnswerLkupService.Methods || (InjStatusAnswerLkupService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                InjStatusAnswerLkupService[x] = function (r, s, o) { return Q.serviceRequest(InjStatusAnswerLkupService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = InjStatusAnswerLkupService.baseUrl + '/' + x;
            });
        })(InjStatusAnswerLkupService = Setup.InjStatusAnswerLkupService || (Setup.InjStatusAnswerLkupService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryForm = /** @class */ (function (_super) {
            __extends(InjuryForm, _super);
            function InjuryForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InjuryForm.formKey = 'Setup.Injury';
            return InjuryForm;
        }(Serenity.PrefixedContext));
        Setup.InjuryForm = InjuryForm;
        [['Injury', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(InjuryForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryRow;
        (function (InjuryRow) {
            InjuryRow.idProperty = 'Id';
            InjuryRow.nameProperty = 'Injury';
            InjuryRow.localTextPrefix = 'Setup.Injury';
            InjuryRow.lookupKey = 'WebApp.Injury';
            function getLookup() {
                return Q.getLookup('WebApp.Injury');
            }
            InjuryRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = InjuryRow.Fields || (InjuryRow.Fields = {}));
            ['Id', 'Injury'].forEach(function (x) { return Fields[x] = x; });
        })(InjuryRow = Setup.InjuryRow || (Setup.InjuryRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryService;
        (function (InjuryService) {
            InjuryService.baseUrl = 'Setup/Injury';
            var Methods;
            (function (Methods) {
            })(Methods = InjuryService.Methods || (InjuryService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                InjuryService[x] = function (r, s, o) { return Q.serviceRequest(InjuryService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = InjuryService.baseUrl + '/' + x;
            });
        })(InjuryService = Setup.InjuryService || (Setup.InjuryService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusForm = /** @class */ (function (_super) {
            __extends(InjuryStatusForm, _super);
            function InjuryStatusForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InjuryStatusForm.formKey = 'Setup.InjuryStatus';
            return InjuryStatusForm;
        }(Serenity.PrefixedContext));
        Setup.InjuryStatusForm = InjuryStatusForm;
        [['InjuryStatus', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(InjuryStatusForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusRow;
        (function (InjuryStatusRow) {
            InjuryStatusRow.idProperty = 'Id';
            InjuryStatusRow.nameProperty = 'InjuryStatus';
            InjuryStatusRow.localTextPrefix = 'Setup.InjuryStatus';
            InjuryStatusRow.lookupKey = 'WebApp.InjuryStatus';
            function getLookup() {
                return Q.getLookup('WebApp.InjuryStatus');
            }
            InjuryStatusRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = InjuryStatusRow.Fields || (InjuryStatusRow.Fields = {}));
            ['Id', 'InjuryStatus', 'rankOrd'].forEach(function (x) { return Fields[x] = x; });
        })(InjuryStatusRow = Setup.InjuryStatusRow || (Setup.InjuryStatusRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var InjuryStatusService;
        (function (InjuryStatusService) {
            InjuryStatusService.baseUrl = 'Setup/InjuryStatus';
            var Methods;
            (function (Methods) {
            })(Methods = InjuryStatusService.Methods || (InjuryStatusService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                InjuryStatusService[x] = function (r, s, o) { return Q.serviceRequest(InjuryStatusService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = InjuryStatusService.baseUrl + '/' + x;
            });
        })(InjuryStatusService = Setup.InjuryStatusService || (Setup.InjuryStatusService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioForm = /** @class */ (function (_super) {
            __extends(PhysioForm, _super);
            function PhysioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PhysioForm.formKey = 'Setup.Physio';
            return PhysioForm;
        }(Serenity.PrefixedContext));
        Setup.PhysioForm = PhysioForm;
        [['Physio', function () { return Serenity.StringEditor; }], ['Username', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(PhysioForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioRow;
        (function (PhysioRow) {
            PhysioRow.idProperty = 'Id';
            PhysioRow.nameProperty = 'Physio';
            PhysioRow.localTextPrefix = 'Setup.Physio';
            PhysioRow.lookupKey = 'WebApp.Physio';
            function getLookup() {
                return Q.getLookup('WebApp.Physio');
            }
            PhysioRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = PhysioRow.Fields || (PhysioRow.Fields = {}));
            ['Id', 'Physio', 'Username'].forEach(function (x) { return Fields[x] = x; });
        })(PhysioRow = Setup.PhysioRow || (Setup.PhysioRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var PhysioService;
        (function (PhysioService) {
            PhysioService.baseUrl = 'Setup/Physio';
            var Methods;
            (function (Methods) {
            })(Methods = PhysioService.Methods || (PhysioService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                PhysioService[x] = function (r, s, o) { return Q.serviceRequest(PhysioService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = PhysioService.baseUrl + '/' + x;
            });
        })(PhysioService = Setup.PhysioService || (Setup.PhysioService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsForm = /** @class */ (function (_super) {
            __extends(SportsForm, _super);
            function SportsForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SportsForm.formKey = 'Setup.Sports';
            return SportsForm;
        }(Serenity.PrefixedContext));
        Setup.SportsForm = SportsForm;
        [['Sport', function () { return Serenity.StringEditor; }], ['SessionsPerWeek', function () { return Serenity.IntegerEditor; }], ['Trainersport', function () { return Serenity.LookupEditor; }], ['TrainersportCoach', function () { return Serenity.LookupEditor; }]].forEach(function (x) { return Object.defineProperty(SportsForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsRow;
        (function (SportsRow) {
            SportsRow.idProperty = 'SportId';
            SportsRow.nameProperty = 'Sport';
            SportsRow.localTextPrefix = 'Setup.Sports';
            SportsRow.lookupKey = 'WebApp.Sports';
            function getLookup() {
                return Q.getLookup('WebApp.Sports');
            }
            SportsRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = SportsRow.Fields || (SportsRow.Fields = {}));
            ['SportId', 'Sport', 'SessionsPerWeek', 'sandccoachId', 'scoachId', 'Trainer', 'TrainerScoach', 'Trainersport', 'TrainersportCoach'].forEach(function (x) { return Fields[x] = x; });
        })(SportsRow = Setup.SportsRow || (Setup.SportsRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var SportsService;
        (function (SportsService) {
            SportsService.baseUrl = 'Setup/Sports';
            var Methods;
            (function (Methods) {
            })(Methods = SportsService.Methods || (SportsService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                SportsService[x] = function (r, s, o) { return Q.serviceRequest(SportsService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = SportsService.baseUrl + '/' + x;
            });
        })(SportsService = Setup.SportsService || (Setup.SportsService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersForm = /** @class */ (function (_super) {
            __extends(TrainersForm, _super);
            function TrainersForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TrainersForm.formKey = 'Setup.Trainers';
            return TrainersForm;
        }(Serenity.PrefixedContext));
        Setup.TrainersForm = TrainersForm;
        [['Trainer', function () { return Serenity.StringEditor; }], ['username', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(TrainersForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListForm = /** @class */ (function (_super) {
            __extends(TrainersListForm, _super);
            function TrainersListForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TrainersListForm.formKey = 'Setup.TrainersList';
            return TrainersListForm;
        }(Serenity.PrefixedContext));
        Setup.TrainersListForm = TrainersListForm;
        [['AthId', function () { return Serenity.IntegerEditor; }], ['TrainerId', function () { return Serenity.IntegerEditor; }]].forEach(function (x) { return Object.defineProperty(TrainersListForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListRow;
        (function (TrainersListRow) {
            TrainersListRow.idProperty = 'Id';
            TrainersListRow.localTextPrefix = 'Setup.TrainersList';
            TrainersListRow.lookupKey = 'WebApp.TrainersList';
            function getLookup() {
                return Q.getLookup('WebApp.TrainersList');
            }
            TrainersListRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = TrainersListRow.Fields || (TrainersListRow.Fields = {}));
            ['Id', 'AthId', 'TrainerId', 'AthLive', 'AthUsername', 'AthStuId', 'AthFirstName', 'AthLastName', 'AthSportId', 'AthTrainerId', 'AthGenderId', 'AthYearId', 'AthUserImage'].forEach(function (x) { return Fields[x] = x; });
        })(TrainersListRow = Setup.TrainersListRow || (Setup.TrainersListRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersListService;
        (function (TrainersListService) {
            TrainersListService.baseUrl = 'Setup/TrainersList';
            var Methods;
            (function (Methods) {
            })(Methods = TrainersListService.Methods || (TrainersListService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                TrainersListService[x] = function (r, s, o) { return Q.serviceRequest(TrainersListService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = TrainersListService.baseUrl + '/' + x;
            });
        })(TrainersListService = Setup.TrainersListService || (Setup.TrainersListService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersRow;
        (function (TrainersRow) {
            TrainersRow.idProperty = 'TrainerId';
            TrainersRow.nameProperty = 'Trainer';
            TrainersRow.localTextPrefix = 'Setup.Trainers';
            TrainersRow.lookupKey = 'WebApp.Trainers';
            function getLookup() {
                return Q.getLookup('WebApp.Trainers');
            }
            TrainersRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = TrainersRow.Fields || (TrainersRow.Fields = {}));
            ['TrainerId', 'Trainer', 'username'].forEach(function (x) { return Fields[x] = x; });
        })(TrainersRow = Setup.TrainersRow || (Setup.TrainersRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var TrainersService;
        (function (TrainersService) {
            TrainersService.baseUrl = 'Setup/Trainers';
            var Methods;
            (function (Methods) {
            })(Methods = TrainersService.Methods || (TrainersService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                TrainersService[x] = function (r, s, o) { return Q.serviceRequest(TrainersService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = TrainersService.baseUrl + '/' + x;
            });
        })(TrainersService = Setup.TrainersService || (Setup.TrainersService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupForm = /** @class */ (function (_super) {
            __extends(YesNoAnswerLkupForm, _super);
            function YesNoAnswerLkupForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            YesNoAnswerLkupForm.formKey = 'Setup.YesNoAnswerLkup';
            return YesNoAnswerLkupForm;
        }(Serenity.PrefixedContext));
        Setup.YesNoAnswerLkupForm = YesNoAnswerLkupForm;
        [['Answer', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(YesNoAnswerLkupForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupRow;
        (function (YesNoAnswerLkupRow) {
            YesNoAnswerLkupRow.idProperty = 'Id';
            YesNoAnswerLkupRow.nameProperty = 'Answer';
            YesNoAnswerLkupRow.localTextPrefix = 'Setup.YesNoAnswerLkup';
            YesNoAnswerLkupRow.lookupKey = 'WebApp.YesNoAnswer';
            function getLookup() {
                return Q.getLookup('WebApp.YesNoAnswer');
            }
            YesNoAnswerLkupRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = YesNoAnswerLkupRow.Fields || (YesNoAnswerLkupRow.Fields = {}));
            ['Id', 'Answer'].forEach(function (x) { return Fields[x] = x; });
        })(YesNoAnswerLkupRow = Setup.YesNoAnswerLkupRow || (Setup.YesNoAnswerLkupRow = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Setup;
    (function (Setup) {
        var YesNoAnswerLkupService;
        (function (YesNoAnswerLkupService) {
            YesNoAnswerLkupService.baseUrl = 'Setup/YesNoAnswerLkup';
            var Methods;
            (function (Methods) {
            })(Methods = YesNoAnswerLkupService.Methods || (YesNoAnswerLkupService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                YesNoAnswerLkupService[x] = function (r, s, o) { return Q.serviceRequest(YesNoAnswerLkupService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = YesNoAnswerLkupService.baseUrl + '/' + x;
            });
        })(YesNoAnswerLkupService = Setup.YesNoAnswerLkupService || (Setup.YesNoAnswerLkupService = {}));
    })(Setup = Serene2.Setup || (Serene2.Setup = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListForm = /** @class */ (function (_super) {
            __extends(SportsCoachListForm, _super);
            function SportsCoachListForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SportsCoachListForm.formKey = 'SportsTrainerList.SportsCoachList';
            return SportsCoachListForm;
        }(Serenity.PrefixedContext));
        SportsTrainerList.SportsCoachListForm = SportsCoachListForm;
        [['SportId', function () { return Serenity.IntegerEditor; }], ['TrainerId', function () { return Serenity.IntegerEditor; }]].forEach(function (x) { return Object.defineProperty(SportsCoachListForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListRow;
        (function (SportsCoachListRow) {
            SportsCoachListRow.idProperty = 'Id';
            SportsCoachListRow.localTextPrefix = 'Setup.SportsCoachList';
            SportsCoachListRow.lookupKey = 'WebApp.SportsTrainerList';
            function getLookup() {
                return Q.getLookup('WebApp.SportsTrainerList');
            }
            SportsCoachListRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = SportsCoachListRow.Fields || (SportsCoachListRow.Fields = {}));
            ['Id', 'SportId', 'TrainerId', 'Sport', 'SportSessionsPerWeek', 'SportSandccoachId', 'SportScoachId'].forEach(function (x) { return Fields[x] = x; });
        })(SportsCoachListRow = SportsTrainerList.SportsCoachListRow || (SportsTrainerList.SportsCoachListRow = {}));
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var SportsTrainerList;
    (function (SportsTrainerList) {
        var SportsCoachListService;
        (function (SportsCoachListService) {
            SportsCoachListService.baseUrl = 'SportsTrainerList/SportsCoachList';
            var Methods;
            (function (Methods) {
            })(Methods = SportsCoachListService.Methods || (SportsCoachListService.Methods = {}));
            ['Create', 'Update', 'Delete', 'Retrieve', 'List'].forEach(function (x) {
                SportsCoachListService[x] = function (r, s, o) { return Q.serviceRequest(SportsCoachListService.baseUrl + '/' + x, r, s, o); };
                Methods[x] = SportsCoachListService.baseUrl + '/' + x;
            });
        })(SportsCoachListService = SportsTrainerList.SportsCoachListService || (SportsTrainerList.SportsCoachListService = {}));
    })(SportsTrainerList = Serene2.SportsTrainerList || (Serene2.SportsTrainerList = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ChangePasswordPanel = /** @class */ (function (_super) {
            __extends(ChangePasswordPanel, _super);
            function ChangePasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ChangePasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.w('ConfirmPassword', Serenity.PasswordEditor).value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ChangePassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ChangePassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ChangePasswordPanel.prototype.getFormKey = function () { return Membership.ChangePasswordForm.formKey; };
            ChangePasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ChangePasswordPanel);
            return ChangePasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ChangePasswordPanel = ChangePasswordPanel;
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordPanel = /** @class */ (function (_super) {
            __extends(ForgotPasswordPanel, _super);
            function ForgotPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ForgotPasswordForm(_this.idPrefix);
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ForgotPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ForgotPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ForgotPasswordPanel.prototype.getFormKey = function () { return Membership.ForgotPasswordForm.formKey; };
            ForgotPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ForgotPasswordPanel);
            return ForgotPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ForgotPasswordPanel = ForgotPasswordPanel;
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var ResetPasswordPanel = /** @class */ (function (_super) {
            __extends(ResetPasswordPanel, _super);
            function ResetPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ResetPasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    request.Token = _this.byId('Token').val();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ResetPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ResetPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/Account/Login');
                            });
                        }
                    });
                });
                return _this;
            }
            ResetPasswordPanel.prototype.getFormKey = function () { return Membership.ResetPasswordForm.formKey; };
            ResetPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ResetPasswordPanel);
            return ResetPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ResetPasswordPanel = ResetPasswordPanel;
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
var Serene2;
(function (Serene2) {
    var Membership;
    (function (Membership) {
        var SignUpPanel = /** @class */ (function (_super) {
            __extends(SignUpPanel, _super);
            function SignUpPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.SignUpForm(_this.idPrefix);
                _this.form.ConfirmEmail.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmEmail.value !== _this.form.Email.value) {
                        return Q.text('Validation.EmailConfirm');
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.Password.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/SignUp'),
                        request: {
                            DisplayName: _this.form.DisplayName.value,
                            Email: _this.form.Email.value,
                            Password: _this.form.Password.value
                        },
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.SignUp.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            SignUpPanel.prototype.getFormKey = function () { return Membership.SignUpForm.formKey; };
            SignUpPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], SignUpPanel);
            return SignUpPanel;
        }(Serenity.PropertyPanel));
        Membership.SignUpPanel = SignUpPanel;
    })(Membership = Serene2.Membership || (Serene2.Membership = {}));
})(Serene2 || (Serene2 = {}));
//# sourceMappingURL=Serene2.Web.js.map